package net.lasagu.takyon360.jobs;

import android.content.Context;
import android.util.Log;

import com.path.android.jobqueue.Job;
import com.path.android.jobqueue.Params;

import net.lasagu.takyon360.MyApplication;
import net.lasagu.takyon360.events.UpdatePasswordEvent;
import net.lasagu.takyon360.models.GenericResponse;
import net.lasagu.takyon360.models.UpdatePasswordSubmission;
import net.lasagu.takyon360.utils.BasicAuthInterceptor;
import net.lasagu.takyon360.utils.PreferencesData;
import net.lasagu.takyon360.utils.ReusableClass;
import net.lasagu.takyon360.utils.URLConst;

import java.net.HttpURLConnection;
import java.util.concurrent.atomic.AtomicInteger;

import de.greenrobot.event.EventBus;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by anirban on 1/31/17.
 */

public class UpdatePasswordJob extends Job {
    private static final AtomicInteger jobCounter = new AtomicInteger(0);
    private final int id;
    private UpdatePasswordSubmission updatePasswordSubmission;

    public UpdatePasswordJob(UpdatePasswordSubmission updatePasswordSubmission) {
        super(new Params(1000).requireNetwork());
        id = jobCounter.incrementAndGet();
        this.updatePasswordSubmission = updatePasswordSubmission;
    }


    @Override
    public void onAdded() {
    }

    @Override
    public void onRun() throws Throwable {

        if (id != jobCounter.get()) {
            // looks like other fetch jobs has been added after me. no reason to
            // keep fetching many times, cancel me, let the other one fetch
            return;
        }
        Context context = MyApplication.getInstance();

        String url = URLConst.getUpdatePasswordUrl(context);
        Log.d("TAG", "url = " + url);


        Log.d("TAG", "sending = " + updatePasswordSubmission.toString());
        Log.d("TAG", "sending Auth Username = " + PreferencesData.getUser(context).getUsername() + " Password: " + ReusableClass.getMd5(PreferencesData.getUser(context).getPassword()));

        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(new BasicAuthInterceptor(PreferencesData.getUser(context).getUsername(), ReusableClass.getMd5(PreferencesData.getUser(context).getPassword())))
                .build();

        Request request = new Request.Builder()
                .url(url)
                .post(RequestBody.create(MediaType.parse("application/json; charset=utf-8"), updatePasswordSubmission.toString()))
                .build();

        Response response = client.newCall(request).execute();
        String json = response.body().string();

        Log.d("TAG", "response = " + json);

        if (response.code() != HttpURLConnection.HTTP_ACCEPTED) {
            EventBus.getDefault().post(new UpdatePasswordEvent.Fail("Some thing went wrong, did not get proper response from server."));
            return;
        } else {
            GenericResponse genericResponse = GenericResponse.fromJson(json);
            EventBus.getDefault().post(new UpdatePasswordEvent.Success(genericResponse));
        }
    }

    @Override
    protected void onCancel() {

    }

    @Override
    protected boolean shouldReRunOnThrowable(Throwable throwable) {
        EventBus.getDefault().post(new UpdatePasswordEvent.Fail(throwable.getMessage()));
        return false;
    }
}
