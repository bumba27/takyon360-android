package net.lasagu.takyon360.jobs;

import android.content.Context;
import android.util.Log;

import com.path.android.jobqueue.Job;
import com.path.android.jobqueue.Params;

import net.lasagu.takyon360.MyApplication;
import net.lasagu.takyon360.events.ForgotPasswordEvent;
import net.lasagu.takyon360.models.GenericResponse;
import net.lasagu.takyon360.models.ResetPasswordSubmission;
import net.lasagu.takyon360.utils.BasicAuthInterceptor;
import net.lasagu.takyon360.utils.Constant;
import net.lasagu.takyon360.utils.ReusableClass;
import net.lasagu.takyon360.utils.URLConst;

import java.net.HttpURLConnection;
import java.util.concurrent.atomic.AtomicInteger;

import de.greenrobot.event.EventBus;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by anirban on 1/31/17.
 */

public class ForgotPasswordJob extends Job {
    private static final AtomicInteger jobCounter = new AtomicInteger(0);
    private final int id;
    private ResetPasswordSubmission resetPasswordSubmission;

    public ForgotPasswordJob(ResetPasswordSubmission resetPasswordSubmission) {
        super(new Params(1000).requireNetwork());
        id = jobCounter.incrementAndGet();
        this.resetPasswordSubmission = resetPasswordSubmission;
    }


    @Override
    public void onAdded() {
    }

    @Override
    public void onRun() throws Throwable {

        if (id != jobCounter.get()) {
            // looks like other fetch jobs has been added after me. no reason to
            // keep fetching many times, cancel me, let the other one fetch
            return;
        }
        Context context = MyApplication.getInstance();

        String url = URLConst.getResetPasswordUrl(context);
        Log.d("TAG", "url = " + url);


        Log.d("TAG", "sending = " + resetPasswordSubmission.toString());

        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(new BasicAuthInterceptor(Constant.USERNAME, ReusableClass.getMd5(Constant.PASSWORD)))
                .build();

        Request request = new Request.Builder()
                .url(url)
                .post(RequestBody.create(MediaType.parse("application/json; charset=utf-8"), resetPasswordSubmission.toString()))
                .build();

        Response response = client.newCall(request).execute();
        String json = response.body().string();

        Log.d("TAG", "response = " + json);

        if (response.code() != HttpURLConnection.HTTP_ACCEPTED) {
            EventBus.getDefault().post(new ForgotPasswordEvent.Fail("Some thing went wrong, did not get proper response from server."));
            return;
        } else {
            GenericResponse genericResponse = GenericResponse.fromJson(json);
            EventBus.getDefault().post(new ForgotPasswordEvent.Success(genericResponse));
        }
    }

    @Override
    protected void onCancel() {

    }

    @Override
    protected boolean shouldReRunOnThrowable(Throwable throwable) {
        EventBus.getDefault().post(new ForgotPasswordEvent.Fail(throwable.getMessage()));
        return false;
    }
}
