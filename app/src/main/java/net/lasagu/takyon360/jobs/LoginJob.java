package net.lasagu.takyon360.jobs;

import android.content.Context;
import android.util.Log;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.path.android.jobqueue.Job;
import com.path.android.jobqueue.Params;

import net.lasagu.takyon360.MyApplication;
import net.lasagu.takyon360.events.LoginEvent;
import net.lasagu.takyon360.models.LoginAuth;
import net.lasagu.takyon360.models.LoginResponse;
import net.lasagu.takyon360.models.LoginSubmission;
import net.lasagu.takyon360.utils.BasicAuthInterceptor;
import net.lasagu.takyon360.utils.ReusableClass;
import net.lasagu.takyon360.utils.URLConst;

import java.net.HttpURLConnection;
import java.util.concurrent.atomic.AtomicInteger;

import de.greenrobot.event.EventBus;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by anirban on 1/31/17.
 */

public class LoginJob extends Job {
    private static final AtomicInteger jobCounter = new AtomicInteger(0);
    private final int id;
    private LoginSubmission userData;

    public LoginJob(LoginSubmission userData) {
        super(new Params(1000).requireNetwork());
        id = jobCounter.incrementAndGet();
        this.userData = userData;
    }


    @Override
    public void onAdded() {
    }

    @Override
    public void onRun() throws Throwable {

        if (id != jobCounter.get()) {
            // looks like other fetch jobs has been added after me. no reason to
            // keep fetching many times, cancel me, let the other one fetch
            return;
        }
        Context context = MyApplication.getInstance();

        String url = URLConst.getLoginUrl(context);
        Log.d("TAG", "url = " + url);
        LoginAuth loginAuth = new LoginAuth();
        loginAuth.setUserName(userData.getUsername());
        loginAuth.setPassword(ReusableClass.getMd5(userData.getPassword()));
        if (userData.getLanguage().equalsIgnoreCase("English"))
            loginAuth.setLanguage("1");
        else if (userData.getLanguage().equalsIgnoreCase("हिंदी"))
            loginAuth.setLanguage("3");
        else if (userData.getLanguage().equalsIgnoreCase("नेपाली"))
            loginAuth.setLanguage("4");
        else
            loginAuth.setLanguage("2");

        Log.d("TAG", "sending = " + loginAuth.toString());

        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(new BasicAuthInterceptor(userData.getUsername(), ReusableClass.getMd5(userData.getPassword())))
                .build();

        Request request = new Request.Builder()
                .url(url)
                .post(RequestBody.create(MediaType.parse("application/json; charset=utf-8"), loginAuth.toString()))
                .build();

        Response response = client.newCall(request).execute();
        String json = response.body().string();

        Log.d("TAG", "response = " + json);

        if (response.code() != HttpURLConnection.HTTP_ACCEPTED) {
            if (response.code() == HttpURLConnection.HTTP_UNAUTHORIZED) {
                EventBus.getDefault().post(new LoginEvent.Fail("Please check your credentials.", true));
                return;
            } else {
                EventBus.getDefault().post(new LoginEvent.Fail("There is a server error. Please try again later", false));
                return;
            }
        } else {
            LoginResponse loginResponse = LoginResponse.fromJson(json);
            EventBus.getDefault().post(new LoginEvent.Success(loginResponse));

        }
    }

    @Override
    protected void onCancel() {

    }

    @Override
    protected boolean shouldReRunOnThrowable(Throwable throwable) {
        EventBus.getDefault().post(new LoginEvent.Fail(throwable.getMessage(), false));
        return false;
    }

}
