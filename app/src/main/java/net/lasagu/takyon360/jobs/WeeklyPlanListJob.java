package net.lasagu.takyon360.jobs;

import android.content.Context;
import android.util.Log;

import com.path.android.jobqueue.Job;
import com.path.android.jobqueue.Params;

import net.lasagu.takyon360.MyApplication;
import net.lasagu.takyon360.events.WeeklyPlanDownloadResponseEvent;
import net.lasagu.takyon360.events.WeeklyPlanListResponseEvent;
import net.lasagu.takyon360.models.WeeklyPlanDownloadResponse;
import net.lasagu.takyon360.models.WeeklyPlanListResponse;
import net.lasagu.takyon360.models.WeeklyPlanSubmission;
import net.lasagu.takyon360.utils.BasicAuthInterceptor;
import net.lasagu.takyon360.utils.PreferencesData;
import net.lasagu.takyon360.utils.ReusableClass;
import net.lasagu.takyon360.utils.URLConst;

import java.net.HttpURLConnection;
import java.util.concurrent.atomic.AtomicInteger;

import de.greenrobot.event.EventBus;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by anirban on 1/31/17.
 */

public class WeeklyPlanListJob extends Job {
    private static final AtomicInteger jobCounter = new AtomicInteger(0);
    private final int id;
    private WeeklyPlanSubmission weeklyPlanSubmission;

    public WeeklyPlanListJob(WeeklyPlanSubmission weeklyPlanSubmission) {
        super(new Params(1000).requireNetwork());
        id = jobCounter.incrementAndGet();
        this.weeklyPlanSubmission = weeklyPlanSubmission;
    }


    @Override
    public void onAdded() {
    }

    @Override
    public void onRun() throws Throwable {

        if (id != jobCounter.get()) {
            // looks like other fetch jobs has been added after me. no reason to
            // keep fetching many times, cancel me, let the other one fetch
            return;
        }
        Context context = MyApplication.getInstance();

        String url = URLConst.getWeeklyPlanListUrl(context);
        Log.d("TAG", "url = " + url);

        Log.d("TAG", "sending = " + weeklyPlanSubmission.toString());

        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(new BasicAuthInterceptor(PreferencesData.getUser(context).getUsername(), ReusableClass.getMd5(PreferencesData.getUser(context).getPassword())))
                .build();

        Request request = new Request.Builder()
                .url(url)
                .post(RequestBody.create(MediaType.parse("application/json; charset=utf-8"), weeklyPlanSubmission.toString()))
                .build();

        Response response = client.newCall(request).execute();
        String json = response.body().string();

        Log.d("TAG", "response = " + json);
        Log.d("TAG", "response.code() = " + response.code());

        // TODO: 12/19/17 Need to add a logic with the respose code when there is AttachmentLink (mean user click download button)
        if (response.code() == HttpURLConnection.HTTP_ACCEPTED) {
            WeeklyPlanListResponse weeklyPlanListResponse = WeeklyPlanListResponse.fromJson(json);
            EventBus.getDefault().post(new WeeklyPlanListResponseEvent.Success(weeklyPlanListResponse));
        } else if (response.code() == HttpURLConnection.HTTP_SEE_OTHER || response.code() == HttpURLConnection.HTTP_PARTIAL) {
            WeeklyPlanDownloadResponse weeklyPlanDownloadResponse = WeeklyPlanDownloadResponse.fromJson(json);
            EventBus.getDefault().post(new WeeklyPlanDownloadResponseEvent.Success(weeklyPlanDownloadResponse));
        } else {
            EventBus.getDefault().post(new WeeklyPlanListResponseEvent.Fail("Some thing went wrong, did not get proper response from server."));
        }
    }

    @Override
    protected void onCancel() {

    }

    @Override
    protected boolean shouldReRunOnThrowable(Throwable throwable) {
        EventBus.getDefault().post(new WeeklyPlanListResponseEvent.Fail(throwable.getMessage()));
        return false;
    }
}
