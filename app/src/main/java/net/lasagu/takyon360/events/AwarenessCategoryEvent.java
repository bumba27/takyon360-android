package net.lasagu.takyon360.events;

import android.util.Log;

import net.lasagu.takyon360.models.AwarenessCategoryResponse;


/**
 * Created by anirban on 1/31/17.
 */

public class AwarenessCategoryEvent {
    public static class Fail {
        String ex;

        public Fail(String ex) {
            this.ex = ex;
        }

        public String getEx() {
            Log.d("TAG", "Error");

            return ex;
        }
    }

    public static class Success {
        AwarenessCategoryResponse awarenessCategoryResponse;

        public Success(AwarenessCategoryResponse awarenessCategoryResponse) {
            this.awarenessCategoryResponse = awarenessCategoryResponse;
        }

        public AwarenessCategoryResponse getAwarenessCategoryResponse() {
            return awarenessCategoryResponse;
        }

    }
}
