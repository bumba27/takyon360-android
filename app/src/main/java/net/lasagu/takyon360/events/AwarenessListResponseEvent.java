package net.lasagu.takyon360.events;

import android.util.Log;

import net.lasagu.takyon360.models.AwarenessListResponse;


/**
 * Created by anirban on 1/31/17.
 */

public class AwarenessListResponseEvent {
    public static class Fail {
        String ex;

        public Fail(String ex) {
            this.ex = ex;
        }

        public String getEx() {
            Log.d("TAG", "Error");

            return ex;
        }
    }

    public static class Success {
        AwarenessListResponse awarenessListResponse;

        public Success(AwarenessListResponse awarenessListResponse) {
            this.awarenessListResponse = awarenessListResponse;
        }

        public AwarenessListResponse getAwarenessListResponse() {
            return awarenessListResponse;
        }

    }
}
