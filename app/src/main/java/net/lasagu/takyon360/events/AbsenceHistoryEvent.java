package net.lasagu.takyon360.events;

import android.util.Log;

import net.lasagu.takyon360.models.AbsenceHistoryResponse;


/**
 * Created by anirban on 1/31/17.
 */

public class AbsenceHistoryEvent {
    public static class Fail {
        String ex;

        public Fail(String ex) {
            this.ex = ex;
        }

        public String getEx() {
            Log.d("TAG", "Error");

            return ex;
        }
    }

    public static class Success {
        AbsenceHistoryResponse absenceHistoryResponse;

        public Success(AbsenceHistoryResponse absenceHistoryResponse) {
            this.absenceHistoryResponse = absenceHistoryResponse;
        }

        public AbsenceHistoryResponse getAbsenceHistoryResponse() {
            return absenceHistoryResponse;
        }

    }
}
