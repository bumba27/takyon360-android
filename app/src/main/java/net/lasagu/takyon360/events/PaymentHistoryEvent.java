package net.lasagu.takyon360.events;

import android.util.Log;

import net.lasagu.takyon360.models.PaymentHistoryResponse;


/**
 * Created by anirban on 1/31/17.
 */

public class PaymentHistoryEvent {
    public static class Fail {
        String ex;

        public Fail(String ex) {
            this.ex = ex;
        }

        public String getEx() {
            Log.d("TAG", "Error");

            return ex;
        }
    }

    public static class Success {
        PaymentHistoryResponse paymentHistoryResponse;

        public Success(PaymentHistoryResponse paymentHistoryResponse) {
            this.paymentHistoryResponse = paymentHistoryResponse;
        }

        public PaymentHistoryResponse getPaymentHistoryResponse() {
            return paymentHistoryResponse;
        }

    }
}
