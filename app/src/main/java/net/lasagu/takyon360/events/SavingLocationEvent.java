package net.lasagu.takyon360.events;

import android.util.Log;

import net.lasagu.takyon360.models.GenericResponse;


/**
 * Created by anirban on 1/31/17.
 */

public class SavingLocationEvent {
    public static class Fail {
        String ex;

        public Fail(String ex) {
            this.ex = ex;
        }

        public String getEx() {
            Log.d("TAG", "Error");

            return ex;
        }
    }

    public static class Success {
        GenericResponse genericResponse;

        public Success(GenericResponse genericResponse) {
            this.genericResponse = genericResponse;
        }

        public GenericResponse getGenericResponse() {
            return genericResponse;
        }

    }
}
