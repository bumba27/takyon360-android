package net.lasagu.takyon360.events;

/**
 * Created by anirban on 1/31/17.
 */

public class LoadNotificationEvent {

    public static class Success {
        boolean isRead;
        private int siblingId;

        public Success(boolean isRead, int siblingId) {
            this.isRead = isRead;
            this.siblingId = siblingId;
        }

        public boolean getReadStatus() {
            return isRead;
        }

        public int getSiblingId() {
            return siblingId;
        }
    }
}
