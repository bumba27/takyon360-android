package net.lasagu.takyon360.events;

import net.lasagu.takyon360.models.ClassWorkBean;

import java.util.ArrayList;

public class ClassworkEvent {
    private ArrayList<ClassWorkBean> classWorkBeans;

    public ClassworkEvent(ArrayList<ClassWorkBean> classWorkBeans) {
        this.classWorkBeans = classWorkBeans;
    }

    public ArrayList<ClassWorkBean> getClassWork() {
        return classWorkBeans;
    }
}
