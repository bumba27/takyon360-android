package net.lasagu.takyon360.events;

import android.util.Log;

import net.lasagu.takyon360.models.WeeklyPlanDownloadResponse;


/**
 * Created by anirban on 1/31/17.
 */

public class WeeklyPlanDownloadResponseEvent {
    public static class Fail {
        String ex;

        public Fail(String ex) {
            this.ex = ex;
        }

        public String getEx() {
            Log.d("TAG", "Error");

            return ex;
        }
    }

    public static class Success {
        WeeklyPlanDownloadResponse weeklyPlanDownloadResponse;

        public Success(WeeklyPlanDownloadResponse weeklyPlanDownloadResponse) {
            this.weeklyPlanDownloadResponse = weeklyPlanDownloadResponse;
        }

        public WeeklyPlanDownloadResponse getWeeklyPlanDownloadResponse() {
            return weeklyPlanDownloadResponse;
        }

    }
}
