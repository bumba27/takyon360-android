package net.lasagu.takyon360.events;

import android.util.Log;

import net.lasagu.takyon360.models.GalleryImageListResponse;


/**
 * Created by anirban on 1/31/17.
 */

public class GalleryListResponseEvent {
    public static class Fail {
        String ex;

        public Fail(String ex) {
            this.ex = ex;
        }

        public String getEx() {
            Log.d("TAG", "Error");

            return ex;
        }
    }

    public static class Success {
        GalleryImageListResponse galleryImageListResponse;

        public Success(GalleryImageListResponse galleryImageListResponse) {
            this.galleryImageListResponse = galleryImageListResponse;
        }

        public GalleryImageListResponse getGalleryImageListResponse() {
            return galleryImageListResponse;
        }

    }
}
