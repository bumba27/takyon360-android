package net.lasagu.takyon360.events;

import android.util.Log;

import net.lasagu.takyon360.models.FtpDetailsResponse;


/**
 * Created by anirban on 1/31/17.
 */

public class FtpDetailsEvent {
    public static class Fail {
        String ex;

        public Fail(String ex) {
            this.ex = ex;
        }

        public String getEx() {
            Log.d("TAG", "Error");

            return ex;
        }
    }

    public static class Success {
        FtpDetailsResponse ftpDetailsResponse;

        public Success(FtpDetailsResponse ftpDetailsResponse) {
            this.ftpDetailsResponse = ftpDetailsResponse;
        }

        public FtpDetailsResponse getFtpDetailsResponse() {
            return ftpDetailsResponse;
        }

    }
}
