package net.lasagu.takyon360.events;

import android.util.Log;

import net.lasagu.takyon360.models.GenericResponse;


/**
 * Created by anirban on 1/31/17.
 */

public class EmailOtpSendEvent {
    public static class Fail {
        String ex;

        public Fail(String ex) {
            this.ex = ex;
        }

        public String getEx() {
            Log.d("TAG", "Error");

            return ex;
        }
    }

    public static class Success {
        GenericResponse genericResponse;
        private String userEmailId;

        public Success(GenericResponse genericResponse, String userEmailId) {
            this.genericResponse = genericResponse;
            this.userEmailId = userEmailId;
        }

        public GenericResponse genericResponse() {
            return genericResponse;
        }

        public String getUserEmailId() {
            return userEmailId;
        }
    }
}
