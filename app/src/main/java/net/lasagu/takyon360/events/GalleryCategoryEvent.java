package net.lasagu.takyon360.events;

import android.util.Log;

import net.lasagu.takyon360.models.GalleryCategoryResponse;


/**
 * Created by anirban on 1/31/17.
 */

public class GalleryCategoryEvent {
    public static class Fail {
        String ex;

        public Fail(String ex) {
            this.ex = ex;
        }

        public String getEx() {
            Log.d("TAG", "Error");

            return ex;
        }
    }

    public static class Success {
        GalleryCategoryResponse galleryCategoryResponse;

        public Success(GalleryCategoryResponse galleryCategoryResponse) {
            this.galleryCategoryResponse = galleryCategoryResponse;
        }

        public GalleryCategoryResponse getGalleryCategoryResponse() {
            return galleryCategoryResponse;
        }

    }
}
