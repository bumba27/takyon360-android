package net.lasagu.takyon360.events;

import android.util.Log;

import net.lasagu.takyon360.models.MemberListResponse;


/**
 * Created by anirban on 1/31/17.
 */

public class GetMemeberListEvent {
    public static class Fail {
        String ex;

        public Fail(String ex) {
            this.ex = ex;
        }

        public String getEx() {
            Log.d("TAG", "Error");

            return ex;
        }
    }

    public static class Success {
        MemberListResponse memberListResponse;

        public Success(MemberListResponse memberListResponse) {
            this.memberListResponse = memberListResponse;
        }

        public MemberListResponse getMemberListResponse() {
            return memberListResponse;
        }

    }
}
