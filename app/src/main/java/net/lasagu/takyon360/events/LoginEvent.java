package net.lasagu.takyon360.events;

import android.util.Log;

import net.lasagu.takyon360.models.LoginResponse;


/**
 * Created by anirban on 1/31/17.
 */

public class LoginEvent {
    public static class Fail {
        String ex;
        private boolean isInvalidVredentials;

        public Fail(String ex, boolean isInvalidVredentials) {
            this.ex = ex;
            this.isInvalidVredentials = isInvalidVredentials;
        }

        public String getEx() {
            Log.d("TAG", "Error");

            return ex;
        }

        public boolean isInvalidVredentials() {
            return isInvalidVredentials;
        }
    }

    public static class Success {
        LoginResponse loginResponse;

        public Success(LoginResponse loginResponse) {
            this.loginResponse = loginResponse;
        }

        public LoginResponse getLoginResponse() {
            return loginResponse;
        }

    }
}
