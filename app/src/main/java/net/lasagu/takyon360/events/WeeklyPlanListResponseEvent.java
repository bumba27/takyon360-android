package net.lasagu.takyon360.events;

import android.util.Log;

import net.lasagu.takyon360.models.WeeklyPlanListResponse;


/**
 * Created by anirban on 1/31/17.
 */

public class WeeklyPlanListResponseEvent {
    public static class Fail {
        String ex;

        public Fail(String ex) {
            this.ex = ex;
        }

        public String getEx() {
            Log.d("TAG", "Error");

            return ex;
        }
    }

    public static class Success {
        WeeklyPlanListResponse weeklyPlanListResponse;

        public Success(WeeklyPlanListResponse weeklyPlanListResponse) {
            this.weeklyPlanListResponse = weeklyPlanListResponse;
        }

        public WeeklyPlanListResponse getWeeklyPlanListResponse() {
            return weeklyPlanListResponse;
        }

    }
}
