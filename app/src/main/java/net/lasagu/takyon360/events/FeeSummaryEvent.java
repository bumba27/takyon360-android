package net.lasagu.takyon360.events;

import android.util.Log;

import net.lasagu.takyon360.models.FeeSummaryResponse;


/**
 * Created by anirban on 1/31/17.
 */

public class FeeSummaryEvent {
    public static class Fail {
        String ex;

        public Fail(String ex) {
            this.ex = ex;
        }

        public String getEx() {
            Log.d("TAG", "Error");

            return ex;
        }
    }

    public static class Success {
        FeeSummaryResponse feeSummaryResponse;

        public Success(FeeSummaryResponse feeSummaryResponse) {
            this.feeSummaryResponse = feeSummaryResponse;
        }

        public FeeSummaryResponse getFeeSummaryResponse() {
            return feeSummaryResponse;
        }

    }
}
