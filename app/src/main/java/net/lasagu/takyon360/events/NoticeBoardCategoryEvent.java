package net.lasagu.takyon360.events;

import android.util.Log;

import net.lasagu.takyon360.models.NoticeBoardResponse;


/**
 * Created by anirban on 1/31/17.
 */

public class NoticeBoardCategoryEvent {
    public static class Fail {
        String ex;

        public Fail(String ex) {
            this.ex = ex;
        }

        public String getEx() {
            Log.d("TAG", "Error");

            return ex;
        }
    }

    public static class Success {
        NoticeBoardResponse noticeBoardResponse;

        public Success(NoticeBoardResponse noticeBoardResponse) {
            this.noticeBoardResponse = noticeBoardResponse;
        }

        public NoticeBoardResponse getNoticeBoardCategoryResponse() {
            return noticeBoardResponse;
        }

    }
}
