package net.lasagu.takyon360.events;

import android.util.Log;

import net.lasagu.takyon360.models.ClassesListResponse;


/**
 * Created by anirban on 1/31/17.
 */

public class ClassesListResponseEvent {
    public static class Fail {
        String ex;

        public Fail(String ex) {
            this.ex = ex;
        }

        public String getEx() {
            Log.d("TAG", "Error");

            return ex;
        }
    }

    public static class Success {
        ClassesListResponse classesListResponse;

        public Success(ClassesListResponse classesListResponse) {
            this.classesListResponse = classesListResponse;
        }

        public ClassesListResponse getClassesListResponse() {
            return classesListResponse;
        }

    }
}
