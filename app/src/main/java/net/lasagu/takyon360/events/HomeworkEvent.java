package net.lasagu.takyon360.events;

import net.lasagu.takyon360.models.HomeWorkBean;

import java.util.ArrayList;

public class HomeworkEvent {
    private ArrayList<HomeWorkBean> homeWork;

    public HomeworkEvent(ArrayList<HomeWorkBean> homeWork) {
        this.homeWork = homeWork;
    }

    public ArrayList<HomeWorkBean> getHomeWork() {
        return homeWork;
    }
}
