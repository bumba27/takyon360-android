package net.lasagu.takyon360.events;

import android.util.Log;

import net.lasagu.takyon360.models.DigitalResourceCategoryResponse;


/**
 * Created by anirban on 1/31/17.
 */

public class DigitalResourceCategoryEvent {
    public static class Fail {
        String ex;

        public Fail(String ex) {
            this.ex = ex;
        }

        public String getEx() {
            Log.d("TAG", "Error");

            return ex;
        }
    }

    public static class Success {
        DigitalResourceCategoryResponse digitalResourceCategoryResponse;

        public Success(DigitalResourceCategoryResponse digitalResourceCategoryResponse) {
            this.digitalResourceCategoryResponse = digitalResourceCategoryResponse;
        }

        public DigitalResourceCategoryResponse getDigitalResourceCategoryResponse() {
            return digitalResourceCategoryResponse;
        }

    }
}
