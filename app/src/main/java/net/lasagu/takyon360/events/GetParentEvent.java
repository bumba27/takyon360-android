package net.lasagu.takyon360.events;

import android.util.Log;

import net.lasagu.takyon360.models.StudentDetailsResponse;


/**
 * Created by anirban on 1/31/17.
 */

public class GetParentEvent {
    public static class Fail {
        String ex;

        public Fail(String ex) {
            this.ex = ex;
        }

        public String getEx() {
            Log.d("TAG", "Error");

            return ex;
        }
    }

    public static class Success {
        StudentDetailsResponse studentDetailsResponse;

        public Success(StudentDetailsResponse studentDetailsResponse) {
            this.studentDetailsResponse = studentDetailsResponse;
        }

        public StudentDetailsResponse getStudentDetailsResponse() {
            return studentDetailsResponse;
        }

    }
}
