package net.lasagu.takyon360.events;

import android.util.Log;

import net.lasagu.takyon360.models.DigitalResourceListResponse;


/**
 * Created by anirban on 1/31/17.
 */

public class DigitalResourceListResponseEvent {
    public static class Fail {
        String ex;

        public Fail(String ex) {
            this.ex = ex;
        }

        public String getEx() {
            Log.d("TAG", "Error");

            return ex;
        }
    }

    public static class Success {
        DigitalResourceListResponse digitalResourceListResponse;

        public Success(DigitalResourceListResponse digitalResourceListResponse) {
            this.digitalResourceListResponse = digitalResourceListResponse;
        }

        public DigitalResourceListResponse getDigitalResourceListResponse() {
            return digitalResourceListResponse;
        }

    }
}
