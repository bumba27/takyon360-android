package net.lasagu.takyon360.events;

import android.util.Log;

import net.lasagu.takyon360.models.FeeDetailsResponse;


/**
 * Created by anirban on 1/31/17.
 */

public class FeeDetailsEvent {
    public static class Fail {
        String ex;

        public Fail(String ex) {
            this.ex = ex;
        }

        public String getEx() {
            Log.d("TAG", "Error");

            return ex;
        }
    }

    public static class Success {
        FeeDetailsResponse feeSummaryResponse;

        public Success(FeeDetailsResponse feeDetailsResponse) {
            this.feeSummaryResponse = feeDetailsResponse;
        }

        public FeeDetailsResponse getFeeSummaryResponse() {
            return feeSummaryResponse;
        }

    }
}
