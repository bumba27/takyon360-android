package net.lasagu.takyon360.events;

import android.util.Log;

import net.lasagu.takyon360.models.GenericResponse;


/**
 * Created by anirban on 1/31/17.
 */

public class NotificationReadEvent {
    public static class Fail {
        String ex;

        public Fail(String ex) {
            this.ex = ex;
        }

        public String getEx() {
            Log.d("TAG", "Error");

            return ex;
        }
    }

    public static class Success {
        GenericResponse genericResponse;
        private int position;

        public Success(GenericResponse genericResponse, int position) {
            this.genericResponse = genericResponse;
            this.position = position;
        }

        public GenericResponse genericResponse() {
            return genericResponse;
        }

        public int getPosition() {
            return position;
        }
    }
}
