package net.lasagu.takyon360.events;

import android.util.Log;

import net.lasagu.takyon360.models.MessageDetailsResponse;


/**
 * Created by anirban on 1/31/17.
 */

public class GetMessageDetailsEvent {
    public static class Fail {
        String ex;

        public Fail(String ex) {
            this.ex = ex;
        }

        public String getEx() {
            Log.d("TAG", "Error");

            return ex;
        }
    }

    public static class Success {
        MessageDetailsResponse messageDetailsResponse;

        public Success(MessageDetailsResponse messageDetailsResponse) {
            this.messageDetailsResponse = messageDetailsResponse;
        }

        public MessageDetailsResponse getMessageDetailsResponse() {
            return messageDetailsResponse;
        }

    }
}
