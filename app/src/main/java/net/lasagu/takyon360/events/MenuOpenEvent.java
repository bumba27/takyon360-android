package net.lasagu.takyon360.events;

import android.util.Log;


/**
 * Created by anirban on 1/31/17.
 */

public class MenuOpenEvent {
    public static class Fail {
        String ex;

        public Fail(String ex) {
            this.ex = ex;
        }

        public String getEx() {
            Log.d("TAG", "Error");

            return ex;
        }
    }

    public static class Success {
        int menuId;

        public Success(int menuId) {
            this.menuId = menuId;
        }

        public int getMenuId() {
            return menuId;
        }

    }
}
