package net.lasagu.takyon360.events;

import net.lasagu.takyon360.models.AssessmentsBean;

import java.util.ArrayList;

public class AssessmentEvent {
    private ArrayList<AssessmentsBean> assessmentsBeans;

    public AssessmentEvent(ArrayList<AssessmentsBean> assessmentsBeans) {
        this.assessmentsBeans = assessmentsBeans;
    }

    public ArrayList<AssessmentsBean> getAssessments() {
        return assessmentsBeans;
    }
}
