package net.lasagu.takyon360.events;

import android.util.Log;

import net.lasagu.takyon360.models.SubjectListResponse;


/**
 * Created by anirban on 1/31/17.
 */

public class SubjectListResponseEvent {
    public static class Fail {
        String ex;

        public Fail(String ex) {
            this.ex = ex;
        }

        public String getEx() {
            Log.d("TAG", "Error");

            return ex;
        }
    }

    public static class Success {
        SubjectListResponse subjectListResponse;

        public Success(SubjectListResponse subjectListResponse) {
            this.subjectListResponse = subjectListResponse;
        }

        public SubjectListResponse getSubjectListResponse() {
            return subjectListResponse;
        }

    }
}
