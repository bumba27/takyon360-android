package net.lasagu.takyon360.events;

import net.lasagu.takyon360.models.AssessmentsBean;
import net.lasagu.takyon360.models.Quizes;

import java.util.ArrayList;

public class QuizeEvent {
    private ArrayList<Quizes> quizes;

    public QuizeEvent(ArrayList<Quizes> quizes) {
        this.quizes = quizes;
    }

    public ArrayList<Quizes> getQuizes() {
        return quizes;
    }
}
