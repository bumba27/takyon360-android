package net.lasagu.takyon360.models;

/**
 * Created by anirban on 8/27/17.
 */

public class ArticleCategoriesBean {
    /**
     * CategoryId : 1
     * Category : Article 1
     * ArticleCount : 3
     * ParentId : 0
     */

    private int CategoryId;
    private String Category;
    private int ArticleCount;
    private int ParentId;

    public int getCategoryId() {
        return CategoryId;
    }

    public void setCategoryId(int CategoryId) {
        this.CategoryId = CategoryId;
    }

    public String getCategory() {
        return Category;
    }

    public void setCategory(String Category) {
        this.Category = Category;
    }

    public int getArticleCount() {
        return ArticleCount;
    }

    public void setArticleCount(int ArticleCount) {
        this.ArticleCount = ArticleCount;
    }

    public int getParentId() {
        return ParentId;
    }

    public void setParentId(int ParentId) {
        this.ParentId = ParentId;
    }
}
