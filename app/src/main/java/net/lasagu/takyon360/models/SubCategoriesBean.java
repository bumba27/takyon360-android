package net.lasagu.takyon360.models;

/**
 * Created by anirban on 8/19/17.
 */

public class SubCategoriesBean {
    /**
     * CategoryId : 7
     * Category : Flag Day 1
     */

    private int CategoryId;
    private String Category;

    public int getCategoryId() {
        return CategoryId;
    }

    public void setCategoryId(int CategoryId) {
        this.CategoryId = CategoryId;
    }

    public String getCategory() {
        return Category;
    }

    public void setCategory(String Category) {
        this.Category = Category;
    }
}
