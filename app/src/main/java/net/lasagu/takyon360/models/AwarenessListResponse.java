package net.lasagu.takyon360.models;

import com.google.gson.Gson;

import java.util.ArrayList;

/**
 * Created by anirban on 8/27/17.
 */

public class AwarenessListResponse {
    /**
     * StatusCode : 1
     * StatusMessage : Success
     * ArticleCategory : Article 1
     * ArticleLabel : Articles
     * ArticlesItems : [{"ArticleId":1,"ArticleName":"Article 1 1","ArticleDescription":"Dear parents, Hijra New Year has been confirmed to be on Thursday 15th October 2015.","ArticleImage":"http://lasagu.net/school/uploadtest/default/company_logo.jpg","MediaType":"IMAGE","ArticleDate":"2017-02-24"},{"ArticleId":3,"ArticleName":"Article 1 3","ArticleDescription":"Dear parents, Hijra New Year has been confirmed to be on Thursday 15th October 2015.","ArticleImage":"http://lasagu.net/school/uploadtest/default/company_logo.jpg","MediaType":"IMAGE","ArticleDate":"2017-03-24"},{"ArticleId":4,"ArticleName":"Article 1 4","ArticleDescription":"Dear parents, Hijra New Year has been confirmed to be on Thursday 15th October 2015.","ArticleImage":"http://lasagu.net/school/uploadtest/default/company_logo.jpg","MediaType":"IMAGE","ArticleDate":"2017-04-24"}]
     * SubCategories : [{"CategoryId":7,"Category":"Article 1 11"},{"CategoryId":8,"Category":"Flag Day 1 12"}]
     * MessageTotalCount : 43
     * MessageListCurrentCount : 10
     * PaginationNumber : 1
     * PaginationTotalNumber : 5
     * PaginationStartFrom : 0
     */

    private int StatusCode;
    private String StatusMessage;
    private String ArticleCategory;
    private String ArticleLabel;
    private int MessageTotalCount;
    private int MessageListCurrentCount;
    private int PaginationNumber;
    private int PaginationTotalNumber;
    private int PaginationStartFrom;
    private ArrayList<ArticlesItemsBean> ArticlesItems;
    private ArrayList<SubCategoriesBean> SubCategories;

    public int getStatusCode() {
        return StatusCode;
    }

    public void setStatusCode(int StatusCode) {
        this.StatusCode = StatusCode;
    }

    public String getStatusMessage() {
        return StatusMessage;
    }

    public void setStatusMessage(String StatusMessage) {
        this.StatusMessage = StatusMessage;
    }

    public String getArticleCategory() {
        return ArticleCategory;
    }

    public void setArticleCategory(String ArticleCategory) {
        this.ArticleCategory = ArticleCategory;
    }

    public String getArticleLabel() {
        return ArticleLabel;
    }

    public void setArticleLabel(String ArticleLabel) {
        this.ArticleLabel = ArticleLabel;
    }

    public int getMessageTotalCount() {
        return MessageTotalCount;
    }

    public void setMessageTotalCount(int MessageTotalCount) {
        this.MessageTotalCount = MessageTotalCount;
    }

    public int getMessageListCurrentCount() {
        return MessageListCurrentCount;
    }

    public void setMessageListCurrentCount(int MessageListCurrentCount) {
        this.MessageListCurrentCount = MessageListCurrentCount;
    }

    public int getPaginationNumber() {
        return PaginationNumber;
    }

    public void setPaginationNumber(int PaginationNumber) {
        this.PaginationNumber = PaginationNumber;
    }

    public int getPaginationTotalNumber() {
        return PaginationTotalNumber;
    }

    public void setPaginationTotalNumber(int PaginationTotalNumber) {
        this.PaginationTotalNumber = PaginationTotalNumber;
    }

    public int getPaginationStartFrom() {
        return PaginationStartFrom;
    }

    public void setPaginationStartFrom(int PaginationStartFrom) {
        this.PaginationStartFrom = PaginationStartFrom;
    }

    public ArrayList<ArticlesItemsBean> getArticlesItems() {
        return ArticlesItems;
    }

    public void setArticlesItems(ArrayList<ArticlesItemsBean> ArticlesItems) {
        this.ArticlesItems = ArticlesItems;
    }

    public ArrayList<SubCategoriesBean> getSubCategories() {
        return SubCategories;
    }

    public void setSubCategories(ArrayList<SubCategoriesBean> SubCategories) {
        this.SubCategories = SubCategories;
    }

    public static AwarenessListResponse fromJson(String json) {
            return new Gson().fromJson(json, AwarenessListResponse.class);
    }
}
