package net.lasagu.takyon360.models;

import com.google.gson.Gson;

/**
 * Created by anirban on 1/3/18.
 */

public class FeeSummaryResponse {
    /**
     * StatusCode : 1
     * StatusMessage : Success
     * Name : Arav Jayesh
     * Division : 3 E
     * AccountDetailsLabel : Payment Details
     * HeadLabel : Fee Summary
     * TotalFeeLabel : Total Fee (Of the entire year)
     * StudentDetailsLabel : Student Details
     * PayLabel : Pay
     * NameLabel : Name
     * ClassDivisionLabel : Class and Division
     * DueAmountLabel : Due Amount
     * AmountLabel : Amount
     * OtherLabel : Other
     * TotalFee : 7358.00
     * TotalPaidLabel : Total Paid (Payment you made for this year )
     * TotalPaid : 3988.00
     * TotalPayableLabel : Total Payable (Payment pending for this year)
     * TotalDue : 3370.00
     * CurrentDueLabel : Current Due
     * CurrentDueFormLabel : Current Due (Current payable amount)
     * CurrentDue : 2050.00
     * PaymentUrl :
     */

    private int StatusCode;
    private String StatusMessage;
    private String Name;
    private String Division;
    private String AccountDetailsLabel;
    private String HeadLabel;
    private String TotalFeeLabel;
    private String StudentDetailsLabel;
    private String PayLabel;
    private String NameLabel;
    private String ClassDivisionLabel;
    private String DueAmountLabel;
    private String AmountLabel;
    private String OtherLabel;
    private String TotalFee;
    private String TotalPaidLabel;
    private String TotalPaid;
    private String TotalPayableLabel;
    private String TotalDue;
    private String CurrentDueLabel;
    private String CurrentDueFormLabel;
    private String CurrentDue;
    private String PaymentUrl;

    public static FeeSummaryResponse fromJson(String str) {

        return new Gson().fromJson(str, FeeSummaryResponse.class);
    }

    public String getAmountLabel() {
        return AmountLabel;
    }

    public void setAmountLabel(String amountLabel) {
        AmountLabel = amountLabel;
    }

    public int getStatusCode() {
        return StatusCode;
    }

    public void setStatusCode(int StatusCode) {
        this.StatusCode = StatusCode;
    }

    public String getStatusMessage() {
        return StatusMessage;
    }

    public void setStatusMessage(String StatusMessage) {
        this.StatusMessage = StatusMessage;
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public String getDivision() {
        return Division;
    }

    public void setDivision(String Division) {
        this.Division = Division;
    }

    public String getAccountDetailsLabel() {
        return AccountDetailsLabel;
    }

    public void setAccountDetailsLabel(String AccountDetailsLabel) {
        this.AccountDetailsLabel = AccountDetailsLabel;
    }

    public String getHeadLabel() {
        return HeadLabel;
    }

    public void setHeadLabel(String HeadLabel) {
        this.HeadLabel = HeadLabel;
    }

    public String getTotalFeeLabel() {
        return TotalFeeLabel;
    }

    public void setTotalFeeLabel(String TotalFeeLabel) {
        this.TotalFeeLabel = TotalFeeLabel;
    }

    public String getStudentDetailsLabel() {
        return StudentDetailsLabel;
    }

    public void setStudentDetailsLabel(String StudentDetailsLabel) {
        this.StudentDetailsLabel = StudentDetailsLabel;
    }

    public String getPayLabel() {
        return PayLabel;
    }

    public void setPayLabel(String PayLabel) {
        this.PayLabel = PayLabel;
    }

    public String getNameLabel() {
        return NameLabel;
    }

    public void setNameLabel(String NameLabel) {
        this.NameLabel = NameLabel;
    }

    public String getClassDivisionLabel() {
        return ClassDivisionLabel;
    }

    public void setClassDivisionLabel(String ClassDivisionLabel) {
        this.ClassDivisionLabel = ClassDivisionLabel;
    }

    public String getDueAmountLabel() {
        return DueAmountLabel;
    }

    public void setDueAmountLabel(String DueAmountLabel) {
        this.DueAmountLabel = DueAmountLabel;
    }

    public String getOtherLabel() {
        return OtherLabel;
    }

    public void setOtherLabel(String OtherLabel) {
        this.OtherLabel = OtherLabel;
    }

    public String getTotalFee() {
        return TotalFee;
    }

    public void setTotalFee(String TotalFee) {
        this.TotalFee = TotalFee;
    }

    public String getTotalPaidLabel() {
        return TotalPaidLabel;
    }

    public void setTotalPaidLabel(String TotalPaidLabel) {
        this.TotalPaidLabel = TotalPaidLabel;
    }

    public String getTotalPaid() {
        return TotalPaid;
    }

    public void setTotalPaid(String TotalPaid) {
        this.TotalPaid = TotalPaid;
    }

    public String getTotalPayableLabel() {
        return TotalPayableLabel;
    }

    public void setTotalPayableLabel(String TotalPayableLabel) {
        this.TotalPayableLabel = TotalPayableLabel;
    }

    public String getTotalDue() {
        return TotalDue;
    }

    public void setTotalDue(String TotalDue) {
        this.TotalDue = TotalDue;
    }

    public String getCurrentDueLabel() {
        return CurrentDueLabel;
    }

    public void setCurrentDueLabel(String CurrentDueLabel) {
        this.CurrentDueLabel = CurrentDueLabel;
    }

    public String getCurrentDueFormLabel() {
        return CurrentDueFormLabel;
    }

    public void setCurrentDueFormLabel(String CurrentDueFormLabel) {
        this.CurrentDueFormLabel = CurrentDueFormLabel;
    }

    public String getCurrentDue() {
        return CurrentDue;
    }

    public void setCurrentDue(String CurrentDue) {
        this.CurrentDue = CurrentDue;
    }

    public String getPaymentUrl() {
        return PaymentUrl;
    }

    public void setPaymentUrl(String PaymentUrl) {
        this.PaymentUrl = PaymentUrl;
    }
}
