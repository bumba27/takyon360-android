package net.lasagu.takyon360.models;

import com.google.gson.Gson;

/**
 * Created by ajana on 21/09/2017.
 */

public class ReadNotificationSubmission {
    /**
     * UserId : 98189
     * ModuleCode : HMW
     * ProcessId : 1
     */

    private String UserId;
    private String ModuleCode;
    private int ProcessId;

    public String getUserId() {
        return UserId;
    }

    public void setUserId(String UserId) {
        this.UserId = UserId;
    }

    public String getModuleCode() {
        return ModuleCode;
    }

    public void setModuleCode(String ModuleCode) {
        this.ModuleCode = ModuleCode;
    }

    public int getProcessId() {
        return ProcessId;
    }

    public void setProcessId(int ProcessId) {
        this.ProcessId = ProcessId;
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }

}
