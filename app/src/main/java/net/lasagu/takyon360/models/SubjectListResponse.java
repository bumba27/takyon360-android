package net.lasagu.takyon360.models;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

public class SubjectListResponse {

    /**
     * StatusCode : 1
     * StatusMessage : Success
     * subjectlist : [{"subject_id":" Activities","subject_name":" Activities"},{"subject_id":"Accounting","subject_name":"Accounting"},{"subject_id":"Arabic","subject_name":"Arabic"}]
     */

    private int StatusCode;
    private String StatusMessage;
    private ArrayList<Subjectlist> subjectlist;

    public int getStatusCode() {
        return StatusCode;
    }

    public void setStatusCode(int StatusCode) {
        this.StatusCode = StatusCode;
    }

    public String getStatusMessage() {
        return StatusMessage;
    }

    public void setStatusMessage(String StatusMessage) {
        this.StatusMessage = StatusMessage;
    }

    public ArrayList<Subjectlist> getSubjectlist() {
        return subjectlist;
    }

    public void setSubjectlist(ArrayList<Subjectlist> subjectlist) {
        this.subjectlist = subjectlist;
    }

    public static SubjectListResponse fromJson(String json) {
        return new Gson().fromJson(json, SubjectListResponse.class);
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}
