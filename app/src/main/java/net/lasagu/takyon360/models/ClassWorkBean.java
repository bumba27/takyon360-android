package net.lasagu.takyon360.models;

import com.google.gson.Gson;

import java.util.List;

/**
 * Created by anirban on 8/29/17.
 */

public class ClassWorkBean {
    /**
     * Id : 5
     * Topic : Arabic
     * Description : إحضارالوان خشبية وشمعية وفلوماستر و اوراق ملونة وملاعق بلاستكية وعيون متحركة وخيط صوف وسيلكون لاصق بدون المسدس
     * AttachIcon : 0
     * Attachments :
     * Date : 2017-04-19
     */

    private int Id;
    private String Topic;
    private String Description;
    private int AttachIcon;
    private List<AttachmentsBean> Attachments;
    private String Date;

    public static ClassWorkBean fromJson(String str) {

        return new Gson().fromJson(str, ClassWorkBean.class);
    }

    public int getId() {
        return Id;
    }

    public void setId(int Id) {
        this.Id = Id;
    }

    public String getTopic() {
        return Topic;
    }

    public void setTopic(String Topic) {
        this.Topic = Topic;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String Description) {
        this.Description = Description;
    }

    public int getAttachIcon() {
        return AttachIcon;
    }

    public void setAttachIcon(int AttachIcon) {
        this.AttachIcon = AttachIcon;
    }

    public List<AttachmentsBean> getAttachments() {
        return Attachments;
    }

    public void setAttachments(List<AttachmentsBean> attachments) {
        Attachments = attachments;
    }

    public String getDate() {
        return Date;
    }

    public void setDate(String Date) {
        this.Date = Date;
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}
