package net.lasagu.takyon360.models;

import com.google.gson.Gson;

/**
 * Created by ajana on 23/08/2017.
 */

public class CategoriesBeanGallery {
    private String CategoryId;
    private String Category;
    private String ParentId;
    private String Thumbnail;

    public String getCategoryId() {
        return CategoryId;
    }

    public void setCategoryId(String categoryId) {
        CategoryId = categoryId;
    }

    public String getCategory() {
        return Category;
    }

    public void setCategory(String category) {
        Category = category;
    }

    public String getParentId() {
        return ParentId;
    }

    public void setParentId(String parentId) {
        ParentId = parentId;
    }

    public String getThumbnail() {
        return Thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        Thumbnail = thumbnail;
    }

    public static CategoriesBeanGallery fromJson(String json) {
        return new Gson().fromJson(json, CategoriesBeanGallery.class);
    }

}
