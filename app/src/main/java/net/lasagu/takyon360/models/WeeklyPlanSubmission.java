package net.lasagu.takyon360.models;

import com.google.gson.Gson;

/**
 * Created by anirban on 8/27/17.
 */

public class WeeklyPlanSubmission {
    /**
     * UserId : 98189
     * Div_Id :
     * Sub_Id :
     * FromDate :
     * ToDate :
     * IsLatest : 1
     * Type :
     * OffSet : 0
     * Limit : 5
     */

    private String UserId;
    private String Div_Id;
    private String Sub_Id;
    private String FromDate;
    private String ToDate;
    private int IsLatest;
    private String Type;
    private int OffSet;
    private int Limit;

    public String getUserId() {
        return UserId;
    }

    public void setUserId(String UserId) {
        this.UserId = UserId;
    }

    public String getDiv_Id() {
        return Div_Id;
    }

    public void setDiv_Id(String Div_Id) {
        this.Div_Id = Div_Id;
    }

    public String getSub_Id() {
        return Sub_Id;
    }

    public void setSub_Id(String sub_Id) {
        Sub_Id = sub_Id;
    }

    public String getFromDate() {
        return FromDate;
    }

    public void setFromDate(String FromDate) {
        this.FromDate = FromDate;
    }

    public String getToDate() {
        return ToDate;
    }

    public void setToDate(String ToDate) {
        this.ToDate = ToDate;
    }

    public int getIsLatest() {
        return IsLatest;
    }

    public void setIsLatest(int IsLatest) {
        this.IsLatest = IsLatest;
    }

    public String getType() {
        return Type;
    }

    public void setType(String Type) {
        this.Type = Type;
    }

    public int getOffSet() {
        return OffSet;
    }

    public void setOffSet(int OffSet) {
        this.OffSet = OffSet;
    }

    public int getLimit() {
        return Limit;
    }

    public void setLimit(int Limit) {
        this.Limit = Limit;
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }

}
