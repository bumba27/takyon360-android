package net.lasagu.takyon360.models;

/**
 * Created by ajana on 12/07/2017.
 */

public class Recipients {
    private Members[] Members;

    private Group[] Group;

    public Members[] getMembers() {
        return Members;
    }

    public void setMembers(Members[] Members) {
        this.Members = Members;
    }

    public Group[] getGroup() {
        return Group;
    }

    public void setGroup(Group[] Group) {
        this.Group = Group;
    }

}
