package net.lasagu.takyon360.models;

/**
 * Created by ajana on 05/07/2017.
 */

public class Notification {
    private Downloads[] Downloads;

    private String id;
    private String cat_id;

    private String Date;

    private String Details;

    private String Type;

    private String CreatedBy;

    private String Title;

    public String getCat_id() {
        return cat_id;
    }

    public void setCat_id(String cat_id) {
        this.cat_id = cat_id;
    }

    public Downloads[] getDownloads ()
    {
        return Downloads;
    }

    public void setDownloads (Downloads[] Downloads)
    {
        this.Downloads = Downloads;
    }

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public String getDate ()
    {
        return Date;
    }

    public void setDate (String Date)
    {
        this.Date = Date;
    }

    public String getDetails ()
    {
        return Details;
    }

    public void setDetails (String Details)
    {
        this.Details = Details;
    }

    public String getType ()
    {
        return Type;
    }

    public void setType (String Type)
    {
        this.Type = Type;
    }

    public String getCreatedBy ()
    {
        return CreatedBy;
    }

    public void setCreatedBy (String CreatedBy)
    {
        this.CreatedBy = CreatedBy;
    }

    public String getTitle ()
    {
        return Title;
    }

    public void setTitle (String Title)
    {
        this.Title = Title;
    }
}
