package net.lasagu.takyon360.models;

/**
 * Created by ajana on 12/07/2017.
 */

public class Group {
    private String GroupName;

    private String GroupId;

    public String getGroupName() {
        return GroupName;
    }

    public void setGroupName(String GroupName) {
        this.GroupName = GroupName;
    }

    public String getGroupId() {
        return GroupId;
    }

    public void setGroupId(String GroupId) {
        this.GroupId = GroupId;
    }

}
