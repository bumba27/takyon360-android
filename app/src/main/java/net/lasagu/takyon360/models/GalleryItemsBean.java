package net.lasagu.takyon360.models;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;

/**
 * Created by anirban on 8/19/17.
 */

public class GalleryItemsBean {
    /**
     * GalleryId : 1
     * GalleryTitle : Flag 1
     * Media : http://www.lasagu.net/school/uploadtest/company52/gallery/beoyGi_media_1483680113_session info.png
     * Thumbnail : http://www.lasagu.net/school/uploadtest/company52/gallery/beoyGi_media_1483680113_session info.png
     * MediaType : IMAGE
     */

    private int GalleryId;
    private String GalleryTitle;
    private String Media;
    private String Thumbnail;
    private String MediaType;

    public int getGalleryId() {
        return GalleryId;
    }

    public void setGalleryId(int GalleryId) {
        this.GalleryId = GalleryId;
    }

    public String getGalleryTitle() {
        return GalleryTitle;
    }

    public void setGalleryTitle(String GalleryTitle) {
        this.GalleryTitle = GalleryTitle;
    }

    public String getMedia() {
        return Media;
    }

    public void setMedia(String Media) {
        this.Media = Media;
    }

    public String getThumbnail() {
        return Thumbnail;
    }

    public void setThumbnail(String Thumbnail) {
        this.Thumbnail = Thumbnail;
    }

    public String getMediaType() {
        return MediaType;
    }

    public void setMediaType(String MediaType) {
        this.MediaType = MediaType;
    }

    public static ArrayList<GalleryItemsBean> toList(String json) {
        return new Gson().fromJson(json, new TypeToken<ArrayList<GalleryItemsBean>>() {
        }.getType());
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}