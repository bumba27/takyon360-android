package net.lasagu.takyon360.models;

/**
 * Created by ajana on 19/07/2017.
 */

public class Groups {
    private int id;

    private String name;

    public Groups(String name) {

        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
