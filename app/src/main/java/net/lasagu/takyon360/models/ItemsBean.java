package net.lasagu.takyon360.models;

import com.google.gson.Gson;

/**
 * Created by anirban on 8/19/17.
 */

public class ItemsBean {
    /**
     * Id : 5998
     * Title : Termly Newsletter - Secondary
     * ShortDesc : Termly Newsletter - Secondary
     * Description : &lt;p style=&quot;text-align: center;&quot;&gt;&lt;span style=&quot;text-decoration: underline; color: #800000;&quot;&gt;&lt;strong&gt;Termly Newsletter - Secondary&lt;/strong&gt;&lt;/span&gt;&lt;/p&gt;
     &lt;p style=&quot;text-align: left;&quot;&gt;&lt;span style=&quot;color: #000000;&quot;&gt;&lt;strong&gt;April-June - 2017&lt;br /&gt;&lt;/strong&gt;&lt;/span&gt;&lt;/p&gt;
     &lt;p style=&quot;text-align: left;&quot;&gt;&lt;span style=&quot;color: #000000;&quot;&gt;&lt;strong&gt;&amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp;&amp;nbsp;&lt;span style=&quot;color: #800000;&quot;&gt;&amp;nbsp;Heart to Heart: From the Principal&lt;/span&gt;&lt;br /&gt;&lt;/strong&gt;&lt;/span&gt;&lt;/p&gt;
     &lt;p style=&quot;text-align: left;&quot;&gt;&lt;span style=&quot;color: #000000;&quot;&gt;&lt;strong&gt;Family Time- Building Bonds&lt;br /&gt;&amp;lsquo;If you want your children to turn out well, spend twice as much time with them and half as much&lt;br /&gt;money&amp;rsquo;- Abigal Van Buren&lt;/strong&gt;&lt;/span&gt;&lt;/p&gt;
     &lt;p style=&quot;text-align: left;&quot;&gt;&lt;span style=&quot;color: #000000;&quot;&gt;&lt;strong&gt;Please click on the below link to view the complete Newsletter&lt;/strong&gt;&lt;/span&gt;&lt;/p&gt;
     &lt;p&gt;&lt;span style=&quot;color: #000080;&quot;&gt;&lt;a style=&quot;color: #000080;&quot; href=&quot;http://ambassadorschool.com/upload/SECONDARY.pdf&quot; target=&quot;_blank&quot; data-saferedirecturl=&quot;https://www.google.com/url?hl=en&amp;amp;q=http://ambassadorschool.com/upload/SECONDARY.pdf&amp;amp;source=gmail&amp;amp;ust=1499578563952000&amp;amp;usg=AFQjCNEJ8TOcsOiX-2ly1P4-OWvT_Tyeow&quot;&gt;http://ambassadorschool.com/&lt;wbr /&gt;upload/SECONDARY.pdf&lt;/a&gt;&lt;/span&gt;&lt;/p&gt;
     &lt;p style=&quot;text-align: left;&quot;&gt;&amp;nbsp;&lt;span style=&quot;color: #000000;&quot;&gt;&lt;strong&gt;Regards,&lt;/strong&gt;&lt;/span&gt;&lt;/p&gt;
     &lt;p style=&quot;text-align: left;&quot;&gt;&lt;span style=&quot;color: #000000;&quot;&gt;&lt;strong&gt;Ambassador Admin&lt;/strong&gt;&lt;/span&gt;&lt;/p&gt;
     * Date : 08 Jul 2017 08:21
     * Image : http://lasagu.net/school/uploadtest/company72/news/doc_5998_1.jpg
     * thumbnail : http://lasagu.net/school/uploadtest/company72/news/doc_5998_thumb.png
     * ReadStatus : 0
     */

    private String Id;
    private String Title;
    private String ShortDesc;
    private String Description;
    private String Date;
    private String Image;
    private String thumbnail;
    private String ReadStatus;

    public String getId() {
        return Id;
    }

    public void setId(String Id) {
        this.Id = Id;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String Title) {
        this.Title = Title;
    }

    public String getShortDesc() {
        return ShortDesc;
    }

    public void setShortDesc(String ShortDesc) {
        this.ShortDesc = ShortDesc;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String Description) {
        this.Description = Description;
    }

    public String getDate() {
        return Date;
    }

    public void setDate(String Date) {
        this.Date = Date;
    }

    public String getImage() {
        return Image;
    }

    public void setImage(String Image) {
        this.Image = Image;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getReadStatus() {
        return ReadStatus;
    }

    public void setReadStatus(String ReadStatus) {
        this.ReadStatus = ReadStatus;
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }

}
