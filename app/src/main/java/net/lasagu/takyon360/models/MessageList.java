package net.lasagu.takyon360.models;

import com.google.gson.Gson;

import java.util.ArrayList;

/**
 * Created by anirban on 7/9/17.
 */

public class MessageList {
    private String User;
    private String AttachIcon;
    private String Sujbect;
    private String Date;
    private String Message;
    private int IsRead;
    private String UserProfileImg;
    private String Id;
    private Recipients Recipients;
    private String SenderProfileImg;
    private ArrayList<Attachments> Attachments;
    private String ClassLabel;
    private String Sender;
    private String Division;
    private String Class;
    private String SenderID;


    public String getUser() {
        return User;
    }

    public void setUser(String user) {
        User = user;
    }

    public String getAttachIcon() {
        return AttachIcon;
    }

    public void setAttachIcon(String attachIcon) {
        AttachIcon = attachIcon;
    }

    public String getSujbect() {
        return Sujbect;
    }

    public void setSujbect(String sujbect) {
        Sujbect = sujbect;
    }

    public String getDate() {
        return Date;
    }

    public void setDate(String date) {
        Date = date;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public int getIsRead() {
        return IsRead;
    }

    public void setIsRead(int isRead) {
        IsRead = isRead;
    }

    public String getUserProfileImg() {
        return UserProfileImg;
    }

    public void setUserProfileImg(String userProfileImg) {
        UserProfileImg = userProfileImg;
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public net.lasagu.takyon360.models.Recipients getRecipients() {
        return Recipients;
    }

    public void setRecipients(net.lasagu.takyon360.models.Recipients recipients) {
        Recipients = recipients;
    }

    public String getSenderProfileImg() {
        return SenderProfileImg;
    }

    public void setSenderProfileImg(String senderProfileImg) {
        SenderProfileImg = senderProfileImg;
    }

    public ArrayList<net.lasagu.takyon360.models.Attachments> getAttachments() {
        return Attachments;
    }

    public void setAttachments(ArrayList<net.lasagu.takyon360.models.Attachments> attachments) {
        Attachments = attachments;
    }

    public String getClassLabel() {
        return ClassLabel;
    }

    public void setClassLabel(String classLabel) {
        ClassLabel = classLabel;
    }

    public String getSender() {
        return Sender;
    }

    public void setSender(String sender) {
        Sender = sender;
    }

    public String getDivision() {
        return Division;
    }

    public void setDivision(String division) {
        Division = division;
    }

    public String getClassName() {
        return Class;
    }

    public void setClass(String aClass) {
        Class = aClass;
    }

    public String getSenderID() {
        return SenderID;
    }

    public void setSenderID(String senderID) {
        SenderID = senderID;
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }

}
