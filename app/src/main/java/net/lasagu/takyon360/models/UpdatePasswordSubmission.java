package net.lasagu.takyon360.models;

import com.google.gson.Gson;

/**
 * Created by ajana on 11/07/2017.
 */

public class UpdatePasswordSubmission {
    private String ReapeatNewPassword;
    private String NewPassword;
    private String CurrentPassword;
    private String UserId;
    private String client_ip;

    public String getReapeatNewPassword() {
        return ReapeatNewPassword;
    }

    public void setReapeatNewPassword(String ReapeatNewPassword) {
        this.ReapeatNewPassword = ReapeatNewPassword;
    }

    public String getNewPassword() {
        return NewPassword;
    }

    public void setNewPassword(String NewPassword) {
        this.NewPassword = NewPassword;
    }

    public String getCurrentPassword() {
        return CurrentPassword;
    }

    public void setCurrentPassword(String CurrentPassword) {
        this.CurrentPassword = CurrentPassword;
    }

    public String getUserId() {
        return UserId;
    }

    public void setUserId(String UserId) {
        this.UserId = UserId;
    }

    public String getClient_ip() {
        return client_ip;
    }

    public void setClient_ip(String client_ip) {
        this.client_ip = client_ip;
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }

}
