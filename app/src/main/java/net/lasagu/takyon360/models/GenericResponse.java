package net.lasagu.takyon360.models;

import com.google.gson.Gson;

/**
 * Created by ajana on 11/07/2017.
 */

public class GenericResponse {
    private int StatusCode;
    private String StatusMessage;

    public int getStatusCode() {
        return StatusCode;
    }

    public void setStatusCode(int statusCode) {
        StatusCode = statusCode;
    }

    public String getStatusMessage() {
        return StatusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        StatusMessage = statusMessage;
    }

    public static GenericResponse fromJson(String json) {
        return new Gson().fromJson(json, GenericResponse.class);
    }
}
