package net.lasagu.takyon360.models;

import com.google.gson.Gson;

/**
 * Created by anirban on 8/10/17.
 */

public class FtpSubmission {
    /**
     * UserId : 98189
     * IsMobile : 1
     */

    private String UserId;
    private int IsMobile;

    public String getUserId() {
        return UserId;
    }

    public void setUserId(String UserId) {
        this.UserId = UserId;
    }

    public int getIsMobile() {
        return IsMobile;
    }

    public void setIsMobile(int IsMobile) {
        this.IsMobile = IsMobile;
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}
