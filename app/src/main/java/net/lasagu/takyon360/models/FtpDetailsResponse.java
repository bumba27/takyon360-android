package net.lasagu.takyon360.models;

import com.google.gson.Gson;

/**
 * Created by anirban on 8/10/17.
 */

public class FtpDetailsResponse {
    /**
     * StatusCode : 1
     * StatusMessage : Success
     * Server : 193.321.456
     * UserName     : ftpusername
     * Password : MTIzNDU2
     * ServerPath : /home/lnet/public_html/comm
     */

    private int StatusCode;
    private String StatusMessage;
    private String Server;
    private String UserName;
    private String Password;
    private String ServerPath;

    public int getStatusCode() {
        return StatusCode;
    }

    public void setStatusCode(int StatusCode) {
        this.StatusCode = StatusCode;
    }

    public String getStatusMessage() {
        return StatusMessage;
    }

    public void setStatusMessage(String StatusMessage) {
        this.StatusMessage = StatusMessage;
    }

    public String getServer() {
        return Server;
    }

    public void setServer(String Server) {
        this.Server = Server;
    }

    public String getUserName() {
        return UserName;
    }

    public void setUserName(String UserName) {
        this.UserName = UserName;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String Password) {
        this.Password = Password;
    }

    public String getServerPath() {
        return ServerPath;
    }

    public void setServerPath(String ServerPath) {
        this.ServerPath = ServerPath;
    }

    public static FtpDetailsResponse fromJson(String json) {
        return new Gson().fromJson(json, FtpDetailsResponse.class);
    }

}
