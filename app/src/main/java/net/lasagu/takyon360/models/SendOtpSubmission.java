package net.lasagu.takyon360.models;

import com.google.gson.Gson;

/**
 * Created by ajana on 11/01/2018.
 */

public class SendOtpSubmission {
    /**
     * VEmail : sajiya@takyonline.com
     */

    private String VEmail;

    public String getVEmail() {
        return VEmail;
    }

    public void setVEmail(String VEmail) {
        this.VEmail = VEmail;
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}
