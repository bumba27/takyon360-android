package net.lasagu.takyon360.models;

import com.google.gson.Gson;

import java.util.List;

/**
 * Created by anirban on 1/3/18.
 */

public class PaymentHistoryResponse {
    /**
     * StatusCode : 1
     * StatusMessage : Success
     * Name : Arav Jayesh
     * NameLabel : Name
     * ClassLabel : Class
     * Division : 3 E
     * HeadLabel : Payment Details
     * ReceiptLabel : Receipt No
     * AmountLabel : Amount
     * DateLabel : Date
     * TotalLabel : Total
     * Transactions : [{"Date":"01/11/2017","ReceiptNo":"227663","Amount":"120.00"},{"Date":"26/09/2017","ReceiptNo":"225994","Amount":"3343.00"},{"Date":"28/03/2017","ReceiptNo":"220071","Amount":"550.00"},{"Date":"21/02/2017","ReceiptNo":"217495","Amount":"80.00"},{"Date":"31/01/2017","ReceiptNo":"216265","Amount":"2140.00"},{"Date":"03/10/2016","ReceiptNo":"0425979","Amount":"400.00"},{"Date":"03/10/2016","ReceiptNo":"210241","Amount":"200.00"},{"Date":"03/10/2016","ReceiptNo":"210336","Amount":"1855.00"}]
     */

    private int StatusCode;
    private String StatusMessage;
    private String Name;
    private String NameLabel;
    private String ClassLabel;
    private String Division;
    private String HeadLabel;
    private String ReceiptLabel;
    private String AmountLabel;
    private String DateLabel;
    private String TotalLabel;
    private List<TransactionsBean> Transactions;

    public static PaymentHistoryResponse fromJson(String str) {

        return new Gson().fromJson(str, PaymentHistoryResponse.class);
    }

    public int getStatusCode() {
        return StatusCode;
    }

    public void setStatusCode(int StatusCode) {
        this.StatusCode = StatusCode;
    }

    public String getStatusMessage() {
        return StatusMessage;
    }

    public void setStatusMessage(String StatusMessage) {
        this.StatusMessage = StatusMessage;
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public String getNameLabel() {
        return NameLabel;
    }

    public void setNameLabel(String NameLabel) {
        this.NameLabel = NameLabel;
    }

    public String getClassLabel() {
        return ClassLabel;
    }

    public void setClassLabel(String ClassLabel) {
        this.ClassLabel = ClassLabel;
    }

    public String getDivision() {
        return Division;
    }

    public void setDivision(String Division) {
        this.Division = Division;
    }

    public String getHeadLabel() {
        return HeadLabel;
    }

    public void setHeadLabel(String HeadLabel) {
        this.HeadLabel = HeadLabel;
    }

    public String getReceiptLabel() {
        return ReceiptLabel;
    }

    public void setReceiptLabel(String ReceiptLabel) {
        this.ReceiptLabel = ReceiptLabel;
    }

    public String getAmountLabel() {
        return AmountLabel;
    }

    public void setAmountLabel(String AmountLabel) {
        this.AmountLabel = AmountLabel;
    }

    public String getDateLabel() {
        return DateLabel;
    }

    public void setDateLabel(String DateLabel) {
        this.DateLabel = DateLabel;
    }

    public String getTotalLabel() {
        return TotalLabel;
    }

    public void setTotalLabel(String TotalLabel) {
        this.TotalLabel = TotalLabel;
    }

    public List<TransactionsBean> getTransactions() {
        return Transactions;
    }

    public void setTransactions(List<TransactionsBean> Transactions) {
        this.Transactions = Transactions;
    }
}
