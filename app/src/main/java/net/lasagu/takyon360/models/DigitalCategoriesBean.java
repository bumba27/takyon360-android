package net.lasagu.takyon360.models;

/**
 * Created by anirban on 8/13/17.
 */

public class DigitalCategoriesBean {
    /**
     * CategoryId : 1
     * Category : Category 1
     * CategoryCount : 3
     * ParentId : 0
     */

    private int CategoryId;
    private String Category;
    private int CategoryCount;
    private int ParentId;

    public int getCategoryId() {
        return CategoryId;
    }

    public void setCategoryId(int CategoryId) {
        this.CategoryId = CategoryId;
    }

    public String getCategory() {
        return Category;
    }

    public void setCategory(String Category) {
        this.Category = Category;
    }

    public int getCategoryCount() {
        return CategoryCount;
    }

    public void setCategoryCount(int CategoryCount) {
        this.CategoryCount = CategoryCount;
    }

    public int getParentId() {
        return ParentId;
    }

    public void setParentId(int ParentId) {
        this.ParentId = ParentId;
    }
}
