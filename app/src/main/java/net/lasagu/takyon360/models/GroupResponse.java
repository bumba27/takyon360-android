package net.lasagu.takyon360.models;

/**
 * Created by ajana on 19/07/2017.
 */

public class GroupResponse {
    private Groups[] Groups;

    private String StatusCode;

    private String StatusMessage;

    public Groups[] getGroups() {
        return Groups;
    }

    public void setGroups(Groups[] Groups) {
        this.Groups = Groups;
    }

    public String getStatusCode() {
        return StatusCode;
    }

    public void setStatusCode(String StatusCode) {
        this.StatusCode = StatusCode;
    }

    public String getStatusMessage() {
        return StatusMessage;
    }

    public void setStatusMessage(String StatusMessage) {
        this.StatusMessage = StatusMessage;
    }
}
