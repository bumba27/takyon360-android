package net.lasagu.takyon360.models;

/**
 * Created by anirban on 8/19/17.
 */

public class EventsBean {
    /**
     * Id : 3792
     * start_date : 2016-11-16 07:00:00
     * end_date : 2016-11-16 08:00:00
     * Text : Test 2
     * Details : Test 2 ( Social Arabic Grade ( 1: 9 )
     */

    private int Id;
    private String start_date;
    private String end_date;
    private String Text;
    private String Details;

    public int getId() {
        return Id;
    }

    public void setId(int Id) {
        this.Id = Id;
    }

    public String getStart_date() {
        return start_date;
    }

    public void setStart_date(String start_date) {
        this.start_date = start_date;
    }

    public String getEnd_date() {
        return end_date;
    }

    public void setEnd_date(String end_date) {
        this.end_date = end_date;
    }

    public String getText() {
        return Text;
    }

    public void setText(String Text) {
        this.Text = Text;
    }

    public String getDetails() {
        return Details;
    }

    public void setDetails(String Details) {
        this.Details = Details;
    }
}
