package net.lasagu.takyon360.models;

import com.google.gson.Gson;

/**
 * Created by ajana on 28/07/2017.
 */

public class UserIdAndSearchTextSubmission {
    /**
     * UserId : 98189
     * SearchText :
     */

    private String UserId;
    private String SearchText;

    public String getUserId() {
        return UserId;
    }

    public void setUserId(String UserId) {
        this.UserId = UserId;
    }

    public String getSearchText() {
        return SearchText;
    }

    public void setSearchText(String SearchText) {
        this.SearchText = SearchText;
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}
