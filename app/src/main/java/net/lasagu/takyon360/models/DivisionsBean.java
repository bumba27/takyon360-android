package net.lasagu.takyon360.models;

/**
 * Created by anirban on 8/27/17.
 */

public class DivisionsBean {
    /**
     * DivId : 123
     * Division : VII A
     */

    private String DivId;
    private String Division;

    public String getDivId() {
        return DivId;
    }

    public void setDivId(String DivId) {
        this.DivId = DivId;
    }

    public String getDivision() {
        return Division;
    }

    public void setDivision(String Division) {
        this.Division = Division;
    }
}
