package net.lasagu.takyon360.models;

import com.google.gson.Gson;

public class ForgotPasswordConfirmationSubmission {
    /**
     * Key : 4568
     * VEmail : sajiya@takyonline.com
     * NewPassword : 123456
     */

    private String Key;
    private String VEmail;
    private String NewPassword;

    public String getKey() {
        return Key;
    }

    public void setKey(String key) {
        Key = key;
    }

    public String getVEmail() {
        return VEmail;
    }

    public void setVEmail(String VEmail) {
        this.VEmail = VEmail;
    }

    public String getNewPassword() {
        return NewPassword;
    }

    public void setNewPassword(String newPassword) {
        NewPassword = newPassword;
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}
