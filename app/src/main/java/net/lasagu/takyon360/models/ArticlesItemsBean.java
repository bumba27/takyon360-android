package net.lasagu.takyon360.models;

import com.google.gson.Gson;

/**
 * Created by anirban on 8/27/17.
 */

public class ArticlesItemsBean {
    /**
     * ArticleId : 1
     * ArticleName : Article 1 1
     * ArticleDescription : Dear parents, Hijra New Year has been confirmed to be on Thursday 15th October 2015.
     * ArticleImage : http://lasagu.net/school/uploadtest/default/company_logo.jpg
     * MediaType : IMAGE
     * ArticleDate : 2017-02-24
     */

    private int ArticleId;
    private String ArticleName;
    private String ArticleDescription;
    private String ArticleImage;
    private String MediaType;
    private String ArticleDate;

    public int getArticleId() {
        return ArticleId;
    }

    public void setArticleId(int ArticleId) {
        this.ArticleId = ArticleId;
    }

    public String getArticleName() {
        return ArticleName;
    }

    public void setArticleName(String ArticleName) {
        this.ArticleName = ArticleName;
    }

    public String getArticleDescription() {
        return ArticleDescription;
    }

    public void setArticleDescription(String ArticleDescription) {
        this.ArticleDescription = ArticleDescription;
    }

    public String getArticleImage() {
        return ArticleImage;
    }

    public void setArticleImage(String ArticleImage) {
        this.ArticleImage = ArticleImage;
    }

    public String getMediaType() {
        return MediaType;
    }

    public void setMediaType(String MediaType) {
        this.MediaType = MediaType;
    }

    public String getArticleDate() {
        return ArticleDate;
    }

    public void setArticleDate(String ArticleDate) {
        this.ArticleDate = ArticleDate;
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }

}
