package net.lasagu.takyon360.models;

import com.google.gson.Gson;

import java.util.ArrayList;

/**
 * Created by anirban on 7/9/17.
 */

public class CommunicateResponse {

    private String StatusCode;
    private String MarkAsUneadLabel;
    private String PaginationStartFrom;
    private String CheckReadLabel;
    private String CommunicateLabel;
    private String ComposeLabel;
    private String CCLabel;
    private String ToLabel;
    private String GroupLabel;
    private String SubjectLabel;
    private String PaginationTotalNumber;
    private ArrayList<MessageList> MessageList;
    private String DeleteMailLabel;
    private String MessageTotalCount;
    private String FilterLabel;
    private String MarkAsReadLabel;
    private String PaginationNumber;
    private String SentItemsLabel;
    private String CheckUnreadLabel;
    private String StatusMessage;
    private String CheckAllLabel;
    private String InboxLabel;
    private String MessageListCurrentCount;
    private String TrashLabel;

    public String getCCLabel() {
        return CCLabel;
    }

    public void setCCLabel(String CCLabel) {
        this.CCLabel = CCLabel;
    }

    public String getToLabel() {
        return ToLabel;
    }

    public void setToLabel(String toLabel) {
        ToLabel = toLabel;
    }

    public String getGroupLabel() {
        return GroupLabel;
    }

    public void setGroupLabel(String groupLabel) {
        GroupLabel = groupLabel;
    }

    public String getSubjectLabel() {
        return SubjectLabel;
    }

    public void setSubjectLabel(String subjectLabel) {
        SubjectLabel = subjectLabel;
    }

    public String getStatusCode() {
        return StatusCode;
    }

    public void setStatusCode(String StatusCode) {
        this.StatusCode = StatusCode;
    }

    public String getMarkAsUneadLabel() {
        return MarkAsUneadLabel;
    }

    public void setMarkAsUneadLabel(String MarkAsUneadLabel) {
        this.MarkAsUneadLabel = MarkAsUneadLabel;
    }

    public String getPaginationStartFrom() {
        return PaginationStartFrom;
    }

    public void setPaginationStartFrom(String PaginationStartFrom) {
        this.PaginationStartFrom = PaginationStartFrom;
    }

    public String getCheckReadLabel() {
        return CheckReadLabel;
    }

    public void setCheckReadLabel(String CheckReadLabel) {
        this.CheckReadLabel = CheckReadLabel;
    }

    public String getCommunicateLabel() {
        return CommunicateLabel;
    }

    public void setCommunicateLabel(String CommunicateLabel) {
        this.CommunicateLabel = CommunicateLabel;
    }

    public String getComposeLabel() {
        return ComposeLabel;
    }

    public void setComposeLabel(String ComposeLabel) {
        this.ComposeLabel = ComposeLabel;
    }

    public String getPaginationTotalNumber() {
        return PaginationTotalNumber;
    }

    public void setPaginationTotalNumber(String PaginationTotalNumber) {
        this.PaginationTotalNumber = PaginationTotalNumber;
    }

    public ArrayList<MessageList> getMessageList() {
        return MessageList;
    }

    public void setMessageList(ArrayList<MessageList> MessageList) {
        this.MessageList = MessageList;
    }

    public String getDeleteMailLabel() {
        return DeleteMailLabel;
    }

    public void setDeleteMailLabel(String DeleteMailLabel) {
        this.DeleteMailLabel = DeleteMailLabel;
    }

    public String getMessageTotalCount() {
        return MessageTotalCount;
    }

    public void setMessageTotalCount(String MessageTotalCount) {
        this.MessageTotalCount = MessageTotalCount;
    }

    public String getFilterLabel() {
        return FilterLabel;
    }

    public void setFilterLabel(String FilterLabel) {
        this.FilterLabel = FilterLabel;
    }

    public String getMarkAsReadLabel() {
        return MarkAsReadLabel;
    }

    public void setMarkAsReadLabel(String MarkAsReadLabel) {
        this.MarkAsReadLabel = MarkAsReadLabel;
    }

    public String getPaginationNumber() {
        return PaginationNumber;
    }

    public void setPaginationNumber(String PaginationNumber) {
        this.PaginationNumber = PaginationNumber;
    }

    public String getSentItemsLabel() {
        return SentItemsLabel;
    }

    public void setSentItemsLabel(String SentItemsLabel) {
        this.SentItemsLabel = SentItemsLabel;
    }

    public String getCheckUnreadLabel() {
        return CheckUnreadLabel;
    }

    public void setCheckUnreadLabel(String CheckUnreadLabel) {
        this.CheckUnreadLabel = CheckUnreadLabel;
    }

    public String getStatusMessage() {
        return StatusMessage;
    }

    public void setStatusMessage(String StatusMessage) {
        this.StatusMessage = StatusMessage;
    }

    public String getCheckAllLabel() {
        return CheckAllLabel;
    }

    public void setCheckAllLabel(String CheckAllLabel) {
        this.CheckAllLabel = CheckAllLabel;
    }

    public String getInboxLabel() {
        return InboxLabel;
    }

    public void setInboxLabel(String InboxLabel) {
        this.InboxLabel = InboxLabel;
    }

    public String getMessageListCurrentCount() {
        return MessageListCurrentCount;
    }

    public void setMessageListCurrentCount(String MessageListCurrentCount) {
        this.MessageListCurrentCount = MessageListCurrentCount;
    }

    public String getTrashLabel() {
        return TrashLabel;
    }

    public void setTrashLabel(String TrashLabel) {
        this.TrashLabel = TrashLabel;
    }

    public static CommunicateResponse fromJson(String json) {
        return new Gson().fromJson(json, CommunicateResponse.class);
    }
}
