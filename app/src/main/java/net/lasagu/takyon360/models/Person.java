package net.lasagu.takyon360.models;

import java.io.Serializable;

/**
 * Created by ajana on 08/09/2017.
 */

public class Person implements Serializable {
    private String name;
    private String email;
    private int id;
    private int parentId;

    public Person(String n, String e, int id, int pId) { name = n; email = e; this.id = id; parentId = pId;}

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public int getId() {
        return id;
    }

    public int getParentId() {
        return parentId;
    }

    @Override
    public String toString() { return name; }
}
