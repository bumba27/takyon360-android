package net.lasagu.takyon360.models;

import com.google.gson.Gson;

import java.util.ArrayList;

/**
 * Created by anirban on 8/14/17.
 */

public class CategoryItemsBean {
    /**
     * Id : 3792
     * Title : computer grade 2
     * Content : http://www.school.reportz.co.in/uploadtest/company52/digital_resources/16-11-2016-106630949.pdf
     * ContentType : pdf
     * AttachIcon : 1
     * Attachment : http://www.school.reportz.co.in/uploadtest/company52/digital_resources/16-11-2016-106630949.pdf
     * Date : 2016-11-16 07:00:00
     */

    private int Id;
    private String Title;
    private String Content;
    private String ContentType;
    private int AttachIcon;
    private String Attachment;
    private ArrayList<MultipleAttachments> MultipleAttachment;
    private String Date;

    public ArrayList<MultipleAttachments> getMultipleAttachment() {
        return MultipleAttachment;
    }

    public void setMultipleAttachment(ArrayList<MultipleAttachments> multipleAttachment) {
        MultipleAttachment = multipleAttachment;
    }

    public int getId() {
        return Id;
    }

    public void setId(int Id) {
        this.Id = Id;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String Title) {
        this.Title = Title;
    }

    public String getContent() {
        return Content;
    }

    public void setContent(String Content) {
        this.Content = Content;
    }

    public String getContentType() {
        return ContentType;
    }

    public void setContentType(String ContentType) {
        this.ContentType = ContentType;
    }

    public int getAttachIcon() {
        return AttachIcon;
    }

    public void setAttachIcon(int AttachIcon) {
        this.AttachIcon = AttachIcon;
    }

    public String getAttachment() {
        return Attachment;
    }

    public void setAttachment(String Attachment) {
        this.Attachment = Attachment;
    }

    public String getDate() {
        return Date;
    }

    public void setDate(String Date) {
        this.Date = Date;
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }

}
