package net.lasagu.takyon360.models;

import com.google.gson.Gson;

import java.util.List;

/**
 * Created by anirban on 1/4/18.
 */

public class FeeDetailsResponse {
    /**
     * StatusCode : 1
     * StatusMessage : Success
     * Name : Arav Jayesh
     * NameLabel : Name
     * ClassLabel : Class
     * Division : 3 E
     * HeadLabel : Fee Details
     * FeeLabel : Fee
     * PaidLabel : Paid
     * DateLabel : Month
     * TotalLabel : Total
     * Transactions : [{"Date":"Opening","Fee":"0.00","Amount":"525.00"},{"Date":"Apr2017","Fee":"1213.00","Amount":"0.00"},{"Date":"May2017","Fee":"660.00","Amount":"0.00"},{"Date":"Jun2017","Fee":"675.00","Amount":"0.00"},{"Date":"Sep2017","Fee":"660.00","Amount":"3343.00"},{"Date":"Oct2017","Fee":"780.00","Amount":"0.00"},{"Date":"Nov2017","Fee":"730.00","Amount":"120.00"},{"Date":"Dec2017","Fee":"660.00","Amount":"0.00"},{"Date":"Jan2018","Fee":"660.00","Amount":"0.00"},{"Date":"Feb2018","Fee":"660.00","Amount":"0.00"},{"Date":"Mar2018","Fee":"660.00","Amount":"0.00"}]
     */

    private int StatusCode;
    private String StatusMessage;
    private String Name;
    private String NameLabel;
    private String ClassLabel;
    private String Division;
    private String HeadLabel;
    private String FeeLabel;
    private String PaidLabel;
    private String DateLabel;
    private String TotalLabel;
    private List<FeeDetailsTransactionsBean> Transactions;

    public static FeeDetailsResponse fromJson(String str) {

        return new Gson().fromJson(str, FeeDetailsResponse.class);
    }

    public int getStatusCode() {
        return StatusCode;
    }

    public void setStatusCode(int StatusCode) {
        this.StatusCode = StatusCode;
    }

    public String getStatusMessage() {
        return StatusMessage;
    }

    public void setStatusMessage(String StatusMessage) {
        this.StatusMessage = StatusMessage;
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public String getNameLabel() {
        return NameLabel;
    }

    public void setNameLabel(String NameLabel) {
        this.NameLabel = NameLabel;
    }

    public String getClassLabel() {
        return ClassLabel;
    }

    public void setClassLabel(String ClassLabel) {
        this.ClassLabel = ClassLabel;
    }

    public String getDivision() {
        return Division;
    }

    public void setDivision(String Division) {
        this.Division = Division;
    }

    public String getHeadLabel() {
        return HeadLabel;
    }

    public void setHeadLabel(String HeadLabel) {
        this.HeadLabel = HeadLabel;
    }

    public String getFeeLabel() {
        return FeeLabel;
    }

    public void setFeeLabel(String FeeLabel) {
        this.FeeLabel = FeeLabel;
    }

    public String getPaidLabel() {
        return PaidLabel;
    }

    public void setPaidLabel(String PaidLabel) {
        this.PaidLabel = PaidLabel;
    }

    public String getDateLabel() {
        return DateLabel;
    }

    public void setDateLabel(String DateLabel) {
        this.DateLabel = DateLabel;
    }

    public String getTotalLabel() {
        return TotalLabel;
    }

    public void setTotalLabel(String TotalLabel) {
        this.TotalLabel = TotalLabel;
    }

    public List<FeeDetailsTransactionsBean> getTransactions() {
        return Transactions;
    }

    public void setTransactions(List<FeeDetailsTransactionsBean> Transactions) {
        this.Transactions = Transactions;
    }
}
