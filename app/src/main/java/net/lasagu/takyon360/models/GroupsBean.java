package net.lasagu.takyon360.models;

/**
 * Created by ajana on 28/07/2017.
 */

public class GroupsBean {
    /**
     * name : 1 A
     * id : 1403
     */

    private String name;
    private int id;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
