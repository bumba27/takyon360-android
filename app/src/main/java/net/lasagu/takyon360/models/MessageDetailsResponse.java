package net.lasagu.takyon360.models;

import com.google.gson.Gson;

/**
 * Created by ajana on 12/07/2017.
 */

public class MessageDetailsResponse {
    private int StatusCode;

    private String AttachmentLabel;

    private String MarkAsUneadLabel;

    private String MarkAsReadLabel;

    private String ReplyAllLabel;

    private String ForwardMailLabel;

    private String StatusMessage;

    private String DeleteLabel;

    private MessageList[] MessageList;

    private String ReplyLabel;

    public int getStatusCode() {
        return StatusCode;
    }

    public void setStatusCode(int StatusCode) {
        this.StatusCode = StatusCode;
    }

    public String getAttachmentLabel() {
        return AttachmentLabel;
    }

    public void setAttachmentLabel(String AttachmentLabel) {
        this.AttachmentLabel = AttachmentLabel;
    }

    public String getMarkAsUneadLabel() {
        return MarkAsUneadLabel;
    }

    public void setMarkAsUneadLabel(String MarkAsUneadLabel) {
        this.MarkAsUneadLabel = MarkAsUneadLabel;
    }

    public String getMarkAsReadLabel() {
        return MarkAsReadLabel;
    }

    public void setMarkAsReadLabel(String MarkAsReadLabel) {
        this.MarkAsReadLabel = MarkAsReadLabel;
    }

    public String getReplyAllLabel() {
        return ReplyAllLabel;
    }

    public void setReplyAllLabel(String ReplyAllLabel) {
        this.ReplyAllLabel = ReplyAllLabel;
    }

    public String getForwardMailLabel() {
        return ForwardMailLabel;
    }

    public void setForwardMailLabel(String ForwardMailLabel) {
        this.ForwardMailLabel = ForwardMailLabel;
    }

    public String getStatusMessage() {
        return StatusMessage;
    }

    public void setStatusMessage(String StatusMessage) {
        this.StatusMessage = StatusMessage;
    }

    public String getDeleteLabel() {
        return DeleteLabel;
    }

    public void setDeleteLabel(String DeleteLabel) {
        this.DeleteLabel = DeleteLabel;
    }

    public MessageList[] getMessageList() {
        return MessageList;
    }

    public void setMessageList(MessageList[] MessageList) {
        this.MessageList = MessageList;
    }

    public String getReplyLabel() {
        return ReplyLabel;
    }

    public void setReplyLabel(String ReplyLabel) {
        this.ReplyLabel = ReplyLabel;
    }

    public static MessageDetailsResponse fromJson(String json) {
        return new Gson().fromJson(json, MessageDetailsResponse.class);
    }
}
