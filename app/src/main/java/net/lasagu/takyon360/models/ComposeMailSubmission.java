package net.lasagu.takyon360.models;

import com.google.gson.Gson;

import java.util.List;

/**
 * Created by anirban on 7/29/17.
 */

public class ComposeMailSubmission {
    /**
     * UserId : 98189
     * GroupId : [1566,1403]
     * Recipients : [{"UserId":1345,"Name":"Person 1","RecipientType":0,"GroupId":1405},{"UserId":5666,"Name":"Person 2","RecipientType":1,"GroupId":1403},{"UserId":7878,"Name":"Person 3","RecipientType":1,"GroupId":1404}]
     * Subject : Test Subject
     * Message : Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi id neque quam. Aliquam sollicitudin venenatis ipsum ac feugiat. Vestibulum ullamcorper sodales nisi nec condimentum. Mauris convallis mauris at pellentesque volutpat. Phasellus at ultricies neque, quis malesuada augue. Donec eleifend condimentum nisl eu consectetur. Integer eleifend, nisl venenatis consequat iaculis, lectus arcu malesuada sem, dapibus porta quam lacus eu neque.Lorem ipsum dolor sit amet, consectetur adipiscing elit.
     * AttachIcon : 1
     * Attachments : [{"LinkName":"elevation.jpg","Link":"\\tmp\\php6F40.tmp"},{"LinkName":"Weekly plan settings.pdf","Link":"\\tmp\\php6F41.tmp"}]
     * ParentMsgId : 0
     * IsMobile : 1
     */

    private String UserId;
    private String Subject;
    private String Message;
    private int AttachIcon;
    private int ParentMsgId;
    private int IsMobile;
    private List<Integer> GroupId;
    private List<RecipientsBean> Recipients;
    private List<AttachmentsBean> Attachments;

    public String getUserId() {
        return UserId;
    }

    public void setUserId(String UserId) {
        this.UserId = UserId;
    }

    public String getSubject() {
        return Subject;
    }

    public void setSubject(String Subject) {
        this.Subject = Subject;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public int getAttachIcon() {
        return AttachIcon;
    }

    public void setAttachIcon(int AttachIcon) {
        this.AttachIcon = AttachIcon;
    }

    public int getParentMsgId() {
        return ParentMsgId;
    }

    public void setParentMsgId(int ParentMsgId) {
        this.ParentMsgId = ParentMsgId;
    }

    public int getIsMobile() {
        return IsMobile;
    }

    public void setIsMobile(int IsMobile) {
        this.IsMobile = IsMobile;
    }

    public List<Integer> getGroupId() {
        return GroupId;
    }

    public void setGroupId(List<Integer> GroupId) {
        this.GroupId = GroupId;
    }

    public List<RecipientsBean> getRecipients() {
        return Recipients;
    }

    public void setRecipients(List<RecipientsBean> Recipients) {
        this.Recipients = Recipients;
    }

    public List<AttachmentsBean> getAttachments() {
        return Attachments;
    }

    public void setAttachments(List<AttachmentsBean> Attachments) {
        this.Attachments = Attachments;
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }

}
