package net.lasagu.takyon360.models;

import com.google.gson.Gson;

/**
 * Created by anirban on 1/31/17.
 */

public class LoginSubmission {
    private String username;
    private String password;
    private String Language;
    private String Platform = "android";

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getLanguage() {
        return Language;
    }

    public void setLanguage(String language) {
        Language = language;
    }

    public String getPlatform() {
        return Platform;
    }

    public void setPlatform(String platform) {
        Platform = platform;
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}
