package net.lasagu.takyon360.models;

/**
 * Created by anirban on 12/4/17.
 */

public class MultipleAttachments {
    private String FileName;
    private String Filelink;

    public String getFileName() {
        return FileName;
    }

    public void setFileName(String fileName) {
        FileName = fileName;
    }

    public String getFilelink() {
        return Filelink;
    }

    public void setFilelink(String filelink) {
        Filelink = filelink;
    }
}
