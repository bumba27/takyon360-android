package net.lasagu.takyon360.models;

import com.google.gson.Gson;

/**
 * Created by anirban on 12/19/17.
 */

public class WeeklyPlanDownloadResponse {
    /**
     * StatusCode : 1
     * StatusMessage : Success
     * AttachmentName : 1513698018_1008341604_34.doc
     * AttachmentLink : http://school.reportz.co.in/uploadtest/company34/reports/1513698018_1008341604_34.doc
     * FromDate : 2017-12-15
     * ToDate : 2017-12-19
     * OffSet : 0
     * Limit : 5
     */

    private int StatusCode;
    private String StatusMessage;
    private String AttachmentName;
    private String AttachmentLink;
    private String FromDate;
    private String ToDate;
    private int OffSet;
    private int Limit;

    public static WeeklyPlanDownloadResponse fromJson(String str) {

        return new Gson().fromJson(str, WeeklyPlanDownloadResponse.class);
    }

    public int getStatusCode() {
        return StatusCode;
    }

    public void setStatusCode(int StatusCode) {
        this.StatusCode = StatusCode;
    }

    public String getStatusMessage() {
        return StatusMessage;
    }

    public void setStatusMessage(String StatusMessage) {
        this.StatusMessage = StatusMessage;
    }

    public String getAttachmentName() {
        return AttachmentName;
    }

    public void setAttachmentName(String AttachmentName) {
        this.AttachmentName = AttachmentName;
    }

    public String getAttachmentLink() {
        return AttachmentLink;
    }

    public void setAttachmentLink(String AttachmentLink) {
        this.AttachmentLink = AttachmentLink;
    }

    public String getFromDate() {
        return FromDate;
    }

    public void setFromDate(String FromDate) {
        this.FromDate = FromDate;
    }

    public String getToDate() {
        return ToDate;
    }

    public void setToDate(String ToDate) {
        this.ToDate = ToDate;
    }

    public int getOffSet() {
        return OffSet;
    }

    public void setOffSet(int OffSet) {
        this.OffSet = OffSet;
    }

    public int getLimit() {
        return Limit;
    }

    public void setLimit(int Limit) {
        this.Limit = Limit;
    }
}
