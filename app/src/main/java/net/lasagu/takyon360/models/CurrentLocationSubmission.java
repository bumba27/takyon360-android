package net.lasagu.takyon360.models;

import com.google.gson.Gson;

/**
 * Created by ajana on 11/07/2017.
 */

public class CurrentLocationSubmission {
    private String Latitude;
    private String Longitude;
    private String UserId;
    private String client_ip;

    public String getLatitude() {
        return Latitude;
    }

    public void setLatitude(String Latitude) {
        this.Latitude = Latitude;
    }

    public String getLongitude() {
        return Longitude;
    }

    public void setLongitude(String Longitude) {
        this.Longitude = Longitude;
    }

    public String getUserId() {
        return UserId;
    }

    public void setUserId(String UserId) {
        this.UserId = UserId;
    }

    public String getClient_ip() {
        return client_ip;
    }

    public void setClient_ip(String client_ip) {
        this.client_ip = client_ip;
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}
