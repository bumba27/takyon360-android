package net.lasagu.takyon360.models;

import com.google.gson.Gson;

/**
 * Created by anirban on 7/9/17.
 */

public class CommunicateSubmission {

    private String SearchText;

    private int PaginationNumber;

    private String UserId;

    public String getSearchText() {
        return SearchText;
    }

    public void setSearchText(String SearchText) {
        this.SearchText = SearchText;
    }

    public int getPaginationNumber() {
        return PaginationNumber;
    }

    public void setPaginationNumber(int PaginationNumber) {
        this.PaginationNumber = PaginationNumber;
    }

    public String getUserId() {
        return UserId;
    }

    public void setUserId(String UserId) {
        this.UserId = UserId;
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }

}
