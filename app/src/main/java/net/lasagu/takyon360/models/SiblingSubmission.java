package net.lasagu.takyon360.models;

import com.google.gson.Gson;

/**
 * Created by ajana on 05/07/2017.
 */

public class SiblingSubmission {
    private String UserId;
    private String Platform = "android";

    public String getUserId() {
        return UserId;
    }
    public String getPlatform() {
        return Platform;
    }

    public void setUserId(String userId) {
        UserId = userId;
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}
