package net.lasagu.takyon360.models;

import com.google.gson.Gson;

public class SubjectListRequest {

    /**
     * UserId : 98189
     * Div_Id : 2407
     */

    private int UserId;
    private int Div_Id;

    public int getUserId() {
        return UserId;
    }

    public void setUserId(int UserId) {
        this.UserId = UserId;
    }

    public int getDiv_Id() {
        return Div_Id;
    }

    public void setDiv_Id(int Div_Id) {
        this.Div_Id = Div_Id;
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}
