package net.lasagu.takyon360.models;

import com.google.gson.Gson;

/**
 * Created by ajana on 31/07/2017.
 */

public class MemberNameSubmission {
    /**
     * UserId : 98189
     * grp_id : 1403,1404,1405,1406
     * SearchText :
     */

    private String UserId;
    private String grp_id;
    private String SearchText;

    public String getUserId() {
        return UserId;
    }

    public void setUserId(String UserId) {
        this.UserId = UserId;
    }

    public String getGrp_id() {
        return grp_id;
    }

    public void setGrp_id(String grp_id) {
        this.grp_id = grp_id;
    }

    public String getSearchText() {
        return SearchText;
    }

    public void setSearchText(String SearchText) {
        this.SearchText = SearchText;
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}
