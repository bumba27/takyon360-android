package net.lasagu.takyon360.models;

import com.google.gson.Gson;

public class ResetPasswordSubmission {
    /**
     * UserName : AKAP4449
     * VEmail : sajiya@takyonline.com
     */

    private String UserName;
    private String VEmail;

    public String getUserName() {
        return UserName;
    }

    public void setUserName(String userName) {
        UserName = userName;
    }

    public String getVEmail() {
        return VEmail;
    }

    public void setVEmail(String VEmail) {
        this.VEmail = VEmail;
    }


    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}
