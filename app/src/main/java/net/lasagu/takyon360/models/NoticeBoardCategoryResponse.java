package net.lasagu.takyon360.models;

import com.google.gson.Gson;

import java.util.ArrayList;

/**
 * Created by anirban on 8/19/17.
 */

public class NoticeBoardCategoryResponse {
    /**
     * StatusCode : 1
     * StatusMessage : Success
     * Categories : [{"id":836,"Category":"CBSE Practice papers","ParentId":0},{"id":837,"Category":"CBSE Practice papers 1","ParentId":836},{"id":642,"Category":"weekly plan ","ParentId":0},{"id":501,"Category":"Guidelines for Student(s) Timing","ParentId":0}]
     */

    private int StatusCode;
    private String StatusMessage;
    private ArrayList<CategoriesBean> Categories;

    public int getStatusCode() {
        return StatusCode;
    }

    public void setStatusCode(int StatusCode) {
        this.StatusCode = StatusCode;
    }

    public String getStatusMessage() {
        return StatusMessage;
    }

    public void setStatusMessage(String StatusMessage) {
        this.StatusMessage = StatusMessage;
    }

    public ArrayList<CategoriesBean> getCategories() {
        return Categories;
    }

    public void setCategories(ArrayList<CategoriesBean> Categories) {
        this.Categories = Categories;
    }

    public static NoticeBoardCategoryResponse fromJson(String json) {
        return new Gson().fromJson(json, NoticeBoardCategoryResponse.class);
    }
}
