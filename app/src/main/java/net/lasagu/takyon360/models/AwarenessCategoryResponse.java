package net.lasagu.takyon360.models;

import com.google.gson.Gson;

import java.util.ArrayList;

/**
 * Created by anirban on 8/27/17.
 */

public class AwarenessCategoryResponse {
    /**
     * StatusCode : 1
     * StatusMessage : Success
     * ArticleCategoryLabel : Article Categories
     * ArticleLabel : Articles
     * ArticleCategories : [{"CategoryId":1,"Category":"Article 1","ArticleCount":3,"ParentId":0},{"CategoryId":2,"Category":"Article 2","ArticleCount":2,"ParentId":0},{"CategoryId":3,"Category":"Article 3","ArticleCount":4,"ParentId":0},{"CategoryId":4,"Category":"Article 4","ArticleCount":5,"ParentId":0},{"CategoryId":5,"Category":"Article 5","ArticleCount":2,"ParentId":0},{"CategoryId":6,"Category":"Article 6","ArticleCount":6,"ParentId":0},{"CategoryId":7,"Category":"Article 6-1","ArticleCount":5,"ParentId":6},{"CategoryId":8,"Category":"Article 6-2","ArticleCount":2,"ParentId":6}]
     */

    private int StatusCode;
    private String StatusMessage;
    private String ArticleCategoryLabel;
    private String ArticleLabel;
    private ArrayList<ArticleCategoriesBean> ArticleCategories;

    public int getStatusCode() {
        return StatusCode;
    }

    public void setStatusCode(int StatusCode) {
        this.StatusCode = StatusCode;
    }

    public String getStatusMessage() {
        return StatusMessage;
    }

    public void setStatusMessage(String StatusMessage) {
        this.StatusMessage = StatusMessage;
    }

    public String getArticleCategoryLabel() {
        return ArticleCategoryLabel;
    }

    public void setArticleCategoryLabel(String ArticleCategoryLabel) {
        this.ArticleCategoryLabel = ArticleCategoryLabel;
    }

    public String getArticleLabel() {
        return ArticleLabel;
    }

    public void setArticleLabel(String ArticleLabel) {
        this.ArticleLabel = ArticleLabel;
    }

    public ArrayList<ArticleCategoriesBean> getArticleCategories() {
        return ArticleCategories;
    }

    public void setArticleCategories(ArrayList<ArticleCategoriesBean> ArticleCategories) {
        this.ArticleCategories = ArticleCategories;
    }

    public static AwarenessCategoryResponse fromJson(String json) {
        return new Gson().fromJson(json, AwarenessCategoryResponse.class);
    }
}
