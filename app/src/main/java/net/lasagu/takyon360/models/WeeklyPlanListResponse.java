package net.lasagu.takyon360.models;

import com.google.gson.Gson;

import java.util.ArrayList;

/**
 * Created by anirban on 8/29/17.
 */

public class WeeklyPlanListResponse {
    /**
     * StatusCode : 1
     * StatusMessage : Success
     * HomeWork : [{"Id":1,"Topic":"Arabic","Description":"","AttachIcon":1,"Attachments":[{"LinkName":"elevation.jpg","Link":"http://lasagu.net/school/uploadtest/company52/homework/FkjQ26_CLW_1484810241_elevation.jpg"},{"LinkName":"Weekly plan settings.pdf","Link":"http://lasagu.net/school/uploadtest/company52/homework/R3AjK1_CLW_1485921215_1485598867_28266_52.pdf"}],"Date":"2017-04-19"},{"Id":2,"Topic":"English","Description":"","AttachIcon":0,"Attachments":"","Date":"2017-04-19"},{"Id":3,"Topic":"Arabic","Description":"إعطاء أوراق مراجعة للاختبار النهائي للفصل الدراسي الثاني ","AttachIcon":0,"Attachments":"","Date":"2017-04-20"},{"Id":4,"Topic":"Maths","Description":"","AttachIcon":0,"Attachments":"","Date":"2017-04-20"}]
     * ClassWork : [{"Id":5,"Topic":"Arabic","Description":"إحضارالوان خشبية وشمعية وفلوماستر و اوراق ملونة وملاعق بلاستكية وعيون متحركة وخيط صوف وسيلكون لاصق بدون المسدس","AttachIcon":0,"Attachments":"","Date":"2017-04-19"},{"Id":6,"Topic":"Maths","Description":"","AttachIcon":1,"Attachments":[{"LinkName":"elevation.jpg","Link":"http://lasagu.net/school/uploadtest/company52/homework/FkjQ26_CLW_1484810241_elevation.jpg"},{"LinkName":"Weekly plan settings.pdf","Link":"http://lasagu.net/school/uploadtest/company52/homework/R3AjK1_CLW_1485921215_1485598867_28266_52.pdf"}],"Date":"2017-04-19"},{"Id":7,"Topic":"Arabic","Description":"","AttachIcon":0,"Attachments":"","Date":"2017-04-20"},{"Id":8,"Topic":"English","Description":"","AttachIcon":0,"Attachments":"","Date":"2017-04-20"}]
     * Assessments : [{"Id":10,"Topic":"Arabic","Description":"إحضارالوان خشبية وشمعية وفلوماستر و اوراق ملونة وملاعق بلاستكية وعيون متحركة وخيط صوف وسيلكون لاصق بدون المسدس","AttachIcon":0,"Attachments":"","Date":"2017-04-19"},{"Id":11,"Topic":"Arabic","Description":"","AttachIcon":0,"Attachments":"","Date":"2017-04-20"},{"Id":12,"Topic":"English","Description":"test content","AttachIcon":0,"Attachments":"","Date":"2017-04-20"},{"Id":13,"Topic":"Maths","Description":"","AttachIcon":1,"Attachments":[{"LinkName":"elevation.jpg","Link":"http://lasagu.net/school/uploadtest/company52/homework/FkjQ26_CLW_1484810241_elevation.jpg"},{"LinkName":"Weekly plan settings.pdf","Link":"http://lasagu.net/school/uploadtest/company52/homework/R3AjK1_CLW_1485921215_1485598867_28266_52.pdf"}],"Date":"2017-04-19"}]
     * FromDate : 04-23-2017
     * ToDate : 04-27-2017
     * OffSet : null
     * Limit : null
     */

    private int StatusCode;
    private String StatusMessage;
    private String FromDate;
    private String ToDate;
    private int OffSet;
    private int Limit;
    private ArrayList<HomeWorkBean> HomeWork;
    private ArrayList<ClassWorkBean> ClassWork;
    private ArrayList<AssessmentsBean> Assessments;
    private ArrayList<Quizes> Quizes;

    public static WeeklyPlanListResponse fromJson(String str) {

        return new Gson().fromJson(str, WeeklyPlanListResponse.class);
    }

    public int getStatusCode() {
        return StatusCode;
    }

    public void setStatusCode(int StatusCode) {
        this.StatusCode = StatusCode;
    }

    public String getStatusMessage() {
        return StatusMessage;
    }

    public void setStatusMessage(String StatusMessage) {
        this.StatusMessage = StatusMessage;
    }

    public String getFromDate() {
        return FromDate;
    }

    public void setFromDate(String FromDate) {
        this.FromDate = FromDate;
    }

    public String getToDate() {
        return ToDate;
    }

    public void setToDate(String ToDate) {
        this.ToDate = ToDate;
    }

    public Object getOffSet() {
        return OffSet;
    }

    public void setOffSet(int OffSet) {
        this.OffSet = OffSet;
    }

    public int getLimit() {
        return Limit;
    }

    public void setLimit(int Limit) {
        this.Limit = Limit;
    }

    public ArrayList<HomeWorkBean> getHomeWork() {
        return HomeWork;
    }

    public void setHomeWork(ArrayList<HomeWorkBean> HomeWork) {
        this.HomeWork = HomeWork;
    }

    public ArrayList<ClassWorkBean> getClassWork() {
        return ClassWork;
    }

    public void setClassWork(ArrayList<ClassWorkBean> ClassWork) {
        this.ClassWork = ClassWork;
    }

    public ArrayList<AssessmentsBean> getAssessments() {
        return Assessments;
    }

    public void setAssessments(ArrayList<AssessmentsBean> Assessments) {
        this.Assessments = Assessments;
    }

    public ArrayList<Quizes> getQuizes() {
        return Quizes;
    }

    public void setQuizes(ArrayList<Quizes> quizes) {
        Quizes = quizes;
    }
}
