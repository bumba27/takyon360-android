package net.lasagu.takyon360.models;

/**
 * Created by anirban on 7/29/17.
 */

public class RecipientsBean {
    /**
     * UserId : 1345
     * Name : Person 1
     * RecipientType : 0
     * GroupId : 1405
     */

    private int UserId;
    private String Name;
    private int RecipientType;
    private int GroupId;

    public int getUserId() {
        return UserId;
    }

    public void setUserId(int UserId) {
        this.UserId = UserId;
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public int getRecipientType() {
        return RecipientType;
    }

    public void setRecipientType(int RecipientType) {
        this.RecipientType = RecipientType;
    }

    public int getGroupId() {
        return GroupId;
    }

    public void setGroupId(int GroupId) {
        this.GroupId = GroupId;
    }
}