package net.lasagu.takyon360.models;

import com.google.gson.Gson;

import java.util.ArrayList;

/**
 * Created by anirban on 8/19/17.
 */

public class CalenderEventResponse {
    /**
     * StatusCode : 1
     * StatusMessage : Success
     * CalendarLabel : Calendar
     * MonthLabel : Month
     * WeekLabel : Week
     * DayLabel : Day
     * CharteredEventsLabel : Chartered Events
     * Events : [{"Id":3792,"start_date":"2016-11-16 07:00:00","end_date":"2016-11-16 08:00:00","Text":"Test 2","Details":"Test 2 ( Social Arabic Grade ( 1: 9 )"},{"Id":3792,"start_date":"2016-11-16 07:00:00","end_date":"2016-11-16 08:00:00","Text":"Test 3","Details":"Test 3 ( Social Arabic Grade ( 1: 9 )"}]
     */

    private int StatusCode;
    private String StatusMessage;
    private String CalendarLabel;
    private String ViewEvents;
    private String NameLabel;
    private String StartDateLabel;
    private String EndDateLabel;
    private String NoteLabel;
    private String MonthLabel;
    private String WeekLabel;
    private String DayLabel;
    private String CharteredEventsLabel;
    private ArrayList<EventsBean> Events;

    public String getViewEvents() {
        return ViewEvents;
    }

    public void setViewEvents(String viewEvents) {
        ViewEvents = viewEvents;
    }

    public String getNameLabel() {
        return NameLabel;
    }

    public void setNameLabel(String nameLabel) {
        NameLabel = nameLabel;
    }

    public String getStartDateLabel() {
        return StartDateLabel;
    }

    public void setStartDateLabel(String startDateLabel) {
        StartDateLabel = startDateLabel;
    }

    public String getEndDateLabel() {
        return EndDateLabel;
    }

    public void setEndDateLabel(String endDateLabel) {
        EndDateLabel = endDateLabel;
    }

    public String getNoteLabel() {
        return NoteLabel;
    }

    public void setNoteLabel(String noteLabel) {
        NoteLabel = noteLabel;
    }

    public int getStatusCode() {
        return StatusCode;
    }

    public void setStatusCode(int StatusCode) {
        this.StatusCode = StatusCode;
    }

    public String getStatusMessage() {
        return StatusMessage;
    }

    public void setStatusMessage(String StatusMessage) {
        this.StatusMessage = StatusMessage;
    }

    public String getCalendarLabel() {
        return CalendarLabel;
    }

    public void setCalendarLabel(String CalendarLabel) {
        this.CalendarLabel = CalendarLabel;
    }

    public String getMonthLabel() {
        return MonthLabel;
    }

    public void setMonthLabel(String MonthLabel) {
        this.MonthLabel = MonthLabel;
    }

    public String getWeekLabel() {
        return WeekLabel;
    }

    public void setWeekLabel(String WeekLabel) {
        this.WeekLabel = WeekLabel;
    }

    public String getDayLabel() {
        return DayLabel;
    }

    public void setDayLabel(String DayLabel) {
        this.DayLabel = DayLabel;
    }

    public String getCharteredEventsLabel() {
        return CharteredEventsLabel;
    }

    public void setCharteredEventsLabel(String CharteredEventsLabel) {
        this.CharteredEventsLabel = CharteredEventsLabel;
    }

    public ArrayList<EventsBean> getEvents() {
        return Events;
    }

    public void setEvents(ArrayList<EventsBean> Events) {
        this.Events = Events;
    }

    public static CalenderEventResponse fromJson(String json) {
        return new Gson().fromJson(json, CalenderEventResponse.class);
    }
}
