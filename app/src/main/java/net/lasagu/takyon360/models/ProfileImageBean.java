package net.lasagu.takyon360.models;

/**
 * Created by ajana on 02/02/2018.
 */

public class ProfileImageBean {
    /**
     * name :  iVBORw0KGgoAAAANSUhEUgAAAI8AAACZCAYAAAAW7GkVAAAACXBIWXMAAAsTAAALEwEAmpwYAAAKT2lDQ1BQaG90b3Nob3AgSUNDIHByb2ZpbGUAAHjanVNnVFPpFj333vRCS4iAlEtvUhUIIFJCi4AUkSYqIQkQSoghodkVUcERRUUEG8igiAOOjoCMFVEsDIoK2A
     * mime :
     * postname :
     */

    private String name;
    private String mime;
    private String postname;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMime() {
        return mime;
    }

    public void setMime(String mime) {
        this.mime = mime;
    }

    public String getPostname() {
        return postname;
    }

    public void setPostname(String postname) {
        this.postname = postname;
    }
}
