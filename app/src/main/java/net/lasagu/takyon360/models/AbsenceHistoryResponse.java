package net.lasagu.takyon360.models;

import com.google.gson.Gson;

import java.util.ArrayList;

public class AbsenceHistoryResponse {
    /**
     * StatusCode : 1
     * StatusMessage : Success
     * Name : ABDUR REHMAN KHAN
     * Division : II A
     * HeadLabel : Details
     * DayLabel : Day
     * TypeLabel : Type
     * DateLabel : Date
     * TotalLabel : Total
     * Transactions : [{"Date":"22/06/2017","Day":"Thursday","Type":"UnAuthorised"},{"Date":"20/06/2017","Day":"Tuesday","Type":"UnAuthorised"},{"Date":"20/04/2017","Day":"Thursday","Type":"UnAuthorised"},{"Date":"19/06/2017","Day":"Monday","Type":"UnAuthorised"},{"Date":"18/06/2017","Day":"Sunday","Type":"UnAuthorised"},{"Date":"11/10/2017","Day":"Wednesday","Type":"UnAuthorised"},{"Date":"04/05/2017","Day":"Thursday","Type":"UnAuthorised"},{"Date":"04/04/2017","Day":"Tuesday","Type":"UnAuthorised"},{"Date":"03/04/2017","Day":"Monday","Type":"UnAuthorised"}]
     */

    private int StatusCode;
    private String StatusMessage;
    private String Name;
    private String NameLabel;
    private String ClassLabel;
    private String Division;
    private String HeadLabel;
    private String DayLabel;
    private String TypeLabel;
    private String DateLabel;
    private String TotalLabel;
    private ArrayList<TransactionsBeanX> Transactions;

    public static AbsenceHistoryResponse fromJson(String str) {
        return new Gson().fromJson(str, AbsenceHistoryResponse.class);
    }

    public String getNameLabel() {
        return NameLabel;
    }

    public void setNameLabel(String nameLabel) {
        NameLabel = nameLabel;
    }

    public String getClassLabel() {
        return ClassLabel;
    }

    public void setClassLabel(String classLabel) {
        ClassLabel = classLabel;
    }

    public int getStatusCode() {
        return StatusCode;
    }

    public void setStatusCode(int statusCode) {
        StatusCode = statusCode;
    }

    public String getStatusMessage() {
        return StatusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        StatusMessage = statusMessage;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getDivision() {
        return Division;
    }

    public void setDivision(String division) {
        Division = division;
    }

    public String getHeadLabel() {
        return HeadLabel;
    }

    public void setHeadLabel(String headLabel) {
        HeadLabel = headLabel;
    }

    public String getDayLabel() {
        return DayLabel;
    }

    public void setDayLabel(String dayLabel) {
        DayLabel = dayLabel;
    }

    public String getTypeLabel() {
        return TypeLabel;
    }

    public void setTypeLabel(String typeLabel) {
        TypeLabel = typeLabel;
    }

    public String getDateLabel() {
        return DateLabel;
    }

    public void setDateLabel(String dateLabel) {
        DateLabel = dateLabel;
    }

    public String getTotalLabel() {
        return TotalLabel;
    }

    public void setTotalLabel(String totalLabel) {
        TotalLabel = totalLabel;
    }

    public ArrayList<TransactionsBeanX> getTransactions() {
        return Transactions;
    }

    public void setTransactions(ArrayList<TransactionsBeanX> transactions) {
        Transactions = transactions;
    }
}

