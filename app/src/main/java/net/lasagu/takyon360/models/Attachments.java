package net.lasagu.takyon360.models;

/**
 * Created by ajana on 12/07/2017.
 */

public class Attachments {
    private String LinkName;

    private String Link;

    public String getLinkName() {
        return LinkName;
    }

    public void setLinkName(String LinkName) {
        this.LinkName = LinkName;
    }

    public String getLink() {
        return Link;
    }

    public void setLink(String Link) {
        this.Link = Link;
    }

}
