package net.lasagu.takyon360.models;


import com.google.gson.Gson;

public class GcmTokenRegistrationSubmission {
    /**
     * UserId : 98190
     * RegId : f__AgXJCjfM:APA91bGpZHkiARSxg9SqnLDo9AJ3HxoavFRReTngjJVL2ZvGxPCLu975n360XP1r3AiZQ0mSCf9mmkqw-3tzXThL_w9cLrnQG0Si75jrJNWhFTerRCu_lCd1EngeiQYsUbl_CSB8nDSh
     * PhoneType : 0
     */

    private int UserId;
    private String RegId;
    private String PhoneType;

    public int getUserId() {
        return UserId;
    }

    public void setUserId(int userId) {
        UserId = userId;
    }

    public String getRegId() {
        return RegId;
    }

    public void setRegId(String regId) {
        RegId = regId;
    }

    public String getPhoneType() {
        return PhoneType;
    }

    public void setPhoneType(String phoneType) {
        PhoneType = phoneType;
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}
