package net.lasagu.takyon360.models;

import com.google.gson.Gson;

/**
 * Created by anirban on 1/4/18.
 */

public class FeeDetailsTransactionsBean {
    /**
     * Date : Opening
     * Fee : 0.00
     * Amount : 525.00
     */

    private String Date;
    private String Fee;
    private String Amount;

    public static FeeDetailsTransactionsBean fromJson(String str) {

        return new Gson().fromJson(str, FeeDetailsTransactionsBean.class);
    }

    public String getDate() {
        return Date;
    }

    public void setDate(String Date) {
        this.Date = Date;
    }

    public String getFee() {
        return Fee;
    }

    public void setFee(String Fee) {
        this.Fee = Fee;
    }

    public String getAmount() {
        return Amount;
    }

    public void setAmount(String Amount) {
        this.Amount = Amount;
    }
}
