package net.lasagu.takyon360.models;

/**
 * Created by ajana on 12/07/2017.
 */

public class Members {
    private String Name;

    private String GroupName;

    private String RecipientType;

    private String UserId;

    private String GroupId;

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public String getGroupName() {
        return GroupName;
    }

    public void setGroupName(String GroupName) {
        this.GroupName = GroupName;
    }

    public String getRecipientType() {
        return RecipientType;
    }

    public void setRecipientType(String RecipientType) {
        this.RecipientType = RecipientType;
    }

    public String getUserId() {
        return UserId;
    }

    public void setUserId(String UserId) {
        this.UserId = UserId;
    }

    public String getGroupId() {
        return GroupId;
    }

    public void setGroupId(String GroupId) {
        this.GroupId = GroupId;
    }
}
