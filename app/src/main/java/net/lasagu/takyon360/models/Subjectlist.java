package net.lasagu.takyon360.models;


public class Subjectlist {
    /**
     * subject_id :  Activities
     * subject_name :  Activities
     */

    private String subject_id;
    private String subject_name;

    public String getSubject_id() {
        return subject_id;
    }

    public void setSubject_id(String subject_id) {
        this.subject_id = subject_id;
    }

    public String getSubject_name() {
        return subject_name;
    }

    public void setSubject_name(String subject_name) {
        this.subject_name = subject_name;
    }
}
