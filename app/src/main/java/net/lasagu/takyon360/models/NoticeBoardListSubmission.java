package net.lasagu.takyon360.models;

import com.google.gson.Gson;

/**
 * Created by anirban on 8/14/17.
 */

public class NoticeBoardListSubmission {
    /**
     * CategoryId : 1
     * SearchText :
     * PaginationNumber : 2
     */

    private String UserId;
    private int CategoryId;
    private String SearchText;
    private int PaginationNumber;

    public int getCategoryId() {
        return CategoryId;
    }

    public void setCategoryId(int CategoryId) {
        this.CategoryId = CategoryId;
    }

    public String getSearchText() {
        return SearchText;
    }

    public void setSearchText(String SearchText) {
        this.SearchText = SearchText;
    }

    public int getPaginationNumber() {
        return PaginationNumber;
    }

    public void setPaginationNumber(int PaginationNumber) {
        this.PaginationNumber = PaginationNumber;
    }

    public String getUserId() {
        return UserId;
    }

    public void setUserId(String userId) {
        UserId = userId;
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}
