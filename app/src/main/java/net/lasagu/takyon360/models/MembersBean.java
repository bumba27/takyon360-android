package net.lasagu.takyon360.models;

/**
 * Created by ajana on 31/07/2017.
 */

public class MembersBean {
    /**
     * name : MAGED GALAL ABDELHAKIM (MOHAMED MAGED GALAL ABDELHAKIM EL SHAZLY )
     * id : 85836
     * GroupId : 1405
     * GroupName : 1 C
     */

    private String name;
    private String id;
    private int GroupId;
    private String GroupName;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getGroupId() {
        return GroupId;
    }

    public void setGroupId(int GroupId) {
        this.GroupId = GroupId;
    }

    public String getGroupName() {
        return GroupName;
    }

    public void setGroupName(String GroupName) {
        this.GroupName = GroupName;
    }
}
