package net.lasagu.takyon360.models;

import com.google.gson.Gson;

import java.util.ArrayList;

/**
 * Created by anirban on 8/13/17.
 */

public class DigitalResourceCategoryResponse {
    /**
     * StatusCode : 1
     * StatusMessage : Success
     * DigitalCategoryLabel : Digital Resources Categories
     * DigitalResourcesLabel : Digital Resources
     * DigitalCategories : [{"CategoryId":1,"Category":"Category 1","CategoryCount":3,"ParentId":0},{"CategoryId":2,"Category":"Category 2","CategoryCount":2,"ParentId":0},{"CategoryId":3,"Category":"Category 3","CategoryCount":4,"ParentId":0},{"CategoryId":4,"Category":"Category 4","CategoryCount":5,"ParentId":0},{"CategoryId":5,"Category":"Category 5","CategoryCount":2,"ParentId":0},{"CategoryId":6,"Category":"Category 6","CategoryCount":6,"ParentId":0},{"CategoryId":7,"Category":"Category 6-1","CategoryCount":5,"ParentId":6},{"CategoryId":8,"Category":"Category 6-2","CategoryCount":2,"ParentId":6}]
     */

    private int StatusCode;
    private String StatusMessage;
    private String DigitalCategoryLabel;
    private String DigitalResourcesLabel;
    private ArrayList<DigitalCategoriesBean> DigitalCategories;

    public int getStatusCode() {
        return StatusCode;
    }

    public void setStatusCode(int StatusCode) {
        this.StatusCode = StatusCode;
    }

    public String getStatusMessage() {
        return StatusMessage;
    }

    public void setStatusMessage(String StatusMessage) {
        this.StatusMessage = StatusMessage;
    }

    public String getDigitalCategoryLabel() {
        return DigitalCategoryLabel;
    }

    public void setDigitalCategoryLabel(String DigitalCategoryLabel) {
        this.DigitalCategoryLabel = DigitalCategoryLabel;
    }

    public String getDigitalResourcesLabel() {
        return DigitalResourcesLabel;
    }

    public void setDigitalResourcesLabel(String DigitalResourcesLabel) {
        this.DigitalResourcesLabel = DigitalResourcesLabel;
    }

    public ArrayList<DigitalCategoriesBean> getDigitalCategories() {
        return DigitalCategories;
    }

    public void setDigitalCategories(ArrayList<DigitalCategoriesBean> DigitalCategories) {
        this.DigitalCategories = DigitalCategories;
    }

    public static DigitalResourceCategoryResponse fromJson(String json) {
        return new Gson().fromJson(json, DigitalResourceCategoryResponse.class);
    }
}
