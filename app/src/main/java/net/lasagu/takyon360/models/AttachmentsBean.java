package net.lasagu.takyon360.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by anirban on 7/29/17.
 */

public class AttachmentsBean implements Parcelable {
    /**
     * LinkName : elevation.jpg
     * Link : \tmp\php6F40.tmp
     */


    private String LinkName;
    private String Link;

    public String getLinkName() {
        return LinkName;
    }

    public void setLinkName(String LinkName) {
        this.LinkName = LinkName;
    }

    public String getLink() {
        return Link;
    }

    public void setLink(String Link) {
        this.Link = Link;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.LinkName);
        dest.writeString(this.Link);
    }

    public AttachmentsBean() {
    }

    protected AttachmentsBean(Parcel in) {
        this.LinkName = in.readString();
        this.Link = in.readString();
    }

    public static final Parcelable.Creator<AttachmentsBean> CREATOR = new Parcelable.Creator<AttachmentsBean>() {
        @Override
        public AttachmentsBean createFromParcel(Parcel source) {
            return new AttachmentsBean(source);
        }

        @Override
        public AttachmentsBean[] newArray(int size) {
            return new AttachmentsBean[size];
        }
    };
}