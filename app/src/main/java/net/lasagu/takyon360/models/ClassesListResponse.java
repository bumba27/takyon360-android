package net.lasagu.takyon360.models;

import com.google.gson.Gson;

import java.util.ArrayList;

/**
 * Created by anirban on 8/27/17.
 */

public class ClassesListResponse {
    /**
     * StatusCode : 1
     * StatusMessage : Success
     * WeelyPlanLabel : WEEKLY PLAN
     * SelectWeekLabel : SELECT WEEK
     * FromLabel : FROM
     * ToLabel : To
     * GetWeeklyPlanLabel : Get Weekly Plan
     * LatestViewLabel : Latest View
     * StartingDateLabel : Starting Date
     * EndingDateLabel : Ending Date
     * SelectClassLabel : Select Class
     * HomeWorkLabel : Home Work
     * ClassWorkLabel : Class Work
     * AssessmentLabel : Assessment
     * CreatedOnLabel : CreatedOn
     * Divisions : [{"DivId":"123","Division":"VII A"},{"DivId":"124","Division":"VII B"}]
     */

    private int StatusCode;
    private String StatusMessage;
    private String WeelyPlanLabel;
    private String SelectWeekLabel;
    private String FromLabel;
    private String ToLabel;
    private String GetWeeklyPlanLabel;
    private String LatestViewLabel;
    private String StartingDateLabel;
    private String EndingDateLabel;
    private String SelectClassLabel;
    private String HomeWorkLabel;
    private String ClassWorkLabel;
    private String ClassLabel;
    private String AssessmentLabel;
    private String QuizLabel;
    private String CreatedOnLabel;
    private ArrayList<DivisionsBean> Divisions;


    public String getClassLabel() {
        return ClassLabel;
    }

    public void setClassLabel(String classLabel) {
        ClassLabel = classLabel;
    }

    public int getStatusCode() {
        return StatusCode;
    }

    public void setStatusCode(int StatusCode) {
        this.StatusCode = StatusCode;
    }

    public String getStatusMessage() {
        return StatusMessage;
    }

    public void setStatusMessage(String StatusMessage) {
        this.StatusMessage = StatusMessage;
    }

    public String getWeelyPlanLabel() {
        return WeelyPlanLabel;
    }

    public void setWeelyPlanLabel(String WeelyPlanLabel) {
        this.WeelyPlanLabel = WeelyPlanLabel;
    }

    public String getSelectWeekLabel() {
        return SelectWeekLabel;
    }

    public void setSelectWeekLabel(String SelectWeekLabel) {
        this.SelectWeekLabel = SelectWeekLabel;
    }

    public String getFromLabel() {
        return FromLabel;
    }

    public void setFromLabel(String FromLabel) {
        this.FromLabel = FromLabel;
    }

    public String getToLabel() {
        return ToLabel;
    }

    public void setToLabel(String ToLabel) {
        this.ToLabel = ToLabel;
    }

    public String getGetWeeklyPlanLabel() {
        return GetWeeklyPlanLabel;
    }

    public void setGetWeeklyPlanLabel(String GetWeeklyPlanLabel) {
        this.GetWeeklyPlanLabel = GetWeeklyPlanLabel;
    }

    public String getLatestViewLabel() {
        return LatestViewLabel;
    }

    public void setLatestViewLabel(String LatestViewLabel) {
        this.LatestViewLabel = LatestViewLabel;
    }

    public String getStartingDateLabel() {
        return StartingDateLabel;
    }

    public void setStartingDateLabel(String StartingDateLabel) {
        this.StartingDateLabel = StartingDateLabel;
    }

    public String getEndingDateLabel() {
        return EndingDateLabel;
    }

    public void setEndingDateLabel(String EndingDateLabel) {
        this.EndingDateLabel = EndingDateLabel;
    }

    public String getSelectClassLabel() {
        return SelectClassLabel;
    }

    public void setSelectClassLabel(String SelectClassLabel) {
        this.SelectClassLabel = SelectClassLabel;
    }

    public String getHomeWorkLabel() {
        return HomeWorkLabel;
    }

    public void setHomeWorkLabel(String HomeWorkLabel) {
        this.HomeWorkLabel = HomeWorkLabel;
    }

    public String getClassWorkLabel() {
        return ClassWorkLabel;
    }

    public void setClassWorkLabel(String ClassWorkLabel) {
        this.ClassWorkLabel = ClassWorkLabel;
    }

    public String getAssessmentLabel() {
        return AssessmentLabel;
    }

    public void setAssessmentLabel(String AssessmentLabel) {
        this.AssessmentLabel = AssessmentLabel;
    }

    public String getCreatedOnLabel() {
        return CreatedOnLabel;
    }

    public void setCreatedOnLabel(String CreatedOnLabel) {
        this.CreatedOnLabel = CreatedOnLabel;
    }

    public ArrayList<DivisionsBean> getDivisions() {
        return Divisions;
    }

    public void setDivisions(ArrayList<DivisionsBean> Divisions) {
        this.Divisions = Divisions;
    }

    public String getQuizLabel() {
        return QuizLabel;
    }

    public void setQuizLabel(String quizLabel) {
        QuizLabel = quizLabel;
    }

    public static ClassesListResponse fromJson(String json) {
        return new Gson().fromJson(json, ClassesListResponse.class);
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}
