package net.lasagu.takyon360.models;

import com.google.gson.Gson;

import java.util.List;

/**
 * Created by ajana on 28/07/2017.
 */

public class GroupListResponse {
    /**
     * StatusCode : 1
     * StatusMessage : Success
     * Groups : [{"name":"1 A","id":1403},{"name":"1 B","id":1404},{"name":"1 C","id":1405},{"name":"1 D","id":1406},{"name":"10 BAS","id":1688},{"name":"10 BBS","id":1690},{"name":"10 GAS","id":1692},{"name":"10 GBS","id":2767},{"name":"11 BAC","id":1972},{"name":"11 BAS","id":1693},{"name":"11 BBS","id":1695},{"name":"11 GAS","id":1696},{"name":"12 BAC","id":2720},{"name":"12 BAC","id":1973},{"name":"12 BAS","id":1698},{"name":"12 BBS","id":2769},{"name":"12 GAS","id":1700},{"name":"12 GBS","id":1701},{"name":"2 A","id":1422},{"name":"2 B","id":1423},{"name":"2 C","id":1424},{"name":"2 D","id":1425},{"name":"3 A","id":1427},{"name":"3 B","id":1428},{"name":"3 C","id":1429}]
     */

    private int StatusCode;
    private String StatusMessage;
    private List<GroupsBean> Groups;

    public int getStatusCode() {
        return StatusCode;
    }

    public void setStatusCode(int StatusCode) {
        this.StatusCode = StatusCode;
    }

    public String getStatusMessage() {
        return StatusMessage;
    }

    public void setStatusMessage(String StatusMessage) {
        this.StatusMessage = StatusMessage;
    }

    public List<GroupsBean> getGroups() {
        return Groups;
    }

    public void setGroups(List<GroupsBean> Groups) {
        this.Groups = Groups;
    }

    public static GroupListResponse fromJson(String json) {
        return new Gson().fromJson(json, GroupListResponse.class);
    }
}
