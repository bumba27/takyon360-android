package net.lasagu.takyon360.models;

import com.google.gson.Gson;

/**
 * Created by ajana on 12/07/2017.
 */

public class MessageDetailsSubmission {
    private String UserId;
    private int MsgId;

    public String getUserId() {
        return UserId;
    }

    public void setUserId(String userId) {
        UserId = userId;
    }

    public int getMsgId() {
        return MsgId;
    }

    public void setMsgId(int msgId) {
        MsgId = msgId;
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }

}
