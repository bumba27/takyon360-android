package net.lasagu.takyon360.models;

import com.google.gson.Gson;

import java.util.ArrayList;

/**
 * Created by anirbanjana on 01/02/2017.
 */

public class LoginResponse {
    private int StatusCode;
    private String UserType;
    private String BaseURL;
    private String SchoolName;
    private String ProfileImage;
    private String StudentName;
    private String UserId;
    private ArrayList<Notification> Notification;
    private String LogoPath;
    private String Name;
    private String ProductLogo;
    private String VerifiedUser;
    private String Email;
    private String Class;
    private String Verify;
    private String StatusMessage;
    private Siblings[] Siblings;
    private String ProductName;
    private String ReportCardEnabled;
    private MenuList[] MenuList;
    private String ProfileLabel;
    private String LogoutLabel;

    public static LoginResponse fromJson(String json) {
        return new Gson().fromJson(json, LoginResponse.class);
    }

    public String getLogoutLabel() {
        return LogoutLabel;
    }

    public void setLogoutLabel(String logoutLabel) {
        LogoutLabel = logoutLabel;
    }

    public int getStatusCode() {
        return StatusCode;
    }

    public void setStatusCode(int StatusCode) {
        this.StatusCode = StatusCode;
    }

    public String getUserType() {
        return UserType;
    }

    public void setUserType(String UserType) {
        this.UserType = UserType;
    }

    public String getBaseURL() {
        return BaseURL;
    }

    public void setBaseURL(String BaseURL) {
        this.BaseURL = BaseURL;
    }

    public String getSchoolName() {
        return SchoolName;
    }

    public void setSchoolName(String SchoolName) {
        this.SchoolName = SchoolName;
    }

    public String getProfileImage() {
        return ProfileImage;
    }

    public void setProfileImage(String ProfileImage) {
        this.ProfileImage = ProfileImage;
    }

    public String getStudentName() {
        return StudentName;
    }

    public void setStudentName(String StudentName) {
        this.StudentName = StudentName;
    }

    public String getUserId() {
        return UserId;
    }

    public void setUserId(String UserId) {
        this.UserId = UserId;
    }

    public ArrayList<Notification> getNotification() {
        return Notification;
    }

    public void setNotification(ArrayList<Notification> Notification) {
        this.Notification = Notification;
    }

    public String getLogoPath() {
        return LogoPath;
    }

    public void setLogoPath(String LogoPath) {
        this.LogoPath = LogoPath;
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public String getProductLogo() {
        return ProductLogo;
    }

    public void setProductLogo(String ProductLogo) {
        this.ProductLogo = ProductLogo;
    }

    public String getVerifiedUser() {
        return VerifiedUser;
    }

    public void setVerifiedUser(String VerifiedUser) {
        this.VerifiedUser = VerifiedUser;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String Email) {
        this.Email = Email;
    }

    public String getClassName() {
        return Class;
    }

    public void setClass(String Class) {
        this.Class = Class;
    }

    public String getVerify() {
        return Verify;
    }

    public void setVerify(String Verify) {
        this.Verify = Verify;
    }

    public String getStatusMessage() {
        return StatusMessage;
    }

    public void setStatusMessage(String StatusMessage) {
        this.StatusMessage = StatusMessage;
    }

    public Siblings[] getSiblings() {
        return Siblings;
    }

    public void setSiblings(Siblings[] Siblings) {
        this.Siblings = Siblings;
    }

    public String getProductName() {
        return ProductName;
    }

    public void setProductName(String ProductName) {
        this.ProductName = ProductName;
    }

    public String getReportCardEnabled() {
        return ReportCardEnabled;
    }

    public void setReportCardEnabled(String ReportCardEnabled) {
        this.ReportCardEnabled = ReportCardEnabled;
    }

    public MenuList[] getMenuList() {
        return MenuList;
    }

    public void setMenuList(MenuList[] MenuList) {
        this.MenuList = MenuList;
    }

    public String getProfileLabel() {
        return ProfileLabel;
    }

    public void setProfileLabel(String profileLabel) {
        ProfileLabel = profileLabel;
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}
