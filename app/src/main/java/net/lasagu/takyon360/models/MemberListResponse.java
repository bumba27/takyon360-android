package net.lasagu.takyon360.models;

import com.google.gson.Gson;

import java.util.List;

/**
 * Created by ajana on 31/07/2017.
 */

public class MemberListResponse {
    /**
     * StatusCode : 1
     * StatusMessage : Success
     * Members : [{"name":"MAGED GALAL ABDELHAKIM (MOHAMED MAGED GALAL ABDELHAKIM EL SHAZLY )","id":"85836","GroupId":1405,"GroupName":"1 C"},{"name":"MAHMOUD HILWA (TALA MAHMOUD HELWA )","id":"85900","GroupId":1405,"GroupName":"1 C"},{"name":"MUHAMMAD ALI (HASSAN MUHAMMAD ALI )","id":"85928","GroupId":1405,"GroupName":"1 C"},{"name":"CHAIM HATIT (AMJAD HOTAYIT )","id":"85940","GroupId":1405,"GroupName":"1 C"},{"name":"HUMAID HASSAN HUMAID ALHAJERI (HASSAN HUMAID HASSAN HUMAID ALHAJERI )","id":"85992","GroupId":1405,"GroupName":"1 C"},{"name":"Mohammed Fadel Abu Assi (LEEN MOHAMMED FADEL ABU ASSI )","id":"86040","GroupId":1405,"GroupName":"1 C"},{"name":"MOHAMMAD REZA NADERY (ELNAZ MOHAMMAD REZA NADERY )","id":"86084","GroupId":1405,"GroupName":"1 C"},{"name":"KHAED AHMED (ATIYAH NAILA AUREO IMATONG )","id":"86462","GroupId":1405,"GroupName":"1 C"},{"name":"HASSAN NAZARI FARD (ELNAZ HASSAN NAZARI FARD )","id":"86542","GroupId":1405,"GroupName":"1 C"},{"name":"AHMAD MAHMMOUD ABDALKADER MOUSSA (HAMZA AHMAD MAHMOUD ABDELKADER MOUSSA )","id":"86664","GroupId":1405,"GroupName":"1 C"},{"name":"AVANTY YUNIFAR (SEIF ALDIN YUNIVAR )","id":"86720","GroupId":1405,"GroupName":"1 C"},{"name":"TARQ ABDELRAOF (MOHAMED TAREK ABDELROOF ELGMMAL )","id":"86722","GroupId":1403,"GroupName":"1 A"},{"name":"MOHAMED ELSAID ELSAYED BADAWY (MALEK MOHAMED ELSAID ELSAYED BADAWY )","id":"86780","GroupId":1403,"GroupName":"1 A"},{"name":"MOHAMJED IBRAHIM MOHAMED (YASMEEN MOHAMED IBRAHIM MOHAMED IBRAHIM )","id":"86830","GroupId":1403,"GroupName":"1 A"},{"name":"MOHAMED ALI HAMED (ALYAA MOHAMED ALI HAMED )","id":"86880","GroupId":1403,"GroupName":"1 A"},{"name":"Muthanna Al Khalaf (Elaf Mouthanna Al Khalaf )","id":"98033","GroupId":1403,"GroupName":"1 A"},{"name":"Shahid Mahmood Yaqoob Ali (Mohammad Hamza Ferrer Shahid )","id":"98093","GroupId":1403,"GroupName":"1 A"},{"name":"Zouhair Ismail Maarouf (Saad Zouhair Maarouf )","id":"98189","GroupId":1403,"GroupName":"1 A"},{"name":"Mohamed Abdelalim Galal Atwa (Fares Mohamed Abdelalim Galal Mohamed Atwa )","id":"98191","GroupId":1403,"GroupName":"1 A"},{"name":"HASSNAIN (Rebah Hassanain Hassnain Waqar Shah )","id":"161794","GroupId":1403,"GroupName":"1 A"},{"name":"Adnan Kika (Bunej Kika )","id":"161800","GroupId":1403,"GroupName":"1 A"},{"name":"Farhan Abdul Azeem Azeem (Abdul Arafat Shaikh Farhan )","id":"161810","GroupId":1403,"GroupName":"1 A"},{"name":"Tarek Mohammed (Mohammed Tarek Mohammed Amin Ali Fernisa )","id":"161812","GroupId":1403,"GroupName":"1 A"}]
     */

    private int StatusCode;
    private String StatusMessage;
    private List<MembersBean> Members;

    public int getStatusCode() {
        return StatusCode;
    }

    public void setStatusCode(int StatusCode) {
        this.StatusCode = StatusCode;
    }

    public String getStatusMessage() {
        return StatusMessage;
    }

    public void setStatusMessage(String StatusMessage) {
        this.StatusMessage = StatusMessage;
    }

    public List<MembersBean> getMembers() {
        return Members;
    }

    public void setMembers(List<MembersBean> Members) {
        this.Members = Members;
    }

    public static MemberListResponse fromJson(String json) {
        return new Gson().fromJson(json, MemberListResponse.class);
    }
}
