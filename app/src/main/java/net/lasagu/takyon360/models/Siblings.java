package net.lasagu.takyon360.models;

/**
 * Created by ajana on 05/07/2017.
 */

public class Siblings {
    private String login_username;

    private String Class;

    private String studentID;

    private String login_password;

    private String ProfileImage;

    private String UserId;

    private String studentName;

    public String getLogin_username ()
    {
        return login_username;
    }

    public void setLogin_username (String login_username)
    {
        this.login_username = login_username;
    }

    public String getClassName ()
    {
        return Class;
    }

    public void setClass (String Class)
    {
        this.Class = Class;
    }

    public String getStudentID ()
    {
        return studentID;
    }

    public void setStudentID (String studentID)
    {
        this.studentID = studentID;
    }

    public String getLogin_password ()
    {
        return login_password;
    }

    public void setLogin_password (String login_password)
    {
        this.login_password = login_password;
    }

    public String getProfileImage ()
    {
        return ProfileImage;
    }

    public void setProfileImage (String ProfileImage)
    {
        this.ProfileImage = ProfileImage;
    }

    public String getUserId ()
    {
        return UserId;
    }

    public void setUserId (String UserId)
    {
        this.UserId = UserId;
    }

    public String getStudentName ()
    {
        return studentName;
    }

    public void setStudentName (String studentName)
    {
        this.studentName = studentName;
    }
}
