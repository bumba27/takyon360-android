package net.lasagu.takyon360.models;

public class TransactionsBeanX {
    /**
     * Date : 22/06/2017
     * Day : Thursday
     * Type : UnAuthorised
     */

    private String Date;
    private String Day;
    private String Type;

    public String getDate() {
        return Date;
    }

    public void setDate(String date) {
        Date = date;
    }

    public String getDay() {
        return Day;
    }

    public void setDay(String day) {
        Day = day;
    }

    public String getType() {
        return Type;
    }

    public void setType(String type) {
        Type = type;
    }
}
