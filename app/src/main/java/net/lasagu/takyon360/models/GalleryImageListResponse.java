package net.lasagu.takyon360.models;

import com.google.gson.Gson;

import java.util.ArrayList;

/**
 * Created by anirban on 8/19/17.
 */

public class GalleryImageListResponse {
    /**
     * StatusCode : 1
     * StatusMessage : Success
     * Category : Flag Day
     * GalleryItems : [{"GalleryId":1,"GalleryTitle":"Flag 1","Media":"http://www.lasagu.net/school/uploadtest/company52/gallery/beoyGi_media_1483680113_session info.png","Thumbnail":"http://www.lasagu.net/school/uploadtest/company52/gallery/beoyGi_media_1483680113_session info.png","MediaType":"IMAGE"},{"GalleryId":2,"GalleryTitle":"Flag 2","Media":"http://www.lasagu.net/school/uploadtest/company52/gallery/SHlG20_media_1483680113_ies.png","Thumbnail":"http://www.lasagu.net/school/uploadtest/company52/gallery/SHlG20_media_1483680113_ies.png","MediaType":"IMAGE"},{"GalleryId":3,"GalleryTitle":"Flag 3","Media":"http://www.lasagu.net/school/uploadtest/company52/gallery/BP4aj6_media_1484025733_alerts.jpg","Thumbnail":"http://www.lasagu.net/school/uploadtest/company52/gallery/BP4aj6_media_1484025733_alerts.jpg","MediaType":"IMAGE"},{"GalleryId":4,"GalleryTitle":"Flag 1","Media":"http://www.lasagu.net/school/uploadtest/company52/gallery/beoyGi_media_1483680113_session info.png","Thumbnail":"http://www.lasagu.net/school/uploadtest/company52/gallery/beoyGi_media_1483680113_session info.png","MediaType":"IMAGE"},{"GalleryId":5,"GalleryTitle":"Flag 2","Media":"http://www.lasagu.net/school/uploadtest/company52/gallery/SHlG20_media_1483680113_ies.png","Thumbnail":"http://www.lasagu.net/school/uploadtest/company52/gallery/SHlG20_media_1483680113_ies.png","MediaType":"IMAGE"},{"GalleryId":6,"GalleryTitle":"Flag 3","Media":"http://www.lasagu.net/school/uploadtest/company52/gallery/BP4aj6_media_1484025733_alerts.jpg","Thumbnail":"http://www.lasagu.net/school/uploadtest/company52/gallery/BP4aj6_media_1484025733_alerts.jpg","MediaType":"IMAGE"},{"GalleryId":7,"GalleryTitle":"Flag 1","Media":"http://www.lasagu.net/school/uploadtest/company52/gallery/beoyGi_media_1483680113_session info.png","Thumbnail":"http://www.lasagu.net/school/uploadtest/company52/gallery/beoyGi_media_1483680113_session info.png","MediaType":"IMAGE"},{"GalleryId":8,"GalleryTitle":"Flag 2","Media":"http://www.lasagu.net/school/uploadtest/company52/gallery/SHlG20_media_1483680113_ies.png","Thumbnail":"http://www.lasagu.net/school/uploadtest/company52/gallery/SHlG20_media_1483680113_ies.png","MediaType":"IMAGE"},{"GalleryId":9,"GalleryTitle":"Flag 3","Media":"http://www.lasagu.net/school/uploadtest/company52/gallery/BP4aj6_media_1484025733_alerts.jpg","Thumbnail":"http://www.lasagu.net/school/uploadtest/company52/gallery/BP4aj6_media_1484025733_alerts.jpg","MediaType":"IMAGE"},{"GalleryId":10,"GalleryTitle":"Flag 1","Media":"http://www.lasagu.net/school/uploadtest/company52/gallery/beoyGi_media_1483680113_session info.png","Thumbnail":"http://www.lasagu.net/school/uploadtest/company52/gallery/beoyGi_media_1483680113_session info.png","MediaType":"IMAGE"},{"GalleryId":11,"GalleryTitle":"Flag 2","Media":"http://www.lasagu.net/school/uploadtest/company52/gallery/SHlG20_media_1483680113_ies.png","Thumbnail":"http://www.lasagu.net/school/uploadtest/company52/gallery/SHlG20_media_1483680113_ies.png","MediaType":"IMAGE"},{"GalleryId":12,"GalleryTitle":"Flag 3","Media":"http://www.lasagu.net/school/uploadtest/company52/gallery/BP4aj6_media_1484025733_alerts.jpg","Thumbnail":"http://www.lasagu.net/school/uploadtest/company52/gallery/BP4aj6_media_1484025733_alerts.jpg","MediaType":"IMAGE"}]
     * SubCategories : [{"CategoryId":7,"Category":"Flag Day 1"},{"CategoryId":8,"Category":"Flag Day 2"}]
     * MessageTotalCount : 12
     * MessageListCurrentCount : 10
     * PaginationNumber : 1
     * PaginationTotalNumber : 2
     * PaginationStartFrom : 1
     */

    private int StatusCode;
    private String StatusMessage;
    private String Category;
    private int MessageTotalCount;
    private int MessageListCurrentCount;
    private int PaginationNumber;
    private int PaginationTotalNumber;
    private int PaginationStartFrom;
    private ArrayList<GalleryItemsBean> GalleryItems;
    private ArrayList<SubCategoriesBean> SubCategories;

    public int getStatusCode() {
        return StatusCode;
    }

    public void setStatusCode(int StatusCode) {
        this.StatusCode = StatusCode;
    }

    public String getStatusMessage() {
        return StatusMessage;
    }

    public void setStatusMessage(String StatusMessage) {
        this.StatusMessage = StatusMessage;
    }

    public String getCategory() {
        return Category;
    }

    public void setCategory(String Category) {
        this.Category = Category;
    }

    public int getMessageTotalCount() {
        return MessageTotalCount;
    }

    public void setMessageTotalCount(int MessageTotalCount) {
        this.MessageTotalCount = MessageTotalCount;
    }

    public int getMessageListCurrentCount() {
        return MessageListCurrentCount;
    }

    public void setMessageListCurrentCount(int MessageListCurrentCount) {
        this.MessageListCurrentCount = MessageListCurrentCount;
    }

    public int getPaginationNumber() {
        return PaginationNumber;
    }

    public void setPaginationNumber(int PaginationNumber) {
        this.PaginationNumber = PaginationNumber;
    }

    public int getPaginationTotalNumber() {
        return PaginationTotalNumber;
    }

    public void setPaginationTotalNumber(int PaginationTotalNumber) {
        this.PaginationTotalNumber = PaginationTotalNumber;
    }

    public int getPaginationStartFrom() {
        return PaginationStartFrom;
    }

    public void setPaginationStartFrom(int PaginationStartFrom) {
        this.PaginationStartFrom = PaginationStartFrom;
    }

    public ArrayList<GalleryItemsBean> getGalleryItems() {
        return GalleryItems;
    }

    public void setGalleryItems(ArrayList<GalleryItemsBean> GalleryItems) {
        this.GalleryItems = GalleryItems;
    }

    public ArrayList<SubCategoriesBean> getSubCategories() {
        return SubCategories;
    }

    public void setSubCategories(ArrayList<SubCategoriesBean> SubCategories) {
        this.SubCategories = SubCategories;
    }

    public static GalleryImageListResponse fromJson(String json) {
        return new Gson().fromJson(json, GalleryImageListResponse.class);
    }
}
