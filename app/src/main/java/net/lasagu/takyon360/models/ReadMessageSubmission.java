package net.lasagu.takyon360.models;

import com.google.gson.Gson;

/**
 * Created by anirban on 7/16/17.
 */

public class ReadMessageSubmission {
    private int MarkType;

    private String[] MsgIds;

    private String UserId;

    private int MsgType;

    public int getMarkType() {
        return MarkType;
    }

    public void setMarkType(int MarkType) {
        this.MarkType = MarkType;
    }

    public String[] getMsgIds() {
        return MsgIds;
    }

    public void setMsgIds(String[] MsgIds) {
        this.MsgIds = MsgIds;
    }

    public String getUserId() {
        return UserId;
    }

    public void setUserId(String UserId) {
        this.UserId = UserId;
    }

    public int getMsgType() {
        return MsgType;
    }

        public void setMsgType(int MsgType) {
        this.MsgType = MsgType;
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}
