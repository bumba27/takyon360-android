package net.lasagu.takyon360.models;

import com.google.gson.Gson;

import java.util.ArrayList;

/**
 * Created by anirban on 8/19/17.
 */

public class GalleryCategoryResponse {
    /**
     * StatusCode : 1
     * StatusMessage : Success
     * CategoryLabel : Gallery Catagories
     * ViewMoreLabel : View More
     * Categories : [{"CategoryId":1,"Category":"Flag Day","ParentId":0},{"CategoryId":2,"Category":"National Day","ParentId":0},{"CategoryId":3,"Category":"National Day 45 Grades","ParentId":2},{"CategoryId":4,"Category":"National Day 45 KG","ParentId":2},{"CategoryId":5,"Category":"Annual Day","ParentId":0},{"CategoryId":6,"Category":"Colours Day","ParentId":0},{"CategoryId":7,"Category":"Flowers Day","ParentId":0},{"CategoryId":8,"Category":"National Day 45 Grades -1","ParentId":3},{"CategoryId":9,"Category":"National Day 45 Grades -2","ParentId":3}]
     */

    private int StatusCode;
    private String StatusMessage;
    private String HeadLabel;
    private String CategoryLabel;
    private String ViewMoreLabel;
    private ArrayList<CategoriesBeanGallery> Categories;

    public int getStatusCode() {
        return StatusCode;
    }

    public void setStatusCode(int StatusCode) {
        this.StatusCode = StatusCode;
    }

    public String getHeadLabel() {
        return HeadLabel;
    }

    public void setHeadLabel(String headLabel) {
        HeadLabel = headLabel;
    }

    public String getStatusMessage() {
        return StatusMessage;
    }

    public void setStatusMessage(String StatusMessage) {
        this.StatusMessage = StatusMessage;
    }

    public String getCategoryLabel() {
        return CategoryLabel;
    }

    public void setCategoryLabel(String CategoryLabel) {
        this.CategoryLabel = CategoryLabel;
    }

    public String getViewMoreLabel() {
        return ViewMoreLabel;
    }

    public void setViewMoreLabel(String ViewMoreLabel) {
        this.ViewMoreLabel = ViewMoreLabel;
    }

    public ArrayList<CategoriesBeanGallery> getCategories() {
        return Categories;
    }

    public void setCategories(ArrayList<CategoriesBeanGallery> Categories) {
        this.Categories = Categories;
    }

    public static GalleryCategoryResponse fromJson(String json) {
        return new Gson().fromJson(json, GalleryCategoryResponse.class);
    }
}
