package net.lasagu.takyon360.models;

/**
 * Created by anirban on 8/19/17.
 */

public class CategoriesBean {
    /**
     * CategoryId : 1
     * Category : Flag Day
     * ParentId : 0
     */

    private String id;
    private String Category;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCategory() {
        return Category;
    }

    public void setCategory(String Category) {
        this.Category = Category;
    }
}
