package net.lasagu.takyon360.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.Gson;

/**
 * Created by ajana on 05/07/2017.
 */

public class StudentDetailsResponse implements Parcelable {

    private int StatusCode;

    private String ChangePasswordLabel;

    private String ContactNumber;

    private String EditAccountLabel;

    private String Contact;

    private String SaveLabel;

    private String ProfileImage;

    private String CurrentPasswordLabel;

    private String UserName;

    private String NewPasswordLabel;

    private String NameLabel;

    private String EmailIDLabel;

    private String Name;

    private String ReapeatNewPasswordLabel;

    private String MyColleaguesLabel;

    private String ProfileLabel;

    private String MyLocationLabel;

    private String SetMyLocationLabel;

    private String ContactLabel;

    private String ContactNumberLabel;

    private String Latitude;

    private String StatusMessage;

    private String UserNameLabel;

    private String Longitude;

    private String EmailID;

    private String VerifiedEmailLabel;

    public int getStatusCode() {
        return StatusCode;
    }

    public void setStatusCode(int StatusCode) {
        this.StatusCode = StatusCode;
    }

    public String getChangePasswordLabel() {
        return ChangePasswordLabel;
    }

    public void setChangePasswordLabel(String ChangePasswordLabel) {
        this.ChangePasswordLabel = ChangePasswordLabel;
    }

    public String getContactNumber() {
        return ContactNumber;
    }

    public void setContactNumber(String ContactNumber) {
        this.ContactNumber = ContactNumber;
    }

    public String getEditAccountLabel() {
        return EditAccountLabel;
    }

    public void setEditAccountLabel(String EditAccountLabel) {
        this.EditAccountLabel = EditAccountLabel;
    }

    public String getContact() {
        return Contact;
    }

    public void setContact(String Contact) {
        this.Contact = Contact;
    }

    public String getSaveLabel() {
        return SaveLabel;
    }

    public void setSaveLabel(String SaveLabel) {
        this.SaveLabel = SaveLabel;
    }

    public String getProfileImage() {
        return ProfileImage;
    }

    public void setProfileImage(String ProfileImage) {
        this.ProfileImage = ProfileImage;
    }

    public String getCurrentPasswordLabel() {
        return CurrentPasswordLabel;
    }

    public void setCurrentPasswordLabel(String CurrentPasswordLabel) {
        this.CurrentPasswordLabel = CurrentPasswordLabel;
    }

    public String getUserName() {
        return UserName;
    }

    public void setUserName(String UserName) {
        this.UserName = UserName;
    }

    public String getNewPasswordLabel() {
        return NewPasswordLabel;
    }

    public void setNewPasswordLabel(String NewPasswordLabel) {
        this.NewPasswordLabel = NewPasswordLabel;
    }

    public String getNameLabel() {
        return NameLabel;
    }

    public void setNameLabel(String NameLabel) {
        this.NameLabel = NameLabel;
    }

    public String getEmailIDLabel() {
        return EmailIDLabel;
    }

    public void setEmailIDLabel(String EmailIDLabel) {
        this.EmailIDLabel = EmailIDLabel;
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public String getReapeatNewPasswordLabel() {
        return ReapeatNewPasswordLabel;
    }

    public void setReapeatNewPasswordLabel(String ReapeatNewPasswordLabel) {
        this.ReapeatNewPasswordLabel = ReapeatNewPasswordLabel;
    }

    public String getMyColleaguesLabel() {
        return MyColleaguesLabel;
    }

    public void setMyColleaguesLabel(String MyColleaguesLabel) {
        this.MyColleaguesLabel = MyColleaguesLabel;
    }

    public String getProfileLabel() {
        return ProfileLabel;
    }

    public void setProfileLabel(String ProfileLabel) {
        this.ProfileLabel = ProfileLabel;
    }

    public String getMyLocationLabel() {
        return MyLocationLabel;
    }

    public void setMyLocationLabel(String MyLocationLabel) {
        this.MyLocationLabel = MyLocationLabel;
    }

    public String getSetMyLocationLabel() {
        return SetMyLocationLabel;
    }

    public void setSetMyLocationLabel(String SetMyLocationLabel) {
        this.SetMyLocationLabel = SetMyLocationLabel;
    }

    public String getContactLabel() {
        return ContactLabel;
    }

    public void setContactLabel(String ContactLabel) {
        this.ContactLabel = ContactLabel;
    }

    public String getContactNumberLabel() {
        return ContactNumberLabel;
    }

    public void setContactNumberLabel(String ContactNumberLabel) {
        this.ContactNumberLabel = ContactNumberLabel;
    }

    public String getLatitude() {
        return Latitude;
    }

    public void setLatitude(String Latitude) {
        this.Latitude = Latitude;
    }

    public String getStatusMessage() {
        return StatusMessage;
    }

    public void setStatusMessage(String StatusMessage) {
        this.StatusMessage = StatusMessage;
    }

    public String getUserNameLabel() {
        return UserNameLabel;
    }

    public void setUserNameLabel(String UserNameLabel) {
        this.UserNameLabel = UserNameLabel;
    }

    public String getLongitude() {
        return Longitude;
    }

    public void setLongitude(String Longitude) {
        this.Longitude = Longitude;
    }

    public String getEmailID() {
        return EmailID;
    }

    public void setEmailID(String EmailID) {
        this.EmailID = EmailID;
    }

    public String getVerifiedEmailLabel() {
        return VerifiedEmailLabel;
    }

    public void setVerifiedEmailLabel(String verifiedEmailLabel) {
        VerifiedEmailLabel = verifiedEmailLabel;
    }

    public static StudentDetailsResponse fromJson(String json) {
        return new Gson().fromJson(json, StudentDetailsResponse.class);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.StatusCode);
        dest.writeString(this.ChangePasswordLabel);
        dest.writeString(this.ContactNumber);
        dest.writeString(this.EditAccountLabel);
        dest.writeString(this.Contact);
        dest.writeString(this.SaveLabel);
        dest.writeString(this.ProfileImage);
        dest.writeString(this.CurrentPasswordLabel);
        dest.writeString(this.UserName);
        dest.writeString(this.NewPasswordLabel);
        dest.writeString(this.NameLabel);
        dest.writeString(this.EmailIDLabel);
        dest.writeString(this.Name);
        dest.writeString(this.ReapeatNewPasswordLabel);
        dest.writeString(this.MyColleaguesLabel);
        dest.writeString(this.ProfileLabel);
        dest.writeString(this.MyLocationLabel);
        dest.writeString(this.SetMyLocationLabel);
        dest.writeString(this.ContactLabel);
        dest.writeString(this.ContactNumberLabel);
        dest.writeString(this.Latitude);
        dest.writeString(this.StatusMessage);
        dest.writeString(this.UserNameLabel);
        dest.writeString(this.Longitude);
        dest.writeString(this.EmailID);
        dest.writeString(this.VerifiedEmailLabel);
    }

    public StudentDetailsResponse() {
    }

    protected StudentDetailsResponse(Parcel in) {
        this.StatusCode = in.readInt();
        this.ChangePasswordLabel = in.readString();
        this.ContactNumber = in.readString();
        this.EditAccountLabel = in.readString();
        this.Contact = in.readString();
        this.SaveLabel = in.readString();
        this.ProfileImage = in.readString();
        this.CurrentPasswordLabel = in.readString();
        this.UserName = in.readString();
        this.NewPasswordLabel = in.readString();
        this.NameLabel = in.readString();
        this.EmailIDLabel = in.readString();
        this.Name = in.readString();
        this.ReapeatNewPasswordLabel = in.readString();
        this.MyColleaguesLabel = in.readString();
        this.ProfileLabel = in.readString();
        this.MyLocationLabel = in.readString();
        this.SetMyLocationLabel = in.readString();
        this.ContactLabel = in.readString();
        this.ContactNumberLabel = in.readString();
        this.Latitude = in.readString();
        this.StatusMessage = in.readString();
        this.UserNameLabel = in.readString();
        this.Longitude = in.readString();
        this.EmailID = in.readString();
        this.VerifiedEmailLabel = in.readString();
    }

    public static final Parcelable.Creator<StudentDetailsResponse> CREATOR = new Parcelable.Creator<StudentDetailsResponse>() {
        @Override
        public StudentDetailsResponse createFromParcel(Parcel source) {
            return new StudentDetailsResponse(source);
        }

        @Override
        public StudentDetailsResponse[] newArray(int size) {
            return new StudentDetailsResponse[size];
        }
    };
}
