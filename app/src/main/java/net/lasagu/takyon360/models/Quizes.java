package net.lasagu.takyon360.models;

import com.google.gson.Gson;

import java.io.Serializable;
import java.util.List;

public class Quizes implements Serializable {
    /**
     * Id : 1
     * Topic : Arabic
     * Description :
     * AttachIcon : 1
     * Attachments : [{"LinkName":"elevation.jpg","Link":"http://lasagu.net/school/uploadtest/company52/homework/FkjQ26_CLW_1484810241_elevation.jpg"},{"LinkName":"Weekly plan settings.pdf","Link":"http://lasagu.net/school/uploadtest/company52/homework/R3AjK1_CLW_1485921215_1485598867_28266_52.pdf"}]
     * Date : 2017-04-19
     */

    private int Id;
    private String Topic;
    private String Description;
    private int AttachIcon;
    private String Date;
    private List<AttachmentsBean> Attachments;

    public static Quizes fromJson(String str) {

        return new Gson().fromJson(str, Quizes.class);
    }

    public int getId() {
        return Id;
    }

    public void setId(int Id) {
        this.Id = Id;
    }

    public String getTopic() {
        return Topic;
    }

    public void setTopic(String Topic) {
        this.Topic = this.Topic;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String Description) {
        this.Description = Description;
    }

    public int getAttachIcon() {
        return AttachIcon;
    }

    public void setAttachIcon(int AttachIcon) {
        this.AttachIcon = AttachIcon;
    }

    public String getDate() {
        return Date;
    }

    public void setDate(String Date) {
        this.Date = Date;
    }

    public List<AttachmentsBean> getAttachments() {
        return Attachments;
    }

    public void setAttachments(List<AttachmentsBean> Attachments) {
        this.Attachments = Attachments;
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}
