package net.lasagu.takyon360.models;

import com.google.gson.Gson;

/**
 * Created by ajana on 11/07/2017.
 */

public class EditAccountSubmission {
    private String Name;
    private String ContactNumber;
    private String Contact;
    private ProfileImageBean ProfileImage;
    private String UserId;
    private String EmailID;

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public String getContactNumber() {
        return ContactNumber;
    }

    public void setContactNumber(String ContactNumber) {
        this.ContactNumber = ContactNumber;
    }

    public String getContact() {
        return Contact;
    }

    public void setContact(String Contact) {
        this.Contact = Contact;
    }

    public ProfileImageBean getProfileImage() {
        return ProfileImage;
    }

    public void setProfileImage(ProfileImageBean ProfileImage) {
        this.ProfileImage = ProfileImage;
    }

    public String getUserId() {
        return UserId;
    }

    public void setUserId(String UserId) {
        this.UserId = UserId;
    }

    public String getEmailID() {
        return EmailID;
    }

    public void setEmailID(String EmailID) {
        this.EmailID = EmailID;
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}
