package net.lasagu.takyon360.models;

import com.google.gson.Gson;

/**
 * Created by anirban on 1/3/18.
 */

public class TransactionsBean {
    /**
     * Date : 01/11/2017
     * ReceiptNo : 227663
     * Amount : 120.00
     */

    private String Date;
    private String ReceiptNo;
    private String Amount;

    public static TransactionsBean fromJson(String str) {

        return new Gson().fromJson(str, TransactionsBean.class);
    }

    public String getDate() {
        return Date;
    }

    public void setDate(String Date) {
        this.Date = Date;
    }

    public String getReceiptNo() {
        return ReceiptNo;
    }

    public void setReceiptNo(String ReceiptNo) {
        this.ReceiptNo = ReceiptNo;
    }

    public String getAmount() {
        return Amount;
    }

    public void setAmount(String Amount) {
        this.Amount = Amount;
    }
}
