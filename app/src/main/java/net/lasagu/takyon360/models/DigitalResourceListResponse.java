package net.lasagu.takyon360.models;

import com.google.gson.Gson;

import java.util.ArrayList;

/**
 * Created by anirban on 8/14/17.
 */

public class DigitalResourceListResponse {
    /**
     * StatusCode : 1
     * StatusMessage : Success
     * DigitalResourcesLabel : Digital Resources
     * CategoryItems : [{"Id":3792,"Title":"computer grade 2","Content":"http://www.school.reportz.co.in/uploadtest/company52/digital_resources/16-11-2016-106630949.pdf","ContentType":"pdf","AttachIcon":1,"Attachment":"http://www.school.reportz.co.in/uploadtest/company52/digital_resources/16-11-2016-106630949.pdf","Date":"2016-11-16 07:00:00"},{"Id":4926,"Title":"مراجعة اسلامي","Content":"http://www.school.reportz.co.in/uploadtest/company52/digital_resources/09-03-2017-1243930113.doc","ContentType":"doc","AttachIcon":1,"Attachment":"http://www.school.reportz.co.in/uploadtest/company52/digital_resources/09-03-2017-1243930113.doc","Date":"2017-03-09 07:00:00"},{"Id":4926,"Title":"12 صف","Content":"http://www.school.reportz.co.in/uploadtest/company52/digital_resources/09-03-2017-1186374599.docx","ContentType":"doc","AttachIcon":0,"Attachment":"Test 1","Date":"2017-03-09 07:00:00"},{"Id":3792,"Title":"computer grade 2","Content":"http://www.school.reportz.co.in/uploadtest/company52/digital_resources/16-11-2016-106630949.pdf","ContentType":"pdf","AttachIcon":1,"Attachment":"http://www.school.reportz.co.in/uploadtest/company52/digital_resources/16-11-2016-106630949.pdf","Date":"2016-11-16 00:00:00"},{"Id":4926,"Title":"مراجعة اسلامي","Content":"http://www.school.reportz.co.in/uploadtest/company52/digital_resources/09-03-2017-1243930113.doc","ContentType":"doc","AttachIcon":1,"Attachment":"http://www.school.reportz.co.in/uploadtest/company52/digital_resources/09-03-2017-1243930113.doc","Date":"2017-03-09 07:00:00"},{"Id":4926,"Title":"12 صف","Content":"http://www.school.reportz.co.in/uploadtest/company52/digital_resources/09-03-2017-1186374599.docx","ContentType":"doc","AttachIcon":0,"Attachment":"Test 1","Date":"2016-11-16 07:00:00"},{"Id":3792,"Title":"computer grade 2","Content":"http://www.school.reportz.co.in/uploadtest/company52/digital_resources/16-11-2016-106630949.pdf","ContentType":"pdf","AttachIcon":1,"Attachment":"http://www.school.reportz.co.in/uploadtest/company52/digital_resources/16-11-2016-106630949.pdf","Date":"2016-11-16 07:00:00"},{"Id":4926,"Title":"مراجعة اسلامي","Content":"http://www.school.reportz.co.in/uploadtest/company52/digital_resources/09-03-2017-1243930113.doc","ContentType":"doc","AttachIcon":1,"Attachment":"http://www.school.reportz.co.in/uploadtest/company52/digital_resources/09-03-2017-1243930113.doc","Date":"2016-11-16 07:00:00"},{"Id":4926,"Title":"12 صف","Content":"http://www.school.reportz.co.in/uploadtest/company52/digital_resources/09-03-2017-1186374599.docx","ContentType":"doc","AttachIcon":0,"Attachment":"Test 1","Date":"2016-11-16 07:00:00"},{"Id":3792,"Title":"computer grade 2","Content":"http://www.school.reportz.co.in/uploadtest/company52/digital_resources/16-11-2016-106630949.pdf","ContentType":"pdf","AttachIcon":1,"Attachment":"http://www.school.reportz.co.in/uploadtest/company52/digital_resources/16-11-2016-106630949.pdf","Date":"2016-11-16 07:00:00"},{"Id":4926,"Title":"مراجعة اسلامي","Content":"http://www.school.reportz.co.in/uploadtest/company52/digital_resources/09-03-2017-1243930113.doc","ContentType":"doc","AttachIcon":1,"Attachment":"http://www.school.reportz.co.in/uploadtest/company52/digital_resources/09-03-2017-1243930113.doc","Date":"2016-11-16 07:00:00"},{"Id":4926,"Title":"12 صف","Content":"http://www.school.reportz.co.in/uploadtest/company52/digital_resources/09-03-2017-1186374599.docx","ContentType":"doc","AttachIcon":0,"Attachment":"Test 1","Date":"2016-11-16 07:00:00"}]
     * MessageTotalCount : 12
     * MessageListCurrentCount : 2
     * PaginationNumber : 2
     * PaginationTotalNumber : 2
     * PaginationStartFrom : 11
     */

    private int StatusCode;
    private String StatusMessage;
    private String DigitalResourcesLabel;
    private int MessageTotalCount;
    private int MessageListCurrentCount;
    private int PaginationNumber;
    private int PaginationTotalNumber;
    private int PaginationStartFrom;
    private ArrayList<CategoryItemsBean> CategoryItems;

    public int getStatusCode() {
        return StatusCode;
    }

    public void setStatusCode(int StatusCode) {
        this.StatusCode = StatusCode;
    }

    public String getStatusMessage() {
        return StatusMessage;
    }

    public void setStatusMessage(String StatusMessage) {
        this.StatusMessage = StatusMessage;
    }

    public String getDigitalResourcesLabel() {
        return DigitalResourcesLabel;
    }

    public void setDigitalResourcesLabel(String DigitalResourcesLabel) {
        this.DigitalResourcesLabel = DigitalResourcesLabel;
    }

    public int getMessageTotalCount() {
        return MessageTotalCount;
    }

    public void setMessageTotalCount(int MessageTotalCount) {
        this.MessageTotalCount = MessageTotalCount;
    }

    public int getMessageListCurrentCount() {
        return MessageListCurrentCount;
    }

    public void setMessageListCurrentCount(int MessageListCurrentCount) {
        this.MessageListCurrentCount = MessageListCurrentCount;
    }

    public int getPaginationNumber() {
        return PaginationNumber;
    }

    public void setPaginationNumber(int PaginationNumber) {
        this.PaginationNumber = PaginationNumber;
    }

    public int getPaginationTotalNumber() {
        return PaginationTotalNumber;
    }

    public void setPaginationTotalNumber(int PaginationTotalNumber) {
        this.PaginationTotalNumber = PaginationTotalNumber;
    }

    public int getPaginationStartFrom() {
        return PaginationStartFrom;
    }

    public void setPaginationStartFrom(int PaginationStartFrom) {
        this.PaginationStartFrom = PaginationStartFrom;
    }

    public ArrayList<CategoryItemsBean> getCategoryItems() {
        return CategoryItems;
    }

    public void setCategoryItems(ArrayList<CategoryItemsBean> CategoryItems) {
        this.CategoryItems = CategoryItems;
    }

    public static DigitalResourceListResponse fromJson(String json) {
        return new Gson().fromJson(json, DigitalResourceListResponse.class);
    }

}
