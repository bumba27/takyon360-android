package net.lasagu.takyon360.utils;

import android.content.Context;
import android.net.Uri;


/**
 * Created by anirban on 1/31/17.
 */

public class URLConst {
    //Test Server 1
//    private static final String DEFAULT_URL = "http://lasagu.net/school/T360_API";

    //Test Server 2
//    private static final String DEFAULT_URL = "http://lasagu.net/school/T360Api";


    // Live server
    private static final String DEFAULT_URL = "http://reportz.co.in/Takyon360/T360_API";

    private static Context context;

    public static String getLoginUrl(Context context) {
        URLConst.context = context;
        return Uri.parse(DEFAULT_URL + "/LOGIN").buildUpon()
                .toString();
    }

    public static String getStudentInfo(Context context) {
        URLConst.context = context;
        return Uri.parse(DEFAULT_URL + "/T0013").buildUpon()
                .toString();
    }

    public static String getFeeSummary(Context context, boolean isTrasportPayment) {
        URLConst.context = context;
        if (isTrasportPayment)
            return Uri.parse(DEFAULT_URL + "/T0055").buildUpon()
                    .toString();
        else
            return Uri.parse(DEFAULT_URL + "/T0035").buildUpon()
                    .toString();
    }

    public static String getPaymentHistory(Context context) {
        URLConst.context = context;
        return Uri.parse(DEFAULT_URL + "/T0037").buildUpon()
                .toString();
    }

    public static String getFeeDetails(Context context) {
        URLConst.context = context;
        return Uri.parse(DEFAULT_URL + "/T0036").buildUpon()
                .toString();
    }

    public static String getSiblingLoginUrl(Context context) {
        URLConst.context = context;
        return Uri.parse(DEFAULT_URL + "/SIBLING_LOGIN").buildUpon()
                .toString();
    }

    public static String getCommunicateInboxUrl(Context context) {
        URLConst.context = context;
        return Uri.parse(DEFAULT_URL + "/T0009").buildUpon()
                .toString();
    }

    public static String getCommunicateOutboxUrl(Context context) {
        URLConst.context = context;
        return Uri.parse(DEFAULT_URL + "/T0024").buildUpon()
                .toString();
    }

    public static String getEditAccountUrl(Context context) {
        URLConst.context = context;
        return Uri.parse(DEFAULT_URL + "/T0033").buildUpon()
                .toString();
    }

    public static String getCurrentLocaitonSavingUrl(Context context) {
        URLConst.context = context;
        return Uri.parse(DEFAULT_URL + "/T0015").buildUpon()
                .toString();
    }

    public static String getUpdatePasswordUrl(Context context) {
        URLConst.context = context;
        return Uri.parse(DEFAULT_URL + "/T0032").buildUpon()
                .toString();
    }

    public static String getMessageDetailsUrl(Context context) {
        URLConst.context = context;
        return Uri.parse(DEFAULT_URL + "/GETMSGLOOP").buildUpon()
                .toString();
    }

    public static String getUnreadMessageUrl(Context context) {
        URLConst.context = context;
        return Uri.parse(DEFAULT_URL + "/T0030").buildUpon()
                .toString();
    }

    public static String getGroupListUrl(Context context) {
        URLConst.context = context;
        return Uri.parse(DEFAULT_URL + "/T0026").buildUpon()
                .toString();
    }

    public static String getMemberListUrl(Context context) {
        URLConst.context = context;
        return Uri.parse(DEFAULT_URL + "/T0027").buildUpon()
                .toString();
    }

    public static String geFtpUrl(Context context) {
        URLConst.context = context;
        return Uri.parse(DEFAULT_URL + "/T0051").buildUpon()
                .toString();
    }

    public static String geComposeMailUrl(Context context) {
        URLConst.context = context;
        return Uri.parse(DEFAULT_URL + "/T0029").buildUpon()
                .toString();
    }

    public static String getDigitalResourceCategoryUrl(Context context) {
        URLConst.context = context;
        return Uri.parse(DEFAULT_URL + "/T0021").buildUpon()
                .toString();
    }

    public static String getDigitalResourceListUrl(Context context) {
        URLConst.context = context;
        return Uri.parse(DEFAULT_URL + "/T0022").buildUpon()
                .toString();
    }

    public static String getGalleryCategoryUrl(Context context) {
        URLConst.context = context;
        return Uri.parse(DEFAULT_URL + "/T0018").buildUpon()
                .toString();
    }

    public static String getGalleryResourceListUrl(Context context) {
        URLConst.context = context;
        return Uri.parse(DEFAULT_URL + "/T0003").buildUpon()
                .toString();
    }

    public static String getNoticeBoardCategoryUrl(Context context) {
        URLConst.context = context;
        return Uri.parse(DEFAULT_URL + "/T0011").buildUpon()
                .toString();
    }

    public static String getCalenderEventUrl(Context context) {
        URLConst.context = context;
        return Uri.parse(DEFAULT_URL + "/T0002").buildUpon()
                .toString();
    }

    public static String getAwarenessCategoryUrl(Context context) {
        URLConst.context = context;
        return Uri.parse(DEFAULT_URL + "/T0019").buildUpon()
                .toString();
    }

    public static String getAwarenessListUrl(Context context) {
        URLConst.context = context;
        return Uri.parse(DEFAULT_URL + "/T0020").buildUpon()
                .toString();
    }

    public static String getClassesListUrl(Context context) {
        URLConst.context = context;
        return Uri.parse(DEFAULT_URL + "/T0005").buildUpon()
                .toString();
    }

    public static String getWeeklyPlanListUrl(Context context) {
        URLConst.context = context;
        return Uri.parse(DEFAULT_URL + "/T0006").buildUpon()
                .toString();
    }

    public static String getReadNotificationUrl(Context context) {
        URLConst.context = context;
        return Uri.parse(DEFAULT_URL + "/T0044").buildUpon()
                .toString();
    }

    public static String getSendOptForEmailVerification(Context context) {
        URLConst.context = context;
        return Uri.parse(DEFAULT_URL + "/T0046").buildUpon()
                .toString();
    }

    public static String getVerifyEmailId(Context context) {
        URLConst.context = context;
        return Uri.parse(DEFAULT_URL + "/T0047").buildUpon()
                .toString();
    }

    public static String getDeleteMessageUrl(Context context) {
        URLConst.context = context;
        return Uri.parse(DEFAULT_URL + "/T0031").buildUpon()
                .toString();
    }

    public static String getResetPasswordUrl(Context context) {
        URLConst.context = context;
        return Uri.parse(DEFAULT_URL + "/T0048").buildUpon()
                .toString();
    }

    public static String getVerifyForgetPassword(Context context) {
        URLConst.context = context;
        return Uri.parse(DEFAULT_URL + "/T0049").buildUpon()
                .toString();
    }

    public static String getAbsenceHistory(Context context) {
        URLConst.context = context;
        return Uri.parse(DEFAULT_URL + "/T0038").buildUpon()
                .toString();
    }

    public static String registerDeviceForPushNotification(Context context) {
        URLConst.context = context;
        return Uri.parse(DEFAULT_URL).buildUpon()
                .appendPath("GCMREG")
                .toString();
    }

    public static String getSubjectListUrl(Context context) {
        URLConst.context = context;
        return Uri.parse(DEFAULT_URL + "/T0067").buildUpon()
                .toString();
    }
}
