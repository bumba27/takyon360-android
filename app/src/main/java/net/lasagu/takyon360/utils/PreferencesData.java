package net.lasagu.takyon360.utils;

import android.content.Context;
import android.text.TextUtils;

import com.google.gson.Gson;

import net.lasagu.takyon360.models.LoginResponse;
import net.lasagu.takyon360.models.UserDetails;


/**
 * Created by anirban on 1/31/17.
 */

public class PreferencesData {
    private static final String USER_INFO = "user_info";
    private static final String REMEMBER_ME = "remember_me";
    public static final String LOGIN_RESPONSE = "loginResponse";


    public static void setUser(Context context, UserDetails userDetails) {
        SharedPreferenceStore.storeValue(context, USER_INFO, userDetails.toString());
    }

    public static UserDetails getUser(Context context) {
        String userJson = SharedPreferenceStore.getValue(context, USER_INFO, "");
        UserDetails userDetails = new UserDetails();
        if (!TextUtils.isEmpty(userJson)) {
            userDetails = UserDetails.fromJson(userJson);
        }
        return userDetails;
    }

    public static void resetUser(Context context) {
        SharedPreferenceStore.storeValue(context, USER_INFO, "");
        setDontRemember(context);
    }

    public static boolean isLoggedIn(Context context) {
        if (SharedPreferenceStore.getValue(context, USER_INFO, "").equalsIgnoreCase(""))
            return false;
        else
            return true;
    }

    public static void setRememberMe(Context context) {
        SharedPreferenceStore.storeValue(context, REMEMBER_ME, "Remembered");
    }

    public static boolean isRemembered(Context context) {
        if (SharedPreferenceStore.getValue(context, REMEMBER_ME, "").equalsIgnoreCase(""))
            return false;
        else
            return true;
    }

    public static void setDontRemember(Context context) {
        SharedPreferenceStore.storeValue(context, REMEMBER_ME, "");
    }

    public static String getUserId(Context context) {
        String loginResponseString = SharedPreferenceStore.getValue(context, LOGIN_RESPONSE, "");
        LoginResponse loginResponse = new Gson().fromJson(loginResponseString, LoginResponse.class);
        return loginResponse.getUserId();
    }

    public static LoginResponse getLoginResponse(Context context) {
        String loginResponseString = SharedPreferenceStore.getValue(context, LOGIN_RESPONSE, "");
        LoginResponse LoginResponse = new Gson().fromJson(loginResponseString, LoginResponse.class);
        return LoginResponse;
    }

    public static void setLoginResponse(Context context, LoginResponse loginResponse) {
        SharedPreferenceStore.storeValue(context, LOGIN_RESPONSE, loginResponse.toString());
    }
}
