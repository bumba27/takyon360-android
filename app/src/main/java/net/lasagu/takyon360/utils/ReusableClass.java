package net.lasagu.takyon360.utils;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.os.Build;
import android.support.graphics.drawable.VectorDrawableCompat;
import android.support.v4.content.ContextCompat;
import android.text.Html;
import android.text.Spanned;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.bumptech.glide.load.ImageHeaderParser;

import net.lasagu.takyon360.views.ContactsCompletionView;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import static net.lasagu.takyon360.utils.Constant.LANGUAGE_CODE;

/**
 * Created by anirbanjana on 02/02/2017.
 */

public class ReusableClass {
    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager = ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE));
        return connectivityManager.getActiveNetworkInfo() != null && connectivityManager.getActiveNetworkInfo().isConnected();
    }

    public static String getMd5(final String password) {
        try {

            MessageDigest digest = MessageDigest
                    .getInstance("MD5");
            digest.update(password.getBytes());
            byte messageDigest[] = digest.digest();

            StringBuffer hexString = new StringBuffer();
            for (int i = 0; i < messageDigest.length; i++) {
                String h = Integer.toHexString(0xFF & messageDigest[i]);
                while (h.length() < 2)
                    h = "0" + h;
                hexString.append(h);
            }
            return hexString.toString();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }

    public static long getDateInMillis(String srcDate) {
        SimpleDateFormat desiredFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        long dateInMillis = 0;
        try {
            Date date = desiredFormat.parse(srcDate);
            dateInMillis = date.getTime();
            return dateInMillis;
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return 0;
    }

    @SuppressWarnings("deprecation")
    public static Spanned fromHtml(String source) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            return Html.fromHtml(source, Html.FROM_HTML_MODE_LEGACY);
        } else {
            return Html.fromHtml(source);
        }
    }

    public static InputStream getCompressedInputStream(String myPath, ImageHeaderParser.ImageType imageType) {
        InputStream inputStream = null;
        try {
            Bitmap bitmap = BitmapFactory.decodeFile(myPath);

            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            if (imageType == ImageHeaderParser.ImageType.PNG)
                bitmap.compress(Bitmap.CompressFormat.PNG, 0, bos);
            else
                bitmap.compress(Bitmap.CompressFormat.JPEG, 70, bos);

            inputStream = new ByteArrayInputStream(bos.toByteArray());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return inputStream;
    }

    public static String getFileExtention(String filePath) {
        return filePath.substring(filePath.lastIndexOf(".") + 1, filePath.length());
    }

    public static String getBase64(String content) {
        try {
            byte[] data = content.getBytes(StandardCharsets.UTF_8);
            return Base64.encodeToString(data, Base64.DEFAULT);
        } catch (Throwable t) {
            return "";
        }
    }

    public static int calculateNoOfColumns(Context context) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        float dpWidth = displayMetrics.widthPixels / displayMetrics.density;
        int noOfColumns = (int) (dpWidth / 180);
        return noOfColumns;
    }

    public static void setVectorBackground(Context context, int icon, ImageView imageView, int color) {
        imageView.setImageResource(icon);
        imageView.setColorFilter(ContextCompat.getColor(context, color), android.graphics.PorterDuff.Mode.SRC_IN);
    }

    public static void setVectorEditTextDrawable(Context context, int leftIcon, int rightIcon, EditText editText, int color) {
        editText.setCompoundDrawablesWithIntrinsicBounds((leftIcon != 0) ? getColouredIcon(context, leftIcon, color) : null,
                null,
                (rightIcon != 0) ? getColouredIcon(context, rightIcon, color) : null,
                null);
    }

    public static Drawable getColouredIcon(Context context, int icon, int color) {
        Drawable drawable = VectorDrawableCompat.create(context.getResources(), icon, null);
        drawable.setColorFilter(context.getResources().getColor(color), PorterDuff.Mode.SRC_ATOP);

        return drawable;
    }

    public static void setVectorDrawableButton(Context context, int leftIcon, int rightIcon, Button button, int color) {
        button.setCompoundDrawablesWithIntrinsicBounds((leftIcon != 0) ? getColouredIcon(context, leftIcon, color) : null,
                null,
                (rightIcon != 0) ? getColouredIcon(context, rightIcon, color) : null,
                null);
    }

    public static void setVectorCustomViewBackground(Context context, int leftIcon, int rightIcon, ContactsCompletionView contactsCompletionView, int color) {
        contactsCompletionView.setCompoundDrawables((leftIcon != 0) ? getColouredIcon(context, leftIcon, color) : null,
                null,
                (rightIcon != 0) ? getColouredIcon(context, rightIcon, color) : null,
                null);

    }

    public static void updateResources(Context context) {
        Locale locale = new Locale(SharedPreferenceStore.getValue(context, LANGUAGE_CODE, "en"));
        Locale.setDefault(locale);

        Resources resources = context.getResources();

        Configuration configuration = resources.getConfiguration();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            configuration.setLocale(locale);
        } else {
            //noinspection deprecation
            configuration.locale = locale;
        }

        resources.updateConfiguration(configuration, resources.getDisplayMetrics());
    }
}
