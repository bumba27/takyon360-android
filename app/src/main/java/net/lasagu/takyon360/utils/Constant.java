package net.lasagu.takyon360.utils;

/**
 * Created by ajana on 26/04/2017.
 */

public class Constant {
    public static final String BASE_URL = "baseUrl";
    public static final String PARENT = "Parent";
    public static final String ADMIN = "admin";
    public static final String STUDENT = "Student";
    public static final String NEW_MAIL = "NEW";
    public static final String REPLY_MAIL = "REPLY";
    public static final String FORWARD_MAIL = "FORWARD";
    public static final String REPLY_ALL_MAIL = "REPLY_ALL_MAIL";
    public static final String USERNAME = "TakAdmin";
    public static final String PASSWORD = "T@k@dm!nT#$T";
    public static final String LANGUAGE_CODE = "languageCode";
    public static String IMAGE = "IMAGE";
    public static final String DOC = "DOC";
    public static int INBOX = 2;
    public static int OUTBOX = 1;
}
