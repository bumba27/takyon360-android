package net.lasagu.takyon360.ui.fragments;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.sdsmdg.tastytoast.TastyToast;

import net.lasagu.takyon360.MyApplication;
import net.lasagu.takyon360.R;
import net.lasagu.takyon360.events.CloseForgotPasswordDialogEvent;
import net.lasagu.takyon360.events.ForgotPasswordEvent;
import net.lasagu.takyon360.jobs.ForgotPasswordJob;
import net.lasagu.takyon360.models.ResetPasswordSubmission;
import net.lasagu.takyon360.utils.ReusableClass;

import butterknife.BindColor;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.pedant.SweetAlert.SweetAlertDialog;
import de.greenrobot.event.EventBus;

/**
 * Created by ajana on 06/07/2017.
 */

public class ForgotPasswordDialogFragment extends DialogFragment {

    @BindView(R.id.EditTextUsername)
    EditText EditTextUsername;
    @BindView(R.id.EditTextNewEmail)
    EditText EditTextNewEmail;
    @BindView(R.id.ForgotPasswordButton)
    Button ResetButton;
    private SweetAlertDialog pDialog;
    @BindColor(R.color.colorPrimary)
    int colorPrimary;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getDialog().setTitle("Forgot password");
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        View view = inflater.inflate(R.layout.forgot_password_dialog_fragment, container, false);

        ButterKnife.bind(this, view);
        return view;
    }

    @OnClick(R.id.ForgotPasswordButton)
    public void resetting() {
        if (validated()) {
            ResetPasswordSubmission resetPasswordSubmission = new ResetPasswordSubmission();
            resetPasswordSubmission.setUserName(EditTextUsername.getText().toString());
            resetPasswordSubmission.setVEmail(EditTextNewEmail.getText().toString());

            if (ReusableClass.isNetworkAvailable(getContext())) {
                showingProgress("Sending email ...");
                MyApplication.addJobInBackground(new ForgotPasswordJob(resetPasswordSubmission));
            } else
                TastyToast.makeText(getContext(), "Sorry no internet connection.", Toast.LENGTH_LONG, TastyToast.ERROR).show();

        } else
            TastyToast.makeText(getContext(), "All fields are required.", TastyToast.LENGTH_LONG, TastyToast.ERROR).show();
    }

    private boolean validated() {
        if (TextUtils.isEmpty(EditTextUsername.getText()))
            return false;
        if (TextUtils.isEmpty(EditTextNewEmail.getText()))
            return false;
        return true;
    }

    public void showingProgress(String title) {
        pDialog = new SweetAlertDialog(getContext(), SweetAlertDialog.PROGRESS_TYPE);
        pDialog.getProgressHelper().setBarColor(colorPrimary);
        pDialog.setTitleText(title);
        pDialog.setCancelable(false);
        pDialog.show();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().registerSticky(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    public void onEventMainThread(ForgotPasswordEvent.Success event) {
        if (event.getGenericResponse().getStatusCode() == 1) {
            pDialog.setTitleText("Success!!!");
            pDialog.setContentText("A mail will be send with a OTP for resetting the password.");
            pDialog.changeAlertType(SweetAlertDialog.SUCCESS_TYPE);
            pDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                @Override
                public void onClick(SweetAlertDialog sweetAlertDialog) {
                    pDialog.dismiss();

                    ForgotPasswordConfirmationDialogFragment forgotPasswordConfirmationDialogFragment = new ForgotPasswordConfirmationDialogFragment();
                    forgotPasswordConfirmationDialogFragment.show(getChildFragmentManager(), "forgotPasswordConfirmationDialogFragment");
                }
            });
        } else {
            pDialog.setTitleText("We Are Sorry ...");
            pDialog.setContentText(event.getGenericResponse().getStatusMessage());
            pDialog.changeAlertType(SweetAlertDialog.ERROR_TYPE);
            pDialog.setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                @Override
                public void onClick(SweetAlertDialog sweetAlertDialog) {
                    pDialog.dismiss();
                }
            });
        }
    }

    public void onEventMainThread(ForgotPasswordEvent.Fail event) {
        if (event.getEx() != null) {
            pDialog.setTitleText("We Are Sorry ...");
            pDialog.setContentText(event.getEx());
            pDialog.changeAlertType(SweetAlertDialog.ERROR_TYPE);
        }
    }

    public void onEventMainThread(CloseForgotPasswordDialogEvent event) {
        dismiss();
        EventBus.getDefault().removeStickyEvent(event);
    }
}