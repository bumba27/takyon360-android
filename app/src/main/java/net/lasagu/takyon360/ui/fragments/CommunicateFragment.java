package net.lasagu.takyon360.ui.fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import net.lasagu.takyon360.R;
import net.lasagu.takyon360.ui.ComposeNewMailActivity;
import net.lasagu.takyon360.utils.Constant;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class CommunicateFragment extends Fragment {


    @BindView(R.id.tabs)
    TabLayout tabLayout;
    @BindView(R.id.viewpager)
    ViewPager viewPager;
    @BindView(R.id.fab)
    FloatingActionButton fab;
    private String inboxLabel;
    private String composeLabel;
    private String sentItemsLabel;

    public CommunicateFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_communicate, container, false);
        ButterKnife.bind(this, view);

        setupViewPager(viewPager);
        viewPager.setOffscreenPageLimit(1);
        tabLayout.setupWithViewPager(viewPager);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), ComposeNewMailActivity.class);
                intent.putExtra("TYPE", Constant.NEW_MAIL);
                intent.putExtra("TITLE", composeLabel);
                startActivity(intent);
            }
        });
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getChildFragmentManager());
        adapter.addFragment(new CommunicateInboxFragment(), inboxLabel);
        adapter.addFragment(new CommunicateSentBoxFragment(), sentItemsLabel);
        viewPager.setAdapter(adapter);
    }

    public void setDynamicMails(String composeLabel, String inboxLabel, String sentItemsLabel) {
        this.composeLabel = composeLabel;
        this.inboxLabel = inboxLabel;
        this.sentItemsLabel = sentItemsLabel;

        tabLayout.getTabAt(0).setText(inboxLabel);
        tabLayout.getTabAt(1).setText(sentItemsLabel);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }
}
