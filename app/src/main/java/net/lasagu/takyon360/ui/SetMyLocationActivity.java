package net.lasagu.takyon360.ui;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.sdsmdg.tastytoast.TastyToast;

import net.lasagu.takyon360.BaseActivity;
import net.lasagu.takyon360.MyApplication;
import net.lasagu.takyon360.R;
import net.lasagu.takyon360.events.SavingLocationEvent;
import net.lasagu.takyon360.jobs.SavingLocationJob;
import net.lasagu.takyon360.models.CurrentLocationSubmission;
import net.lasagu.takyon360.utils.PreferencesData;
import net.lasagu.takyon360.utils.ReusableClass;

import java.util.UUID;

import butterknife.BindColor;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnCheckedChanged;
import cn.pedant.SweetAlert.SweetAlertDialog;
import de.greenrobot.event.EventBus;
import io.nlopez.smartlocation.OnLocationUpdatedListener;
import io.nlopez.smartlocation.SmartLocation;

import static net.lasagu.takyon360.R.id.map;
import static net.lasagu.takyon360.utils.ReusableClass.updateResources;

public class SetMyLocationActivity extends BaseActivity implements OnMapReadyCallback {

    public static final int REQUEST_LOCATION = 199;
    private static final int REQUEST_ACCESS_FINE_LOCATION = 2;
    private static String[] ACCESS_FINE_LOCATION = {
            Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION
    };
    @BindView(R.id.toolBar)
    Toolbar toolBar;
    @BindView(R.id.togglebutton)
    ToggleButton togglebutton;
    @BindColor(R.color.colorPrimary)
    int colorPrimary;
    private GoogleMap mMap;
    private GoogleApiClient googleApiClient;
    private Location currentLocation;
    private double lat = 0.0;
    private double lng = 0.0;
    private double currentLat = 0.0;
    private double currentLng = 0.0;
    private SweetAlertDialog pDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set_my_location);
        ButterKnife.bind(this);
        setSupportActionBar(toolBar);
        updateResources(SetMyLocationActivity.this);

        String lat = getIntent().getStringExtra("lat");
        String lng = getIntent().getStringExtra("lng");

        if (!lat.equalsIgnoreCase("") && !lng.equalsIgnoreCase("")) {
            try {
                currentLat = Double.parseDouble(lat);
                currentLng = Double.parseDouble(lng);
                TastyToast.makeText(SetMyLocationActivity.this, "Please tap/drag on the map to set your location.", TastyToast.LENGTH_LONG, TastyToast.INFO).show();
            } catch (Throwable t) {
                TastyToast.makeText(SetMyLocationActivity.this, "Sorry currently no location saved. Please tap on the map to set your location.", TastyToast.LENGTH_LONG, TastyToast.CONFUSING).show();
            }
        } else
            TastyToast.makeText(SetMyLocationActivity.this, "Sorry currently no location saved. Please tap on the map to set your location.", TastyToast.LENGTH_LONG, TastyToast.CONFUSING).show();


        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(map);
        mapFragment.getMapAsync(this);
        verifyLocation();
    }

    public void verifyLocation() {
        int permission = ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);

        if (permission != PackageManager.PERMISSION_GRANTED)
            requestPermissions(ACCESS_FINE_LOCATION, REQUEST_ACCESS_FINE_LOCATION);
        else
            checkingGps();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == REQUEST_ACCESS_FINE_LOCATION) {
            for (int i = 0; i < permissions.length; i++) {
                String permission = permissions[i];
                int grantResult = grantResults[i];

                if (permission.equals(Manifest.permission.ACCESS_FINE_LOCATION)) {
                    if (grantResult == PackageManager.PERMISSION_GRANTED) {
                        checkingGps();
                    } else {
                        TastyToast.makeText(SetMyLocationActivity.this, "Sorry we need the Location permission.", TastyToast.LENGTH_LONG, TastyToast.ERROR).show();
                        finish();
                    }
                }
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.saving_location_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_save) {
            savingLocation();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void savingLocation() {
        if (ReusableClass.isNetworkAvailable(this)) {
            showingProgress("Please wait ...");

            CurrentLocationSubmission currentLocationSubmission = new CurrentLocationSubmission();
            currentLocationSubmission.setUserId(PreferencesData.getUserId(this));
            currentLocationSubmission.setLatitude(String.valueOf(lat));
            currentLocationSubmission.setLongitude(String.valueOf(lng));
            currentLocationSubmission.setClient_ip(String.valueOf(UUID.randomUUID().toString()));

            MyApplication.addJobInBackground(new SavingLocationJob(currentLocationSubmission));
        } else
            TastyToast.makeText(this, "Sorry no internet connection.", Toast.LENGTH_LONG, TastyToast.ERROR).show();

    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.addMarker(new MarkerOptions()
                .draggable(true)
                .position(new LatLng(currentLat, currentLng))
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.person_marker))
                .title("Your saved location")
                .snippet("This is your current saved location"))
                .showInfoWindow();

        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                mMap.clear();
                mMap.addMarker(new MarkerOptions()
                        .draggable(true)
                        .position(latLng)
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.person_marker))
                        .title("You have selected")
                        .snippet("This will be your new location"))
                        .showInfoWindow();
                lat = latLng.latitude;
                lng = latLng.longitude;
            }
        });

        mMap.setOnMarkerDragListener(new GoogleMap.OnMarkerDragListener() {

            @Override
            public void onMarkerDragStart(Marker marker) {
                // TODO Auto-generated method stub
                //Here your code
            }

            @Override
            public void onMarkerDragEnd(Marker marker) {
                lat = marker.getPosition().latitude;
                lng = marker.getPosition().longitude;

                mMap.clear();
                mMap.addMarker(new MarkerOptions()
                        .draggable(true)
                        .position(new LatLng(lat, lng))
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.person_marker))
                        .title("You have selected")
                        .snippet("This will be your new location"))
                        .showInfoWindow();
            }

            @Override
            public void onMarkerDrag(Marker marker) {
                // TODO Auto-generated method stub
            }
        });
    }

    private void getCurrentLocation() {
        SmartLocation.with(SetMyLocationActivity.this).location()
                .oneFix()
                .start(new OnLocationUpdatedListener() {
                    @Override
                    public void onLocationUpdated(Location location) {
                        currentLocation = new Location("");
                        currentLocation.setLatitude(location.getLatitude());
                        currentLocation.setLongitude(location.getLongitude());

                        lat = location.getLatitude();
                        lng = location.getLongitude();
                        LatLng myLatLng = new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude());

                        CameraPosition myPosition = new CameraPosition.Builder().target(myLatLng).zoom(16).bearing(90).tilt(30).build();
                        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(myPosition));
                        mMap.setMyLocationEnabled(true);
                    }
                });
    }

    private void checkingGps() {
        if (googleApiClient == null) {
            googleApiClient = new GoogleApiClient.Builder(SetMyLocationActivity.this).addApi(LocationServices.API).build();
            googleApiClient.connect();

            LocationRequest locationRequest = LocationRequest.create();
            locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            locationRequest.setInterval(30 * 1000);
            locationRequest.setFastestInterval(5 * 1000);
            LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(locationRequest);

            // **************************
            builder.setAlwaysShow(true); // this is the key ingredient
            // **************************

            PendingResult result = LocationServices.SettingsApi.checkLocationSettings(googleApiClient, builder.build());
            result.setResultCallback(
                    new ResultCallback() {
                        @Override
                        public void onResult(@NonNull Result result) {
                            final Status status = result.getStatus();

                            switch (status.getStatusCode()) {
                                case LocationSettingsStatusCodes.SUCCESS:
                                    if (currentLat == 0 && currentLng == 0)
                                        getCurrentLocation();
                                    else
                                        setCenter();
                                    break;

                                case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                                    try {
                                        status.startResolutionForResult(SetMyLocationActivity.this, REQUEST_LOCATION);
                                    } catch (IntentSender.SendIntentException e) {

                                    }
                                    break;
                                case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                                    TastyToast.makeText(SetMyLocationActivity.this, "Sorry we need the GPS permission.", TastyToast.LENGTH_LONG, TastyToast.ERROR).show();
                                    finish();
                                    break;

                            }
                        }
                    });

            googleApiClient = new GoogleApiClient.Builder(SetMyLocationActivity.this).addApi(LocationServices.API).build();
        }
    }

    @OnCheckedChanged(R.id.togglebutton)
    public void onToggleClicked(boolean isChecked) {
        if (isChecked) {
            mMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
        } else {
            mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        }
    }

    private void setCenter() {
        LatLng myLatLng = new LatLng(currentLat, currentLng);

        CameraPosition myPosition = new CameraPosition.Builder().target(myLatLng).zoom(16).bearing(90).tilt(30).build();
        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(myPosition));
        mMap.setMyLocationEnabled(true);
    }

    public void showingProgress(String title) {
        pDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
        pDialog.getProgressHelper().setBarColor(colorPrimary);
        pDialog.setTitleText(title);
        pDialog.setCancelable(false);
        pDialog.show();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    public void onEventMainThread(final SavingLocationEvent.Success event) {
        if (event.getGenericResponse().getStatusCode() == 1) {
            pDialog.setTitleText("Success!!!");
            pDialog.changeAlertType(SweetAlertDialog.SUCCESS_TYPE);

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    pDialog.dismiss();
                    Intent returnIntent = new Intent();
                    setResult(Activity.RESULT_OK, returnIntent);
                    finish();
                }
            }, 800);
        } else {
            pDialog.setTitleText("We Are Sorry ...");
            pDialog.setContentText("Something went wrong while saving. Please try after sometime.");
            pDialog.changeAlertType(SweetAlertDialog.ERROR_TYPE);
            pDialog.setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                @Override
                public void onClick(SweetAlertDialog sweetAlertDialog) {
                    pDialog.dismiss();
                }
            });
        }
    }

    public void onEventMainThread(SavingLocationEvent.Fail event) {
        if (event.getEx() != null) {
            pDialog.setTitleText("Oops Server Error");
            pDialog.setContentText(event.getEx());
            pDialog.changeAlertType(SweetAlertDialog.ERROR_TYPE);
        }
    }

}
