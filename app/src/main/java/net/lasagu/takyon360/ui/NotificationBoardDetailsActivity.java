package net.lasagu.takyon360.ui;

import android.Manifest;
import android.app.DownloadManager;
import android.content.Context;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.text.Html;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.sdsmdg.tastytoast.TastyToast;

import net.lasagu.takyon360.BaseActivity;
import net.lasagu.takyon360.R;
import net.lasagu.takyon360.models.ItemsBean;

import butterknife.BindView;
import butterknife.ButterKnife;

import static net.lasagu.takyon360.utils.ReusableClass.updateResources;


public class NotificationBoardDetailsActivity extends BaseActivity {

    private static final int EXTERNAL_STORAGE_PERMISSION_RESPONSE = 1;
    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.htmlContent)
    WebView htmlContent;
    @BindView(R.id.attachmentIcon)
    TextView attachmentIcon;
    @BindView(R.id.attachmentLinearLayout)
    LinearLayout attachmentLinearLayout;
    @BindView(R.id.attachmentText)
    TextView attachmentText;
    @BindView(R.id.attachmentDownloadIcon)
    ImageView attachmentDownloadIcon;
    private String attachmentUrl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_title_and_details);
        ButterKnife.bind(this);
        updateResources(NotificationBoardDetailsActivity.this);

        ItemsBean newsBean = new Gson().fromJson(getIntent().getStringExtra("CATEGORY_DETAILS"), ItemsBean.class);
        loadingData(newsBean);
    }

    private void loadingData(final ItemsBean newsBean) {
        title.setText(newsBean.getTitle());
        title.setVisibility(View.GONE);
        getSupportActionBar().setTitle(newsBean.getTitle());
        htmlContent.getSettings().setJavaScriptEnabled(true);

        htmlContent.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView view, int progress) {
                // Activities and WebViews measure progress with different scales.
                // The progress meter will automatically disappear when we reach 100%
                NotificationBoardDetailsActivity.this.setProgress(progress * 1000);
            }
        });
        htmlContent.setWebViewClient(new WebViewClient() {
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                Toast.makeText(NotificationBoardDetailsActivity.this, "Oh no! " + description, Toast.LENGTH_SHORT).show();
            }
        });

        htmlContent.setDownloadListener(new android.webkit.DownloadListener() {
            public void onDownloadStart(String url, String userAgent, String contentDisposition, String mimeType, long contentLength) {
                android.content.Intent i = new android.content.Intent(android.content.Intent.ACTION_VIEW);
                i.setData(android.net.Uri.parse(url));
                startActivity(i);
            }
        });

        htmlContent.loadDataWithBaseURL(null, Html.fromHtml(newsBean.getDescription()).toString(), "text/html", "utf-8", null);
    }

    private void havingPermission(String attachmentUrl) {
        this.attachmentUrl = attachmentUrl;
        // Check if we have write permission
        int permission = ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (permission != PackageManager.PERMISSION_GRANTED)
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, EXTERNAL_STORAGE_PERMISSION_RESPONSE);
        else
            downloading(attachmentUrl);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case EXTERNAL_STORAGE_PERMISSION_RESPONSE: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                    downloading(attachmentUrl);
                else
                    TastyToast.makeText(this, "Sorry we need the permission.", Toast.LENGTH_LONG, TastyToast.ERROR).show();

                return;
            }
        }
    }

    private void downloading(final String url) {
        String fileName = url.substring(url.lastIndexOf('/') + 1);
        try {
            if (url != null && !url.isEmpty()) {
                Uri uri = Uri.parse(url.replaceAll(" " , "%20"));

                DownloadManager.Request request = new DownloadManager.Request(uri);
                request.setTitle(fileName);
                request.setDescription("Downloading attachment..");
                request.allowScanningByMediaScanner();
                request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
                request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, fileName);
                DownloadManager dm = (DownloadManager) this.getSystemService(Context.DOWNLOAD_SERVICE);
                dm.enqueue(request);
            }
        } catch (IllegalStateException e) {
            Toast.makeText(this, "Please insert an SD card to download file", Toast.LENGTH_SHORT).show();
        }

    }
}
