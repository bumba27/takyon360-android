package net.lasagu.takyon360.ui;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.webkit.DownloadListener;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import net.lasagu.takyon360.BaseActivity;
import net.lasagu.takyon360.R;
import net.lasagu.takyon360.models.ArticlesItemsBean;

import butterknife.BindView;
import butterknife.ButterKnife;

import static net.lasagu.takyon360.utils.ReusableClass.updateResources;


public class AwarenessDetailsActivity extends BaseActivity {

    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.htmlContent)
    WebView htmlContent;
    @BindView(R.id.attachmentIcon)
    TextView attachmentIcon;
    @BindView(R.id.attachmentText)
    TextView attachmentText;
    @BindView(R.id.attachmentDownloadIcon)
    ImageView attachmentDownloadIcon;
    @BindView(R.id.attachmentLinearLayout)
    LinearLayout attachmentLinearLayout;
    @BindView(R.id.headerLine)
    View headerLine;
    private String attachmentUrl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_title_and_details);
        ButterKnife.bind(this);
        updateResources(AwarenessDetailsActivity.this);

        ArticlesItemsBean articlesItemsBean = new Gson().fromJson(getIntent().getStringExtra("CATEGORY_DETAILS"), ArticlesItemsBean.class);
        loadingData(articlesItemsBean);
    }

    private void loadingData(final ArticlesItemsBean articlesItemsBean) {
        title.setText(articlesItemsBean.getArticleName());
        title.setVisibility(View.GONE);
        headerLine.setVisibility(View.GONE);

        getSupportActionBar().setTitle(articlesItemsBean.getArticleName());

        htmlContent.getSettings().setJavaScriptEnabled(true);

        htmlContent.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView view, int progress) {
                // Activities and WebViews measure progress with different scales.
                // The progress meter will automatically disappear when we reach 100%
                AwarenessDetailsActivity.this.setProgress(progress * 1000);
            }
        });
        htmlContent.setWebViewClient(new WebViewClient() {
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                Toast.makeText(AwarenessDetailsActivity.this, "Oh no! " + description, Toast.LENGTH_SHORT).show();
            }
        });

        htmlContent.setDownloadListener(new DownloadListener() {
            public void onDownloadStart(String url, String userAgent, String contentDisposition, String mimeType, long contentLength) {
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
            }
        });

        htmlContent.loadDataWithBaseURL(null, Html.fromHtml(articlesItemsBean.getArticleDescription()).toString(), "text/html", "utf-8", null);
    }
}
