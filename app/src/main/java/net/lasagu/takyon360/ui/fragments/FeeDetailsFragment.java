package net.lasagu.takyon360.ui.fragments;


import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.sdsmdg.tastytoast.TastyToast;

import net.lasagu.takyon360.MyApplication;
import net.lasagu.takyon360.R;
import net.lasagu.takyon360.events.FeeDetailsEvent;
import net.lasagu.takyon360.events.PaymentHistoryEvent;
import net.lasagu.takyon360.jobs.GetFeeDetailsJob;
import net.lasagu.takyon360.models.FeeDetailsResponse;
import net.lasagu.takyon360.models.FeeDetailsTransactionsBean;
import net.lasagu.takyon360.utils.PreferencesData;
import net.lasagu.takyon360.utils.ReusableClass;

import java.util.List;

import butterknife.BindColor;
import butterknife.BindView;
import butterknife.ButterKnife;
import cn.pedant.SweetAlert.SweetAlertDialog;
import de.greenrobot.event.EventBus;

/**
 * A simple {@link Fragment} subclass.
 */
public class FeeDetailsFragment extends Fragment {

    @BindView(R.id.editTextName)
    EditText editTextName;
    @BindView(R.id.editTextClassDevision)
    EditText editTextClassDevision;
    @BindView(R.id.LinearLayoutPaymentHistoryLayout)
    LinearLayout LinearLayoutPaymentHistoryLayout;
    @BindColor(R.color.colorPrimary)
    int colorPrimary;
    @BindView(R.id.studentDetailsTitle)
    TextView studentDetailsTitle;
    @BindView(R.id.fee_details_title)
    TextView feeDetailsTitle;
    @BindView(R.id.TextInputLayoutName)
    TextInputLayout TextInputLayoutName;
    @BindView(R.id.TextInputLayoutNameClass)
    TextInputLayout TextInputLayoutNameClass;
    private SweetAlertDialog pDialog;

    public FeeDetailsFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fragment_fee_details, container, false);
        ButterKnife.bind(this, root);

        populateUserData();
        return root;
    }

    private void populateUserData() {
        if (ReusableClass.isNetworkAvailable(getContext())) {
            showingProgress("Loading ...");
            MyApplication.addJobInBackground(new GetFeeDetailsJob(PreferencesData.getUserId(getContext())));
        } else
            TastyToast.makeText(getContext(), "Sorry no internet connection.", Toast.LENGTH_LONG, TastyToast.ERROR).show();
    }

    public void showingProgress(String title) {
        pDialog = new SweetAlertDialog(getContext(), SweetAlertDialog.PROGRESS_TYPE);
        pDialog.getProgressHelper().setBarColor(colorPrimary);
        pDialog.setTitleText(title);
        pDialog.setCancelable(false);
        pDialog.show();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    public void onEventMainThread(PaymentHistoryEvent.Fail event) {
        if (event.getEx() != null) {
            pDialog.setTitleText("We Are Sorry ...");
            pDialog.setContentText(event.getEx());
            pDialog.changeAlertType(SweetAlertDialog.ERROR_TYPE);
        }
    }

    public void onEventMainThread(FeeDetailsEvent.Success event) {
        if (event.getFeeSummaryResponse().getStatusCode() == 1) {
            pDialog.setTitleText("Success!!!");
            pDialog.changeAlertType(SweetAlertDialog.SUCCESS_TYPE);

            FeeDetailsResponse feeDetailsResponse = event.getFeeSummaryResponse();

            TextInputLayoutName.setHint(feeDetailsResponse.getNameLabel());
            TextInputLayoutNameClass.setHint(feeDetailsResponse.getClassLabel());
            feeDetailsTitle.setText(feeDetailsResponse.getHeadLabel());

            editTextName.setText(feeDetailsResponse.getName());
            editTextClassDevision.setText(feeDetailsResponse.getDivision());
            List<FeeDetailsTransactionsBean> transactions = feeDetailsResponse.getTransactions();

            float totalAmount = 0.0f;
            float totalFeeAmount = 0.0f;
            for (int i = 0; i < transactions.size(); i++) {
                final View child = getActivity().getLayoutInflater().inflate(R.layout.payment_history_row, null);

                ((TextView) child.findViewById(R.id.editTextReceipt)).setText(feeDetailsResponse.getFeeLabel() + " : " + transactions.get(i).getFee());
                ((TextView) child.findViewById(R.id.editTexDateTime)).setText(feeDetailsResponse.getDateLabel() + " : " + transactions.get(i).getDate());
                ((TextView) child.findViewById(R.id.editTexamount)).setText(feeDetailsResponse.getPaidLabel() + " : " + transactions.get(i).getAmount());

                totalAmount = totalAmount + Float.parseFloat(transactions.get(i).getAmount());
                totalFeeAmount = totalFeeAmount + Float.parseFloat(transactions.get(i).getFee());
                LinearLayoutPaymentHistoryLayout.addView(child);
            }

            Typeface boldTypeface = Typeface.defaultFromStyle(Typeface.BOLD);

            final View child = getActivity().getLayoutInflater().inflate(R.layout.payment_history_row, null);
            ((TextView) child.findViewById(R.id.editTextReceipt)).setText(feeDetailsResponse.getFeeLabel() + " : " + totalFeeAmount);
            ((TextView) child.findViewById(R.id.editTextReceipt)).setTypeface(boldTypeface);
            ((TextView) child.findViewById(R.id.editTexDateTime)).setVisibility(View.GONE);
            ((TextView) child.findViewById(R.id.editTexamount)).setTypeface(boldTypeface);
            ((TextView) child.findViewById(R.id.editTexamount)).setText(feeDetailsResponse.getTotalLabel() + " : " + String.valueOf(totalAmount));
            LinearLayoutPaymentHistoryLayout.addView(child);

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    pDialog.dismiss();
                }
            }, 800);
        } else {
            pDialog.setTitleText("We Are Sorry ...");
            pDialog.setContentText("Not be able to get your details, please try again.");
            pDialog.changeAlertType(SweetAlertDialog.ERROR_TYPE);
            pDialog.setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                @Override
                public void onClick(SweetAlertDialog sweetAlertDialog) {
                    pDialog.dismiss();
                }
            });
        }
    }
}
