package net.lasagu.takyon360.ui;

import android.Manifest;
import android.app.DownloadManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.Toolbar;
import android.text.format.DateUtils;
import android.text.method.LinkMovementMethod;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.sdsmdg.tastytoast.TastyToast;

import net.lasagu.takyon360.BaseActivity;
import net.lasagu.takyon360.MyApplication;
import net.lasagu.takyon360.R;
import net.lasagu.takyon360.events.DeletingEvent;
import net.lasagu.takyon360.events.GetMessageDetailsEvent;
import net.lasagu.takyon360.events.UnreadMessageEvent;
import net.lasagu.takyon360.jobs.DeletingJob;
import net.lasagu.takyon360.jobs.MessageDetailsJob;
import net.lasagu.takyon360.jobs.UnreadingJob;
import net.lasagu.takyon360.models.Attachments;
import net.lasagu.takyon360.models.DeleteMessageSubmission;
import net.lasagu.takyon360.models.Members;
import net.lasagu.takyon360.models.MessageDetailsResponse;
import net.lasagu.takyon360.models.MessageDetailsSubmission;
import net.lasagu.takyon360.models.MessageList;
import net.lasagu.takyon360.models.ReadMessageSubmission;
import net.lasagu.takyon360.utils.Constant;
import net.lasagu.takyon360.utils.PreferencesData;
import net.lasagu.takyon360.utils.ReusableClass;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.greenrobot.event.EventBus;
import de.hdodenhof.circleimageview.CircleImageView;

import static net.lasagu.takyon360.utils.ReusableClass.getDateInMillis;
import static net.lasagu.takyon360.utils.ReusableClass.updateResources;

public class MessageDetailsActivity extends BaseActivity {

    private static final int EXTERNAL_STORAGE_PERMISSION_RESPONSE = 1;
    @BindView(R.id.noData)
    LinearLayout noData;
    @BindView(R.id.progress)
    ProgressBar progress;
    @BindView(R.id.TextViewMessageSubject)
    TextView TextViewMessageSubject;
    @BindView(R.id.frameLayoutLoading)
    FrameLayout frameLayoutLoading;
    @BindView(R.id.linearLayoutMessageList)
    LinearLayout linearLayoutMessageList;
    @BindView(R.id.topProgressBar)
    ProgressBar topProgressBar;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    private int messageId = 0;
    private int messageBox = 0;
    private String attachmentUrl;
    private String ImageViewReplyattachmentUrl;
    private String replyAllLabel;
    private String forwardMailLabel;
    private String deleteLabel;
    private String markAsReadLabel;
    private String markAsUneadLabel;
    private String replyLabel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message_details);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        updateResources(MessageDetailsActivity.this);

        messageId = Integer.parseInt(getIntent().getStringExtra("MESSAGE_ID"));
        messageBox = getIntent().getIntExtra("MESSAGE_BOX", 0);

        frameLayoutLoading.setVisibility(View.VISIBLE);
        loadingData();
    }

    private void loadingData() {
        if (ReusableClass.isNetworkAvailable(this)) {
            progress.setVisibility(View.VISIBLE);

            MessageDetailsSubmission messageDetailsSubmission = new MessageDetailsSubmission();
            messageDetailsSubmission.setUserId(PreferencesData.getUserId(this));
            messageDetailsSubmission.setMsgId(messageId);

            MyApplication.addJobInBackground(new MessageDetailsJob(messageDetailsSubmission));
        } else
            TastyToast.makeText(this, "Sorry no internet connection.", Toast.LENGTH_LONG, TastyToast.ERROR).show();
    }

    public void setForceShowIcon(PopupMenu popupMenu) {
        try {
            Field[] fields = popupMenu.getClass().getDeclaredFields();
            for (Field field : fields) {
                if ("mPopup".equals(field.getName())) {
                    field.setAccessible(true);
                    Object menuPopupHelper = field.get(popupMenu);
                    Class<?> classPopupHelper = Class.forName(menuPopupHelper
                            .getClass().getName());
                    Method setForceIcons = classPopupHelper.getMethod(
                            "setForceShowIcon", boolean.class);
                    setForceIcons.invoke(menuPopupHelper, true);
                    break;
                }
            }
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    public void onEventMainThread(GetMessageDetailsEvent.Success event) {
        if (event.getMessageDetailsResponse().getStatusCode() == 1) {
            frameLayoutLoading.setVisibility(View.INVISIBLE);
            final MessageDetailsResponse messageDetailsResponse = event.getMessageDetailsResponse();
            final MessageList[] messageLists = messageDetailsResponse.getMessageList();

            replyAllLabel = messageDetailsResponse.getReplyAllLabel();
            replyLabel = messageDetailsResponse.getReplyLabel();
            forwardMailLabel = messageDetailsResponse.getForwardMailLabel();
            deleteLabel = messageDetailsResponse.getDeleteLabel();
            markAsReadLabel = messageDetailsResponse.getMarkAsReadLabel();
            markAsUneadLabel = messageDetailsResponse.getMarkAsUneadLabel();

            for (int i = 0; i < messageLists.length; i++) {
                View singleMessageLayout = getLayoutInflater().inflate(R.layout.single_message_layout, null);
                CircleImageView profileImage = (CircleImageView) singleMessageLayout.findViewById(R.id.schoolImage);
                TextView TextViewSenderName = (TextView) singleMessageLayout.findViewById(R.id.TextViewSenderName);
                TextView TextViewSendTo = (TextView) singleMessageLayout.findViewById(R.id.TextViewSendTo);
                TextView TextViewDateTime = (TextView) singleMessageLayout.findViewById(R.id.TextViewDateTime);
                TextView TextViewMessageBody = (TextView) singleMessageLayout.findViewById(R.id.TextViewMessageBody);
                ImageView ImageViewMore = (ImageView) singleMessageLayout.findViewById(R.id.ImageViewMore);
                ImageView ImageViewReply = (ImageView) singleMessageLayout.findViewById(R.id.ImageViewReply);
                LinearLayout attachmentLinearLayout = (LinearLayout) singleMessageLayout.findViewById(R.id.attachmentLinearLayout);


                ReusableClass.setVectorBackground(MessageDetailsActivity.this, R.drawable.ic_reply_black_24dp, ImageViewReply, R.color.colorAccent);
                ReusableClass.setVectorBackground(MessageDetailsActivity.this, R.drawable.ic_more_horiz_black_24dp, ImageViewMore, R.color.colorAccent);
                ReusableClass.setVectorBackground(MessageDetailsActivity.this, R.drawable.ic_more_horiz_black_24dp, ImageViewMore, R.color.colorAccent);

                final ArrayList<Attachments> attachments = messageLists[i].getAttachments();
                for (int j = 0; j < attachments.size(); j++) {
                    View child = getLayoutInflater().inflate(R.layout.attachment_layout, null);
                    ((TextView) child.findViewById(R.id.attachmentText)).setText(attachments.get(j).getLinkName());
                    final int finalJ = j;
                    ReusableClass.setVectorBackground(MessageDetailsActivity.this, R.drawable.ic_file_download_black_24dp, (ImageView) child.findViewById(R.id.attachmentDownloadIcon), R.color.colorPrimary);
                    ((ImageView) child.findViewById(R.id.attachmentDownloadIcon)).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            havingPermission(attachments.get(finalJ).getLink());
                        }
                    });
                    attachmentLinearLayout.addView(child);
                }


                if (messageLists[i].getSenderProfileImg() != null && !messageLists[i].getSenderProfileImg().equalsIgnoreCase(""))
                    Glide.with(this).load(messageLists[i].getSenderProfileImg()).into(profileImage);
                else
                    profileImage.setImageDrawable(ContextCompat.getDrawable(MessageDetailsActivity.this, R.drawable.empty_avatar));

                TextViewMessageSubject.setText(messageLists[i].getSujbect());
                TextViewMessageSubject.setVisibility(View.GONE);
                getSupportActionBar().setTitle(messageLists[i].getSujbect());
                TextViewSenderName.setText(messageLists[i].getSender());
                TextViewDateTime.setText(DateUtils.getRelativeTimeSpanString(getDateInMillis(messageLists[i].getDate()), Calendar.getInstance().getTimeInMillis(), 0L, DateUtils.FORMAT_ABBREV_ALL));
                String sendTo = "";
                Members[] memberList = messageLists[i].getRecipients().getMembers();
                for (int j = 0; j < memberList.length; j++) {
                    sendTo = sendTo + memberList[j].getName() + ", ";
                }
                if (sendTo.length() > 0)
                    TextViewSendTo.setText(sendTo.substring(0, sendTo.length() - 2));
                else
                    TextViewSendTo.setText("");
                TextViewMessageBody.setText(ReusableClass.fromHtml(messageLists[i].getMessage() != null ? messageLists[i].getMessage() : " "));
                TextViewMessageBody.setMovementMethod(LinkMovementMethod.getInstance());

                final int finalI = i;
                ImageViewReply.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(MessageDetailsActivity.this, ComposeNewMailActivity.class);
                        intent.putExtra("TYPE", Constant.REPLY_MAIL);
                        intent.putExtra("TITLE", replyLabel);
                        intent.putExtra("MESSAGE_CONTENT", messageLists[finalI].toString());
                        startActivity(intent);
                    }
                });


                final PopupMenu popup = new PopupMenu(MessageDetailsActivity.this, ImageViewMore);
                if (PreferencesData.getLoginResponse(this).getUserType().equalsIgnoreCase(Constant.PARENT) ||
                        PreferencesData.getLoginResponse(this).getUserType().equalsIgnoreCase(Constant.STUDENT)) {
                    popup.getMenuInflater().inflate(R.menu.message_menu_with_out_reply_all, popup.getMenu());
                    updateMenuItem(forwardMailLabel, deleteLabel, markAsReadLabel, markAsUneadLabel, null, popup.getMenu());
                } else {
                    popup.getMenuInflater().inflate(R.menu.message_menu_with_reply_all, popup.getMenu());
                    updateMenuItem(forwardMailLabel, deleteLabel, markAsReadLabel, markAsUneadLabel, replyAllLabel, popup.getMenu());

                }
                setForceShowIcon(popup);

                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    public boolean onMenuItemClick(MenuItem item) {
                        if (item.getTitle().toString().equalsIgnoreCase(replyAllLabel)) {
                            Intent intent = new Intent(MessageDetailsActivity.this, ComposeNewMailActivity.class);
                            intent.putExtra("TYPE", Constant.REPLY_ALL_MAIL);
                            intent.putExtra("TITLE", replyAllLabel);
                            intent.putExtra("MESSAGE_CONTENT", messageLists[finalI].toString());
                            startActivity(intent);
                        } else if (item.getTitle().toString().equalsIgnoreCase(forwardMailLabel)) {
                            Intent intent = new Intent(MessageDetailsActivity.this, ComposeNewMailActivity.class);
                            intent.putExtra("TYPE", Constant.FORWARD_MAIL);
                            intent.putExtra("TITLE", forwardMailLabel);
                            intent.putExtra("MESSAGE_CONTENT", messageLists[finalI].toString());
                            startActivity(intent);
                        } else if (item.getTitle().toString().equalsIgnoreCase(deleteLabel)) {
                            deletingMessage();
                        } else if (item.getTitle().toString().equalsIgnoreCase(markAsReadLabel)) {
                            markingMessage(1);
                        } else if (item.getTitle().toString().equalsIgnoreCase(markAsUneadLabel)) {
                            markingMessage(0);
                        }
                        return true;
                    }
                });
                ImageViewMore.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        popup.show();
                    }
                });

                linearLayoutMessageList.addView(singleMessageLayout);
            }
        } else {
            TastyToast.makeText(this, "Not be able to get message details, please try again.", TastyToast.LENGTH_LONG, TastyToast.ERROR).show();
        }
        progress.setVisibility(View.INVISIBLE);
    }

    private void updateMenuItem(String forwardMailLabel, String deleteLabel, String markAsReadLabel, String markAsUndeadLabel, String replyAllLabel, Menu menu) {

        MenuItem menuItem1 = menu.findItem(R.id.action_forward);
        menuItem1.setTitle(forwardMailLabel);

        MenuItem menuItem2 = menu.findItem(R.id.action_delete);
        menuItem2.setTitle(deleteLabel);

        MenuItem menuItem3 = menu.findItem(R.id.action_mark_read);
        menuItem3.setTitle(markAsReadLabel);

        MenuItem menuItem4 = menu.findItem(R.id.action_mark_unread);
        menuItem4.setTitle(markAsUndeadLabel);

        if (replyAllLabel != null) {
            MenuItem menuItem5 = menu.findItem(R.id.action_reply_all);
            menuItem5.setTitle(replyAllLabel);
        }
    }

    public void onEventMainThread(GetMessageDetailsEvent.Fail event) {
        if (event.getEx() != null) {
            TastyToast.makeText(this, "Unable to communicate with server, please try again after sometime.", TastyToast.LENGTH_LONG, TastyToast.ERROR).show();
        }
    }

    public void onEventMainThread(final UnreadMessageEvent.Success event) {
        if (event.getGenericResponse().getStatusCode() == 1) {
            topProgressBar.setVisibility(View.INVISIBLE);
            TastyToast.makeText(this, "Successfully marked your message.", TastyToast.LENGTH_LONG, TastyToast.SUCCESS).show();
        } else {
            TastyToast.makeText(this, "Something went wrong while updating. Please try after sometime.", TastyToast.LENGTH_LONG, TastyToast.SUCCESS).show();
        }
    }

    public void onEventMainThread(UnreadMessageEvent.Fail event) {
        if (event.getEx() != null) {
            TastyToast.makeText(this, "Unable to communicate with server, please try again after sometime.", TastyToast.LENGTH_LONG, TastyToast.ERROR).show();
        }
    }

    public void onEventMainThread(final DeletingEvent.Success event) {
        if (event.getGenericResponse().getStatusCode() == 1) {
            topProgressBar.setVisibility(View.INVISIBLE);
            finish();
            TastyToast.makeText(this, "Successfully deleted your message.", TastyToast.LENGTH_LONG, TastyToast.SUCCESS).show();
        } else {
            TastyToast.makeText(this, "Something went wrong while updating. Please try after sometime.", TastyToast.LENGTH_LONG, TastyToast.ERROR).show();
        }
    }

    public void onEventMainThread(DeletingEvent.Fail event) {
        if (event.getEx() != null) {
            TastyToast.makeText(this, "Unable to communicate with server, please try again after sometime.", TastyToast.LENGTH_LONG, TastyToast.ERROR).show();
        }
    }

    void markingMessage(int markType) {
        if (ReusableClass.isNetworkAvailable(this)) {
            topProgressBar.setVisibility(View.VISIBLE);

            ReadMessageSubmission readMessageSubmission = new ReadMessageSubmission();
            readMessageSubmission.setUserId(PreferencesData.getUserId(this));
            readMessageSubmission.setMarkType(markType);
            String[] messageIds = new String[]{String.valueOf(messageId)};
            readMessageSubmission.setMsgIds(messageIds);
            readMessageSubmission.setMsgType(messageBox);

            MyApplication.addJobInBackground(new UnreadingJob(readMessageSubmission));
        } else {
            TastyToast.makeText(this, "Sorry no internet connection available.", TastyToast.LENGTH_LONG, TastyToast.ERROR).show();
        }
    }

    void deletingMessage() {
        if (ReusableClass.isNetworkAvailable(this)) {
            topProgressBar.setVisibility(View.VISIBLE);

            DeleteMessageSubmission deleteMessageSubmission = new DeleteMessageSubmission();
            deleteMessageSubmission.setUserId(PreferencesData.getUserId(this));
            String[] messageIds = new String[]{String.valueOf(messageId)};
            deleteMessageSubmission.setMsgIds(messageIds);
            deleteMessageSubmission.setMsgType(messageBox);

            MyApplication.addJobInBackground(new DeletingJob(deleteMessageSubmission));
        } else {
            TastyToast.makeText(this, "Sorry no internet connection available.", TastyToast.LENGTH_LONG, TastyToast.ERROR).show();
        }
    }

    private void havingPermission(String attachmentUrl) {
        this.attachmentUrl = attachmentUrl;
        // Check if we have write permission
        int permission = ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (permission != PackageManager.PERMISSION_GRANTED)
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, EXTERNAL_STORAGE_PERMISSION_RESPONSE);
        else
            downloading(attachmentUrl);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case EXTERNAL_STORAGE_PERMISSION_RESPONSE: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                    downloading(attachmentUrl);
                else
                    TastyToast.makeText(this, "Sorry we need the permission.", Toast.LENGTH_LONG, TastyToast.ERROR).show();

                return;
            }
        }
    }

    private void downloading(final String url) {
        String fileName = url.substring(url.lastIndexOf('/') + 1);
        try {
            if (url != null && !url.isEmpty()) {
                Uri uri = Uri.parse(url.replaceAll(" ", "%20"));

                DownloadManager.Request request = new DownloadManager.Request(uri);
                request.setTitle(fileName);
                request.setDescription("Downloading attachment..");
                request.allowScanningByMediaScanner();
                request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
                request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, fileName);
                DownloadManager dm = (DownloadManager) this.getSystemService(Context.DOWNLOAD_SERVICE);
                dm.enqueue(request);
            }
        } catch (IllegalStateException e) {
            Toast.makeText(this, "Please insert an SD card to download file", Toast.LENGTH_SHORT).show();
        }

    }
}
