package net.lasagu.takyon360.ui;


import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import net.lasagu.takyon360.BaseActivity;
import net.lasagu.takyon360.BuildConfig;
import net.lasagu.takyon360.MyApplication;
import net.lasagu.takyon360.R;
import net.lasagu.takyon360.events.LoginEvent;
import net.lasagu.takyon360.jobs.LoginJob;
import net.lasagu.takyon360.models.LoginSubmission;
import net.lasagu.takyon360.models.UserDetails;
import net.lasagu.takyon360.ui.fragments.ForgotPasswordDialogFragment;
import net.lasagu.takyon360.utils.Constant;
import net.lasagu.takyon360.utils.PreferencesData;
import net.lasagu.takyon360.utils.ReusableClass;
import net.lasagu.takyon360.utils.SharedPreferenceStore;

import java.util.Locale;

import butterknife.BindColor;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.pedant.SweetAlert.SweetAlertDialog;
import de.greenrobot.event.EventBus;

public class LoginActivity extends BaseActivity {

    SweetAlertDialog pDialog;
    @BindColor(R.color.colorPrimary)
    int colorPrimary;
    @BindView(R.id.toolBar)
    Toolbar toolbar;
    @BindView(R.id.input_username)
    EditText inputUsername;
    @BindView(R.id.input_password)
    EditText inputPassword;
    @BindView(R.id.input_language)
    EditText inputLanguage;
    @BindView(R.id.rememberMe)
    CheckBox rememberMe;
    @BindView(R.id.versionNo)
    TextView versionNo;
    @BindView(R.id.activity_main)
    LinearLayout activityMain;
    @BindView(R.id.loginButton)
    Button loginButton;
    private String language = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        inputLanguage.setText("English");
        language = "English";

        versionNo.setText("Version no: " + BuildConfig.VERSION_NAME);
        if (PreferencesData.isRemembered(this)) {
            UserDetails userDetails = PreferencesData.getUser(this);
            inputUsername.setText(userDetails.getUsername());
            inputPassword.setText(userDetails.getPassword());
            language = userDetails.getLanguage();
            inputLanguage.setText(language);
            rememberMe.setChecked(true);
            loginButton.performClick();
        }

        if (BuildConfig.DEBUG) {
//            inputUsername.setText("FAP0218");
//            inputPassword.setText("123456");
//            inputUsername.setText("AKAP1821");
//            inputPassword.setText("12345678");
//            inputUsername.setText("amrp8109");
//            inputPassword.setText("jj123arav");
//            inputUsername.setText("HBTP0519");
//            inputPassword.setText("123456");
//            inputUsername.setText("hbtp2833");
//            inputPassword.setText("123456");
//            inputUsername.setText("LPSP17280");
//            inputPassword.setText("123456");
//            inputUsername.setText("ASDP0010955");
//            inputPassword.setText("123456");
//            inputUsername.setText("LPSP12601");
//            inputPassword.setText("123456");

//            Control Panel and Report
//            inputUsername.setText("amohammed@alameerschool.com");
//            inputPassword.setText("12345678");

//            Admin login
//            inputUsername.setText("TakAdmin");
//            inputPassword.setText("T@k@dm!nT#$T");
//            inputUsername.setText("Stephanie@takyonline.com");
//            inputPassword.setText("123456");
//            inputUsername.setText("CHSWebAdmin");
//            inputPassword.setText("[8g}7z{D)+HZ");
//            inputUsername.setText("Takp1315");
//            inputPassword.setText("12345");
//            inputUsername.setText("hbtp11644");
//            inputPassword.setText("vaishnav2010");
//            inputUsername.setText("SASP0482");
//            inputPassword.setText("123456");
//            inputUsername.setText("SASP0482");
//            inputPassword.setText("123456");
//            inputUsername.setText("AMBWadmin");
//            inputPassword.setText("*vE]dcQy.Vus");
//            inputUsername.setText("CHSP16201186");
//            inputPassword.setText("123456");
//            inputUsername.setText("LPSP7545");
//            inputPassword.setText("123456");
//            inputUsername.setText("LPSP18208");
//            inputPassword.setText("123456");
//            inputUsername.setText("lpsp15021");
//            inputPassword.setText("123456");
//            inputUsername.setText("AJYALWebMBZ");
//            inputPassword.setText("~vUA@!8J(HZh");
//            inputUsername.setText("alameerWeb");
//            inputPassword.setText("alameerWeb#@!");
//            inputUsername.setText("TAKP1314");
//            inputPassword.setText("123456");
//            inputUsername.setText("SASWebadmin");
//            inputPassword.setText("cKy{owGR;oNk");
//            inputUsername.setText("CHSP16201186");
//            inputPassword.setText("123456");
//            inputUsername.setText("tapsp1285");
//            inputPassword.setText("123456");
//            inputUsername.setText("SASWebadmin");
//            inputPassword.setText("cKy{owGR;oNk");
//            inputUsername.setText("SASP1550");
//            inputPassword.setText("123456");
//            inputUsername.setText("TAK1P004775");
//            inputPassword.setText("123456");
//            inputUsername.setText("hbtp0518");
//            inputPassword.setText("123456");
//            inputUsername.setText("Takadmin");
//            inputPassword.setText("T@k@dm!nT#$T");
//            inputUsername.setText("EFIAWebadmin");
//            inputPassword.setText("c*!FD8FKN[,Q");
            inputUsername.setText("TAKP1315");
            inputPassword.setText("123456");
        }
    }

    @OnClick(R.id.input_language)
    public void selectingLanguages(View view) {
        PopupMenu popup = new PopupMenu(LoginActivity.this, view);
        popup.getMenuInflater().inflate(R.menu.language_menu, popup.getMenu());

        //registering popup with OnMenuItemClickListener
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                language = item.getTitle().toString();
                inputLanguage.setText(language);
                return true;
            }
        });

        popup.show(); //showing popup menu
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    public void loggedIn(View view) {

        String email = inputUsername.getText().toString();
        String password = inputPassword.getText().toString();
        if (!TextUtils.isEmpty(email) && !TextUtils.isEmpty(password) && !TextUtils.isEmpty(language)) {
            if (ReusableClass.isNetworkAvailable(this)) {
                pDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
                pDialog.getProgressHelper().setBarColor(colorPrimary);
                pDialog.setTitleText("Loading ...");
                pDialog.setCancelable(false);
                pDialog.show();

                LoginSubmission loginSubmission = new LoginSubmission();
                loginSubmission.setUsername(email);
                loginSubmission.setPassword(password);
                loginSubmission.setLanguage(language);

                MyApplication.addJobInBackground(new LoginJob(loginSubmission));
            } else {
                Snackbar.make(view, "Sorry no internet connection available.", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        } else {
            Snackbar.make(view, "All field are mandatory.", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
        }
    }

    public void onEventMainThread(final LoginEvent.Success event) {
        if (event.getLoginResponse().getStatusCode() == 1) {

            if (rememberMe.isChecked())
                PreferencesData.setRememberMe(this);
            else
                PreferencesData.setDontRemember(this);

            pDialog.setTitleText("Success!!!");
            pDialog.changeAlertType(SweetAlertDialog.SUCCESS_TYPE);
            UserDetails userDetails = new UserDetails();
            userDetails.setUsername(inputUsername.getText().toString());
            userDetails.setPassword(inputPassword.getText().toString());
            userDetails.setLanguage(language);

            PreferencesData.setUser(this, userDetails);

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    String languageCode = "en";
                    if (language.equalsIgnoreCase("عربى"))
                        languageCode = "ar";
                    else if (language.equalsIgnoreCase("हिंदी"))
                        languageCode = "hi";
                    else if (language.equalsIgnoreCase("नेपाली"))
                        languageCode = "hi";

                    Configuration configuration = getResources().getConfiguration();
                    configuration.setLayoutDirection(new Locale(languageCode));
                    getResources().updateConfiguration(configuration, getResources().getDisplayMetrics());

                    PreferencesData.setLoginResponse(LoginActivity.this, event.getLoginResponse());
                    SharedPreferenceStore.storeValue(LoginActivity.this, Constant.BASE_URL, event.getLoginResponse().getBaseURL());
                    SharedPreferenceStore.storeValue(LoginActivity.this, Constant.LANGUAGE_CODE, languageCode);
                    Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                    pDialog.dismiss();
                    finish();
                    startActivity(intent);
                }
            }, 800);
        } else {
            pDialog.setTitleText("Please check your details.");
            pDialog.changeAlertType(SweetAlertDialog.ERROR_TYPE);
            pDialog.setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                @Override
                public void onClick(SweetAlertDialog sweetAlertDialog) {
                    pDialog.dismiss();
                }
            });
        }
    }

    public void onEventMainThread(LoginEvent.Fail event) {

        if (event.getEx() != null) {
            if (event.isInvalidVredentials())
                pDialog.setTitleText("Wrong Credentials ...");
            else
                pDialog.setTitleText("Oops Server Error");
            pDialog.setContentText(event.getEx());
            pDialog.changeAlertType(SweetAlertDialog.ERROR_TYPE);
        }
    }

    public void resetPassword(View view) {
        ForgotPasswordDialogFragment forgotPasswordDialogFragment = new ForgotPasswordDialogFragment();
        forgotPasswordDialogFragment.show(getSupportFragmentManager(), "forgotPasswordDialogFragment");
    }
}