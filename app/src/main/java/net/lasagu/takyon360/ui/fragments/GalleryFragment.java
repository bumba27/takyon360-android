package net.lasagu.takyon360.ui.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.sdsmdg.tastytoast.TastyToast;

import net.lasagu.takyon360.MyApplication;
import net.lasagu.takyon360.R;
import net.lasagu.takyon360.events.GalleryCategoryEvent;
import net.lasagu.takyon360.jobs.GalleryCategoryJob;
import net.lasagu.takyon360.models.CategoriesBeanGallery;
import net.lasagu.takyon360.models.GalleryCategoryResponse;
import net.lasagu.takyon360.models.UserIdAndSearchTextSubmission;
import net.lasagu.takyon360.ui.MainActivity;
import net.lasagu.takyon360.utils.PreferencesData;
import net.lasagu.takyon360.utils.ReusableClass;
import net.lasagu.takyon360.widgets.GalleryListAdapter;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.greenrobot.event.EventBus;

/**
 * A simple {@link Fragment} subclass.
 */
public class GalleryFragment extends Fragment {


    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.SwipeRefreshLayout)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.noData)
    LinearLayout noData;
    @BindView(R.id.progress)
    ProgressBar progress;
    GalleryListAdapter galleryCategoryAdapter;
    GridLayoutManager gridLayoutManager;

    public GalleryFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_gallery, container, false);
        ButterKnife.bind(this, view);

        galleryCategoryAdapter = new GalleryListAdapter(getContext());
        int mNoOfColumns = ReusableClass.calculateNoOfColumns(getContext().getApplicationContext());
        gridLayoutManager = new GridLayoutManager(getContext(), mNoOfColumns);
        recyclerView.setLayoutManager(gridLayoutManager);
        recyclerView.setAdapter(galleryCategoryAdapter);

        loadingData();

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadingData();
            }
        });

        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    private void loadingData() {
        if (ReusableClass.isNetworkAvailable(getContext())) {
            progress.setVisibility(View.VISIBLE);

            UserIdAndSearchTextSubmission userIdAndSearchTextSubmission = new UserIdAndSearchTextSubmission();
            userIdAndSearchTextSubmission.setUserId(PreferencesData.getUserId(getContext()));
            userIdAndSearchTextSubmission.setSearchText("");

            MyApplication.addJobInBackground(new GalleryCategoryJob(userIdAndSearchTextSubmission));
        } else
            TastyToast.makeText(getContext(), "Sorry no internet connection.", Toast.LENGTH_LONG, TastyToast.ERROR).show();
    }


    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    public void onEventMainThread(final GalleryCategoryEvent.Success event) {
        GalleryCategoryResponse galleryCategoryResponse = event.getGalleryCategoryResponse();
        ((MainActivity)getActivity()).getSupportActionBar().setTitle(galleryCategoryResponse.getCategoryLabel());

        // Update title
        ((MainActivity) getActivity()).setActionBarTitle(galleryCategoryResponse.getCategoryLabel());

        ArrayList<CategoriesBeanGallery> categories = galleryCategoryResponse.getCategories();
        if (categories.size() != 0) {
            galleryCategoryAdapter.addCategory(categories);

            noData.setVisibility(View.INVISIBLE);
        } else {
            noData.setVisibility(View.VISIBLE);
        }
        progress.setVisibility(View.INVISIBLE);
        if (swipeRefreshLayout.isRefreshing())
            swipeRefreshLayout.setRefreshing(false);
    }

    public void onEventMainThread(GalleryCategoryEvent.Fail event) {
        if (event.getEx() != null) {
            TastyToast.makeText(getContext(), "Seems server is busy try after sometime.", TastyToast.LENGTH_LONG, TastyToast.ERROR).show();
        }
    }
}
