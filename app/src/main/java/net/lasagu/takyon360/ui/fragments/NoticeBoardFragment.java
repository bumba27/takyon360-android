package net.lasagu.takyon360.ui.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.sdsmdg.tastytoast.TastyToast;

import net.lasagu.takyon360.MyApplication;
import net.lasagu.takyon360.R;
import net.lasagu.takyon360.events.NoticeBoardCategoryEvent;
import net.lasagu.takyon360.jobs.NoticeBoardCategoryJob;
import net.lasagu.takyon360.models.CategoriesBeanX;
import net.lasagu.takyon360.models.NoticeBoardResponse;
import net.lasagu.takyon360.models.UserIdAndSearchTextSubmission;
import net.lasagu.takyon360.ui.MainActivity;
import net.lasagu.takyon360.utils.PreferencesData;
import net.lasagu.takyon360.utils.ReusableClass;
import net.lasagu.takyon360.widgets.NoticeBoardCategoryAdapter;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.greenrobot.event.EventBus;

/**
 * A simple {@link Fragment} subclass.
 */
public class NoticeBoardFragment extends Fragment {
    
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.SwipeRefreshLayout)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.noData)
    LinearLayout noData;
    @BindView(R.id.progress)
    ProgressBar progress;
    LinearLayoutManager linearLayoutManager;
    NoticeBoardCategoryAdapter noticeBoardCategoryAdapter;

    public NoticeBoardFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_notice_board, container, false);
        ButterKnife.bind(this, view);
        setHasOptionsMenu(true);

        noticeBoardCategoryAdapter = new NoticeBoardCategoryAdapter(getContext());
        linearLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(noticeBoardCategoryAdapter);

        loadingData();

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadingData();
            }
        });

        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.search_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);

        MenuItem item = menu.findItem(R.id.action_search);
        SearchView searchView = new SearchView(((MainActivity) getActivity()).getSupportActionBar().getThemedContext());
        MenuItemCompat.setShowAsAction(item, MenuItemCompat.SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW | MenuItemCompat.SHOW_AS_ACTION_IF_ROOM);
        MenuItemCompat.setActionView(item, searchView);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                noticeBoardCategoryAdapter.filter(query);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                noticeBoardCategoryAdapter.filter(newText);
                return true;
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    private void loadingData() {
        if (ReusableClass.isNetworkAvailable(getContext())) {
            progress.setVisibility(View.VISIBLE);

            UserIdAndSearchTextSubmission userIdAndSearchTextSubmission = new UserIdAndSearchTextSubmission();
            userIdAndSearchTextSubmission.setUserId(PreferencesData.getUserId(getContext()));
            userIdAndSearchTextSubmission.setSearchText("");

            MyApplication.addJobInBackground(new NoticeBoardCategoryJob(userIdAndSearchTextSubmission));
        } else
            TastyToast.makeText(getContext(), "Sorry no internet connection.", Toast.LENGTH_LONG, TastyToast.ERROR).show();
    }


    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    public void onEventMainThread(final NoticeBoardCategoryEvent.Success event) {
        NoticeBoardResponse noticeBoardResponse = event.getNoticeBoardCategoryResponse();

        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle(noticeBoardResponse.getHeadLabel());
        ArrayList<CategoriesBeanX> categories = noticeBoardResponse.getCategories();
        if (categories.size() != 0) {
            noticeBoardCategoryAdapter.addAll(categories);

            noData.setVisibility(View.INVISIBLE);
        } else {
            noData.setVisibility(View.VISIBLE);
        }
        progress.setVisibility(View.INVISIBLE);
        if (swipeRefreshLayout.isRefreshing())
            swipeRefreshLayout.setRefreshing(false);
    }

    public void onEventMainThread(NoticeBoardCategoryEvent.Fail event) {
        if (event.getEx() != null) {
            TastyToast.makeText(getContext(), "Seems server is busy try after sometime.", TastyToast.LENGTH_LONG, TastyToast.ERROR).show();
        }
    }
}
