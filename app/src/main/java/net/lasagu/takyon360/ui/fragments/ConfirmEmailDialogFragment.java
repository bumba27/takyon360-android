package net.lasagu.takyon360.ui.fragments;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.sdsmdg.tastytoast.TastyToast;

import net.lasagu.takyon360.MyApplication;
import net.lasagu.takyon360.R;
import net.lasagu.takyon360.events.EmailVerificaionEvent;
import net.lasagu.takyon360.events.NotificationReadEvent;
import net.lasagu.takyon360.events.PassEmailIdToProfileEvent;
import net.lasagu.takyon360.jobs.VerifyEmailJob;
import net.lasagu.takyon360.models.VerifyEmailSubmission;
import net.lasagu.takyon360.utils.ReusableClass;

import butterknife.BindColor;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.pedant.SweetAlert.SweetAlertDialog;
import de.greenrobot.event.EventBus;

/**
 * Created by ajana on 06/07/2017.
 */

public class ConfirmEmailDialogFragment extends DialogFragment {

    @BindView(R.id.SaveButton)
    Button SaveButton;
    @BindView(R.id.confirmationMessageTextView)
    TextView confirmationMessageTextView;
    @BindView(R.id.EditTextVerificationCode)
    EditText EditTextVerificationCode;
    private SweetAlertDialog pDialog;
    @BindColor(R.color.colorPrimary)
    int colorPrimary;
    private String emailId;

    @SuppressLint("ValidFragment")
    public ConfirmEmailDialogFragment() {

    }

    public ConfirmEmailDialogFragment(String emailId) {
        this.emailId = emailId;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getDialog().setTitle("Verify email");
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        View view = inflater.inflate(R.layout.confirm_email_dialog_fragment, container, false);
        ButterKnife.bind(this, view);

        confirmationMessageTextView.setText("Thank you for verifying your email id. A message with a confirmation code was sent to " + emailId + ". To complete the verification process enter the verification code above.");

        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @OnClick(R.id.SaveButton)
    public void saving() {
        if (validated()) {
            VerifyEmailSubmission verifyEmailSubmission = new VerifyEmailSubmission();
            verifyEmailSubmission.setKey(EditTextVerificationCode.getText().toString());
            verifyEmailSubmission.setVEmail(emailId);

            if (ReusableClass.isNetworkAvailable(getContext())) {
                showingProgress("Checking ...");
                MyApplication.addJobInBackground(new VerifyEmailJob(verifyEmailSubmission));
            } else
                TastyToast.makeText(getContext(), "Sorry no internet connection.", Toast.LENGTH_LONG, TastyToast.ERROR).show();

        } else
            TastyToast.makeText(getContext(), "All fields are required.", TastyToast.LENGTH_LONG, TastyToast.ERROR).show();
    }

    private boolean validated() {
        if (TextUtils.isEmpty(EditTextVerificationCode.getText()))
            return false;

        return true;
    }

    public void showingProgress(String title) {
        pDialog = new SweetAlertDialog(getContext(), SweetAlertDialog.PROGRESS_TYPE);
        pDialog.getProgressHelper().setBarColor(colorPrimary);
        pDialog.setTitleText(title);
        pDialog.setCancelable(false);
        pDialog.show();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    public void onEventMainThread(EmailVerificaionEvent.Success event) {
        if (event.genericResponse().getStatusCode() == 1) {
            pDialog.setTitleText("Success!!!");
            pDialog.setContentText(event.genericResponse().getStatusMessage());
            pDialog.changeAlertType(SweetAlertDialog.SUCCESS_TYPE);
            pDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                @Override
                public void onClick(SweetAlertDialog sweetAlertDialog) {
                    pDialog.dismiss();
                    EventBus.getDefault().postSticky(new PassEmailIdToProfileEvent(emailId));
                    dismiss();
                }
            });
        } else {
            pDialog.setTitleText("We Are Sorry ...");
            pDialog.setContentText(event.genericResponse().getStatusMessage());
            pDialog.changeAlertType(SweetAlertDialog.ERROR_TYPE);
            pDialog.setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                @Override
                public void onClick(SweetAlertDialog sweetAlertDialog) {
                    pDialog.dismiss();
                }
            });
        }
    }

    public void onEventMainThread(EmailVerificaionEvent.Fail event) {
        if (event.getEx() != null) {
            pDialog.setTitleText("We Are Sorry ...");
            pDialog.setContentText(event.getEx());
            pDialog.changeAlertType(SweetAlertDialog.ERROR_TYPE);
        }
    }
}