package net.lasagu.takyon360.ui;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.sdsmdg.tastytoast.TastyToast;

import net.lasagu.takyon360.BaseActivity;
import net.lasagu.takyon360.MyApplication;
import net.lasagu.takyon360.R;
import net.lasagu.takyon360.events.GalleryListResponseEvent;
import net.lasagu.takyon360.jobs.GetGalleryImageListJob;
import net.lasagu.takyon360.models.CategoryWithPaginationSubmission;
import net.lasagu.takyon360.models.GalleryImageListResponse;
import net.lasagu.takyon360.models.GalleryItemsBean;
import net.lasagu.takyon360.utils.ReusableClass;
import net.lasagu.takyon360.widgets.GalleryListAdapter;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.greenrobot.event.EventBus;

import static net.lasagu.takyon360.utils.ReusableClass.updateResources;

public class GalleryGridActivity extends BaseActivity {
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.SwipeRefreshLayout)
    android.support.v4.widget.SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.noData)
    LinearLayout noData;
    @BindView(R.id.progress)
    ProgressBar progress;
    GridLayoutManager gridLayoutManager;
    int pastVisibleItems, visibleItemCount, totalItemCount;
    private GalleryListAdapter galleryListAdapter;
    private boolean loading = true;
    private int pageNo = 1;
    private int categoryId = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gallery_grid);
        ButterKnife.bind(this);
        updateResources(GalleryGridActivity.this);

        categoryId = Integer.parseInt(getIntent().getStringExtra("CATEGORY_ID"));
        getSupportActionBar().setTitle(getIntent().getStringExtra("CATEGORY_NAME"));

        galleryListAdapter = new GalleryListAdapter(this);
        int mNoOfColumns = ReusableClass.calculateNoOfColumns(getApplicationContext());
        gridLayoutManager = new GridLayoutManager(this, mNoOfColumns);
        recyclerView.setLayoutManager(gridLayoutManager);
        recyclerView.setAdapter(galleryListAdapter);

        loadingData(1, categoryId);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                pageNo = 1;
                loadingData(pageNo, categoryId);
            }
        });
    }

    private void loadingData(int pagination, int categoryId) {
        if (ReusableClass.isNetworkAvailable(this)) {
            progress.setVisibility(View.VISIBLE);

            CategoryWithPaginationSubmission categoryWithPaginationSubmission = new CategoryWithPaginationSubmission();
            categoryWithPaginationSubmission.setCategoryId(categoryId);
            categoryWithPaginationSubmission.setSearchText("");
            categoryWithPaginationSubmission.setPaginationNumber(pagination);

            MyApplication.addJobInBackground(new GetGalleryImageListJob(categoryWithPaginationSubmission));
        } else
            TastyToast.makeText(this, "Sorry no internet connection.", Toast.LENGTH_LONG, TastyToast.ERROR).show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.search_menu, menu);

        final MenuItem myActionMenuItem = menu.findItem(R.id.action_search);
        final SearchView searchView = (SearchView) myActionMenuItem.getActionView();

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                if (ReusableClass.isNetworkAvailable(GalleryGridActivity.this)) {
                    progress.setVisibility(View.VISIBLE);

                    CategoryWithPaginationSubmission categoryWithPaginationSubmission = new CategoryWithPaginationSubmission();
                    categoryWithPaginationSubmission.setCategoryId(categoryId);
                    categoryWithPaginationSubmission.setSearchText(query);
                    categoryWithPaginationSubmission.setPaginationNumber(1);


                    MyApplication.addJobInBackground(new GetGalleryImageListJob(categoryWithPaginationSubmission));
                } else
                    TastyToast.makeText(GalleryGridActivity.this, "Sorry no internet connection.", Toast.LENGTH_LONG, TastyToast.ERROR).show();

                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                return false;
            }
        });

        ImageView closeButton = (ImageView) searchView.findViewById(android.support.v7.appcompat.R.id.search_close_btn);

        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myActionMenuItem.collapseActionView();

                if (ReusableClass.isNetworkAvailable(GalleryGridActivity.this)) {
                    progress.setVisibility(View.VISIBLE);

                    CategoryWithPaginationSubmission categoryWithPaginationSubmission = new CategoryWithPaginationSubmission();
                    categoryWithPaginationSubmission.setCategoryId(categoryId);
                    categoryWithPaginationSubmission.setSearchText("");
                    categoryWithPaginationSubmission.setPaginationNumber(1);


                    MyApplication.addJobInBackground(new GetGalleryImageListJob(categoryWithPaginationSubmission));
                } else
                    TastyToast.makeText(GalleryGridActivity.this, "Sorry no internet connection.", Toast.LENGTH_LONG, TastyToast.ERROR).show();
            }
        });

        return true;
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    public void onEventMainThread(final GalleryListResponseEvent.Success event) {
        GalleryImageListResponse galleryImageListResponse = event.getGalleryImageListResponse();
        ArrayList<GalleryItemsBean> galleryItemsBeen = galleryImageListResponse.getGalleryItems();
        if (galleryItemsBeen.size() != 0) {
            galleryListAdapter.addAll(galleryItemsBeen);
        }
        progress.setVisibility(View.INVISIBLE);
        if (swipeRefreshLayout.isRefreshing())
            swipeRefreshLayout.setRefreshing(false);
    }

    public void onEventMainThread(GalleryListResponseEvent.Fail event) {
        if (event.getEx() != null) {
            TastyToast.makeText(this, "Seems server is busy try after sometime.", TastyToast.LENGTH_LONG, TastyToast.ERROR).show();
        }
    }
}
