package net.lasagu.takyon360.ui.fragments;


import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.sdsmdg.tastytoast.TastyToast;

import net.lasagu.takyon360.MyApplication;
import net.lasagu.takyon360.R;
import net.lasagu.takyon360.events.FeeSummaryEvent;
import net.lasagu.takyon360.jobs.GetFeeSummaryJob;
import net.lasagu.takyon360.models.FeeSummaryResponse;
import net.lasagu.takyon360.ui.WebViewActivity;
import net.lasagu.takyon360.utils.PreferencesData;
import net.lasagu.takyon360.utils.ReusableClass;

import butterknife.BindColor;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.pedant.SweetAlert.SweetAlertDialog;
import de.greenrobot.event.EventBus;

/**
 * A simple {@link Fragment} subclass.
 */
public class FeeSummaryFragment extends Fragment {


    @BindView(R.id.editTextName)
    EditText editTextName;
    @BindView(R.id.editTextClassDevision)
    EditText editTextClassDivision;
    @BindView(R.id.editTextTotalFee)
    TextView editTextTotalFee;
    @BindView(R.id.TextViewCurrentDue)
    TextView TextViewCurrentDue;
    @BindView(R.id.TextViewTotalFee)
    TextView TextViewTotalFee;
    @BindView(R.id.TextViewTotalPaid)
    TextView TextViewTotalPaid;
    @BindView(R.id.TextViewTotalPayable)
    TextView TextViewTotalPayable;
    @BindView(R.id.TextViewPay)
    TextView TextViewPay;
    @BindView(R.id.TextViewPaymentDetails)
    TextView TextViewPaymentDetails;
    @BindView(R.id.editTextTotalPaid)
    TextView editTextTotalPaid;
    @BindView(R.id.editTextTotalPayable)
    TextView editTextTotalPayable;
    @BindView(R.id.editTextCurrentDue)
    TextView editTextCurrentDue;
    @BindView(R.id.payAmountEditText)
    EditText payAmountEditText;
    @BindView(R.id.payAmountButton)
    Button payAmountButton;
    @BindView(R.id.TextViewStudentDetails)
    TextView TextViewStudentDetails;
    @BindView(R.id.TextInputLayoutName)
    TextInputLayout TextInputLayoutName;
    @BindView(R.id.TextInputLayoutClassDivision)
    TextInputLayout TextInputLayoutClassDivision;
    @BindView(R.id.TextInputLayoutPayAmountEditText)
    TextInputLayout TextInputLayoutPayAmountEditText;
    private SweetAlertDialog pDialog;
    @BindColor(R.color.colorPrimary)
    int colorPrimary;

    public static String isTransportPaymentDetails = "IS_TRANSPORT_PAYMENT_DETAILS";

    public FeeSummaryFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fragment_fee_summary, container, false);
        ButterKnife.bind(this, root);

        populateUserData();
        return root;
    }

    private void populateUserData() {
        boolean isTrasportPayment = false;
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            isTrasportPayment = bundle.getBoolean(isTransportPaymentDetails, false);
        }
        if (ReusableClass.isNetworkAvailable(getContext())) {
            showingProgress("Loading ...");
            MyApplication.addJobInBackground(new GetFeeSummaryJob(isTrasportPayment, PreferencesData.getUserId(getContext())));
        } else
            TastyToast.makeText(getContext(), "Sorry no internet connection.", Toast.LENGTH_LONG, TastyToast.ERROR).show();
    }


    public void showingProgress(String title) {
        pDialog = new SweetAlertDialog(getContext(), SweetAlertDialog.PROGRESS_TYPE);
        pDialog.getProgressHelper().setBarColor(colorPrimary);
        pDialog.setTitleText(title);
        pDialog.setCancelable(false);
        pDialog.show();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    public void onEventMainThread(FeeSummaryEvent.Fail event) {
        if (event.getEx() != null) {
            pDialog.setTitleText("We Are Sorry ...");
            pDialog.setContentText(event.getEx());
            pDialog.changeAlertType(SweetAlertDialog.ERROR_TYPE);
        }
    }

    public void onEventMainThread(FeeSummaryEvent.Success event) {
        if (event.getFeeSummaryResponse().getStatusCode() == 1) {
            pDialog.setTitleText("Success!!!");
            pDialog.changeAlertType(SweetAlertDialog.SUCCESS_TYPE);

            FeeSummaryResponse feeSummaryResponse = event.getFeeSummaryResponse();
            editTextName.setText(feeSummaryResponse.getName());
            editTextClassDivision.setText(feeSummaryResponse.getDivision());
            editTextCurrentDue.setText(feeSummaryResponse.getCurrentDue());
            editTextTotalFee.setText(feeSummaryResponse.getTotalFee());
            editTextTotalPaid.setText(feeSummaryResponse.getTotalPaid());
            editTextTotalPayable.setText(feeSummaryResponse.getTotalDue());
            TextInputLayoutPayAmountEditText.setHint(feeSummaryResponse.getAmountLabel());
            payAmountButton.setText(feeSummaryResponse.getPayLabel());
            payAmountButton.setTag(feeSummaryResponse.getPaymentUrl());
            editTextName.setText(feeSummaryResponse.getName());
            TextInputLayoutClassDivision.setHint(feeSummaryResponse.getClassDivisionLabel());
            TextInputLayoutName.setHint(feeSummaryResponse.getNameLabel());
            TextViewPaymentDetails.setText(feeSummaryResponse.getAccountDetailsLabel());
            TextViewPay.setText(feeSummaryResponse.getPayLabel());
            TextViewStudentDetails.setText(feeSummaryResponse.getStudentDetailsLabel());

            TextViewTotalPaid.setText(feeSummaryResponse.getTotalPaidLabel());
            TextViewTotalFee.setText(feeSummaryResponse.getDueAmountLabel());
            TextViewTotalPayable.setText(feeSummaryResponse.getTotalPayableLabel());
            TextViewCurrentDue.setText(feeSummaryResponse.getCurrentDueFormLabel());

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    pDialog.dismiss();
                }
            }, 800);
        } else {
            pDialog.setTitleText("We Are Sorry ...");
            pDialog.setContentText("Not be able to get your details, please try again.");
            pDialog.changeAlertType(SweetAlertDialog.ERROR_TYPE);
            pDialog.setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                @Override
                public void onClick(SweetAlertDialog sweetAlertDialog) {
                    pDialog.dismiss();
                }
            });
        }
    }

    @OnClick(R.id.payAmountButton)
    public void clickingPay(View view) {

        if (payAmountEditText.getText().toString().trim().length() > 0 && Double.parseDouble(payAmountEditText.getText().toString().trim()) != 0) {
            String paymentLink = payAmountButton.getTag() + "?m_userid=" + PreferencesData.getUserId(getContext()) + "&amount=" + payAmountEditText.getText().toString().trim();

            Intent intent = new Intent(getContext(), WebViewActivity.class);
            intent.putExtra("WEB_URL", paymentLink);
            startActivity(intent);
        } else
            TastyToast.makeText(getContext(), "Please enter a correct amount", TastyToast.LENGTH_LONG, TastyToast.ERROR).show();
    }
}
