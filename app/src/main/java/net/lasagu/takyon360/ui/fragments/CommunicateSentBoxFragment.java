package net.lasagu.takyon360.ui.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.sdsmdg.tastytoast.TastyToast;

import net.lasagu.takyon360.MyApplication;
import net.lasagu.takyon360.R;
import net.lasagu.takyon360.events.CommunicateOutboxEvent;
import net.lasagu.takyon360.jobs.CommunicateOutboxJob;
import net.lasagu.takyon360.models.CommunicateResponse;
import net.lasagu.takyon360.models.CommunicateSubmission;
import net.lasagu.takyon360.models.MessageList;
import net.lasagu.takyon360.ui.MainActivity;
import net.lasagu.takyon360.utils.Constant;
import net.lasagu.takyon360.utils.PreferencesData;
import net.lasagu.takyon360.utils.ReusableClass;
import net.lasagu.takyon360.widgets.InboxSentBoxListAdapter;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.greenrobot.event.EventBus;

/**
 * A simple {@link Fragment} subclass.
 */
public class CommunicateSentBoxFragment extends Fragment {


    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.swipeRefreshLayout)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.noData)
    LinearLayout noData;
    @BindView(R.id.progress)
    ProgressBar progress;
    private InboxSentBoxListAdapter inboxSentBoxListAdapter;
    LinearLayoutManager linearLayoutManager;
    private boolean loading = true;
    int pastVisibleItems, visibleItemCount, totalItemCount;
    private int pageNo = 1;

    public CommunicateSentBoxFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_communicate_sentbox, container, false);
        ButterKnife.bind(this, view);
        setHasOptionsMenu(true);

        inboxSentBoxListAdapter = new InboxSentBoxListAdapter(getContext(), Constant.OUTBOX);
        linearLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(inboxSentBoxListAdapter);

        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.search_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);

        MenuItem item = menu.findItem(R.id.action_search);
        SearchView searchView = new SearchView(((MainActivity) getActivity()).getSupportActionBar().getThemedContext());
        MenuItemCompat.setShowAsAction(item, MenuItemCompat.SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW | MenuItemCompat.SHOW_AS_ACTION_IF_ROOM);
        MenuItemCompat.setActionView(item, searchView);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                inboxSentBoxListAdapter.filter(query);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                inboxSentBoxListAdapter.filter(newText);
                return true;
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();

        loadingData(1);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                pageNo = 1;
                loadingData(pageNo);
            }
        });
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (dy > 0) //check for scroll down
                {
                    visibleItemCount = linearLayoutManager.getChildCount();
                    totalItemCount = linearLayoutManager.getItemCount();
                    pastVisibleItems = linearLayoutManager.findFirstVisibleItemPosition();

                    if (loading) {
                        if ((visibleItemCount + pastVisibleItems) >= totalItemCount) {
                            loading = false;
                            Log.v("...", "Last Item Wow !");
                            pageNo = pageNo + 1;
                            loadingData(pageNo);
                        }
                    }
                }
            }
        });
    }

    private void loadingData(int pagination) {
        if (ReusableClass.isNetworkAvailable(getContext())) {
            progress.setVisibility(View.VISIBLE);

            CommunicateSubmission communicateSubmission = new CommunicateSubmission();
            communicateSubmission.setUserId(PreferencesData.getUserId(getContext()));
            communicateSubmission.setSearchText("");
            communicateSubmission.setPaginationNumber(pagination);

            MyApplication.addJobInBackground(new CommunicateOutboxJob(communicateSubmission));
        } else
            TastyToast.makeText(getContext(), "Sorry no internet connection.", Toast.LENGTH_LONG, TastyToast.ERROR).show();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    public void onEventMainThread(final CommunicateOutboxEvent.Success event) {
        CommunicateResponse communicateResponse = event.getCommunicateResponse();
        ArrayList<MessageList> messageLists = communicateResponse.getMessageList();
        if (messageLists.size() != 0) {
            if (pageNo == 1)
                inboxSentBoxListAdapter.addAll(messageLists);
            else
                inboxSentBoxListAdapter.addMore(messageLists);

            noData.setVisibility(View.INVISIBLE);
            loading = true;
        } else {
            if (pageNo == 1)
                noData.setVisibility(View.VISIBLE);
        }
        progress.setVisibility(View.INVISIBLE);
        if (swipeRefreshLayout.isRefreshing())
            swipeRefreshLayout.setRefreshing(false);
    }

    public void onEventMainThread(CommunicateOutboxEvent.Fail event) {
        if (event.getEx() != null) {
            TastyToast.makeText(getContext(), "Seems server is busy try after sometime.", TastyToast.LENGTH_LONG, TastyToast.ERROR).show();
        }
    }
}
