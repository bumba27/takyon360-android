package net.lasagu.takyon360.ui;

import android.Manifest;
import android.app.DownloadManager;
import android.content.Context;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.text.Html;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.sdsmdg.tastytoast.TastyToast;

import net.lasagu.takyon360.BaseActivity;
import net.lasagu.takyon360.R;
import net.lasagu.takyon360.models.AssessmentsBean;
import net.lasagu.takyon360.models.AttachmentsBean;
import net.lasagu.takyon360.models.ClassWorkBean;
import net.lasagu.takyon360.models.HomeWorkBean;
import net.lasagu.takyon360.ui.fragments.WeeklyPlanAssessmentFragment;
import net.lasagu.takyon360.ui.fragments.WeeklyPlanClassworkFragment;
import net.lasagu.takyon360.ui.fragments.WeeklyPlanHomeworkFragment;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static net.lasagu.takyon360.utils.ReusableClass.updateResources;

public class WeeklyPlanDetailsActivity extends BaseActivity {

    private static final int EXTERNAL_STORAGE_PERMISSION_RESPONSE = 1;
    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.htmlContent)
    WebView htmlContent;
    @BindView(R.id.attachmentIcon)
    TextView attachmentIcon;
    @BindView(R.id.attachmentLinearLayout)
    LinearLayout attachmentLinearLayout;
    private String attachmentUrl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details_with_attachments);
        ButterKnife.bind(this);
        updateResources(WeeklyPlanDetailsActivity.this);

        String type = getIntent().getStringExtra("CATEGORY_TYPE");
        if(type.equalsIgnoreCase(WeeklyPlanHomeworkFragment.class.getSimpleName())) {
            HomeWorkBean homeWorkBean = new Gson().fromJson(getIntent().getStringExtra("CATEGORY_DETAILS"), HomeWorkBean.class);
            loadingData(homeWorkBean);
        }else if(type.equalsIgnoreCase(WeeklyPlanClassworkFragment.class.getSimpleName())){
            ClassWorkBean classWorkBean = new Gson().fromJson(getIntent().getStringExtra("CATEGORY_DETAILS"), ClassWorkBean.class);
            loadingData(classWorkBean);
        }else if(type.equalsIgnoreCase(WeeklyPlanAssessmentFragment.class.getSimpleName())){
            AssessmentsBean assessmentsBean = new Gson().fromJson(getIntent().getStringExtra("CATEGORY_DETAILS"), AssessmentsBean.class);
            loadingData(assessmentsBean);
        }
    }

    private void loadingData(final HomeWorkBean categoryItemsBean) {
        title.setText(categoryItemsBean.getTopic());
        title.setVisibility(View.GONE);

        getSupportActionBar().setTitle(categoryItemsBean.getTopic());
        htmlContent.getSettings().setJavaScriptEnabled(true);

        htmlContent.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView view, int progress) {
                // Activities and WebViews measure progress with different scales.
                // The progress meter will automatically disappear when we reach 100%
                WeeklyPlanDetailsActivity.this.setProgress(progress * 1000);
            }
        });
        htmlContent.setWebViewClient(new WebViewClient() {
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                Toast.makeText(WeeklyPlanDetailsActivity.this, "Oh no! " + description, Toast.LENGTH_SHORT).show();
            }
        });

        htmlContent.setDownloadListener(new android.webkit.DownloadListener() {
            public void onDownloadStart(String url, String userAgent, String contentDisposition, String mimeType, long contentLength) {
                android.content.Intent i = new android.content.Intent(android.content.Intent.ACTION_VIEW);
                i.setData(android.net.Uri.parse(url));
                startActivity(i);
            }
        });

        htmlContent.loadDataWithBaseURL(null, Html.fromHtml(categoryItemsBean.getDescription()).toString(), "text/html", "utf-8", null);


        if (categoryItemsBean.getAttachIcon() == 1) {
            attachmentIcon.setVisibility(View.VISIBLE);
            for (int i = 0; i < categoryItemsBean.getAttachments().size(); i++) {
                View child = getLayoutInflater().inflate(R.layout.attachment_layout, null);
                final List<AttachmentsBean> attachment = categoryItemsBean.getAttachments();
                ((TextView)child.findViewById(R.id.attachmentText)).setText(attachment.get(i).getLinkName());
                final int finalI = i;
                ((ImageView)child.findViewById(R.id.attachmentDownloadIcon)).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        havingPermission(attachment.get(finalI).getLink());
                    }
                });
                attachmentLinearLayout.addView(child);
            }

        } else {
            attachmentIcon.setVisibility(View.INVISIBLE);
            attachmentLinearLayout.setVisibility(View.INVISIBLE);
        }
    }

    private void loadingData(final AssessmentsBean categoryItemsBean) {
        title.setText(categoryItemsBean.getTopic());

        htmlContent.getSettings().setJavaScriptEnabled(true);

        htmlContent.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView view, int progress) {
                // Activities and WebViews measure progress with different scales.
                // The progress meter will automatically disappear when we reach 100%
                WeeklyPlanDetailsActivity.this.setProgress(progress * 1000);
            }
        });
        htmlContent.setWebViewClient(new WebViewClient() {
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                Toast.makeText(WeeklyPlanDetailsActivity.this, "Oh no! " + description, Toast.LENGTH_SHORT).show();
            }
        });

        htmlContent.setDownloadListener(new android.webkit.DownloadListener() {
            public void onDownloadStart(String url, String userAgent, String contentDisposition, String mimeType, long contentLength) {
                android.content.Intent i = new android.content.Intent(android.content.Intent.ACTION_VIEW);
                i.setData(android.net.Uri.parse(url));
                startActivity(i);
            }
        });

        htmlContent.loadDataWithBaseURL(null, Html.fromHtml(categoryItemsBean.getDescription()).toString(), "text/html", "utf-8", null);

        if (categoryItemsBean.getAttachIcon() == 1) {
            attachmentIcon.setVisibility(View.VISIBLE);
            for (int i = 0; i < categoryItemsBean.getAttachments().size(); i++) {
                View child = getLayoutInflater().inflate(R.layout.attachment_layout, null);
                final List<AttachmentsBean> attachment = categoryItemsBean.getAttachments();
                ((TextView)child.findViewById(R.id.attachmentText)).setText(attachment.get(i).getLinkName());
                final int finalI = i;
                ((ImageView)child.findViewById(R.id.attachmentDownloadIcon)).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        havingPermission(attachment.get(finalI).getLink());
                    }
                });
                attachmentLinearLayout.addView(child);
            }

        } else {
            attachmentIcon.setVisibility(View.INVISIBLE);
            attachmentLinearLayout.setVisibility(View.INVISIBLE);
        }
    }

    private void loadingData(final ClassWorkBean categoryItemsBean) {
        title.setText(categoryItemsBean.getTopic());

        htmlContent.getSettings().setJavaScriptEnabled(true);

        htmlContent.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView view, int progress) {
                // Activities and WebViews measure progress with different scales.
                // The progress meter will automatically disappear when we reach 100%
                WeeklyPlanDetailsActivity.this.setProgress(progress * 1000);
            }
        });
        htmlContent.setWebViewClient(new WebViewClient() {
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                Toast.makeText(WeeklyPlanDetailsActivity.this, "Oh no! " + description, Toast.LENGTH_SHORT).show();
            }
        });

        htmlContent.setDownloadListener(new android.webkit.DownloadListener() {
            public void onDownloadStart(String url, String userAgent, String contentDisposition, String mimeType, long contentLength) {
                android.content.Intent i = new android.content.Intent(android.content.Intent.ACTION_VIEW);
                i.setData(android.net.Uri.parse(url));
                startActivity(i);
            }
        });

        htmlContent.loadDataWithBaseURL(null, Html.fromHtml(categoryItemsBean.getDescription()).toString(), "text/html", "utf-8", null);


        if (categoryItemsBean.getAttachIcon() == 1) {
            attachmentIcon.setVisibility(View.VISIBLE);
            for (int i = 0; i < categoryItemsBean.getAttachments().size(); i++) {
                View child = getLayoutInflater().inflate(R.layout.attachment_layout, null);
                final List<AttachmentsBean> attachment = categoryItemsBean.getAttachments();
                ((TextView)child.findViewById(R.id.attachmentText)).setText(attachment.get(i).getLinkName());
                final int finalI = i;
                ((ImageView)child.findViewById(R.id.attachmentDownloadIcon)).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        havingPermission(attachment.get(finalI).getLink());
                    }
                });
                attachmentLinearLayout.addView(child);
            }

        } else {
            attachmentIcon.setVisibility(View.INVISIBLE);
            attachmentLinearLayout.setVisibility(View.INVISIBLE);
        }
    }

    private void havingPermission(String attachmentUrl) {
        this.attachmentUrl = attachmentUrl;
        // Check if we have write permission
        int permission = ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (permission != PackageManager.PERMISSION_GRANTED)
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, EXTERNAL_STORAGE_PERMISSION_RESPONSE);
        else
            downloading(attachmentUrl);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case EXTERNAL_STORAGE_PERMISSION_RESPONSE: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                    downloading(attachmentUrl);
                else
                    TastyToast.makeText(this, "Sorry we need the permission.", Toast.LENGTH_LONG, TastyToast.ERROR).show();

                return;
            }
        }
    }

    private void downloading(final String url) {
        String fileName = url.substring(url.lastIndexOf('/') + 1);
        try {
            if (url != null && !url.isEmpty()) {
                Uri uri = Uri.parse(url.replaceAll(" " , "%20"));

                DownloadManager.Request request = new DownloadManager.Request(uri);
                request.setTitle(fileName);
                request.setDescription("Downloading attachment..");
                request.allowScanningByMediaScanner();
                request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
                request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, fileName);
                DownloadManager dm = (DownloadManager) this.getSystemService(Context.DOWNLOAD_SERVICE);
                dm.enqueue(request);
            }
        } catch (IllegalStateException e) {
            Toast.makeText(this, "Please insert an SD card to download file", Toast.LENGTH_SHORT).show();
        }

    }
}
