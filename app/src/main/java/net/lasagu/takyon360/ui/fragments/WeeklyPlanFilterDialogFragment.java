package net.lasagu.takyon360.ui.fragments;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.Gson;
import com.sdsmdg.tastytoast.TastyToast;

import net.lasagu.takyon360.MyApplication;
import net.lasagu.takyon360.R;
import net.lasagu.takyon360.events.LatestWeeklyPlanListResponseEvent;
import net.lasagu.takyon360.jobs.WeeklyPlanListJob;
import net.lasagu.takyon360.models.ClassesListResponse;
import net.lasagu.takyon360.models.DivisionsBean;
import net.lasagu.takyon360.models.SubjectListResponse;
import net.lasagu.takyon360.models.Subjectlist;
import net.lasagu.takyon360.models.WeeklyPlanSubmission;
import net.lasagu.takyon360.utils.PreferencesData;
import net.lasagu.takyon360.utils.ReusableClass;
import net.lasagu.takyon360.utils.SharedPreferenceStore;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;

import butterknife.BindColor;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.greenrobot.event.EventBus;

/**
 * Created by ajana on 06/07/2017.
 */

public class WeeklyPlanFilterDialogFragment extends DialogFragment {

    @BindView(R.id.EditTextSelectClass)
    EditText EditTextSelectClass;
    @BindView(R.id.EditTextFromDate)
    EditText EditTextFromDate;
    @BindView(R.id.EditTextToDate)
    EditText EditTextToDate;
    @BindView(R.id.SearchButton)
    Button SearchButton;
    @BindColor(R.color.colorPrimary)
    int colorPrimary;
    PopupMenu menuClass;
    PopupMenu menuSubject;
    @BindView(R.id.LatestButton)
    Button LatestButton;
    @BindView(R.id.EditTextSubject)
    EditText EditTextSubject;
    @BindView(R.id.toolBar)
    Toolbar toolBar;
    @BindView(R.id.TextInputLayoutClass)
    TextInputLayout TextInputLayoutClass;
    @BindView(R.id.TextInputLayoutSubject)
    TextInputLayout TextInputLayoutSubject;
    @BindView(R.id.TextInputLayoutFromDate)
    TextInputLayout TextInputLayoutFromDate;
    @BindView(R.id.TextInputLayoutToDate)
    TextInputLayout TextInputLayoutToDate;
    private HashMap<String, String> divisionHashMap;
    private HashMap<String, String> subjectHashMap;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.filter_weekly_plan_dialog_fragment, container, false);
        ButterKnife.bind(this, view);

        Bundle bundle = getArguments();
        String classesListResponseString = bundle.getString("CLASSES", "");
        String subjectsListResponseString = bundle.getString("SUBJECTS", "");

        toolBar.setTitle(bundle.getString("TITLE", ""));
        LatestButton.setText(bundle.getString("LATEST_VIEW_BUTTON_LEVEL", ""));


        TextInputLayoutFromDate.setHint(SharedPreferenceStore.getValue(getContext(), "START_DATE_LABEL", ""));
        TextInputLayoutToDate.setHint(SharedPreferenceStore.getValue(getContext(), "END_DATE_LABEL", ""));
        TextInputLayoutClass.setHint(SharedPreferenceStore.getValue(getContext(), "SELECT_CLASS_LABEL", ""));
        TextInputLayoutSubject.setHint(SharedPreferenceStore.getValue(getContext(), "SELECT_SUBJECT_LABEL", ""));

        populateClassesPopUp(classesListResponseString);
        populateSubjectPopUp(subjectsListResponseString);
        return view;
    }

    private void populateSubjectPopUp(String subjectsListResponseString) {
        SubjectListResponse subjectListResponse = new Gson().fromJson(subjectsListResponseString, SubjectListResponse.class);
        ArrayList<Subjectlist> subjectlist = subjectListResponse.getSubjectlist();
        subjectHashMap = new HashMap<>();
        menuSubject = new PopupMenu(getContext(), EditTextSubject);
        for (int i = 0; i < subjectlist.size(); i++) {
            menuSubject.getMenu().add(subjectlist.get(i).getSubject_name());
            subjectHashMap.put(subjectlist.get(i).getSubject_name(), subjectlist.get(i).getSubject_id());
        }

        menuSubject.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                String seletedTitle = item.getTitle().toString();
                EditTextSubject.setText(seletedTitle);
                EditTextSubject.setTag(subjectHashMap.get(seletedTitle));
                return false;
            }
        });
    }

    private void populateClassesPopUp(String classesListResponseString) {
        ClassesListResponse classesListResponse = new Gson().fromJson(classesListResponseString, ClassesListResponse.class);
        ArrayList<DivisionsBean> divisions = classesListResponse.getDivisions();
        divisionHashMap = new HashMap<>();
        menuClass = new PopupMenu(getContext(), EditTextSelectClass);
        for (int i = 0; i < divisions.size(); i++) {
            menuClass.getMenu().add(divisions.get(i).getDivision());
            divisionHashMap.put(divisions.get(i).getDivision(), divisions.get(i).getDivId());
        }

        menuClass.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                String seletedTitle = item.getTitle().toString();
                EditTextSelectClass.setText(seletedTitle);
                EditTextSelectClass.setTag(divisionHashMap.get(seletedTitle));
                return false;
            }
        });

        EditTextSelectClass.setText(divisions.get(0).getDivision());
        EditTextSelectClass.setTag(divisionHashMap.get(divisions.get(0).getDivision()));
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }

    @OnClick(R.id.EditTextSelectClass)
    public void selectingClasses(final View v) {
        menuClass.show();
    }

    @OnClick(R.id.EditTextSubject)
    public void selectingSubject(final View v) {
        menuSubject.show();
    }

    @OnClick({R.id.EditTextFromDate, R.id.EditTextToDate})
    public void selectingDate(final View v) {
        final Calendar myCalendar = Calendar.getInstance();
        DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                String myFormat = "dd/MM/yyyy"; //In which you need put here
                SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

                ((EditText) v).setText(sdf.format(myCalendar.getTime()));

                myCalendar.add(Calendar.DATE, 4);
                EditTextToDate.setText(sdf.format(myCalendar.getTime()));
            }
        };

        new DatePickerDialog(getContext(), date, myCalendar
                .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                myCalendar.get(Calendar.DAY_OF_MONTH)).show();
    }

    @OnClick(R.id.LatestButton)
    public void getLatest() {
        if (ReusableClass.isNetworkAvailable(getContext())) {

            EventBus.getDefault().post(new LatestWeeklyPlanListResponseEvent());
            dismiss();
        } else
            TastyToast.makeText(getContext(), "Sorry no internet connection.", Toast.LENGTH_LONG, TastyToast.ERROR).show();

    }

    @OnClick(R.id.SearchButton)
    public void searching() {
        if (validated()) {

            WeeklyPlanSubmission weeklyPlanSubmission = new WeeklyPlanSubmission();
            weeklyPlanSubmission.setType("");
            weeklyPlanSubmission.setFromDate(EditTextFromDate.getText().toString());
            weeklyPlanSubmission.setToDate(EditTextToDate.getText().toString());
            weeklyPlanSubmission.setIsLatest(0);
            weeklyPlanSubmission.setUserId(PreferencesData.getUserId(getContext()));
            weeklyPlanSubmission.setDiv_Id(EditTextSelectClass.getTag().toString());
            if (EditTextSubject.getTag() != null)
                weeklyPlanSubmission.setSub_Id(EditTextSubject.getTag().toString());

            if (ReusableClass.isNetworkAvailable(getContext())) {
                MyApplication.addJobInBackground(new WeeklyPlanListJob(weeklyPlanSubmission));
                dismiss();
            } else
                TastyToast.makeText(getContext(), "Sorry no internet connection.", Toast.LENGTH_LONG, TastyToast.ERROR).show();

        } else
            TastyToast.makeText(getContext(), "All fields are required.", TastyToast.LENGTH_LONG, TastyToast.ERROR).show();
    }

    private boolean validated() {
        if (TextUtils.isEmpty(EditTextSelectClass.getText()))
            return false;
        if (TextUtils.isEmpty(EditTextFromDate.getText()))
            return false;
        if (TextUtils.isEmpty(EditTextToDate.getText()))
            return false;

        return true;
    }
}