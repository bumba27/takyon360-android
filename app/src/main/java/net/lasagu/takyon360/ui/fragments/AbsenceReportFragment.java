package net.lasagu.takyon360.ui.fragments;


import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.sdsmdg.tastytoast.TastyToast;

import net.lasagu.takyon360.MyApplication;
import net.lasagu.takyon360.R;
import net.lasagu.takyon360.events.AbsenceHistoryEvent;
import net.lasagu.takyon360.jobs.GetAbsenceHistoryJob;
import net.lasagu.takyon360.models.AbsenceHistoryResponse;
import net.lasagu.takyon360.models.TransactionsBeanX;
import net.lasagu.takyon360.utils.PreferencesData;
import net.lasagu.takyon360.utils.ReusableClass;

import java.util.List;

import butterknife.BindColor;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import cn.pedant.SweetAlert.SweetAlertDialog;
import de.greenrobot.event.EventBus;

/**
 * A simple {@link Fragment} subclass.
 */
public class AbsenceReportFragment extends Fragment {


    @BindView(R.id.editTextName)
    EditText editTextName;
    @BindView(R.id.TextViewStudentDetails)
    TextView TextViewStudentDetails;
    @BindView(R.id.absenceDetailsText)
    TextView absenceDetailsText;
    @BindView(R.id.editTextClassDevision)
    EditText editTextClassDevision;
    @BindView(R.id.LinearLayoutAbsence)
    LinearLayout LinearLayoutAbsence;
    Unbinder unbinder;
    @BindColor(R.color.colorPrimary)
    int colorPrimary;
    @BindView(R.id.TextInputLayoutName)
    TextInputLayout TextInputLayoutName;
    @BindView(R.id.TextInputLayoutClassDivision)
    TextInputLayout TextInputLayoutClassDivision;
    private SweetAlertDialog pDialog;

    public AbsenceReportFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_absence_report, container, false);
        unbinder = ButterKnife.bind(this, view);

        populateUserData();
        return view;
    }

    private void populateUserData() {
        if (ReusableClass.isNetworkAvailable(getContext())) {
            showingProgress("Loading ...");
            MyApplication.addJobInBackground(new GetAbsenceHistoryJob(PreferencesData.getUserId(getContext())));
        } else
            TastyToast.makeText(getContext(), "Sorry no internet connection.", Toast.LENGTH_LONG, TastyToast.ERROR).show();
    }


    public void showingProgress(String title) {
        pDialog = new SweetAlertDialog(getContext(), SweetAlertDialog.PROGRESS_TYPE);
        pDialog.getProgressHelper().setBarColor(colorPrimary);
        pDialog.setTitleText(title);
        pDialog.setCancelable(false);
        pDialog.show();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    public void onEventMainThread(AbsenceHistoryEvent.Fail event) {
        if (event.getEx() != null) {
            pDialog.setTitleText("We Are Sorry ...");
            pDialog.setContentText(event.getEx());
            pDialog.changeAlertType(SweetAlertDialog.ERROR_TYPE);
        }
    }

    public void onEventMainThread(AbsenceHistoryEvent.Success event) {
        if (event.getAbsenceHistoryResponse().getStatusCode() == 1) {
            pDialog.setTitleText("Success!!!");
            pDialog.changeAlertType(SweetAlertDialog.SUCCESS_TYPE);

            AbsenceHistoryResponse absenceHistoryResponse = event.getAbsenceHistoryResponse();

            TextViewStudentDetails.setText(absenceHistoryResponse.getHeadLabel());
            TextInputLayoutName.setHint(absenceHistoryResponse.getNameLabel());
            TextInputLayoutClassDivision.setHint(absenceHistoryResponse.getClassLabel());
            editTextName.setText(absenceHistoryResponse.getName());
            editTextClassDevision.setText(absenceHistoryResponse.getDivision());
            absenceDetailsText.setText(absenceHistoryResponse.getHeadLabel());

            String dateLabel = absenceHistoryResponse.getDateLabel();
            String dayLabel = absenceHistoryResponse.getDayLabel();
            String typeLabel = absenceHistoryResponse.getTypeLabel();
            List<TransactionsBeanX> transactions = absenceHistoryResponse.getTransactions();

            float totalAmount = 0.0f;
            for (int i = 0; i < transactions.size(); i++) {
                final View child = getActivity().getLayoutInflater().inflate(R.layout.payment_history_row, null);

                ((TextView) child.findViewById(R.id.editTextReceipt)).setText(dayLabel + " " + transactions.get(i).getDay());
                ((TextView) child.findViewById(R.id.editTexDateTime)).setText(dateLabel + " " + transactions.get(i).getDate());
                ((TextView) child.findViewById(R.id.editTexamount)).setText(typeLabel + " " + transactions.get(i).getType());

                LinearLayoutAbsence.addView(child);
            }
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    pDialog.dismiss();
                }
            }, 800);
        } else {
            pDialog.setTitleText("We Are Sorry ...");
            pDialog.setContentText("Not be able to get your details, please try again.");
            pDialog.changeAlertType(SweetAlertDialog.ERROR_TYPE);
            pDialog.setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                @Override
                public void onClick(SweetAlertDialog sweetAlertDialog) {
                    pDialog.dismiss();
                }
            });
        }
    }
}
