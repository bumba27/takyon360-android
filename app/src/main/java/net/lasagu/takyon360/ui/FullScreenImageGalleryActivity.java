package net.lasagu.takyon360.ui;

import android.Manifest;
import android.app.DownloadManager;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.text.Html;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.jsibbold.zoomage.ZoomageView;
import com.sdsmdg.tastytoast.TastyToast;

import net.lasagu.takyon360.BaseActivity;
import net.lasagu.takyon360.R;
import net.lasagu.takyon360.models.GalleryItemsBean;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static net.lasagu.takyon360.utils.ReusableClass.updateResources;

public class FullScreenImageGalleryActivity extends BaseActivity {

    @BindView(R.id.myZoomageView)
    ZoomageView myZoomageView;
    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.imageViewArrowNext)
    ImageView imageViewArrowNext;
    @BindView(R.id.imageViewArrowPrevious)
    ImageView imageViewArrowPrevious;
    private ArrayList<GalleryItemsBean> galleryItemsBeans;
    private int currentImagePosition = 0;
    private String imageUrl;
    private static final int EXTERNAL_STORAGE_PERMISSION_RESPONSE = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_full_screen_gallery_image);
        ButterKnife.bind(this);
        updateResources(FullScreenImageGalleryActivity.this);

        imageUrl = getIntent().getStringExtra("IMAGE_URL");
        currentImagePosition = getIntent().getIntExtra("CURRENT_IMAGE_POSITION", 0);
        galleryItemsBeans = GalleryItemsBean.toList(getIntent().getStringExtra("GALLERY_ITEM_LIST"));
        Log.d("AJTAG", "SIZE: " + galleryItemsBeans.size());

        addImage(imageUrl, getIntent().getStringExtra("IMAGE_TITLE"));
    }

    private void addImage(String imageUrl, String titleText) {
        title.setText(Html.fromHtml(titleText));
        myZoomageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
        progressBar.setVisibility(View.VISIBLE);

        Glide.with(this).load(imageUrl).listener(new RequestListener<Drawable>() {
            @Override
            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                progressBar.setVisibility(View.GONE);
                return false;
            }

            @Override
            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                progressBar.setVisibility(View.GONE);
                return false;
            }
        }).into(myZoomageView);
        this.imageUrl = imageUrl;
    }

    @OnClick(R.id.imageViewArrowNext)
    public void clickingNext() {
        if (galleryItemsBeans.size() == currentImagePosition + 1) {
            TastyToast.makeText(this, "This is the last image of this album.", TastyToast.LENGTH_LONG, TastyToast.INFO).show();
        } else {
            addImage(galleryItemsBeans.get(currentImagePosition + 1).getMedia(), galleryItemsBeans.get(currentImagePosition).getGalleryTitle());
            currentImagePosition = currentImagePosition + 1;
        }
    }

    @OnClick(R.id.imageViewArrowPrevious)
    public void clickingPrevious() {
        if (currentImagePosition - 1 == -1) {
            TastyToast.makeText(this, "This is the first image of this album.", TastyToast.LENGTH_LONG, TastyToast.INFO).show();
        } else {
            addImage(galleryItemsBeans.get(currentImagePosition - 1).getMedia(), galleryItemsBeans.get(currentImagePosition).getGalleryTitle());
            currentImagePosition = currentImagePosition - 1;
        }
    }

    public void downloadingImage(View view) {
        if (imageUrl != null) {
            havingPermission(imageUrl);
        }
    }

    private void havingPermission(String attachmentUrl) {
        // Check if we have write permission
        int permission = ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (permission != PackageManager.PERMISSION_GRANTED)
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, EXTERNAL_STORAGE_PERMISSION_RESPONSE);
        else
            downloading(attachmentUrl);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case EXTERNAL_STORAGE_PERMISSION_RESPONSE: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                    downloading(imageUrl);
                else
                    TastyToast.makeText(this, "Sorry we need the permission.", Toast.LENGTH_LONG, TastyToast.ERROR).show();

                return;
            }
        }
    }

    private void downloading(final String url) {
        String fileName = url.substring(url.lastIndexOf('/') + 1);
        try {
            if (url != null && !url.isEmpty() && Patterns.WEB_URL.matcher(url).matches()) {
                Uri uri = Uri.parse(url.replaceAll(" ", "%20"));

                DownloadManager.Request request = new DownloadManager.Request(uri);
                request.setTitle(fileName);
                request.setDescription("Downloading attachment..");
                request.allowScanningByMediaScanner();
                request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
                request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, fileName);
                DownloadManager dm = (DownloadManager) this.getSystemService(Context.DOWNLOAD_SERVICE);
                dm.enqueue(request);
            } else
                TastyToast.makeText(this, "Not a valid url contact support.", TastyToast.LENGTH_SHORT, TastyToast.ERROR).show();

        } catch (IllegalStateException e) {
            Toast.makeText(this, "Please insert an SD card to download file", Toast.LENGTH_SHORT).show();
        }
    }

}
