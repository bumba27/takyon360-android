package net.lasagu.takyon360.ui.fragments;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.Toast;

import com.sdsmdg.tastytoast.TastyToast;

import net.lasagu.takyon360.MyApplication;
import net.lasagu.takyon360.R;
import net.lasagu.takyon360.events.CloseForgotPasswordDialogEvent;
import net.lasagu.takyon360.events.ForgotPasswordConfirmationEvent;
import net.lasagu.takyon360.jobs.VerifyForgetPasswordJob;
import net.lasagu.takyon360.models.ForgotPasswordConfirmationSubmission;
import net.lasagu.takyon360.utils.ReusableClass;

import butterknife.BindColor;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.pedant.SweetAlert.SweetAlertDialog;
import de.greenrobot.event.EventBus;

/**
 * Created by ajana on 06/07/2017.
 */

public class ForgotPasswordConfirmationDialogFragment extends DialogFragment {

    @BindView(R.id.EditTextOtp)
    EditText EditTextOtp;
    @BindView(R.id.EditTextNewEmail)
    EditText EditTextNewEmail;
    @BindView(R.id.EditTextNewPassword)
    EditText EditTextNewPassword;
    @BindView(R.id.EditTextConfirmPassword)
    EditText EditTextConfirmPassword;
    private SweetAlertDialog pDialog;
    @BindColor(R.color.colorPrimary)
    int colorPrimary;
    private String emailId;

    @SuppressLint("ValidFragment")
    public ForgotPasswordConfirmationDialogFragment() {

    }

    public ForgotPasswordConfirmationDialogFragment(String emailId) {
        this.emailId = emailId;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getDialog().setTitle("Verify email");
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        View view = inflater.inflate(R.layout.forgot_password_confirmation_dialog_fragment, container, false);
        ButterKnife.bind(this, view);

        return view;
    }

    @OnClick(R.id.VerifyButton)
    public void saving() {
        if (validated()) {
            ForgotPasswordConfirmationSubmission forgotPasswordConfirmationSubmission = new ForgotPasswordConfirmationSubmission();
            forgotPasswordConfirmationSubmission.setKey(EditTextOtp.getText().toString());
            forgotPasswordConfirmationSubmission.setVEmail(EditTextNewEmail.getText().toString());
            forgotPasswordConfirmationSubmission.setNewPassword(EditTextNewPassword.getText().toString());

            if (ReusableClass.isNetworkAvailable(getContext())) {
                showingProgress("Checking ...");
                MyApplication.addJobInBackground(new VerifyForgetPasswordJob(forgotPasswordConfirmationSubmission));
            } else
                TastyToast.makeText(getContext(), "Sorry no internet connection.", Toast.LENGTH_LONG, TastyToast.ERROR).show();

        } else
            TastyToast.makeText(getContext(), "All fields are required.", TastyToast.LENGTH_LONG, TastyToast.ERROR).show();
    }

    private boolean validated() {
        if (TextUtils.isEmpty(EditTextOtp.getText()))
            return false;
        if (TextUtils.isEmpty(EditTextNewEmail.getText()))
            return false;
        if (TextUtils.isEmpty(EditTextNewPassword.getText()))
            return false;
        if (TextUtils.isEmpty(EditTextConfirmPassword.getText()))
            return false;
        if (!EditTextNewPassword.getText().toString().equalsIgnoreCase(EditTextConfirmPassword.getText().toString())) {
            TastyToast.makeText(getContext(), "New password and Confirm password does not match.", TastyToast.LENGTH_LONG, TastyToast.ERROR).show();
            return false;
        }
        return true;
    }

    public void showingProgress(String title) {
        pDialog = new SweetAlertDialog(getContext(), SweetAlertDialog.PROGRESS_TYPE);
        pDialog.getProgressHelper().setBarColor(colorPrimary);
        pDialog.setTitleText(title);
        pDialog.setCancelable(false);
        pDialog.show();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    public void onEventMainThread(ForgotPasswordConfirmationEvent.Success event) {
        if (event.genericResponse().getStatusCode() == 1) {
            pDialog.setTitleText("Success!!!");
            pDialog.setContentText("Congratulation your password is updated.");
            pDialog.changeAlertType(SweetAlertDialog.SUCCESS_TYPE);
            pDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                @Override
                public void onClick(SweetAlertDialog sweetAlertDialog) {
                    pDialog.dismiss();
                    EventBus.getDefault().postSticky(new CloseForgotPasswordDialogEvent());
                    dismiss();
                }
            });
        } else {
            pDialog.setTitleText("We Are Sorry ...");
            pDialog.setContentText(event.genericResponse().getStatusMessage());
            pDialog.changeAlertType(SweetAlertDialog.ERROR_TYPE);
            pDialog.setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                @Override
                public void onClick(SweetAlertDialog sweetAlertDialog) {
                    pDialog.dismiss();
                }
            });
        }
    }

    public void onEventMainThread(ForgotPasswordConfirmationEvent.Fail event) {
        if (event.getEx() != null) {
            pDialog.setTitleText("We Are Sorry ...");
            pDialog.setContentText(event.getEx());
            pDialog.changeAlertType(SweetAlertDialog.ERROR_TYPE);
        }
    }
}