package net.lasagu.takyon360.ui.fragments;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.sdsmdg.tastytoast.TastyToast;

import net.lasagu.takyon360.R;
import net.lasagu.takyon360.utils.PreferencesData;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


/**
 * A simple {@link Fragment} subclass.
 */
public class LocatorFragment extends Fragment {


    @BindView(R.id.progress)
    ProgressBar progress;
    @BindView(R.id.messageTextView)
    TextView messageTextView;
    @BindView(R.id.ButtonDownload)
    Button ButtonDownload;
    private String TAG = LocatorFragment.class.getName();

    public LocatorFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_locator, null, false);
        ButterKnife.bind(this, view);
        try {
            Intent i = new Intent();
            i.setAction("net.lasagu.takyon360buzz");
            i.setType("text/plain");
            i.putExtra("LOGIN_DATA", PreferencesData.getUser(getContext()).toString());
            i.putExtra("USER_DATA", PreferencesData.getLoginResponse(getContext()).toString());
            startActivity(i);
            progress.setVisibility(View.GONE);
            messageTextView.setText("LOCATOR APP OPENED");
        } catch (Throwable throwable) {
            progress.setVisibility(View.GONE);
            messageTextView.setText("YOU NEED LOCATOR APP FOR THIS");
            ButtonDownload.setVisibility(View.VISIBLE);
            Log.e(TAG, "Error while opening Other App: " + throwable.getMessage());
            TastyToast.makeText(getContext(), "Looks like " + getResources().getString(R.string.dependant_app_name) + " is not installed.", Toast.LENGTH_LONG, TastyToast.ERROR).show();
        }
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @OnClick(R.id.ButtonDownload)
    public void downloading() {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=net.lasagu.takyon360buzz"));
        startActivity(browserIntent);
    }
}
