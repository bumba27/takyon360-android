package net.lasagu.takyon360.ui;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.adeel.library.easyFTP;
import com.bumptech.glide.load.ImageHeaderParser;
import com.google.gson.Gson;
import com.sdsmdg.tastytoast.TastyToast;
import com.tokenautocomplete.TokenCompleteTextView;

import net.lasagu.takyon360.BaseActivity;
import net.lasagu.takyon360.MyApplication;
import net.lasagu.takyon360.R;
import net.lasagu.takyon360.events.ComposeMailEvent;
import net.lasagu.takyon360.events.FtpDetailsEvent;
import net.lasagu.takyon360.events.GetGroupListEvent;
import net.lasagu.takyon360.events.GetMemeberListEvent;
import net.lasagu.takyon360.jobs.ComposeMailJob;
import net.lasagu.takyon360.jobs.FtpDetailsJob;
import net.lasagu.takyon360.jobs.GroupListJob;
import net.lasagu.takyon360.jobs.MemberListJob;
import net.lasagu.takyon360.models.AttachmentsBean;
import net.lasagu.takyon360.models.ComposeMailSubmission;
import net.lasagu.takyon360.models.FtpDetailsResponse;
import net.lasagu.takyon360.models.FtpSubmission;
import net.lasagu.takyon360.models.GenericResponse;
import net.lasagu.takyon360.models.Group;
import net.lasagu.takyon360.models.GroupsBean;
import net.lasagu.takyon360.models.LoginResponse;
import net.lasagu.takyon360.models.MemberNameSubmission;
import net.lasagu.takyon360.models.Members;
import net.lasagu.takyon360.models.MembersBean;
import net.lasagu.takyon360.models.MessageList;
import net.lasagu.takyon360.models.Person;
import net.lasagu.takyon360.models.RecipientsBean;
import net.lasagu.takyon360.models.UserIdAndSearchTextSubmission;
import net.lasagu.takyon360.utils.Constant;
import net.lasagu.takyon360.utils.PreferencesData;
import net.lasagu.takyon360.utils.ReusableClass;
import net.lasagu.takyon360.utils.SharedPreferenceStore;
import net.lasagu.takyon360.views.ContactsCompletionView;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import butterknife.BindColor;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.pedant.SweetAlert.SweetAlertDialog;
import de.greenrobot.event.EventBus;
import droidninja.filepicker.FilePickerBuilder;
import droidninja.filepicker.FilePickerConst;
import jp.wasabeef.richeditor.RichEditor;

import static net.lasagu.takyon360.R.id.editor;
import static net.lasagu.takyon360.utils.ReusableClass.updateResources;

public class ComposeNewMailActivity extends BaseActivity {

    private final int IMAGE_REQUEST = 1;
    @BindView(editor)
    RichEditor mEditor;
    @BindView(R.id.topProgressBar)
    ProgressBar topProgressBar;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    SweetAlertDialog pDialog;
    @BindColor(R.color.colorPrimary)
    int colorPrimary;
    @BindView(R.id.editTextSubject)
    EditText editTextSubject;
    @BindView(R.id.CustomAutoCompleteGroups)
    ContactsCompletionView CustomAutoCompleteGroups;
    @BindView(R.id.CustomAutoCompleteMember)
    ContactsCompletionView CustomAutoCompleteMember;
    @BindView(R.id.CustomAutoCompleteMemberCc)
    ContactsCompletionView CustomAutoCompleteMemberCc;
    @BindView(R.id.HorizontalScrollViewEditor)
    HorizontalScrollView HorizontalScrollViewEditor;
    @BindView(R.id.LinearLayoutDocLayout)
    LinearLayout LinearLayoutDocLayout;
    @BindView(R.id.LinearLayoutImageLayout)
    LinearLayout LinearLayoutImageLayout;
    private String[] PERMISSIONS_IMAGE = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.CAMERA
    };
    private ArrayList<String> photoPaths = new ArrayList<>();
    private ArrayList<String> removedPhotoPaths = new ArrayList<>();
    private ArrayList<String> removedDocPaths = new ArrayList<>();
    private ArrayList<String> docPaths = new ArrayList<>();
    private ArrayList<AttachmentsBean> attachments;
    private int totalPhotos = 0;
    private int uploadedPhotos = 0;
    private int totalDocs = 0;
    private int uploadedDocs = 0;
    private String TAG = ComposeNewMailActivity.class.getSimpleName();
    private AttachmentsBean attachment;
    private boolean docUploading = false;
    private ArrayList<RecipientsBean> selectedRecipients = new ArrayList<>();
    ;
    private String mailType = "";
    private ComposeMailSubmission composeMailSubmission;
    private ArrayList<Integer> selectedGroupIds = new ArrayList<>();
    private HashMap<Integer, Integer> groupHashMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_compose_new_mail);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        updateResources(ComposeNewMailActivity.this);

        composeMailSubmission = new ComposeMailSubmission();

        if (getIntent().hasExtra("TITLE")) {
            getSupportActionBar().setTitle(getIntent().getStringExtra("TITLE"));
        }

        CustomAutoCompleteMemberCc.setHint(SharedPreferenceStore.getValue(this, "MAIL_Cc_label", ""));
        CustomAutoCompleteMember.setHint(SharedPreferenceStore.getValue(this, "MAIL_To_label", ""));
        CustomAutoCompleteGroups.setHint(SharedPreferenceStore.getValue(this, "MAIL_Group_label", ""));
        editTextSubject.setHint(SharedPreferenceStore.getValue(this, "MAIL_Subject_label", ""));

        mailType = getIntent().getStringExtra("TYPE");
        if (mailType.equalsIgnoreCase(Constant.REPLY_ALL_MAIL)) {
            setTitle(getString(R.string.reply_all));

            MessageList messageDetailsResponse = new Gson().fromJson(getIntent().getStringExtra("MESSAGE_CONTENT"), MessageList.class);
            setTitle(getString(R.string.reply_all));

            editTextSubject.setText("Re: " + messageDetailsResponse.getSujbect());
            composeMailSubmission.setParentMsgId(Integer.parseInt(messageDetailsResponse.getId()));
            mEditor.setHtml("<br><br>" + messageDetailsResponse.getMessage());

            Group[] groups = messageDetailsResponse.getRecipients().getGroup();
            Members[] members = messageDetailsResponse.getRecipients().getMembers();

            for (int i = 0; i < groups.length; i++) {
                String groupId = groups[i].getGroupId();
                String groupName = groups[i].getGroupName();

                selectedGroupIds.add(Integer.valueOf(groupId));
                addingMemberView(selectedGroupIds);

                CustomAutoCompleteGroups.addObject(new Person(groupName, groupName, Integer.parseInt(groupId), 0));
            }

            for (int i = 0; i < members.length; i++) {
//                String name = removingBrackets(members[i].getName());
                String name = members[i].getName();
                int userId = Integer.parseInt(members[i].getUserId());
                int groupId = Integer.parseInt(members[i].getGroupId());
                int recipientType = Integer.parseInt(members[i].getRecipientType());

                RecipientsBean recipientsBean = new RecipientsBean();
                recipientsBean.setName(name);
                recipientsBean.setGroupId(groupId);
                recipientsBean.setUserId(userId);
                recipientsBean.setRecipientType(recipientType);

                selectedRecipients.add(recipientsBean);

                if (recipientType == 0)
                    CustomAutoCompleteMember.addObject(new Person(name, name, userId, groupId));
                else if (recipientType == 1)
                    CustomAutoCompleteMemberCc.addObject(new Person(name, name, userId, groupId));
            }
        } else if (mailType.equalsIgnoreCase(Constant.REPLY_MAIL)) {
            setTitle("Reply");
            CustomAutoCompleteGroups.setVisibility(View.GONE);
            CustomAutoCompleteMemberCc.setVisibility(View.GONE);

            MessageList messageDetailsResponse = new Gson().fromJson(getIntent().getStringExtra("MESSAGE_CONTENT"), MessageList.class);
            editTextSubject.setText("Re: " + messageDetailsResponse.getSujbect());
            composeMailSubmission.setParentMsgId(Integer.parseInt(messageDetailsResponse.getId()));
            mEditor.setHtml("<br><br>" + messageDetailsResponse.getMessage());

            String senderName = messageDetailsResponse.getSender();
            int senderID = Integer.parseInt(messageDetailsResponse.getSenderID());

            RecipientsBean recipientsBean = new RecipientsBean();
            recipientsBean.setName(senderName);
            recipientsBean.setGroupId(0);
            recipientsBean.setUserId(senderID);
            recipientsBean.setRecipientType(0);

            selectedRecipients.add(recipientsBean);

            CustomAutoCompleteMember.addObject(new Person(senderName, senderName, senderID, 0));

        } else if (mailType.equalsIgnoreCase(Constant.FORWARD_MAIL)) {
            setTitle("Forward Mail ");
            MessageList messageDetailsResponse = new Gson().fromJson(getIntent().getStringExtra("MESSAGE_CONTENT"), MessageList.class);
            editTextSubject.setText("Fwd: " + messageDetailsResponse.getSujbect());

            Members[] members = messageDetailsResponse.getRecipients().getMembers();
            String sendTo = "";
            for (int i = 0; i < members.length; i++) {
                sendTo = sendTo + members[i].getName();
                if (i != members.length - 1)
                    sendTo = sendTo + " ,";
            }
            mEditor.setHtml("<br><br>---------- Forwarded message ---------"
                    + "<br><br>"
                    + "From: "
                    + messageDetailsResponse.getSender()
                    + "<br><br>"
                    + "Date: " + messageDetailsResponse.getDate()
                    + "<br><br>"
                    + "Subject: " + messageDetailsResponse.getSujbect()
                    + "<br><br>"
                    + "To: " + sendTo
                    + "<br><br>"
                    + messageDetailsResponse.getMessage()
                    + "<br>");
            composeMailSubmission.setParentMsgId(Integer.parseInt(messageDetailsResponse.getId()));
        } else if (mailType.equalsIgnoreCase(Constant.NEW_MAIL)) {
            composeMailSubmission.setParentMsgId(0);
            setTitle("Compose New Mail");
        }


        addingHtmlEditor();
        addingGroupView();
        CustomAutoCompleteGroups.setThreshold(1);
        CustomAutoCompleteGroups.allowDuplicates(false);
        CustomAutoCompleteGroups.setTokenListener(new TokenCompleteTextView.TokenListener<Person>() {
            @Override
            public void onTokenAdded(Person person) {
                if (!selectedGroupIds.contains(person.getId())) {
                    selectedGroupIds.add(person.getId());
                    addingMemberView(selectedGroupIds);
                } else
                    Log.d("AJTAG", "Group Id already added");
            }

            @Override
            public void onTokenRemoved(Person person) {
                for (int i = 0; i < selectedGroupIds.size(); i++) {
                    if (selectedGroupIds.get(i) == person.getId()) {
                        selectedGroupIds.remove(i);
                        addingMemberView(selectedGroupIds);
                    }
                }
            }
        });
        CustomAutoCompleteMember.setThreshold(1);
        CustomAutoCompleteMember.allowDuplicates(false);
        CustomAutoCompleteMember.setTokenListener(new TokenCompleteTextView.TokenListener<Person>() {
            @Override
            public void onTokenAdded(Person parentId) {

                RecipientsBean recipientsBean = new RecipientsBean();
                recipientsBean.setName(parentId.getName());
                recipientsBean.setGroupId(parentId.getParentId());
                recipientsBean.setUserId(parentId.getId());
                recipientsBean.setRecipientType(0);

                selectedRecipients.add(recipientsBean);
            }

            @Override
            public void onTokenRemoved(Person person) {
                RecipientsBean recipientsBean = new RecipientsBean();
                recipientsBean.setName(person.getName());
                recipientsBean.setGroupId(person.getParentId());
                recipientsBean.setUserId(person.getId());
                recipientsBean.setRecipientType(0);

                selectedRecipients.remove(recipientsBean);
            }
        });
        CustomAutoCompleteMemberCc.setThreshold(1);
        CustomAutoCompleteMemberCc.setTokenListener(new TokenCompleteTextView.TokenListener<Person>() {
            @Override
            public void onTokenAdded(Person person) {

                RecipientsBean recipientsBean = new RecipientsBean();
                recipientsBean.setName(person.getName());
                recipientsBean.setGroupId(person.getParentId());
                recipientsBean.setUserId(person.getId());
                recipientsBean.setRecipientType(1);

                selectedRecipients.add(recipientsBean);
            }

            @Override
            public void onTokenRemoved(Person person) {
                RecipientsBean recipientsBean = new RecipientsBean();
                recipientsBean.setName(person.getName());
                recipientsBean.setGroupId(person.getParentId());
                recipientsBean.setUserId(person.getId());
                recipientsBean.setRecipientType(1);

                selectedRecipients.remove(recipientsBean);
            }
        });

        CustomAutoCompleteMemberCc.allowDuplicates(false);
    }

    @OnClick(R.id.CustomAutoCompleteGroups)
    public void showingGroups() {
        CustomAutoCompleteGroups.showDropDown();
    }

    @OnClick(R.id.CustomAutoCompleteMember)
    public void showingMember() {
        CustomAutoCompleteMember.showDropDown();
    }

    @OnClick(R.id.CustomAutoCompleteMemberCc)
    public void showingMemeberCc() {
        CustomAutoCompleteMemberCc.showDropDown();
    }

    private void addingMemberView(ArrayList selectedGroupIds) {
        if (ReusableClass.isNetworkAvailable(this)) {
            topProgressBar.setVisibility(View.VISIBLE);

            String stringGroupIds = "";
            for (int i = 0; i < selectedGroupIds.size(); i++) {
                stringGroupIds += selectedGroupIds.get(i);
                if (selectedGroupIds.size() - 1 != i)
                    stringGroupIds += ",";
            }

            MemberNameSubmission memberNameSubmission = new MemberNameSubmission();
            memberNameSubmission.setUserId(PreferencesData.getUserId(this));
            memberNameSubmission.setSearchText("");
            memberNameSubmission.setGrp_id(stringGroupIds);

            MyApplication.addJobInBackground(new MemberListJob(memberNameSubmission));
        } else
            TastyToast.makeText(this, "Sorry no internet connection.", Toast.LENGTH_LONG, TastyToast.ERROR).show();
    }

    private void addingGroupView() {

        if (ReusableClass.isNetworkAvailable(this)) {
            topProgressBar.setVisibility(View.VISIBLE);

            UserIdAndSearchTextSubmission userIdAndSearchTextSubmission = new UserIdAndSearchTextSubmission();
            userIdAndSearchTextSubmission.setSearchText("");
            userIdAndSearchTextSubmission.setUserId(PreferencesData.getUserId(this));

            MyApplication.addJobInBackground(new GroupListJob(userIdAndSearchTextSubmission));
        } else
            TastyToast.makeText(this, "Sorry no internet connection.", Toast.LENGTH_LONG, TastyToast.ERROR).show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.compose_new_mail_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_attach) {
            attaching();
            return true;
        }
        if (id == R.id.action_send) {
            sendingMail();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void sendingMail() {
        if (ReusableClass.isNetworkAvailable(this)) {
            if (validate()) {
                pDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
                pDialog.getProgressHelper().setBarColor(colorPrimary);
                pDialog.setTitleText("Please wait ...");
                pDialog.setCancelable(false);
                pDialog.show();

                gettingFtpDetails();
            }
        } else {
            TastyToast.makeText(this, "Sorry no internet connection.", Toast.LENGTH_LONG, TastyToast.ERROR).show();
        }
    }

    private void uploadPhotos(FtpDetailsResponse ftpDetailsResponse) {
        if (photoPaths.size() > 0) {
            pDialog.setContentText("Uploading photos ...");
            for (int i = 0; i < photoPaths.size(); i++) {
                if (!removedPhotoPaths.contains(photoPaths.get(i)))
                    new UploadFileTask().execute(ftpDetailsResponse, photoPaths.get(i), Constant.IMAGE);
            }
        } else
            uploadDocs(ftpDetailsResponse);
    }

    private void uploadDocs(FtpDetailsResponse ftpDetailsResponse) {
        if (docPaths.size() > 0) {
            docUploading = true;
            pDialog.setContentText("Uploading documents ...");
            for (int i = 0; i < docPaths.size(); i++) {
                if (!removedDocPaths.contains(docPaths.get(i)))
                    new UploadFileTask().execute(ftpDetailsResponse, docPaths.get(i), Constant.DOC);
            }
        } else
            sendMessageContent();
    }

    private boolean validate() {
        LoginResponse loginResponse = PreferencesData.getLoginResponse(this);
        String userType = loginResponse.getUserType();

        if (userType.equalsIgnoreCase(Constant.PARENT) || userType.equalsIgnoreCase(Constant.STUDENT)) {
            if (!mailType.equalsIgnoreCase(Constant.REPLY_MAIL)) {
                if (CustomAutoCompleteGroups.getText().toString().equalsIgnoreCase("")) {
                    TastyToast.makeText(this, "Sorry please select a group.", Toast.LENGTH_LONG, TastyToast.ERROR).show();
                    return false;
                }
                if (selectedRecipients == null || selectedRecipients.size() == 0) {
                    TastyToast.makeText(this, "Sorry please select a proper person to send.", Toast.LENGTH_LONG, TastyToast.ERROR).show();
                    return false;
                }
            } else {
                if (selectedRecipients == null || selectedRecipients.size() == 0) {
                    TastyToast.makeText(this, "Sorry please select a proper person to send.", Toast.LENGTH_LONG, TastyToast.ERROR).show();
                    return false;
                }
            }
        } else {
            if (!mailType.equalsIgnoreCase(Constant.REPLY_MAIL)) {
                if (CustomAutoCompleteGroups.getText().toString().equalsIgnoreCase("")) {
                    TastyToast.makeText(this, "Sorry please select a group.", Toast.LENGTH_LONG, TastyToast.ERROR).show();
                    return false;
                }
            } else {
                if (selectedRecipients == null || selectedRecipients.size() == 0) {
                    TastyToast.makeText(this, "Sorry please select a proper person to send.", Toast.LENGTH_LONG, TastyToast.ERROR).show();
                    return false;
                }
            }
        }

        if (TextUtils.isEmpty(editTextSubject.getText().toString())) {
            TastyToast.makeText(this, "Please add some subject.", Toast.LENGTH_LONG, TastyToast.ERROR).show();
            return false;
        }
        return true;
    }

    public void attaching() {
        // Check if we have write permission
        int permission1 = ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE);
        int permission2 = ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA);

        if (permission1 != PackageManager.PERMISSION_GRANTED || permission2 != PackageManager.PERMISSION_GRANTED) {
            // We don't have permission so prompt the user
            ActivityCompat.requestPermissions(
                    this,
                    PERMISSIONS_IMAGE,
                    IMAGE_REQUEST
            );
        } else {
            openingAttachmentOption();
        }
    }

    private void openingAttachmentOption() {
        new SweetAlertDialog(ComposeNewMailActivity.this, SweetAlertDialog.WARNING_TYPE)
                .setTitleText("Confirm please")
                .setContentText("You want to attach Photo or Doc?")
                .setConfirmText("Photos !")
                .setCancelText("Documents !")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        sweetAlertDialog.dismissWithAnimation();
                        FilePickerBuilder.getInstance().setMaxCount(5)
                                .setSelectedFiles(photoPaths)
                                .setActivityTheme(R.style.AppTheme)
                                .pickPhoto(ComposeNewMailActivity.this);
                    }
                })
                .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        sweetAlertDialog.dismissWithAnimation();
                        FilePickerBuilder.getInstance().setMaxCount(5)
                                .setSelectedFiles(docPaths)
                                .setActivityTheme(R.style.AppTheme)
                                .pickFile(ComposeNewMailActivity.this);
                    }
                })
                .show();
    }

    private void addingHtmlEditor() {
        //mEditor.setPlaceholder("Compose email");
        mEditor.setPadding(10, 10, 10, 10);
        mEditor.setEditorFontSize(18);

        findViewById(R.id.action_undo).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mEditor.undo();
            }
        });

        findViewById(R.id.action_redo).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mEditor.redo();
            }
        });

        findViewById(R.id.action_bold).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mEditor.setBold();
            }
        });

        findViewById(R.id.action_italic).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mEditor.setItalic();
            }
        });

        findViewById(R.id.action_subscript).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mEditor.setSubscript();
            }
        });

        findViewById(R.id.action_superscript).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mEditor.setSuperscript();
            }
        });

        findViewById(R.id.action_strikethrough).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mEditor.setStrikeThrough();
            }
        });

        findViewById(R.id.action_underline).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mEditor.setUnderline();
            }
        });

        findViewById(R.id.action_heading1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mEditor.setHeading(1);
            }
        });

        findViewById(R.id.action_heading2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mEditor.setHeading(2);
            }
        });

        findViewById(R.id.action_heading3).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mEditor.setHeading(3);
            }
        });

        findViewById(R.id.action_heading4).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mEditor.setHeading(4);
            }
        });

        findViewById(R.id.action_heading5).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mEditor.setHeading(5);
            }
        });

        findViewById(R.id.action_heading6).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mEditor.setHeading(6);
            }
        });

        findViewById(R.id.action_txt_color).setOnClickListener(new View.OnClickListener() {
            private boolean isChanged;

            @Override
            public void onClick(View v) {
                mEditor.setTextColor(isChanged ? Color.BLACK : Color.RED);
                isChanged = !isChanged;
            }
        });

        findViewById(R.id.action_bg_color).setOnClickListener(new View.OnClickListener() {
            private boolean isChanged;

            @Override
            public void onClick(View v) {
                mEditor.setTextBackgroundColor(isChanged ? Color.TRANSPARENT : Color.YELLOW);
                isChanged = !isChanged;
            }
        });

        findViewById(R.id.action_indent).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mEditor.setIndent();
            }
        });

        findViewById(R.id.action_outdent).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mEditor.setOutdent();
            }
        });

        findViewById(R.id.action_align_left).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mEditor.setAlignLeft();
            }
        });

        findViewById(R.id.action_align_center).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mEditor.setAlignCenter();
            }
        });

        findViewById(R.id.action_align_right).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mEditor.setAlignRight();
            }
        });

        findViewById(R.id.action_blockquote).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mEditor.setBlockquote();
            }
        });

        findViewById(R.id.action_insert_bullets).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mEditor.setBullets();
            }
        });

        findViewById(R.id.action_insert_numbers).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mEditor.setNumbers();
            }
        });

//        findViewById(R.id.action_insert_image).setOnClickListener(new View.OnClickListener() {
//            @Override public void onClick(View v) {
//                mEditor.insertImage("http://www.image_link/123.jpg",
//                        "ImageName");
//            }
//        });
//
//        findViewById(R.id.action_insert_link).setOnClickListener(new View.OnClickListener() {
//            @Override public void onClick(View v) {
//                mEditor.insertLink("https://google.com", "LinkName");
//            }
//        });

        findViewById(R.id.action_insert_checkbox).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mEditor.insertTodo();
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case IMAGE_REQUEST: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                    openingAttachmentOption();
                else
                    TastyToast.makeText(this, "Sorry we need the permission.", Toast.LENGTH_LONG, TastyToast.ERROR).show();

                return;
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case FilePickerConst.REQUEST_CODE_PHOTO:
                if (resultCode == Activity.RESULT_OK && data != null) {
                    photoPaths = new ArrayList<>();
                    photoPaths.addAll(data.getStringArrayListExtra(FilePickerConst.KEY_SELECTED_MEDIA));

                    addToView(FilePickerConst.KEY_SELECTED_MEDIA, photoPaths);
                }
                break;

            case FilePickerConst.REQUEST_CODE_DOC:
                if (resultCode == Activity.RESULT_OK && data != null) {
                    docPaths = new ArrayList<>();
                    docPaths.addAll(data.getStringArrayListExtra(FilePickerConst.KEY_SELECTED_DOCS));

                    addToView(FilePickerConst.KEY_SELECTED_DOCS, docPaths);
                }
                break;
        }

        addThemToView(photoPaths, docPaths);
    }

    private void addToView(String selectedMedia, final ArrayList<String> paths) {
        if (selectedMedia.equalsIgnoreCase(FilePickerConst.KEY_SELECTED_MEDIA))
            LinearLayoutImageLayout.removeAllViews();
        else
            LinearLayoutDocLayout.removeAllViews();

        for (int i = 0; i < paths.size(); i++) {
            final View child = getLayoutInflater().inflate(R.layout.attached_file_row, null);
            final String photoPath = paths.get(i);
            String filename = photoPath.substring(photoPath.lastIndexOf("/") + 1);

            ReusableClass.setVectorBackground(ComposeNewMailActivity.this, R.drawable.ic_close_black_24dp, (ImageView) child.findViewById(R.id.ImageViewDelete), R.color.colorPrimary);
            ((TextView) child.findViewById(R.id.title)).setText(filename);
            if (selectedMedia.equalsIgnoreCase(FilePickerConst.KEY_SELECTED_MEDIA)) {
                LinearLayoutImageLayout.addView(child);
                final int finalI = i;
                ((ImageView) child.findViewById(R.id.ImageViewDelete)).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        LinearLayoutImageLayout.removeView(child);
                        removedPhotoPaths.add(paths.get(finalI));
                    }
                });
            } else {
                LinearLayoutDocLayout.addView(child);
                final int finalI1 = i;
                ((ImageView) child.findViewById(R.id.ImageViewDelete)).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        LinearLayoutDocLayout.removeView(child);
                        removedDocPaths.add(paths.get(finalI1));
                    }
                });
            }
        }
    }

    private void addThemToView(ArrayList<String> photoPaths, ArrayList<String> docPaths) {
        int totalFile = 0;
        if (photoPaths.size() > 0)
            totalFile = photoPaths.size();

        if (docPaths.size() > 0)
            totalFile += docPaths.size();

    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    public void onEventMainThread(final GetGroupListEvent.Success event) {
        if (event.getGroupListResponse().getStatusCode() == 1) {
            topProgressBar.setVisibility(View.INVISIBLE);

            ArrayList<String> options = new ArrayList<>();

            final List<GroupsBean> groups = event.getGroupListResponse().getGroups();
            groupHashMap = new HashMap<>();

            Person[] people = new Person[groups.size()];
            ArrayAdapter<Person> adapter;

            for (int i = 0; i < groups.size(); i++) {
                String groupName = groups.get(i).getName();
                int groupId = groups.get(i).getId();
                people[i] = new Person(groups.get(i).getName(), groups.get(i).getName(), groupId, 0);

                groupHashMap.put(i, groupId);
                options.add(groupName);
            }

            adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, people);
            CustomAutoCompleteGroups.setAdapter(adapter);

            topProgressBar.setVisibility(View.INVISIBLE);
        } else {
            topProgressBar.setVisibility(View.INVISIBLE);
            TastyToast.makeText(this, "Something went wrong while getting Group Name.", TastyToast.LENGTH_LONG, TastyToast.SUCCESS).show();
        }
    }

    public void onEventMainThread(GetGroupListEvent.Fail event) {
        topProgressBar.setVisibility(View.INVISIBLE);

        if (event.getEx() != null)
            settingError(event.getEx());
    }

    public void onEventMainThread(GetMemeberListEvent.Success event) {
        if (event.getMemberListResponse().getStatusCode() == 1) {
            topProgressBar.setVisibility(View.INVISIBLE);
            final List<MembersBean> members = event.getMemberListResponse().getMembers();

            final Person[] people = new Person[members.size()];
            ArrayAdapter<Person> adapter;

            for (int i = 0; i < members.size(); i++) {
                String memberName = members.get(i).getName();
                int memberId = Integer.parseInt(members.get(i).getId());
                int groupId = members.get(i).getGroupId();

//                memberName = removingBrackets(memberName);
                people[i] = new Person(memberName, memberName, memberId, groupId);
            }


            adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, people);
            CustomAutoCompleteMember.setAdapter(adapter);
            CustomAutoCompleteMemberCc.setAdapter(adapter);

            topProgressBar.setVisibility(View.INVISIBLE);
        } else {
            topProgressBar.setVisibility(View.INVISIBLE);
            final Person[] people = new Person[0];
            ArrayAdapter<Person> adapter;

            adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, people);
            CustomAutoCompleteMember.setAdapter(adapter);
            CustomAutoCompleteMemberCc.setAdapter(adapter);

            //TastyToast.makeText(this, "Looks like no member in this group.", TastyToast.LENGTH_LONG, TastyToast.CONFUSING).show();
        }
    }

    private String removingBrackets(String name) {
        if (name.toString().indexOf("(") != -1)
            return name.replaceAll("\\(.*\\)", "");
        else
            return name;
    }

    public void onEventMainThread(GetMemeberListEvent.Fail event) {
        topProgressBar.setVisibility(View.INVISIBLE);
        if (event.getEx() != null)
            settingError(event.getEx());
    }

    public void gettingFtpDetails() {
        FtpSubmission ftpSubmission = new FtpSubmission();
        ftpSubmission.setIsMobile(1);
        ftpSubmission.setUserId(PreferencesData.getUserId(this));

        reInitializeUploadVeriable();
        MyApplication.addJobInBackground(new FtpDetailsJob(ftpSubmission));
    }

    private void reInitializeUploadVeriable() {
        attachments = new ArrayList<>();
        totalPhotos = photoPaths.size();
        uploadedPhotos = 0;
        totalDocs = docPaths.size();
        uploadedDocs = 0;
        docUploading = false;
    }

    public void onEventMainThread(final FtpDetailsEvent.Success event) {
        FtpDetailsResponse ftpDetailsResponse = event.getFtpDetailsResponse();
        if (ftpDetailsResponse.getStatusCode() == 1) {
            uploadPhotos(ftpDetailsResponse);
        } else {
            settingError("Sorry unable to send the message. Unable to get FTP details.");
        }
    }

    public void onEventMainThread(FtpDetailsEvent.Fail event) {
        if (event.getEx() != null) {
            settingError(event.getEx());
        }
    }

    private void sendMessageContent() {
        pDialog.setContentText("Sending mail ...");

        composeMailSubmission.setUserId(PreferencesData.getUserId(this));
        composeMailSubmission.setIsMobile(1);
        composeMailSubmission.setAttachments(attachments);
        composeMailSubmission.setSubject(editTextSubject.getText().toString());
        composeMailSubmission.setGroupId(selectedGroupIds);
        composeMailSubmission.setMessage(ReusableClass.getBase64(mEditor.getHtml()));
        composeMailSubmission.setRecipients(selectedRecipients);

        if (attachments.size() > 0)
            composeMailSubmission.setAttachIcon(1);
        else
            composeMailSubmission.setAttachIcon(0);

        if (docPaths.size() > 0 || photoPaths.size() > 0)
            composeMailSubmission.setAttachIcon(1);
        else
            composeMailSubmission.setAttachIcon(0);

        MyApplication.addJobInBackground(new ComposeMailJob(composeMailSubmission));
    }

    private void settingError(String message) {
        pDialog.setTitleText("Sorry ...");
        pDialog.setContentText(message);
        pDialog.changeAlertType(SweetAlertDialog.ERROR_TYPE);
        pDialog.setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                pDialog.dismiss();
            }
        });
    }

    public void onEventMainThread(final ComposeMailEvent.Success event) {
        GenericResponse genericResponse = event.getGenericResponse();
        if (genericResponse.getStatusCode() == 1) {
            pDialog.setTitleText("Success!!!");
            pDialog.changeAlertType(SweetAlertDialog.SUCCESS_TYPE);

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    pDialog.dismiss();
                    finish();
                }
            }, 800);
        } else {
            settingError("Sorry unable to send the message.");
        }
    }

    public void onEventMainThread(ComposeMailEvent.Fail event) {
        if (event.getEx() != null) {
            settingError(event.getEx());
        }
    }

    class UploadFileTask extends AsyncTask<Object, Void, String> {
        private String fileName = "";
        private String fileType = "";
        private FtpDetailsResponse ftpDetailsResponse;

        @Override
        protected String doInBackground(Object... params) {
            try {
                ftpDetailsResponse = (FtpDetailsResponse) params[0];
                String filePath = (String) params[1];
                fileType = (String) params[2];

                InputStream inputStream = null;
                String fileExtension = ReusableClass.getFileExtention(filePath);

                if (fileType.equalsIgnoreCase(Constant.IMAGE)) {
                    // Uploading Images
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy_MM_dd_HH:mm:ss.SSS");
                    String currentDateAndTime = sdf.format(new Date());
                    if (fileExtension.toUpperCase().equalsIgnoreCase("JPEG") || fileExtension.toUpperCase().equalsIgnoreCase("JPG")) {
                        fileName = currentDateAndTime + ".jpeg";
                        inputStream = ReusableClass.getCompressedInputStream(filePath, ImageHeaderParser.ImageType.JPEG);
                    } else if (fileExtension.toUpperCase().equalsIgnoreCase("PNG")) {
                        fileName = currentDateAndTime + ".png";
                        inputStream = ReusableClass.getCompressedInputStream(filePath, ImageHeaderParser.ImageType.PNG);
                    }
                } else if (fileType.equalsIgnoreCase(Constant.DOC)) {

                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy_MM_dd_HH:mm:ss.SSS");
                    String currentDateAndTime = sdf.format(new Date());

                    fileName = currentDateAndTime + "." + fileExtension;
                    File file = new File(filePath);
                    inputStream = new FileInputStream(file);
                }

                byte[] tmp2 = Base64.decode(ftpDetailsResponse.getPassword(), Base64.DEFAULT);
                String password = new String(tmp2, "UTF-8");


                easyFTP ftp = new easyFTP();
                ftp.connect(ftpDetailsResponse.getServer(),
                        ftpDetailsResponse.getUserName(),
                        password);
                ftp.uploadFile(inputStream, fileName);

                return fileName;
            } catch (Exception e) {
                Log.d(TAG, "doInBackground: Failure : " + e.getLocalizedMessage());
                return "error";
            }
        }

        @Override
        protected void onPostExecute(String fileName) {
            if (fileName.equalsIgnoreCase("error")) {
                settingError("Attachment upload failed.");
            } else {
                if (fileType.equalsIgnoreCase(Constant.IMAGE))
                    uploadedPhotos = uploadedPhotos + 1;
                if (fileType.equalsIgnoreCase(Constant.DOC))
                    uploadedDocs = uploadedDocs + 1;

                attachment = new AttachmentsBean();
                attachment.setLink(ftpDetailsResponse.getServerPath() + fileName);
                attachment.setLinkName(fileName);
                attachments.add(attachment);

                if (totalPhotos == uploadedPhotos) {
                    if (totalDocs != uploadedDocs) {
                        if (photoPaths.size() != 0 && !docUploading)
                            uploadDocs(ftpDetailsResponse);
                        else
                            Log.d(TAG, "Already called document upload");
                    } else
                        sendMessageContent();
                }
            }
        }
    }
}
