package net.lasagu.takyon360.ui;

import android.Manifest;
import android.app.DownloadManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.text.Html;
import android.util.Patterns;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.sdsmdg.tastytoast.TastyToast;

import net.lasagu.takyon360.BaseActivity;
import net.lasagu.takyon360.R;
import net.lasagu.takyon360.models.CategoryItemsBean;
import net.lasagu.takyon360.models.MultipleAttachments;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static net.lasagu.takyon360.utils.ReusableClass.updateResources;

public class DigitalResourceDetailsActivity extends BaseActivity {

    private static final int EXTERNAL_STORAGE_PERMISSION_RESPONSE = 1;
    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.htmlContent)
    WebView htmlContent;
    @BindView(R.id.attachmentIcon)
    TextView attachmentIcon;
    @BindView(R.id.attachmentLinearLayout)
    LinearLayout attachmentLinearLayout;
    private String attachmentUrl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details_with_attachments);
        ButterKnife.bind(this);
        updateResources(DigitalResourceDetailsActivity.this);

        CategoryItemsBean categoryItemsBean = new Gson().fromJson(getIntent().getStringExtra("CATEGORY_DETAILS"), CategoryItemsBean.class);
        loadingData(categoryItemsBean);
    }

    private void loadingData(final CategoryItemsBean categoryItemsBean) {
        title.setText(categoryItemsBean.getTitle());
        title.setVisibility(View.GONE);
        getSupportActionBar().setTitle(categoryItemsBean.getTitle());

        htmlContent.getSettings().setJavaScriptEnabled(true);
        htmlContent.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                if (url.contains("https://www.manhal.com") || url.contains(".pdf")) {
                    Intent i = new Intent(Intent.ACTION_VIEW);
                    i.setData(Uri.parse(url));
                    startActivity(i);
                } else
                    view.loadUrl(url);
                return true;
            }
        });

        htmlContent.loadDataWithBaseURL(null, Html.fromHtml(categoryItemsBean.getContent()).toString(), "text/html", "utf-8", null);

        if (categoryItemsBean.getMultipleAttachment().size() > 0) {
            attachmentIcon.setVisibility(View.VISIBLE);
            for (int i = 0; i < categoryItemsBean.getMultipleAttachment().size(); i++) {
                View child = getLayoutInflater().inflate(R.layout.attachment_layout, null);
                final List<MultipleAttachments> attachment = categoryItemsBean.getMultipleAttachment();
                ((TextView) child.findViewById(R.id.attachmentText)).setText(attachment.get(i).getFileName());
                final int finalI = i;
                ((ImageView) child.findViewById(R.id.attachmentDownloadIcon)).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        havingPermission(attachment.get(finalI).getFilelink());
                    }
                });
                attachmentLinearLayout.addView(child);
            }
        } else if (categoryItemsBean.getAttachment() != null && !categoryItemsBean.getAttachment().isEmpty()) {
            final String attachment = categoryItemsBean.getAttachment();
            attachmentIcon.setVisibility(View.VISIBLE);
            View child = getLayoutInflater().inflate(R.layout.attachment_layout, null);
            final String fileName = categoryItemsBean.getTitle();
            ((TextView) child.findViewById(R.id.attachmentText)).setText(fileName);
            ((ImageView) child.findViewById(R.id.attachmentDownloadIcon)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    havingPermission(attachment);
                }
            });
            attachmentLinearLayout.addView(child);

        } else {
            attachmentIcon.setVisibility(View.INVISIBLE);
            attachmentLinearLayout.setVisibility(View.INVISIBLE);
        }
    }

    private void havingPermission(String attachmentUrl) {
        this.attachmentUrl = attachmentUrl;
        // Check if we have write permission
        int permission = ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (permission != PackageManager.PERMISSION_GRANTED)
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, EXTERNAL_STORAGE_PERMISSION_RESPONSE);
        else
            downloading(attachmentUrl);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case EXTERNAL_STORAGE_PERMISSION_RESPONSE: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                    downloading(attachmentUrl);
                else
                    TastyToast.makeText(this, "Sorry we need the permission.", Toast.LENGTH_LONG, TastyToast.ERROR).show();

                return;
            }
        }
    }

    private void downloading(final String url) {
        String fileName = url.substring(url.lastIndexOf('/') + 1);
        try {
            if (url != null && !url.isEmpty() && Patterns.WEB_URL.matcher(url).matches()) {
                Uri uri = Uri.parse(url.replaceAll(" ", "%20"));

                DownloadManager.Request request = new DownloadManager.Request(uri);
                request.setTitle(fileName);
                request.setDescription("Downloading attachment..");
                request.allowScanningByMediaScanner();
                request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
                request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, fileName);
                DownloadManager dm = (DownloadManager) this.getSystemService(Context.DOWNLOAD_SERVICE);
                dm.enqueue(request);
            } else
                TastyToast.makeText(this, "Not a valid url contact support.", TastyToast.LENGTH_SHORT, TastyToast.ERROR).show();

        } catch (IllegalStateException e) {
            Toast.makeText(this, "Please insert an SD card to download file", Toast.LENGTH_SHORT).show();
        }
    }
}
