package net.lasagu.takyon360.ui;

import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.crashlytics.android.Crashlytics;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.gson.Gson;
import com.sdsmdg.tastytoast.TastyToast;

import net.lasagu.takyon360.BaseActivity;
import net.lasagu.takyon360.MyApplication;
import net.lasagu.takyon360.R;
import net.lasagu.takyon360.events.DeviceRegisterEvent;
import net.lasagu.takyon360.events.LoadNotificationEvent;
import net.lasagu.takyon360.events.MenuOpenEvent;
import net.lasagu.takyon360.events.MyCalenderEvent;
import net.lasagu.takyon360.events.SiblingChangeEvent;
import net.lasagu.takyon360.jobs.RegisterPushTokenJob;
import net.lasagu.takyon360.jobs.SiblingLoginJob;
import net.lasagu.takyon360.models.GcmTokenRegistrationSubmission;
import net.lasagu.takyon360.models.LoginResponse;
import net.lasagu.takyon360.models.MenuList;
import net.lasagu.takyon360.models.Siblings;
import net.lasagu.takyon360.models.UserDetails;
import net.lasagu.takyon360.ui.fragments.AbsenceReportFragment;
import net.lasagu.takyon360.ui.fragments.AwarenessAndPoliciesFragment;
import net.lasagu.takyon360.ui.fragments.CalendarFragment;
import net.lasagu.takyon360.ui.fragments.CommunicateFragment;
import net.lasagu.takyon360.ui.fragments.DigitalResourceFragment;
import net.lasagu.takyon360.ui.fragments.FeeDetailsFragment;
import net.lasagu.takyon360.ui.fragments.FeeSummaryFragment;
import net.lasagu.takyon360.ui.fragments.GalleryFragment;
import net.lasagu.takyon360.ui.fragments.HomeFragment;
import net.lasagu.takyon360.ui.fragments.LocatorFragment;
import net.lasagu.takyon360.ui.fragments.MyProfileFragment;
import net.lasagu.takyon360.ui.fragments.NoticeBoardFragment;
import net.lasagu.takyon360.ui.fragments.PaymentHistoryFragment;
import net.lasagu.takyon360.ui.fragments.WebViewFragment;
import net.lasagu.takyon360.ui.fragments.WeeklyPlanFragment;
import net.lasagu.takyon360.utils.Constant;
import net.lasagu.takyon360.utils.PreferencesData;
import net.lasagu.takyon360.utils.ReusableClass;
import net.lasagu.takyon360.utils.SharedPreferenceStore;

import java.util.Locale;

import butterknife.BindColor;
import butterknife.BindView;
import butterknife.ButterKnife;
import cn.pedant.SweetAlert.SweetAlertDialog;
import de.greenrobot.event.EventBus;
import de.hdodenhof.circleimageview.CircleImageView;
import io.fabric.sdk.android.Fabric;

import static net.lasagu.takyon360.utils.ReusableClass.updateResources;

public class MainActivity extends BaseActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.content_frame)
    FrameLayout contentFrame;
    @BindView(R.id.fab)
    FloatingActionButton fab;
    @BindView(R.id.nav_view)
    NavigationView navigationView;
    boolean doubleBackToExitPressedOnce = false;
    @BindColor(R.color.colorPrimary)
    int colorPrimary;
    private PopupMenu popup;
    private SweetAlertDialog pDialog;
    private int siblingPosition = 0;
    private CircleImageView schoolImage;
    private TextView usernameTextView;
    private TextView userEmailTextView;
    private LinearLayout userFullLayout;
    private ImageView dropDownArraowImageView;
    private String TAG = MainActivity.class.getName();
    private int studentId = 0;
    private DrawerLayout drawer;
    private TextView textViewSchoolName;
    private TextView textViewParentName;
    private String reportCardLink = "";
    private String controlPanel1 = "";
    private String controlPanel2 = "";
    private String studentBehaviour = "";
    private String dailyActivity = "";
    private String detailsAttendance = "";
    private String timeTable = "";
    private String absenceReport = "";

    private Menu menu;
    private String homeLevel = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        updateResources(MainActivity.this);

        Fabric.with(this, new Crashlytics());
        Crashlytics.setString("AppName: ", getString(R.string.app_name));
        Crashlytics.setUserName(PreferencesData.getUser(this).getUsername());
        Crashlytics.setString("Password: ", PreferencesData.getUser(this).getPassword());

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        navigationView.setNavigationItemSelectedListener(this);
        navigationView.setItemIconTintList(null);
        setMenu();
        setUpMenuHeader();
        selectingDefaultFragment();
        drawer.openDrawer(Gravity.LEFT);

        registeringGcmToken();
    }

    private void registeringGcmToken() {
        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(new OnSuccessListener<InstanceIdResult>() {
            @Override
            public void onSuccess(InstanceIdResult instanceIdResult) {
                sendRegistrationToServer(instanceIdResult.getToken());
            }
        });
    }

    private void sendRegistrationToServer(String token) {
        GcmTokenRegistrationSubmission gcmTokenRegistrationSubmission = new GcmTokenRegistrationSubmission();
        gcmTokenRegistrationSubmission.setPhoneType("0");
        gcmTokenRegistrationSubmission.setRegId(token);
        gcmTokenRegistrationSubmission.setUserId(Integer.parseInt(PreferencesData.getUserId(this)));

        MyApplication.addJobInBackground(new RegisterPushTokenJob(gcmTokenRegistrationSubmission));
    }


    private void setMenu() {
        menu = navigationView.getMenu();
        LoginResponse loginResponse = PreferencesData.getLoginResponse(this);
        menu.findItem(R.id.nav_my_profile).setTitle(Html.fromHtml(loginResponse.getProfileLabel()));

        MenuList[] menuList = PreferencesData.getLoginResponse(this).getMenuList();
        for (int i = 0; i < menuList.length; i++) {
            switch (menuList[i].getHashKey()) {
                case "T0011":
                    menu.findItem(R.id.nav_notice_board).setTitle(Html.fromHtml(menuList[i].getLabel()));
                    menu.findItem(R.id.nav_notice_board).setVisible(true);
                    break;
                case "T0001":
                    menu.findItem(R.id.nav_home).setTitle(Html.fromHtml(menuList[i].getLabel()));
                    menu.findItem(R.id.nav_home).setVisible(true);
                    homeLevel = Html.fromHtml(menuList[i].getLabel()).toString();
                    break;
                case "T0002":
                    menu.findItem(R.id.nav_calender).setTitle(Html.fromHtml(menuList[i].getLabel()));
                    menu.findItem(R.id.nav_calender).setVisible(true);
                    break;
                case "T0018":
                    menu.findItem(R.id.nav_gallery).setTitle(Html.fromHtml(menuList[i].getLabel()));
                    menu.findItem(R.id.nav_gallery).setVisible(true);
                    break;
                case "T0004":
                    menu.findItem(R.id.nav_academics).setTitle(Html.fromHtml(menuList[i].getLabel()));
                    menu.findItem(R.id.nav_academics).setVisible(true);
                    break;
                case "T0019":
                    menu.findItem(R.id.nav_awareness_and_policies).setTitle(Html.fromHtml(menuList[i].getLabel()));
                    menu.findItem(R.id.nav_awareness_and_policies).setVisible(true);
                    break;
                case "T0021":
                    menu.findItem(R.id.nav_digital_resource).setTitle(Html.fromHtml(menuList[i].getLabel()));
                    menu.findItem(R.id.nav_digital_resource).setVisible(true);
                    break;
                case "T0005":
                    menu.findItem(R.id.nav_weekly_plan).setTitle(Html.fromHtml(menuList[i].getLabel()));
                    menu.findItem(R.id.nav_weekly_plan).setVisible(true);
                    break;
                case "T0039":
                    UserDetails userDetails = PreferencesData.getUser(this);
                    menu.findItem(R.id.nav_control_panel_1).setTitle(Html.fromHtml(menuList[i].getLabel()));
                    controlPanel1 = menuList[i].getLink() + "?name1=" + userDetails.getUsername() + "&pass1=" + ReusableClass.getMd5(userDetails.getPassword());
                    menu.findItem(R.id.nav_control_panel_1).setVisible(true);
                    break;
                case "T0040":
                    menu.findItem(R.id.nav_control_panel_2).setTitle(Html.fromHtml(menuList[i].getLabel()));
                    controlPanel2 = menuList[i].getLink();
                    menu.findItem(R.id.nav_control_panel_2).setVisible(true);
                    break;
                case "T0012":
                    menu.findItem(R.id.nav_report_card).setTitle(Html.fromHtml(menuList[i].getLabel()));
                    reportCardLink = menuList[i].getLink();
                    menu.findItem(R.id.nav_report_card).setVisible(true);
                    break;
                case "T0041":
                    menu.findItem(R.id.nav_time_table).setTitle(Html.fromHtml(menuList[i].getLabel()));
                    timeTable = menuList[i].getLink();
                    menu.findItem(R.id.nav_time_table).setVisible(true);
                    break;
                case "T0038":
                    menu.findItem(R.id.nav_absence_report).setTitle(Html.fromHtml(menuList[i].getLabel()));
                    absenceReport = menuList[i].getLink();
                    menu.findItem(R.id.nav_absence_report).setVisible(true);
                    break;
                case "T0008":
                    menu.findItem(R.id.nav_finance).setTitle(Html.fromHtml(menuList[i].getLabel()));
                    menu.findItem(R.id.nav_finance).setVisible(true);
                    break;
                case "T0035":
                    menu.findItem(R.id.nav_fee_summary).setTitle(Html.fromHtml(menuList[i].getLabel()));
                    menu.findItem(R.id.nav_fee_summary).setVisible(true);
                    break;
                case "T0055":
                    menu.findItem(R.id.nav_transport_payment_details).setTitle(Html.fromHtml(menuList[i].getLabel()));
                    menu.findItem(R.id.nav_transport_payment_details).setVisible(true);
                    break;
                case "T0036":
                    menu.findItem(R.id.nav_fee_details).setTitle(Html.fromHtml(menuList[i].getLabel()));
                    menu.findItem(R.id.nav_fee_details).setVisible(true);
                    break;
                case "T0037":
                    menu.findItem(R.id.nav_payment_history).setTitle(Html.fromHtml(menuList[i].getLabel()));
                    menu.findItem(R.id.nav_payment_history).setVisible(true);
                    break;
                case "T0009":
                    menu.findItem(R.id.nav_communicate).setTitle(Html.fromHtml(menuList[i].getLabel()));
                    menu.findItem(R.id.nav_communicate).setVisible(true);
                    break;
                case "T0010":
                    menu.findItem(R.id.nav_locator).setTitle(Html.fromHtml(menuList[i].getLabel()));
                    menu.findItem(R.id.nav_locator).setVisible(true);
                    break;
                case "T0058":
                    UserDetails user = PreferencesData.getUser(this);
                    menu.findItem(R.id.nav_student_behaviour).setTitle(Html.fromHtml(menuList[i].getLabel()));
                    studentBehaviour = menuList[i].getLink() + "?user_name=" + user.getUsername() + "&password=" + ReusableClass.getMd5(user.getPassword());
                    menu.findItem(R.id.nav_student_behaviour).setVisible(true);
                    break;
                case "T0059_3":
                    menu.findItem(R.id.nav_daily_activity).setTitle(Html.fromHtml(menuList[i].getLabel()));
                    dailyActivity = menuList[i].getLink();
                    menu.findItem(R.id.nav_daily_activity).setVisible(true);
                    break;
                case "T0059_4":
                    menu.findItem(R.id.nav_details_attendance).setTitle(Html.fromHtml(menuList[i].getLabel()));
                    detailsAttendance = menuList[i].getLink();
                    menu.findItem(R.id.nav_details_attendance).setVisible(true);
                    break;
                default:
                    break;
            }
        }
    }

    public void selectingDefaultFragment() {
        navigationView.getMenu().getItem(0).setChecked(true);
        displayView(navigationView.getMenu().getItem(0).getItemId());
    }

    @Override
    public void onBackPressed() {
        final Fragment fragmentInFrame = getSupportFragmentManager().findFragmentById(R.id.content_frame);
        if (fragmentInFrame instanceof HomeFragment) {
            if (!drawer.isDrawerOpen(GravityCompat.START)) {
                drawer.openDrawer(GravityCompat.START);
            } else {
                if (doubleBackToExitPressedOnce) {
                    super.onBackPressed();
                    return;
                }

                this.doubleBackToExitPressedOnce = true;
                Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

                new Handler().postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        doubleBackToExitPressedOnce = false;
                    }
                }, 1500);
            }

        } else {
            Bundle bundle = new Bundle();
            bundle.putInt("SIBLINGS_ID", studentId);

            Fragment fragment = new HomeFragment();
            fragment.setArguments(bundle);
            getSupportActionBar().setTitle(homeLevel);

            replaceFragment(fragment);
            selectingDefaultFragment();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        MenuItem item1 = menu.findItem(R.id.action_logout);

        String loginResponseString = SharedPreferenceStore.getValue(this, PreferencesData.LOGIN_RESPONSE, "");
        LoginResponse loginResponse = new Gson().fromJson(loginResponseString, LoginResponse.class);
        item1.setTitle(loginResponse.getLogoutLabel());

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_logout) {
            loggingOut();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        displayView(id);

        return true;
    }

    private void setUpMenuHeader() {
        View hView = navigationView.getHeaderView(0);
        schoolImage = (CircleImageView) hView.findViewById(R.id.schoolImage);
        usernameTextView = (TextView) hView.findViewById(R.id.username);
        userEmailTextView = (TextView) hView.findViewById(R.id.userEmail);
        textViewSchoolName = (TextView) hView.findViewById(R.id.textViewSchoolName);
        textViewParentName = (TextView) hView.findViewById(R.id.textViewParentName);
        userFullLayout = (LinearLayout) hView.findViewById(R.id.userLayout);
        dropDownArraowImageView = (ImageView) hView.findViewById(R.id.dropDownArraowImageView);

        popup = new PopupMenu(MainActivity.this, usernameTextView);

        String loginResponseString = SharedPreferenceStore.getValue(this, PreferencesData.LOGIN_RESPONSE, "");
        LoginResponse loginResponse = new Gson().fromJson(loginResponseString, LoginResponse.class);

        if (loginResponse.getLogoPath() != null && !loginResponse.getLogoPath().equalsIgnoreCase(""))
            Glide.with(this).load(loginResponse.getLogoPath()).into(schoolImage);
        else
            schoolImage.setImageDrawable(getDrawable(R.drawable.school_default));

        if (loginResponse.getUserType().equalsIgnoreCase(Constant.PARENT) || loginResponse.getUserType().equalsIgnoreCase(Constant.STUDENT)) {
            textViewSchoolName.setText(loginResponse.getSchoolName());
            textViewParentName.setText(loginResponse.getName());

            Siblings[] siblingses = loginResponse.getSiblings();

            if (siblingses.length > 0) {
                usernameTextView.setText(siblingses[siblingPosition].getStudentName());
                usernameTextView.setTag(siblingses[siblingPosition].getUserId());
                userEmailTextView.setText("Class - " + siblingses[siblingPosition].getClassName());

                studentId = Integer.parseInt(siblingses[siblingPosition].getUserId());

                for (int i = 0; i < siblingses.length; i++) {
                    popup.getMenu().add(0, Integer.parseInt(siblingses[i].getUserId()), i, siblingses[i].getStudentName());
                }

                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    public boolean onMenuItemClick(MenuItem item) {
                        siblingPosition = item.getOrder();
                        Log.d(TAG, "siblingPosition: " + siblingPosition);
                        Log.d(TAG, "sibling id: " + item.getItemId());

                        if (!usernameTextView.getTag().toString().equalsIgnoreCase(String.valueOf(item.getItemId()))) {
                            if (ReusableClass.isNetworkAvailable(MainActivity.this)) {
                                showingProgress();
                                studentId = item.getItemId();
                                MyApplication.addJobInBackground(new SiblingLoginJob(String.valueOf(item.getItemId())));
                            } else
                                TastyToast.makeText(MainActivity.this, "Sorry no internet connection.", Toast.LENGTH_LONG, TastyToast.ERROR).show();
                        }
                        return true;
                    }
                });

                userFullLayout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        popup.show();
                    }
                });
            } else {
                TastyToast.makeText(MainActivity.this, "Please contact admin no sibling found.", TastyToast.LENGTH_SHORT, TastyToast.ERROR).show();
            }
        } else {
            studentId = Integer.parseInt(loginResponse.getUserId());
            textViewSchoolName.setText(loginResponse.getSchoolName());

            usernameTextView.setText(loginResponse.getName());
            userEmailTextView.setText(loginResponse.getSchoolName());

            dropDownArraowImageView.setVisibility(View.INVISIBLE);
            textViewParentName.setVisibility(View.GONE);
        }
    }

    public void displayView(int id) {
        Fragment fragment = null;
        navigationView.setCheckedItem(id);

        if (id == R.id.nav_home) {
            Bundle bundle = new Bundle();
            bundle.putInt("SIBLINGS_ID", studentId);
            fragment = new HomeFragment();
            fragment.setArguments(bundle);
            getSupportActionBar().setTitle(menu.findItem(R.id.nav_home).getTitle());
        } else if (id == R.id.nav_my_profile) {
            fragment = new MyProfileFragment();
            getSupportActionBar().setTitle(menu.findItem(R.id.nav_my_profile).getTitle());
        } else if (id == R.id.nav_locator) {
            fragment = new LocatorFragment();
            getSupportActionBar().setTitle(menu.findItem(R.id.nav_locator).getTitle());
        } else if (id == R.id.nav_gallery) {
            fragment = new GalleryFragment();
            getSupportActionBar().setTitle(menu.findItem(R.id.nav_gallery).getTitle());
        } else if (id == R.id.nav_fee_summary) {
            fragment = new FeeSummaryFragment();
            getSupportActionBar().setTitle(menu.findItem(R.id.nav_fee_summary).getTitle());
        } else if (id == R.id.nav_transport_payment_details) {
            getSupportActionBar().setTitle(menu.findItem(R.id.nav_transport_payment_details).getTitle());
            fragment = new FeeSummaryFragment();
            Bundle bundle = new Bundle();
            bundle.putBoolean(FeeSummaryFragment.isTransportPaymentDetails, true);
            fragment.setArguments(bundle);
        } else if (id == R.id.nav_fee_details) {
            getSupportActionBar().setTitle(menu.findItem(R.id.nav_fee_details).getTitle());
            fragment = new FeeDetailsFragment();
        } else if (id == R.id.nav_payment_history) {
            getSupportActionBar().setTitle(menu.findItem(R.id.nav_payment_history).getTitle());
            fragment = new PaymentHistoryFragment();
        } else if (id == R.id.nav_communicate) {
            fragment = new CommunicateFragment();
            getSupportActionBar().setTitle(menu.findItem(R.id.nav_communicate).getTitle());
        } else if (id == R.id.nav_report_card) {
            Bundle bundle = new Bundle();
            bundle.putString("WEB_URL", reportCardLink);
            fragment = new WebViewFragment();
            fragment.setArguments(bundle);
            getSupportActionBar().setTitle(menu.findItem(R.id.nav_report_card).getTitle());
        } else if (id == R.id.nav_notice_board) {
            fragment = new NoticeBoardFragment();
            getSupportActionBar().setTitle(menu.findItem(R.id.nav_notice_board).getTitle());
        } else if (id == R.id.nav_calender) {
            fragment = new CalendarFragment();
            getSupportActionBar().setTitle(menu.findItem(R.id.nav_calender).getTitle());
        } else if (id == R.id.nav_digital_resource) {
            fragment = new DigitalResourceFragment();
            getSupportActionBar().setTitle(menu.findItem(R.id.nav_digital_resource).getTitle());
        } else if (id == R.id.nav_weekly_plan) {
            fragment = new WeeklyPlanFragment();
            getSupportActionBar().setTitle(menu.findItem(R.id.nav_weekly_plan).getTitle());
        } else if (id == R.id.nav_awareness_and_policies) {
            fragment = new AwarenessAndPoliciesFragment();
            getSupportActionBar().setTitle(menu.findItem(R.id.nav_awareness_and_policies).getTitle());
        } else if (id == R.id.nav_control_panel_1) {
            Bundle bundle = new Bundle();
            bundle.putString("WEB_URL", controlPanel1);
            fragment = new WebViewFragment();
            fragment.setArguments(bundle);
            getSupportActionBar().setTitle(menu.findItem(R.id.nav_control_panel_1).getTitle());
        } else if (id == R.id.nav_control_panel_2) {
            Bundle bundle = new Bundle();
            bundle.putString("WEB_URL", controlPanel2);
            fragment = new WebViewFragment();
            fragment.setArguments(bundle);
            getSupportActionBar().setTitle(menu.findItem(R.id.nav_control_panel_2).getTitle());
        } else if (id == R.id.nav_time_table) {
            Bundle bundle = new Bundle();
            bundle.putString("WEB_URL", timeTable);
            fragment = new WebViewFragment();
            fragment.setArguments(bundle);
            getSupportActionBar().setTitle(menu.findItem(R.id.nav_time_table).getTitle());
        } else if (id == R.id.nav_absence_report) {
            fragment = new AbsenceReportFragment();
            getSupportActionBar().setTitle(menu.findItem(R.id.nav_absence_report).getTitle());
        } else if (id == R.id.nav_student_behaviour) {
            Bundle bundle = new Bundle();
            bundle.putString("WEB_URL", studentBehaviour);
            fragment = new WebViewFragment();
            fragment.setArguments(bundle);
            getSupportActionBar().setTitle(menu.findItem(R.id.nav_student_behaviour).getTitle());
        } else if (id == R.id.nav_student_behaviour) {
            Bundle bundle = new Bundle();
            bundle.putString("WEB_URL", dailyActivity);
            fragment = new WebViewFragment();
            fragment.setArguments(bundle);
            getSupportActionBar().setTitle(menu.findItem(R.id.nav_student_behaviour).getTitle());
        } else if (id == R.id.nav_daily_activity) {
            Bundle bundle = new Bundle();
            bundle.putString("WEB_URL", dailyActivity);
            fragment = new WebViewFragment();
            fragment.setArguments(bundle);
            getSupportActionBar().setTitle(menu.findItem(R.id.nav_daily_activity).getTitle());
        } else if (id == R.id.nav_details_attendance) {
            Bundle bundle = new Bundle();
            bundle.putString("WEB_URL", detailsAttendance);
            fragment = new WebViewFragment();
            fragment.setArguments(bundle);
            getSupportActionBar().setTitle(menu.findItem(R.id.nav_details_attendance).getTitle());
        }

        replaceFragment(fragment);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
    }

    private void replaceFragment(Fragment fragment) {
        if (fragment != null) {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.content_frame, fragment);
            ft.commit();
        }
    }

    public void loggingOut() {
        new SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE)
                .setTitleText("Confirm please")
                .setContentText("Are you sure, you want to logout?")
                .setConfirmText("Yes, Please!")
                .setCancelText("Want to stay")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        sweetAlertDialog.dismissWithAnimation();
                        PreferencesData.resetUser(MainActivity.this);
                        Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                        finish();
                        startActivity(intent);
                    }
                })
                .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        sweetAlertDialog.dismissWithAnimation();
                    }
                })
                .show();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    public void onEventMainThread(final SiblingChangeEvent.Success event) {
        if (event.getLoginResponse().getStatusCode() == 1) {
            pDialog.setTitleText("Success!!!");
            pDialog.changeAlertType(SweetAlertDialog.SUCCESS_TYPE);

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    pDialog.dismiss();
                }
            }, 800);

            SharedPreferenceStore.storeValue(MainActivity.this, PreferencesData.LOGIN_RESPONSE, event.getLoginResponse().toString());
            EventBus.getDefault().postSticky(new LoadNotificationEvent.Success(true, studentId));
            setMenu();

            LoginResponse loginResponse = event.getLoginResponse();
            Siblings[] siblingses = loginResponse.getSiblings();

            if (loginResponse.getLogoPath() != null)
                Glide.with(MainActivity.this).load(loginResponse.getLogoPath()).into(schoolImage);
            else
                schoolImage.setImageDrawable(getDrawable(R.drawable.school_default));

            for (int i = 0; i < siblingses.length; i++) {
                if (siblingses[i].getUserId().equalsIgnoreCase(String.valueOf(studentId))) {
                    usernameTextView.setText(siblingses[i].getStudentName());
                    usernameTextView.setTag(siblingses[i].getUserId());
                    userEmailTextView.setText("Class Name - " + siblingses[i].getClassName());
                }
            }

            selectingDefaultFragment();
        } else {
            pDialog.setTitleText("Please check your details.");
            pDialog.changeAlertType(SweetAlertDialog.ERROR_TYPE);
            pDialog.setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                @Override
                public void onClick(SweetAlertDialog sweetAlertDialog) {
                    pDialog.dismiss();
                }
            });
        }
    }

    public void onEventMainThread(SiblingChangeEvent.Fail event) {

        if (event.getEx() != null) {
            pDialog.setTitleText("Oops Server Error");
            pDialog.setContentText(event.getEx());
            pDialog.changeAlertType(SweetAlertDialog.ERROR_TYPE);
        }
    }

    public void showingProgress() {
        pDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
        pDialog.getProgressHelper().setBarColor(colorPrimary);
        pDialog.setTitleText("Loading ...");
        pDialog.setCancelable(false);
        pDialog.show();
    }

    public void onEventMainThread(final MyCalenderEvent.Success event) {
        CalendarFragment fragment = (CalendarFragment) getSupportFragmentManager().findFragmentById(R.id.content_frame);
        fragment.loadingEventData(event.getCalenderEventResponse());
    }

    public void onEventMainThread(MyCalenderEvent.Fail event) {
        if (event.getEx() != null) {
            TastyToast.makeText(this, "Seems server is busy try after sometime.", TastyToast.LENGTH_LONG, TastyToast.ERROR).show();
        }
    }

    public void onEventMainThread(final MenuOpenEvent.Success event) {
        displayView(event.getMenuId());
    }

    public void onEventMainThread(final DeviceRegisterEvent.Success event) {
        Log.d(TAG, "Device registration response: " + event.genericResponse().getStatusMessage());
    }

    public void openMyProfile() {
        displayView(R.id.nav_my_profile);
    }

    public void setActionBarTitle(String title) {
        getSupportActionBar().setTitle(title);
    }
}
