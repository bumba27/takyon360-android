package net.lasagu.takyon360.ui.fragments;


import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutCompat;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.google.gson.Gson;
import com.sdsmdg.tastytoast.TastyToast;
import com.vansuita.pickimage.bean.PickResult;
import com.vansuita.pickimage.bundle.PickSetup;
import com.vansuita.pickimage.dialog.PickImageDialog;
import com.vansuita.pickimage.listeners.IPickResult;

import net.lasagu.takyon360.MyApplication;
import net.lasagu.takyon360.R;
import net.lasagu.takyon360.events.EditAccountEvent;
import net.lasagu.takyon360.events.EmailOtpSendEvent;
import net.lasagu.takyon360.events.GetParentEvent;
import net.lasagu.takyon360.events.PassEmailIdToProfileEvent;
import net.lasagu.takyon360.jobs.EditAccountJob;
import net.lasagu.takyon360.jobs.GetParentInfoJob;
import net.lasagu.takyon360.jobs.SendOtpEmailVerificationJob;
import net.lasagu.takyon360.models.EditAccountSubmission;
import net.lasagu.takyon360.models.LoginResponse;
import net.lasagu.takyon360.models.ProfileImageBean;
import net.lasagu.takyon360.models.StudentDetailsResponse;
import net.lasagu.takyon360.ui.FullScreenImageActivity;
import net.lasagu.takyon360.ui.MainActivity;
import net.lasagu.takyon360.ui.SetMyLocationActivity;
import net.lasagu.takyon360.utils.PreferencesData;
import net.lasagu.takyon360.utils.ReusableClass;
import net.lasagu.takyon360.utils.SharedPreferenceStore;

import java.io.ByteArrayOutputStream;

import butterknife.BindColor;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.pedant.SweetAlert.SweetAlertDialog;
import de.greenrobot.event.EventBus;
import de.hdodenhof.circleimageview.CircleImageView;

import static android.app.Activity.RESULT_OK;

/**
 * A simple {@link Fragment} subclass.
 */
public class MyProfileFragment extends Fragment {

    private final int IMAGE_REQUEST = 1;
    private final int LOCATION_REQUEST = 2;
    @BindColor(R.color.colorPrimary)
    int colorPrimary;
    @BindView(R.id.userImage)
    CircleImageView profileImage;
    @BindView(R.id.EditTextName)
    EditText EditTextName;
    @BindView(R.id.EditTextUserName)
    EditText EditTextUserName;
    @BindView(R.id.EditTextVerifiedEmail)
    EditText EditTextVerifiedEmail;
    @BindView(R.id.EditTextEmail)
    EditText EditTextEmail;
    @BindView(R.id.EditTextPhoneNo)
    EditText EditTextPhoneNo;
    @BindView(R.id.EditTextAddress)
    EditText EditTextAddress;
    @BindView(R.id.setLocationButton)
    Button setLocationButton;
    @BindView(R.id.changePasswordButton)
    Button changePasswordButton;
    @BindView(R.id.editProfileImageView)
    ImageView editProfileImageView;
    @BindView(R.id.verifyEmailButton)
    Button verifyEmailButton;
    @BindView(R.id.TextInputLayoutName)
    TextInputLayout TextInputLayoutName;
    @BindView(R.id.TextInputLayoutUsername)
    TextInputLayout TextInputLayoutUsername;
    @BindView(R.id.TextInputLayoutVerifiedEmail)
    TextInputLayout TextInputLayoutVerifiedEmail;
    @BindView(R.id.TextInputLayoutEmail)
    TextInputLayout TextInputLayoutEmail;
    @BindView(R.id.TextInputLayoutPhoneNo)
    TextInputLayout TextInputLayoutPhoneNo;
    @BindView(R.id.TextInputLayoutAddress)
    TextInputLayout TextInputLayoutAddress;
    private String TAG = MyProfileFragment.class.getSimpleName();

    private Menu menu;
    private ProfileImageBean profileImageBean;
    private String[] PERMISSIONS_IMAGE = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.CAMERA
    };
    private boolean editingMode = false;
    private SweetAlertDialog pDialog;
    private String imageUrl = "";
    private String lat = "";
    private String lng = "";
    private Bitmap userBitmap;
    private StudentDetailsResponse studentDetailsResponse;

    public MyProfileFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_my_profile, container, false);
        ButterKnife.bind(this, view);
        setHasOptionsMenu(true);

        profileImageBean = new ProfileImageBean();
        populateUserData();

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    public void populateUserData() {
        if (ReusableClass.isNetworkAvailable(getContext())) {
            showingProgress("Loading ...");
            MyApplication.addJobInBackground(new GetParentInfoJob(PreferencesData.getUserId(getContext())));
        } else
            TastyToast.makeText(getContext(), "Sorry no internet connection.", Toast.LENGTH_LONG, TastyToast.ERROR).show();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        this.menu = menu;
        inflater.inflate(R.menu.saving_profile_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_save) {
            savingInfo();
            return true;
        }
        if (id == R.id.action_edit) {
            editing();
            menu.findItem(R.id.action_edit).setVisible(false);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void savingInfo() {
        if (validated()) {
            if (ReusableClass.isNetworkAvailable(getContext())) {
                showingProgress("Updating ...");
                savingDetails();
            } else
                TastyToast.makeText(getContext(), "Sorry no internet connection.", Toast.LENGTH_LONG, TastyToast.ERROR).show();

        } else
            Toast.makeText(getContext(), "Name is mandatory.", Toast.LENGTH_LONG).show();
    }

    private void savingDetails() {
        EditAccountSubmission editAccountSubmission = new EditAccountSubmission();
        editAccountSubmission.setName(EditTextName.getText().toString().trim());
        editAccountSubmission.setUserId(PreferencesData.getUserId(getContext()));
        editAccountSubmission.setContact(EditTextAddress.getText().toString().trim());
        editAccountSubmission.setContactNumber(EditTextPhoneNo.getText().toString().trim());
        editAccountSubmission.setEmailID(EditTextEmail.getText().toString().trim());

        if (profileImageBean.getName() != null)
            editAccountSubmission.setProfileImage(profileImageBean);
        else {
            if (userBitmap != null) {
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                userBitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos); //bm is the bitmap object
                byte[] byteArrayImage = baos.toByteArray();

                String encodedImage = Base64.encodeToString(byteArrayImage, Base64.DEFAULT);

                profileImageBean.setName(encodedImage);
                editAccountSubmission.setProfileImage(profileImageBean);
            } else {
                Log.d(TAG, "savingDetails: USER BITMAP IS NULL");
            }
        }

        pDialog.setContentText("Saving ...");

        MyApplication.addJobInBackground(new EditAccountJob(editAccountSubmission));
    }


    private boolean validated() {
        if (TextUtils.isEmpty(EditTextName.getText()) || EditTextName.getText().toString().trim().length() <= 0)
            return false;
        else
            return true;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == IMAGE_REQUEST) {
            if (resultCode == RESULT_OK) {
                PickImageDialog.build(new PickSetup().setButtonOrientation(LinearLayoutCompat.HORIZONTAL)).show(getFragmentManager());
            } else {
                TastyToast.makeText(getContext(), "Sorry we need the permission.", Toast.LENGTH_LONG, TastyToast.ERROR).show();
            }
        } else if (requestCode == LOCATION_REQUEST) {
            if (resultCode == RESULT_OK) {
                populateUserData();
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case IMAGE_REQUEST: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                    PickImageDialog.build(new PickSetup()
                            .setButtonOrientation(LinearLayoutCompat.HORIZONTAL))
                            .setOnPickResult(new IPickResult() {
                                @Override
                                public void onPickResult(PickResult pickResult) {
                                    checkingImageResult(pickResult);
                                }
                            })
                            .show(getFragmentManager());
                else
                    TastyToast.makeText(getContext(), "Sorry we need the permission.", Toast.LENGTH_LONG, TastyToast.ERROR).show();

                return;
            }
        }
    }

    private void checkingImageResult(PickResult pickResult) {
        if (pickResult.getError() == null) {

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            pickResult.getBitmap().compress(Bitmap.CompressFormat.JPEG, 100, baos); //bm is the bitmap object
            byte[] byteArrayImage = baos.toByteArray();

            String encodedImage = Base64.encodeToString(byteArrayImage, Base64.DEFAULT);

            profileImageBean.setName(encodedImage);

            //If you want the Uri.
            //Mandatory to refresh image from Uri.
            profileImage.setImageURI(null);

            //Setting the real returned image.
            profileImage.setImageURI(pickResult.getUri());


            //If you want the Bitmap.
            //profileImage.setImageBitmap(r.getCompressedInputStream());

            //Image path
            //r.getPath();
        } else {
            TastyToast.makeText(getContext(), pickResult.getError().getMessage(), Toast.LENGTH_LONG, TastyToast.ERROR).show();
        }
    }

    public void verifyImagePermissions() {
        // Check if we have write permission
        int permission1 = ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.READ_EXTERNAL_STORAGE);
        int permission2 = ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.CAMERA);

        if (permission1 != PackageManager.PERMISSION_GRANTED || permission2 != PackageManager.PERMISSION_GRANTED) {
            // We don't have permission so prompt the user
            requestPermissions(
                    PERMISSIONS_IMAGE,
                    IMAGE_REQUEST
            );
        } else {
            PickImageDialog.build(new PickSetup()
                    .setButtonOrientation(LinearLayoutCompat.VERTICAL))
                    .setOnPickResult(new IPickResult() {
                        @Override
                        public void onPickResult(PickResult pickResult) {
                            checkingImageResult(pickResult);
                        }
                    })
                    .show(getFragmentManager());
        }
    }

    public void editing() {
        editingMode = true;
        EditTextName.setEnabled(true);
        EditTextEmail.setEnabled(true);
        EditTextAddress.setEnabled(true);
        EditTextPhoneNo.setEnabled(true);
        editProfileImageView.setVisibility(View.VISIBLE);

        menu.findItem(R.id.action_save).setVisible(true);
    }

    @OnClick({R.id.userImage, R.id.editProfileImageView})
    public void editingProfilePicture() {
        if (editingMode)
            verifyImagePermissions();
        else {
            Intent intent = new Intent(getContext(), FullScreenImageActivity.class);
            intent.putExtra("IMAGE_URL", imageUrl);
            intent.putExtra("IMAGE_TITLE", EditTextName.getText().toString());
            startActivity(intent);
        }
    }

    @OnClick(R.id.changePasswordButton)
    public void changingMyPassword(View view) {
        if (studentDetailsResponse != null) {
            ChangePasswordDialogFragment changePasswordDialogFragment = new ChangePasswordDialogFragment();
            Bundle args = new Bundle();
            args.putParcelable("studentDetailsResponse", studentDetailsResponse);
            changePasswordDialogFragment.setArguments(args);
            changePasswordDialogFragment.show(getFragmentManager(), "changePasswordDialogFragment");
        }
    }

    @OnClick(R.id.setLocationButton)
    public void settingMyLocation() {
        Intent intent = new Intent(getContext(), SetMyLocationActivity.class);
        intent.putExtra("lat", lat);
        intent.putExtra("lng", lng);
        startActivityForResult(intent, LOCATION_REQUEST);
    }

    @OnClick(R.id.verifyEmailButton)
    public void verifyingEmailId() {
        AlertDialog.Builder alert = new AlertDialog.Builder(getContext());
        final EditText edittext = new EditText(getContext());
        edittext.setText(PreferencesData.getLoginResponse(getContext()).getVerify());
        alert.setMessage("Would you like to update your email id?");
        alert.setTitle("Verify Your Email");
        alert.setView(edittext);
        alert.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                if (!TextUtils.isEmpty(edittext.getText().toString()))
                    showAlert(edittext.getText().toString());
                else
                    Toast.makeText(getContext(), "This field can't be empty. Try again !!", Toast.LENGTH_LONG).show();
            }
        });

        alert.setNegativeButton("Skip", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                showAlert(PreferencesData.getLoginResponse(getContext()).getVerify());
            }
        });

        alert.show();
    }

    private void showAlert(final String emailId) {
        new SweetAlertDialog(getContext(), SweetAlertDialog.WARNING_TYPE)
                .setTitleText("Are you sure?")
                .setContentText("You want to verify this email - " + emailId)
                .setCancelText("No")
                .setConfirmText("Yes")
                .showCancelButton(true)
                .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.cancel();
                    }
                })
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        if (ReusableClass.isNetworkAvailable(getContext())) {
                            sweetAlertDialog.dismiss();
                            showingProgress("Sending email ...");
                            MyApplication.addJobInBackground(new SendOtpEmailVerificationJob(emailId));
                        } else
                            TastyToast.makeText(getContext(), "Sorry no internet connection.", Toast.LENGTH_LONG, TastyToast.ERROR).show();

                    }
                })
                .show();
    }

    public void showingProgress(String title) {
        pDialog = new SweetAlertDialog(getContext(), SweetAlertDialog.PROGRESS_TYPE);
        pDialog.getProgressHelper().setBarColor(colorPrimary);
        pDialog.setTitleText(title);
        pDialog.setCancelable(false);
        pDialog.show();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    public void onEventMainThread(final GetParentEvent.Success event) {
        if (event.getStudentDetailsResponse().getStatusCode() == 1) {
            studentDetailsResponse = event.getStudentDetailsResponse();

            EditTextName.setText(studentDetailsResponse.getName());
            TextInputLayoutName.setHint(studentDetailsResponse.getNameLabel());

            EditTextUserName.setText(studentDetailsResponse.getUserName());
            TextInputLayoutUsername.setHint(studentDetailsResponse.getUserNameLabel());
            EditTextUserName.setEnabled(false);

            EditTextVerifiedEmail.setText(PreferencesData.getLoginResponse(getContext()).getVerify());
            TextInputLayoutVerifiedEmail.setHint(studentDetailsResponse.getVerifiedEmailLabel());
            EditTextUserName.setEnabled(false);

            EditTextEmail.setText(studentDetailsResponse.getEmailID());
            TextInputLayoutEmail.setHint(studentDetailsResponse.getEmailIDLabel());

            EditTextAddress.setText(studentDetailsResponse.getContact());
            TextInputLayoutAddress.setHint(studentDetailsResponse.getContactLabel());

            EditTextPhoneNo.setText(studentDetailsResponse.getContactNumber());
            TextInputLayoutPhoneNo.setHint(studentDetailsResponse.getContactNumberLabel());

            verifyEmailButton.setText(studentDetailsResponse.getVerifiedEmailLabel());
            setLocationButton.setText(studentDetailsResponse.getSetMyLocationLabel());
            changePasswordButton.setText(studentDetailsResponse.getChangePasswordLabel());

            lat = studentDetailsResponse.getLatitude();
            lng = studentDetailsResponse.getLongitude();

            if (studentDetailsResponse.getProfileImage() != null) {
                try {
                    Glide.with(getActivity())
                            .asBitmap()
                            .load(studentDetailsResponse.getProfileImage())
                            .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.AUTOMATIC))
                            .into(new SimpleTarget<Bitmap>(SimpleTarget.SIZE_ORIGINAL, SimpleTarget.SIZE_ORIGINAL) {
                                @Override
                                public void onResourceReady(Bitmap bitmap, Transition<? super Bitmap> transition) {
                                    profileImage.setImageBitmap(bitmap);
                                    userBitmap = bitmap;
                                }
                            });
                    imageUrl = studentDetailsResponse.getProfileImage();
                } catch (Throwable e) {
                    e.printStackTrace();
                }
            } else
                profileImage.setImageDrawable(getActivity().getDrawable(R.drawable.empty_avatar));
        } else {
            pDialog.setTitleText("Please check your details.");
            pDialog.changeAlertType(SweetAlertDialog.ERROR_TYPE);
            pDialog.setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                @Override
                public void onClick(SweetAlertDialog sweetAlertDialog) {
                    pDialog.dismiss();
                }
            });
        }
        pDialog.dismissWithAnimation();
    }

    public void onEventMainThread(GetParentEvent.Fail event) {
        if (event.getEx() != null) {
            pDialog.setTitleText("We Are Sorry ...");
            pDialog.setContentText(event.getEx());
            pDialog.changeAlertType(SweetAlertDialog.ERROR_TYPE);
        }
    }

    public void onEventMainThread(EditAccountEvent.Success event) {
        if (event.getGenericResponse().getStatusCode() == 1) {
            pDialog.setTitleText("Success!!!");
            pDialog.changeAlertType(SweetAlertDialog.SUCCESS_TYPE);

            String loginResponseString = SharedPreferenceStore.getValue(getContext(), PreferencesData.LOGIN_RESPONSE, "");
            LoginResponse loginResponse = new Gson().fromJson(loginResponseString, LoginResponse.class);
            loginResponse.setName(EditTextName.getText().toString().trim());
            loginResponse.setEmail(EditTextEmail.getText().toString().trim());

            SharedPreferenceStore.storeValue(getContext(), PreferencesData.LOGIN_RESPONSE, loginResponse.toString());

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    pDialog.dismiss();
                    ((MainActivity) getActivity()).openMyProfile();
                }
            }, 800);
        } else {
            pDialog.setTitleText("We Are Sorry ...");
            pDialog.setContentText("Not be able to update your details, please try again.");
            pDialog.changeAlertType(SweetAlertDialog.ERROR_TYPE);
            pDialog.setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                @Override
                public void onClick(SweetAlertDialog sweetAlertDialog) {
                    pDialog.dismiss();
                }
            });
        }
    }

    public void onEventMainThread(EditAccountEvent.Fail event) {
        if (event.getEx() != null) {
            pDialog.setTitleText("We Are Sorry ...");
            pDialog.setContentText(event.getEx());
            pDialog.changeAlertType(SweetAlertDialog.ERROR_TYPE);
        }
    }

    public void onEventMainThread(final EmailOtpSendEvent.Success event) {
        int statusCode = event.genericResponse().getStatusCode();
        if (statusCode == 1) {
            pDialog.dismissWithAnimation();

            ConfirmEmailDialogFragment confirmEmailDialogFragment = new ConfirmEmailDialogFragment(event.getUserEmailId());
            confirmEmailDialogFragment.show(getFragmentManager(), "confirmEmailDialogFragment");
        } else {
            settingError("Sorry unable to send OTP to your email id.");
        }
    }

    public void onEventMainThread(EmailOtpSendEvent.Fail event) {
        if (event.getEx() != null) {
            settingError(event.getEx());
        }
    }

    private void settingError(String message) {
        pDialog.setTitleText("Sorry ...");
        pDialog.setContentText(message);
        pDialog.changeAlertType(SweetAlertDialog.ERROR_TYPE);
        pDialog.setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                pDialog.dismiss();
            }
        });
    }

    public void onEventMainThread(PassEmailIdToProfileEvent event) {
        EditTextVerifiedEmail.setText(event.getEmailId());

        LoginResponse loginResponse = PreferencesData.getLoginResponse(getContext());
        loginResponse.setVerify(event.getEmailId());
        PreferencesData.setLoginResponse(getActivity(), loginResponse);

        EventBus.getDefault().removeStickyEvent(event);
    }
}
