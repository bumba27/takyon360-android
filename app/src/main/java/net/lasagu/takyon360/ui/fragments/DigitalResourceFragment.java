package net.lasagu.takyon360.ui.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.sdsmdg.tastytoast.TastyToast;

import net.lasagu.takyon360.MyApplication;
import net.lasagu.takyon360.R;
import net.lasagu.takyon360.events.DigitalResourceCategoryEvent;
import net.lasagu.takyon360.jobs.DigitalResourceCategoryJob;
import net.lasagu.takyon360.models.DigitalCategoriesBean;
import net.lasagu.takyon360.models.DigitalResourceCategoryResponse;
import net.lasagu.takyon360.models.UserIdAndSearchTextSubmission;
import net.lasagu.takyon360.ui.MainActivity;
import net.lasagu.takyon360.utils.PreferencesData;
import net.lasagu.takyon360.utils.ReusableClass;
import net.lasagu.takyon360.widgets.DigitalResourceAdapter;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.greenrobot.event.EventBus;

/**
 * A simple {@link Fragment} subclass.
 */
public class DigitalResourceFragment extends Fragment {


    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.SwipeRefreshLayout)
    android.support.v4.widget.SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.noData)
    LinearLayout noData;
    @BindView(R.id.progress)
    ProgressBar progress;
    LinearLayoutManager linearLayoutManager;
    DigitalResourceAdapter digitalResourceAdapter;

    public DigitalResourceFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_digitial_resource, container, false);
        ButterKnife.bind(this, view);
        setHasOptionsMenu(true);

        digitalResourceAdapter = new DigitalResourceAdapter(getContext());
        linearLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(digitalResourceAdapter);

        loadingData();

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadingData();
            }
        });

        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.search_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);

        MenuItem item = menu.findItem(R.id.action_search);
        SearchView searchView = new SearchView(((MainActivity) getActivity()).getSupportActionBar().getThemedContext());
        MenuItemCompat.setShowAsAction(item, MenuItemCompat.SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW | MenuItemCompat.SHOW_AS_ACTION_IF_ROOM);
        MenuItemCompat.setActionView(item, searchView);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                digitalResourceAdapter.filter(query);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                digitalResourceAdapter.filter(newText);
                return true;
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    private void loadingData() {
        if (ReusableClass.isNetworkAvailable(getContext())) {
            progress.setVisibility(View.VISIBLE);

            UserIdAndSearchTextSubmission userIdAndSearchTextSubmission = new UserIdAndSearchTextSubmission();
            userIdAndSearchTextSubmission.setUserId(PreferencesData.getUserId(getContext()));
            userIdAndSearchTextSubmission.setSearchText("");

            MyApplication.addJobInBackground(new DigitalResourceCategoryJob(userIdAndSearchTextSubmission));
        } else
            TastyToast.makeText(getContext(), "Sorry no internet connection.", Toast.LENGTH_LONG, TastyToast.ERROR).show();
    }


    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    public void onEventMainThread(final DigitalResourceCategoryEvent.Success event) {
        DigitalResourceCategoryResponse digitalResourceCategoryResponse = event.getDigitalResourceCategoryResponse();
        ArrayList<DigitalCategoriesBean> categories = digitalResourceCategoryResponse.getDigitalCategories();
        if (categories.size() != 0) {
            digitalResourceAdapter.addAll(categories);

            noData.setVisibility(View.INVISIBLE);
        } else {
            noData.setVisibility(View.VISIBLE);
        }
        progress.setVisibility(View.INVISIBLE);
        if (swipeRefreshLayout.isRefreshing())
            swipeRefreshLayout.setRefreshing(false);
    }

    public void onEventMainThread(DigitalResourceCategoryEvent.Fail event) {
        if (event.getEx() != null) {
            TastyToast.makeText(getContext(), "Seems server is busy try after sometime.", TastyToast.LENGTH_LONG, TastyToast.ERROR).show();
        }
    }
}
