package net.lasagu.takyon360.ui.fragments;


import android.Manifest;
import android.app.DownloadManager;
import android.content.Context;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.sdsmdg.tastytoast.TastyToast;

import net.lasagu.takyon360.MyApplication;
import net.lasagu.takyon360.R;
import net.lasagu.takyon360.events.AssessmentEvent;
import net.lasagu.takyon360.events.ClassesListResponseEvent;
import net.lasagu.takyon360.events.ClassworkEvent;
import net.lasagu.takyon360.events.HomeworkEvent;
import net.lasagu.takyon360.events.LatestWeeklyPlanListResponseEvent;
import net.lasagu.takyon360.events.QuizeEvent;
import net.lasagu.takyon360.events.SubjectListResponseEvent;
import net.lasagu.takyon360.events.WeeklyPlanDownloadResponseEvent;
import net.lasagu.takyon360.events.WeeklyPlanListResponseEvent;
import net.lasagu.takyon360.jobs.ClassesListJob;
import net.lasagu.takyon360.jobs.SubjectListJob;
import net.lasagu.takyon360.jobs.WeeklyPlanListJob;
import net.lasagu.takyon360.models.ClassesListResponse;
import net.lasagu.takyon360.models.DivisionsBean;
import net.lasagu.takyon360.models.SubjectListRequest;
import net.lasagu.takyon360.models.SubjectListResponse;
import net.lasagu.takyon360.models.UserIdSubmission;
import net.lasagu.takyon360.models.WeeklyPlanDownloadResponse;
import net.lasagu.takyon360.models.WeeklyPlanListResponse;
import net.lasagu.takyon360.models.WeeklyPlanSubmission;
import net.lasagu.takyon360.ui.MainActivity;
import net.lasagu.takyon360.utils.PreferencesData;
import net.lasagu.takyon360.utils.ReusableClass;
import net.lasagu.takyon360.utils.SharedPreferenceStore;
import net.lasagu.takyon360.widgets.WeeklyPlanViewPagerAdapter;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindColor;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.pedant.SweetAlert.SweetAlertDialog;
import de.greenrobot.event.EventBus;

import static net.lasagu.takyon360.utils.Constant.LANGUAGE_CODE;

/**
 * A simple {@link Fragment} subclass.
 */
public class WeeklyPlanFragment extends Fragment {

    private static final int EXTERNAL_STORAGE_PERMISSION_RESPONSE = 1;
    SweetAlertDialog pDialog;
    @BindColor(R.color.colorPrimary)
    int colorPrimary;
    @BindView(R.id.textViewFromDate)
    TextView textViewFromDate;
    @BindView(R.id.textViewToDate)
    TextView textViewToDate;
    @BindView(R.id.previousDateOnClick)
    ImageView previousDateOnClick;
    @BindView(R.id.nextDateOnClick)
    ImageView nextDateOnClick;
    @BindView(R.id.tabs)
    TabLayout tabs;
    @BindView(R.id.pager)
    ViewPager pager;
    @BindView(R.id.TextViewFrom)
    TextView TextViewFrom;
    @BindView(R.id.TextViewTo)
    TextView TextViewTo;
    private String classesListResponseString = "";
    private String attachmentUrl;
    private String defaultDivId = "";
    private WeeklyPlanViewPagerAdapter adapter;
    private SubjectListResponse subjectListResponse;
    private String weeklyPlanLabel;
    private String latestViewLabel;

    public WeeklyPlanFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_weekly_plan, container, false);
        ButterKnife.bind(this, view);

        adapter = new WeeklyPlanViewPagerAdapter(getChildFragmentManager());

        adapter.addFragment(new WeeklyPlanHomeworkFragment(), "");
        adapter.addFragment(new WeeklyPlanClassworkFragment(), "");
        adapter.addFragment(new WeeklyPlanAssessmentFragment(), "");
        adapter.addFragment(new QuizeFragment(), "");

        pager.setAdapter(adapter);
        pager.setOffscreenPageLimit(4);
        tabs.setupWithViewPager(pager);

        loadingClasses();

        return view;
    }

    private void loadingSubjects() {
        if (ReusableClass.isNetworkAvailable(getContext())) {

            SubjectListRequest subjectListRequest = new SubjectListRequest();
            subjectListRequest.setDiv_Id(Integer.parseInt(defaultDivId));
            subjectListRequest.setUserId(Integer.parseInt(PreferencesData.getUserId(getContext())));

            MyApplication.addJobInBackground(new SubjectListJob(subjectListRequest));
        } else
            TastyToast.makeText(getContext(), "Sorry no internet connection.", Toast.LENGTH_LONG, TastyToast.ERROR).show();

    }

    private void loadingClasses() {
        if (ReusableClass.isNetworkAvailable(getContext())) {

            UserIdSubmission userIdSubmission = new UserIdSubmission();
            userIdSubmission.setUserId(PreferencesData.getUserId(getContext()));

            MyApplication.addJobInBackground(new ClassesListJob(userIdSubmission));
        } else
            TastyToast.makeText(getContext(), "Sorry no internet connection.", Toast.LENGTH_LONG, TastyToast.ERROR).show();
    }

    private void loadingDialog() {
        pDialog = new SweetAlertDialog(getContext(), SweetAlertDialog.PROGRESS_TYPE);
        pDialog.getProgressHelper().setBarColor(colorPrimary);
        pDialog.setTitleText("Loading ...");
        pDialog.setCancelable(false);
        pDialog.show();
    }

    private void loadingData() {
        if (ReusableClass.isNetworkAvailable(getContext())) {
            loadingDialog();

            WeeklyPlanSubmission weeklyPlanSubmission = new WeeklyPlanSubmission();
            weeklyPlanSubmission.setType("");
            weeklyPlanSubmission.setFromDate("");
            weeklyPlanSubmission.setToDate("");
            weeklyPlanSubmission.setIsLatest(1);
            weeklyPlanSubmission.setUserId(PreferencesData.getUserId(getContext()));
            weeklyPlanSubmission.setLimit(5);
            weeklyPlanSubmission.setDiv_Id(defaultDivId);
            weeklyPlanSubmission.setOffSet(0);

            MyApplication.addJobInBackground(new WeeklyPlanListJob(weeklyPlanSubmission));
        } else
            TastyToast.makeText(getContext(), "Sorry no internet connection.", Toast.LENGTH_LONG, TastyToast.ERROR).show();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.filter_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_filter) {
            if (!classesListResponseString.equalsIgnoreCase("") && subjectListResponse != null) {
                WeeklyPlanFilterDialogFragment weeklyPlanFilterDialogFragment = new WeeklyPlanFilterDialogFragment();
                Bundle bundle = new Bundle();
                bundle.putString("TITLE", weeklyPlanLabel);
                bundle.putString("LATEST_VIEW_BUTTON_LEVEL", latestViewLabel);
                bundle.putString("CLASSES", classesListResponseString);
                bundle.putString("SUBJECTS", subjectListResponse.toString());
                weeklyPlanFilterDialogFragment.setArguments(bundle);
                weeklyPlanFilterDialogFragment.show(getFragmentManager(), "weeklyPlanFilterDialogFragment");
            } else
                TastyToast.makeText(getContext(), "Seems no class available.", TastyToast.LENGTH_SHORT, TastyToast.ERROR).show();

            return true;
        } else if (id == R.id.action_download) {
            new SweetAlertDialog(getContext(), SweetAlertDialog.WARNING_TYPE)
                    .setTitleText("Please confirm ...")
                    .setContentText("Please choose the file type you want to download.")
                    .setConfirmText("PDF")
                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sDialog) {
                            sDialog.dismissWithAnimation();
                            downloadFiles("P");
                        }
                    })
                    .setCancelText("WORD")
                    .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sDialog) {
                            sDialog.dismissWithAnimation();
                            downloadFiles("W");
                        }
                    })
                    .show();
        }

        return super.onOptionsItemSelected(item);
    }

    private void downloadFiles(String fileType) {
        if (ReusableClass.isNetworkAvailable(getContext())) {
            loadingDialog();

            String toDate = textViewToDate.getText().toString();
            String fromDate = getDate(textViewToDate.getText().toString(), -4);

            textViewFromDate.setText(fromDate);
            textViewToDate.setText(toDate);

            WeeklyPlanSubmission weeklyPlanSubmission = new WeeklyPlanSubmission();
            weeklyPlanSubmission.setType(fileType);
            weeklyPlanSubmission.setFromDate(fromDate);
            weeklyPlanSubmission.setToDate(toDate);
            weeklyPlanSubmission.setIsLatest(0);
            weeklyPlanSubmission.setUserId(PreferencesData.getUserId(getContext()));
            weeklyPlanSubmission.setLimit(5);
            weeklyPlanSubmission.setDiv_Id("");
            weeklyPlanSubmission.setOffSet(0);

            MyApplication.addJobInBackground(new WeeklyPlanListJob(weeklyPlanSubmission));

        } else
            TastyToast.makeText(getContext(), "Sorry no internet connection.", Toast.LENGTH_LONG, TastyToast.ERROR).show();

    }

    @OnClick(R.id.previousDateOnClick)
    public void previousDateClicked() {
        if (ReusableClass.isNetworkAvailable(getContext())) {
            String fromDate = getDate(textViewFromDate.getText().toString(), -5);
            String toDate = getDate(textViewFromDate.getText().toString(), -1);

            textViewFromDate.setText(fromDate);
            textViewToDate.setText(toDate);

            loadingDialog();

            WeeklyPlanSubmission weeklyPlanSubmission = new WeeklyPlanSubmission();
            weeklyPlanSubmission.setType("");
            weeklyPlanSubmission.setFromDate(fromDate);
            weeklyPlanSubmission.setToDate(toDate);
            weeklyPlanSubmission.setIsLatest(0);
            weeklyPlanSubmission.setUserId(PreferencesData.getUserId(getContext()));
            weeklyPlanSubmission.setLimit(20);
            weeklyPlanSubmission.setDiv_Id("");
            weeklyPlanSubmission.setOffSet(0);

            MyApplication.addJobInBackground(new WeeklyPlanListJob(weeklyPlanSubmission));

        } else
            TastyToast.makeText(getContext(), "Sorry no internet connection.", Toast.LENGTH_LONG, TastyToast.ERROR).show();
    }

    @OnClick(R.id.nextDateOnClick)
    public void nextDateClicked() {
        if (ReusableClass.isNetworkAvailable(getContext())) {
            String fromDate = getDate(textViewToDate.getText().toString(), 1);
            String toDate = getDate(textViewToDate.getText().toString(), 5);

            textViewFromDate.setText(fromDate);
            textViewToDate.setText(toDate);

            loadingDialog();

            WeeklyPlanSubmission weeklyPlanSubmission = new WeeklyPlanSubmission();
            weeklyPlanSubmission.setType("");
            weeklyPlanSubmission.setFromDate(fromDate);
            weeklyPlanSubmission.setToDate(toDate);
            weeklyPlanSubmission.setIsLatest(0);
            weeklyPlanSubmission.setUserId(PreferencesData.getUserId(getContext()));
            weeklyPlanSubmission.setLimit(20);
            weeklyPlanSubmission.setDiv_Id("");
            weeklyPlanSubmission.setOffSet(0);

            MyApplication.addJobInBackground(new WeeklyPlanListJob(weeklyPlanSubmission));

        } else
            TastyToast.makeText(getContext(), "Sorry no internet connection.", Toast.LENGTH_LONG, TastyToast.ERROR).show();
    }

    private String getDate(String textViewFromDateText, int noOfDays) {
        try {
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            Date oldDate = dateFormat.parse(textViewFromDateText);
            Calendar cal = Calendar.getInstance();
            cal.setTime(oldDate);
            cal.add(Calendar.DATE, noOfDays);
            Date dateBefore5Days = cal.getTime();

            return dateFormat.format(dateBefore5Days);

        } catch (ParseException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    public void onEventMainThread(final LatestWeeklyPlanListResponseEvent event) {
        loadingData();
    }

    public void onEventMainThread(final WeeklyPlanDownloadResponseEvent.Success event) {
        WeeklyPlanDownloadResponse weeklyPlanDownloadResponse = event.getWeeklyPlanDownloadResponse();
        textViewFromDate.setText(weeklyPlanDownloadResponse.getFromDate());
        textViewToDate.setText(weeklyPlanDownloadResponse.getToDate());

        havingPermission(weeklyPlanDownloadResponse.getAttachmentLink());
        loadingDialog();
    }

    public void onEventMainThread(final WeeklyPlanListResponseEvent.Success event) {
        WeeklyPlanListResponse weeklyPlanListResponse = event.getWeeklyPlanListResponse();
        textViewFromDate.setText(weeklyPlanListResponse.getFromDate());
        textViewToDate.setText(weeklyPlanListResponse.getToDate());

        EventBus.getDefault().post(new HomeworkEvent(weeklyPlanListResponse.getHomeWork()));
        EventBus.getDefault().post(new ClassworkEvent(weeklyPlanListResponse.getClassWork()));
        EventBus.getDefault().post(new AssessmentEvent(weeklyPlanListResponse.getAssessments()));
        EventBus.getDefault().post(new QuizeEvent(weeklyPlanListResponse.getQuizes()));

        pDialog.setTitleText("Success!!!");
        pDialog.changeAlertType(SweetAlertDialog.SUCCESS_TYPE);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                pDialog.dismiss();
            }
        }, 800);
    }

    public void onEventMainThread(WeeklyPlanListResponseEvent.Fail event) {
        pDialog.setTitleText("Error!!!");
        pDialog.changeAlertType(SweetAlertDialog.ERROR_TYPE);
        pDialog.dismiss();

        WeeklyPlanListResponse weeklyPlanDownloadResponse = new WeeklyPlanListResponse();

        EventBus.getDefault().post(new HomeworkEvent(weeklyPlanDownloadResponse.getHomeWork()));
        EventBus.getDefault().post(new ClassworkEvent(weeklyPlanDownloadResponse.getClassWork()));
        EventBus.getDefault().post(new AssessmentEvent(weeklyPlanDownloadResponse.getAssessments()));

        if (event.getEx() != null) {
            TastyToast.makeText(getContext(), event.getEx(), TastyToast.LENGTH_LONG, TastyToast.ERROR).show();
        } else {
            TastyToast.makeText(getContext(), "Seems server is busy try after sometime.", TastyToast.LENGTH_LONG, TastyToast.ERROR).show();
        }
    }

    public void onEventMainThread(final ClassesListResponseEvent.Success event) {
        final ClassesListResponse classesListResponse = event.getClassesListResponse();

        TextViewFrom.setText(classesListResponse.getFromLabel());
        TextViewTo.setText(classesListResponse.getToLabel());
        ((MainActivity) getActivity()).getSupportActionBar().setTitle(classesListResponse.getWeelyPlanLabel());
        weeklyPlanLabel = classesListResponse.getWeelyPlanLabel();
        latestViewLabel = classesListResponse.getLatestViewLabel();

        SharedPreferenceStore.storeValue(getContext(), "START_DATE_LABEL", classesListResponse.getStartingDateLabel());
        SharedPreferenceStore.storeValue(getContext(), "END_DATE_LABEL", classesListResponse.getEndingDateLabel());
        SharedPreferenceStore.storeValue(getContext(), "SELECT_CLASS_LABEL", classesListResponse.getClassLabel());
        SharedPreferenceStore.storeValue(getContext(), "SELECT_SUBJECT_LABEL", "Select Subject");

        List<String> titleLists = new ArrayList<>();
        String localeLanguage = SharedPreferenceStore.getValue(getContext(), LANGUAGE_CODE, "en");

        if (localeLanguage.equals("ar")) {
            titleLists.add(classesListResponse.getQuizLabel());
            titleLists.add(classesListResponse.getAssessmentLabel());
            titleLists.add(classesListResponse.getClassWorkLabel());
            titleLists.add(classesListResponse.getHomeWorkLabel());
        } else {
            titleLists.add(classesListResponse.getHomeWorkLabel());
            titleLists.add(classesListResponse.getClassWorkLabel());
            titleLists.add(classesListResponse.getAssessmentLabel());
            titleLists.add(classesListResponse.getQuizLabel());
        }

        adapter.updateTitle(titleLists);

        ArrayList<DivisionsBean> divisions = classesListResponse.getDivisions();
        if (divisions.size() == 0)
            TastyToast.makeText(getContext(), "Seems no class available.", TastyToast.LENGTH_LONG, TastyToast.ERROR).show();
        else {
            classesListResponseString = classesListResponse.toString();
            defaultDivId = divisions.get(0).getDivId();
            loadingData();
            loadingSubjects();
        }
    }


    public void onEventMainThread(final SubjectListResponseEvent.Success event) {
        subjectListResponse = event.getSubjectListResponse();
    }

    public void onEventMainThread(ClassesListResponseEvent.Fail event) {
        if (event.getEx() != null) {
            TastyToast.makeText(getContext(), "Seems server is busy try after sometime.", TastyToast.LENGTH_LONG, TastyToast.ERROR).show();
        }
    }

    private void havingPermission(String attachmentUrl) {
        this.attachmentUrl = attachmentUrl;
        // Check if we have write permission
        int permission = ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (permission != PackageManager.PERMISSION_GRANTED)
            requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, EXTERNAL_STORAGE_PERMISSION_RESPONSE);
        else
            downloading(attachmentUrl);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case EXTERNAL_STORAGE_PERMISSION_RESPONSE: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                    downloading(attachmentUrl);
                else
                    TastyToast.makeText(getContext(), "Sorry we need the permission.", Toast.LENGTH_LONG, TastyToast.ERROR).show();

                return;
            }
        }
    }

    private void downloading(final String url) {
        try {
            if (url != null && !url.isEmpty() && url.contains("/")) {
                String fileName = url.substring(url.lastIndexOf('/') + 1);
                Log.d("TAG", "filename = " + fileName);

                Uri uri = Uri.parse(url.replaceAll(" ", "%20"));
                Log.d("TAG", "uri = " + uri);
                DownloadManager.Request request = new DownloadManager.Request(uri);
                request.setTitle(fileName);
                request.setDescription("Downloading attachment..");
                request.allowScanningByMediaScanner();
                request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
                request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, fileName);
                DownloadManager dm = (DownloadManager) getActivity().getSystemService(Context.DOWNLOAD_SERVICE);

                long d = dm.enqueue(request);
                Log.d("TAG", "file = " + dm.getMimeTypeForDownloadedFile(d));
            } else
                TastyToast.makeText(getContext(), "Not a valid url contact support.", TastyToast.LENGTH_SHORT, TastyToast.ERROR).show();

        } catch (IllegalStateException e) {
            Toast.makeText(getContext(), "Please insert an SD card to download file", Toast.LENGTH_SHORT).show();
        }
        pDialog.setTitleText("Success!!!");
        pDialog.changeAlertType(SweetAlertDialog.SUCCESS_TYPE);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                pDialog.dismiss();
            }
        }, 800);
    }
}
