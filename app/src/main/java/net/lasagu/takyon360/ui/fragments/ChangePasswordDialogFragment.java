package net.lasagu.takyon360.ui.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.sdsmdg.tastytoast.TastyToast;

import net.lasagu.takyon360.MyApplication;
import net.lasagu.takyon360.R;
import net.lasagu.takyon360.events.EditAccountEvent;
import net.lasagu.takyon360.events.UpdatePasswordEvent;
import net.lasagu.takyon360.jobs.UpdatePasswordJob;
import net.lasagu.takyon360.models.StudentDetailsResponse;
import net.lasagu.takyon360.models.UpdatePasswordSubmission;
import net.lasagu.takyon360.ui.LoginActivity;
import net.lasagu.takyon360.utils.PreferencesData;
import net.lasagu.takyon360.utils.ReusableClass;

import java.util.UUID;

import butterknife.BindColor;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.pedant.SweetAlert.SweetAlertDialog;
import de.greenrobot.event.EventBus;

/**
 * Created by ajana on 06/07/2017.
 */

public class ChangePasswordDialogFragment extends DialogFragment {

    @BindView(R.id.EditTextCurrentPassword)
    EditText EditTextCurrentPassword;
    @BindView(R.id.EditTextNewPassword)
    EditText EditTextNewPassword;
    @BindView(R.id.EditTextConfirmPassword)
    EditText EditTextConfirmPassword;
    @BindView(R.id.SaveButton)
    Button SaveButton;
    @BindView(R.id.TextInputLayoutCurrentPassword)
    TextInputLayout TextInputLayoutCurrentPassword;
    @BindView(R.id.TextInputLayoutNewPassword)
    TextInputLayout TextInputLayoutNewPassword;
    @BindView(R.id.TextInputLayoutConfirmPassword)
    TextInputLayout TextInputLayoutConfirmPassword;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    private SweetAlertDialog pDialog;
    @BindColor(R.color.colorPrimary)
    int colorPrimary;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getDialog().setTitle("Reset password");
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        View view = inflater.inflate(R.layout.change_password_dialog_fragment, container, false);

        ButterKnife.bind(this, view);


        StudentDetailsResponse studentDetailsResponse = getArguments().getParcelable("studentDetailsResponse");
        toolbar.setTitle(studentDetailsResponse.getChangePasswordLabel());
        SaveButton.setText(studentDetailsResponse.getChangePasswordLabel());
        TextInputLayoutCurrentPassword.setHint(studentDetailsResponse.getCurrentPasswordLabel());
        TextInputLayoutNewPassword.setHint(studentDetailsResponse.getNewPasswordLabel());
        TextInputLayoutConfirmPassword.setHint(studentDetailsResponse.getReapeatNewPasswordLabel());
        return view;
    }

    @OnClick(R.id.SaveButton)
    public void saving() {
        if (validated()) {
            UpdatePasswordSubmission updatePasswordSubmission = new UpdatePasswordSubmission();
            updatePasswordSubmission.setUserId(PreferencesData.getUserId(getContext()));
            updatePasswordSubmission.setCurrentPassword(EditTextCurrentPassword.getText().toString());
            updatePasswordSubmission.setNewPassword(EditTextNewPassword.getText().toString());
            updatePasswordSubmission.setReapeatNewPassword(EditTextConfirmPassword.getText().toString());
            updatePasswordSubmission.setClient_ip(UUID.randomUUID().toString());
            if (ReusableClass.isNetworkAvailable(getContext())) {
                showingProgress("Updating ...");
                MyApplication.addJobInBackground(new UpdatePasswordJob(updatePasswordSubmission));
            } else
                TastyToast.makeText(getContext(), "Sorry no internet connection.", Toast.LENGTH_LONG, TastyToast.ERROR).show();

        } else
            TastyToast.makeText(getContext(), "All fields are required.", TastyToast.LENGTH_LONG, TastyToast.ERROR).show();
    }

    private boolean validated() {
        if (TextUtils.isEmpty(EditTextCurrentPassword.getText()))
            return false;
        if (TextUtils.isEmpty(EditTextNewPassword.getText()))
            return false;
        if (TextUtils.isEmpty(EditTextConfirmPassword.getText()))
            return false;
        if (!EditTextNewPassword.getText().toString().equalsIgnoreCase(EditTextConfirmPassword.getText().toString())) {
            TastyToast.makeText(getContext(), "New password and Confirm password does not match.", TastyToast.LENGTH_LONG, TastyToast.ERROR).show();
            return false;
        }
        return true;
    }

    public void showingProgress(String title) {
        pDialog = new SweetAlertDialog(getContext(), SweetAlertDialog.PROGRESS_TYPE);
        pDialog.getProgressHelper().setBarColor(colorPrimary);
        pDialog.setTitleText(title);
        pDialog.setCancelable(false);
        pDialog.show();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    public void onEventMainThread(UpdatePasswordEvent.Success event) {
        if (event.getGenericResponse().getStatusCode() == 1) {
            pDialog.setTitleText("Success!!!");
            pDialog.changeAlertType(SweetAlertDialog.SUCCESS_TYPE);
            pDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                @Override
                public void onClick(SweetAlertDialog sweetAlertDialog) {
                    pDialog.dismiss();
                    dismiss();

                    PreferencesData.resetUser(getContext());
                    Intent intent = new Intent(getContext(), LoginActivity.class);
                    startActivity(intent);

                }
            });
        } else {
            pDialog.setTitleText("We Are Sorry ...");
            pDialog.setContentText(event.getGenericResponse().getStatusMessage());
            pDialog.changeAlertType(SweetAlertDialog.ERROR_TYPE);
            pDialog.setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                @Override
                public void onClick(SweetAlertDialog sweetAlertDialog) {
                    pDialog.dismiss();
                }
            });
        }
    }

    public void onEventMainThread(EditAccountEvent.Fail event) {
        if (event.getEx() != null) {
            pDialog.setTitleText("We Are Sorry ...");
            pDialog.setContentText(event.getEx());
            pDialog.changeAlertType(SweetAlertDialog.ERROR_TYPE);
        }
    }
}