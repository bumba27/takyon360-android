package net.lasagu.takyon360.ui.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import net.lasagu.takyon360.R;
import net.lasagu.takyon360.events.HomeworkEvent;
import net.lasagu.takyon360.models.HomeWorkBean;
import net.lasagu.takyon360.widgets.HomeworkListAdapter;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.greenrobot.event.EventBus;

/**
 * A simple {@link Fragment} subclass.
 */
public class WeeklyPlanHomeworkFragment extends Fragment {

    @BindView(R.id.noData)
    LinearLayout noData;
    @BindView(R.id.progress)
    ProgressBar progress;
    LinearLayoutManager linearLayoutManager;
    HomeworkListAdapter homeworkListAdapter;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    public WeeklyPlanHomeworkFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_weekly_plan_homework, container, false);
        ButterKnife.bind(this, view);

        homeworkListAdapter = new HomeworkListAdapter(getContext());
        linearLayoutManager = new LinearLayoutManager(getContext());

        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(homeworkListAdapter);


        return view;
    }

    private void loadingData(ArrayList<HomeWorkBean> homeWork) {
        if (homeWork != null && homeWork.size() != 0) {
            homeworkListAdapter.addAllHomeWork(homeWork);
            noData.setVisibility(View.INVISIBLE);
        } else {
            homeworkListAdapter.clearHomeworkAll();
            noData.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    public void onEventMainThread(final HomeworkEvent event) {
        loadingData(event.getHomeWork());
    }
}
