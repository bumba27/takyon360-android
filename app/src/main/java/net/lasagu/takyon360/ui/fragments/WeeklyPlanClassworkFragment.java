package net.lasagu.takyon360.ui.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import net.lasagu.takyon360.R;
import net.lasagu.takyon360.events.ClassworkEvent;
import net.lasagu.takyon360.models.ClassWorkBean;
import net.lasagu.takyon360.widgets.ClassworkAdapter;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.greenrobot.event.EventBus;

/**
 * A simple {@link Fragment} subclass.
 */
public class WeeklyPlanClassworkFragment extends Fragment {

    @BindView(R.id.noData)
    LinearLayout noData;
    @BindView(R.id.progress)
    ProgressBar progress;
    LinearLayoutManager linearLayoutManager;
    ClassworkAdapter classworkAdapter;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    public WeeklyPlanClassworkFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_weekly_plan_classwork, container, false);
        ButterKnife.bind(this, view);


        classworkAdapter = new ClassworkAdapter(getContext());
        linearLayoutManager = new LinearLayoutManager(getContext());

        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(classworkAdapter);

        return view;
    }

    private void loadingData(ArrayList<ClassWorkBean> classWorkBeans) {
        if (classWorkBeans != null && classWorkBeans.size() != 0) {
            classworkAdapter.addAllClassWork(classWorkBeans);
            noData.setVisibility(View.INVISIBLE);
        } else {
            classworkAdapter.clearClassWorkAll();
            noData.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    public void onEventMainThread(final ClassworkEvent event) {
        loadingData(event.getClassWork());
    }
}

