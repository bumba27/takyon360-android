package net.lasagu.takyon360.ui.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import net.lasagu.takyon360.R;
import net.lasagu.takyon360.events.AssessmentEvent;
import net.lasagu.takyon360.events.QuizeEvent;
import net.lasagu.takyon360.models.AssessmentsBean;
import net.lasagu.takyon360.models.Quizes;
import net.lasagu.takyon360.widgets.AssessmentAdapter;
import net.lasagu.takyon360.widgets.QuizeAdapter;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.greenrobot.event.EventBus;

/**
 * A simple {@link Fragment} subclass.
 */
public class QuizeFragment extends Fragment {

    @BindView(R.id.noData)
    LinearLayout noData;
    @BindView(R.id.progress)
    ProgressBar progress;
    LinearLayoutManager linearLayoutManager;
    QuizeAdapter quizeAdapter;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    public QuizeFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_weekly_plan_quize, container, false);
        ButterKnife.bind(this, view);


        quizeAdapter = new QuizeAdapter(getContext());
        linearLayoutManager = new LinearLayoutManager(getContext());

        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(quizeAdapter);

        return view;
    }

    private void loadingData(ArrayList<Quizes> quizes) {
        if (quizes != null && quizes.size() != 0) {
            quizeAdapter.addAllAssessments(quizes);
            noData.setVisibility(View.INVISIBLE);
        } else {
            quizeAdapter.clearAssessmentsAll();
            noData.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    public void onEventMainThread(final QuizeEvent event) {
        loadingData(event.getQuizes());
    }
}
