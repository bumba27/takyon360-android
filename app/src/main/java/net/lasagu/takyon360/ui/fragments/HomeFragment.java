package net.lasagu.takyon360.ui.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.sdsmdg.tastytoast.TastyToast;

import net.lasagu.takyon360.MyApplication;
import net.lasagu.takyon360.R;
import net.lasagu.takyon360.events.LoadNotificationEvent;
import net.lasagu.takyon360.events.NotificationReadEvent;
import net.lasagu.takyon360.jobs.SiblingLoginJob;
import net.lasagu.takyon360.models.LoginResponse;
import net.lasagu.takyon360.models.Notification;
import net.lasagu.takyon360.models.Siblings;
import net.lasagu.takyon360.ui.MainActivity;
import net.lasagu.takyon360.utils.Constant;
import net.lasagu.takyon360.utils.PreferencesData;
import net.lasagu.takyon360.utils.ReusableClass;
import net.lasagu.takyon360.utils.SharedPreferenceStore;
import net.lasagu.takyon360.widgets.NotificationListAdapter;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.greenrobot.event.EventBus;
import de.hdodenhof.circleimageview.CircleImageView;


/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment {

    private static final String TAG = HomeFragment.class.getSimpleName();
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.noData)
    LinearLayout noDataLinearLayout;
    @BindView(R.id.progress)
    ProgressBar progress;
    @BindView(R.id.SwipeRefreshLayout)
    android.support.v4.widget.SwipeRefreshLayout SwipeRefreshLayout;
    @BindView(R.id.imageViewStudentImage)
    CircleImageView imageViewStudentImage;
    @BindView(R.id.textViewStudentName)
    TextView textViewStudentName;
    @BindView(R.id.textViewClassAndDetails)
    TextView textViewClassAndDetails;
    @BindView(R.id.textViewParentName)
    TextView textViewParentName;
    @BindView(R.id.topArrowImage)
    ImageView topArrowImage;
    private NotificationListAdapter notificationListAdapter;
    LinearLayoutManager linearLayoutManager;
    private int siblingId = 0;
    private int siblingPosition = 0;

    public HomeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        ButterKnife.bind(this, view);

        siblingId = getArguments().getInt("SIBLINGS_ID");
        notificationListAdapter = new NotificationListAdapter(getContext());
        linearLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(notificationListAdapter);

        ReusableClass.setVectorBackground(getContext(), R.drawable.ic_arrow_drop_down_red, topArrowImage, R.color.colorPrimary);

        loadingData();

        SwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (ReusableClass.isNetworkAvailable(getContext())) {
                    ((MainActivity) getActivity()).showingProgress();
                    MyApplication.addJobInBackground(new SiblingLoginJob(String.valueOf(siblingId)));
                } else
                    TastyToast.makeText(getContext(), "Sorry no internet connection.", Toast.LENGTH_LONG, TastyToast.ERROR).show();

            }
        });

        return view;
    }

    private void loadingData() {
        progress.setVisibility(View.VISIBLE);
        LoginResponse loginResponse = LoginResponse.fromJson(SharedPreferenceStore.getValue(getContext(), PreferencesData.LOGIN_RESPONSE, ""));

        if (loginResponse.getUserType().equalsIgnoreCase(Constant.PARENT) || loginResponse.getUserType().equalsIgnoreCase(Constant.STUDENT)) {
            Siblings[] siblingses = loginResponse.getSiblings();
            for (int i = 0; i < siblingses.length; i++) {
                if (siblingses[i].getStudentID() == String.valueOf(siblingId)) {
                    siblingPosition = i;
                }
            }
            if (siblingses.length > 0) {
                if (siblingses[siblingPosition].getProfileImage() != null && !siblingses[siblingPosition].getProfileImage().equalsIgnoreCase(""))
                    Glide.with(this).load(siblingses[siblingPosition].getProfileImage()).into(imageViewStudentImage);
                else
                    imageViewStudentImage.setImageDrawable(getActivity().getDrawable(R.drawable.empty_avatar));

                textViewStudentName.setText(siblingses[siblingPosition].getStudentName());
                textViewClassAndDetails.setText("Class - " + siblingses[siblingPosition].getClassName());
                textViewParentName.setText(loginResponse.getName());
            }
        } else {
            if (loginResponse.getProfileImage() != null && !loginResponse.getProfileImage().equalsIgnoreCase(""))
                Glide.with(this).load(loginResponse.getProfileImage()).into(imageViewStudentImage);
            else
                imageViewStudentImage.setImageDrawable(getActivity().getDrawable(R.drawable.empty_avatar));


            textViewStudentName.setText(loginResponse.getName());
            textViewClassAndDetails.setText(loginResponse.getSchoolName());
            textViewParentName.setVisibility(View.GONE);
        }

        ArrayList<Notification> notificationList = loginResponse.getNotification();

        notificationListAdapter.clearAll();
        if (notificationList.size() != 0) {
            notificationListAdapter.addAll(notificationList);
            noDataLinearLayout.setVisibility(View.INVISIBLE);
        } else {
            noDataLinearLayout.setVisibility(View.VISIBLE);
        }
        progress.setVisibility(View.INVISIBLE);
        if (SwipeRefreshLayout.isRefreshing())
            SwipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().registerSticky(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    public void onEventMainThread(final LoadNotificationEvent.Success event) {
        if (event.getReadStatus()) {
            siblingId = event.getSiblingId();
            loadingData();
        }

        EventBus.getDefault().removeStickyEvent(event);
    }

    public void onEventMainThread(final NotificationReadEvent.Success event) {
        if (event.genericResponse().getStatusCode() == 1) {
            LoginResponse loginResponse = LoginResponse.fromJson(SharedPreferenceStore.getValue(getContext(), PreferencesData.LOGIN_RESPONSE, ""));
            ArrayList<Notification> notificationList = loginResponse.getNotification();
            notificationList.remove(event.getPosition());

            loginResponse.setNotification(notificationList);
            SharedPreferenceStore.storeValue(getContext(), PreferencesData.LOGIN_RESPONSE, loginResponse.toString());

            Log.d(TAG, "Notification read successfully");
        }

        EventBus.getDefault().removeStickyEvent(event);
    }
}
