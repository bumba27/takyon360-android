package net.lasagu.takyon360.ui;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.sdsmdg.tastytoast.TastyToast;

import net.lasagu.takyon360.BaseActivity;
import net.lasagu.takyon360.MyApplication;
import net.lasagu.takyon360.R;
import net.lasagu.takyon360.events.DigitalResourceListResponseEvent;
import net.lasagu.takyon360.jobs.GetDigitalResourceListJob;
import net.lasagu.takyon360.models.CategoryItemsBean;
import net.lasagu.takyon360.models.CategoryWithPaginationSubmission;
import net.lasagu.takyon360.models.DigitalResourceListResponse;
import net.lasagu.takyon360.utils.ReusableClass;
import net.lasagu.takyon360.widgets.DigitalResourceListAdapter;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.greenrobot.event.EventBus;

import static net.lasagu.takyon360.utils.ReusableClass.updateResources;

public class DigitalResourceListActivity extends BaseActivity {

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.SwipeRefreshLayout)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.noData)
    LinearLayout noData;
    @BindView(R.id.progress)
    ProgressBar progress;
    LinearLayoutManager linearLayoutManager;
    int pastVisibleItems, visibleItemCount, totalItemCount;
    private DigitalResourceListAdapter digitalResourceListAdapter;
    private boolean loading = true;
    private int pageNo = 1;
    int categoryId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_digital_resource_list);
        ButterKnife.bind(this);
        updateResources(DigitalResourceListActivity.this);

        categoryId = getIntent().getIntExtra("CATEGORY_ID", 0);
        getSupportActionBar().setTitle(getIntent().getStringExtra("CATEGORY_NAME"));

        digitalResourceListAdapter = new DigitalResourceListAdapter(this);
        linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(digitalResourceListAdapter);

        loadingData(1, categoryId);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                pageNo = 1;
                loadingData(pageNo, categoryId);
            }
        });

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (dy > 0) //check for scroll down
                {
                    visibleItemCount = linearLayoutManager.getChildCount();
                    totalItemCount = linearLayoutManager.getItemCount();
                    pastVisibleItems = linearLayoutManager.findFirstVisibleItemPosition();

                    if (loading) {
                        if ((visibleItemCount + pastVisibleItems) >= totalItemCount) {
                            loading = false;
                            Log.v("...", "Last Item Wow !");
                            pageNo = pageNo + 1;
                            loadingData(pageNo, categoryId);
                        }
                    }
                }
            }
        });
    }

    private void loadingData(int pagination, int categoryId) {
        if (ReusableClass.isNetworkAvailable(this)) {
            progress.setVisibility(View.VISIBLE);

            CategoryWithPaginationSubmission categoryWithPaginationSubmission = new CategoryWithPaginationSubmission();
            categoryWithPaginationSubmission.setCategoryId(categoryId);
            categoryWithPaginationSubmission.setSearchText("");
            categoryWithPaginationSubmission.setPaginationNumber(pagination);

            MyApplication.addJobInBackground(new GetDigitalResourceListJob(categoryWithPaginationSubmission));
        } else
            TastyToast.makeText(this, "Sorry no internet connection.", Toast.LENGTH_LONG, TastyToast.ERROR).show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.search_menu, menu);

        final MenuItem myActionMenuItem = menu.findItem(R.id.action_search);
        final SearchView searchView = (SearchView) myActionMenuItem.getActionView();

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                if (ReusableClass.isNetworkAvailable(DigitalResourceListActivity.this)) {
                    progress.setVisibility(View.VISIBLE);

                    CategoryWithPaginationSubmission categoryWithPaginationSubmission = new CategoryWithPaginationSubmission();
                    categoryWithPaginationSubmission.setCategoryId(categoryId);
                    categoryWithPaginationSubmission.setSearchText(query);
                    categoryWithPaginationSubmission.setPaginationNumber(1);

                    MyApplication.addJobInBackground(new GetDigitalResourceListJob(categoryWithPaginationSubmission));
                } else
                    TastyToast.makeText(DigitalResourceListActivity.this, "Sorry no internet connection.", Toast.LENGTH_LONG, TastyToast.ERROR).show();

                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                return false;
            }
        });

        ImageView closeButton = (ImageView) searchView.findViewById(android.support.v7.appcompat.R.id.search_close_btn);

        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myActionMenuItem.collapseActionView();

                if (ReusableClass.isNetworkAvailable(DigitalResourceListActivity.this)) {
                    progress.setVisibility(View.VISIBLE);

                    CategoryWithPaginationSubmission categoryWithPaginationSubmission = new CategoryWithPaginationSubmission();
                    categoryWithPaginationSubmission.setCategoryId(categoryId);
                    categoryWithPaginationSubmission.setSearchText("");
                    categoryWithPaginationSubmission.setPaginationNumber(1);

                    MyApplication.addJobInBackground(new GetDigitalResourceListJob(categoryWithPaginationSubmission));
                } else
                    TastyToast.makeText(DigitalResourceListActivity.this, "Sorry no internet connection.", Toast.LENGTH_LONG, TastyToast.ERROR).show();
            }
        });

        return true;
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    public void onEventMainThread(final DigitalResourceListResponseEvent.Success event) {
        DigitalResourceListResponse digitalResourceListResponse = event.getDigitalResourceListResponse();
        ArrayList<CategoryItemsBean> categoryItems = digitalResourceListResponse.getCategoryItems();
        if (categoryItems.size() != 0) {
            if (pageNo == 1)
                digitalResourceListAdapter.addAll(categoryItems);
            else
                digitalResourceListAdapter.addMore(categoryItems);

            noData.setVisibility(View.INVISIBLE);
            loading = true;
        } else {
            if (pageNo == 1) {
                noData.setVisibility(View.VISIBLE);
                digitalResourceListAdapter.clearAll();
            }
        }
        progress.setVisibility(View.INVISIBLE);
        if (swipeRefreshLayout.isRefreshing())
            swipeRefreshLayout.setRefreshing(false);
    }

    public void onEventMainThread(DigitalResourceListResponseEvent.Fail event) {
        if (event.getEx() != null) {
            TastyToast.makeText(this, "Seems server is busy try after sometime.", TastyToast.LENGTH_LONG, TastyToast.ERROR).show();
        }
    }
}
