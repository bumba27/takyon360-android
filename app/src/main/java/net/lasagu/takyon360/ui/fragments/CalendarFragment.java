package net.lasagu.takyon360.ui.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.github.tibolte.agendacalendarview.AgendaCalendarView;
import com.github.tibolte.agendacalendarview.CalendarPickerController;
import com.github.tibolte.agendacalendarview.models.BaseCalendarEvent;
import com.github.tibolte.agendacalendarview.models.CalendarEvent;
import com.github.tibolte.agendacalendarview.models.DayItem;
import com.sdsmdg.tastytoast.TastyToast;

import net.lasagu.takyon360.MyApplication;
import net.lasagu.takyon360.R;
import net.lasagu.takyon360.jobs.CalenderEventsJob;
import net.lasagu.takyon360.models.CalenderEventResponse;
import net.lasagu.takyon360.models.EventsBean;
import net.lasagu.takyon360.models.UserIdSubmission;
import net.lasagu.takyon360.ui.MainActivity;
import net.lasagu.takyon360.utils.PreferencesData;
import net.lasagu.takyon360.utils.ReusableClass;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.pedant.SweetAlert.SweetAlertDialog;

/**
 * A simple {@link Fragment} subclass.
 */
public class CalendarFragment extends Fragment implements CalendarPickerController {

    @BindView(R.id.noData)
    LinearLayout noData;
    @BindView(R.id.progress)
    ProgressBar progress;
    AgendaCalendarView agendaCalendarView;
    private SweetAlertDialog pDialog;
    private String viewEvents;
    private String nameLabel;
    private String startDateLabel;
    private String endDateLabel;
    private String noteLabel;

    public CalendarFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_calendar, container, false);
        ButterKnife.bind(this, view);
        agendaCalendarView = (AgendaCalendarView) view.findViewById(R.id.agenda_calendar_view);
        loadingData();

        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    private void loadingData() {
        if (ReusableClass.isNetworkAvailable(getContext())) {
            progress.setVisibility(View.VISIBLE);

            UserIdSubmission userIdSubmission = new UserIdSubmission();
            userIdSubmission.setUserId(PreferencesData.getUserId(getContext()));

            MyApplication.addJobInBackground(new CalenderEventsJob(userIdSubmission));
        } else
            TastyToast.makeText(getContext(), "Sorry no internet connection.", Toast.LENGTH_LONG, TastyToast.ERROR).show();
    }


    @Override
    public void onDaySelected(DayItem dayItem) {

    }

    @Override
    public void onEventSelected(CalendarEvent event) {
        pDialog = new SweetAlertDialog(getContext(), SweetAlertDialog.CUSTOM_IMAGE_TYPE);
        pDialog.setTitleText(viewEvents);
        pDialog.setContentText(nameLabel + " : " + event.getTitle() +
                "\n" +
                startDateLabel + " : " + getHumanReadableDate(event.getStartTime()) +
                "\n" +
                endDateLabel + " : " + getHumanReadableDate(event.getEndTime()) +
                "\n" +
                noteLabel + " : " + ((BaseCalendarEvent) event).getDescription());
        pDialog.setCancelable(true);
        pDialog.show();
    }

    @Override
    public void onScrollToDate(Calendar calendar) {

    }

    public void loadingEventData(CalenderEventResponse calenderEventResponse) {
        viewEvents = calenderEventResponse.getViewEvents();
        nameLabel = calenderEventResponse.getNameLabel();
        startDateLabel = calenderEventResponse.getStartDateLabel();
        endDateLabel = calenderEventResponse.getEndDateLabel();
        noteLabel = calenderEventResponse.getNoteLabel();
        ((MainActivity)getActivity()).getSupportActionBar().setTitle(calenderEventResponse.getCalendarLabel());

        ArrayList<EventsBean> eventsBeen = calenderEventResponse.getEvents();
        if (eventsBeen.size() != 0) {

            // minimum and maximum date of our calendar
            // 2 month behind, one year ahead, example: March 2015 <-> May 2015 <-> May 2016
            Calendar minDate = Calendar.getInstance();
            Calendar maxDate = Calendar.getInstance();

            minDate.add(Calendar.YEAR, -10);
            minDate.set(Calendar.DAY_OF_MONTH, 1);
            maxDate.add(Calendar.YEAR, 1);

            List<CalendarEvent> eventList = new ArrayList<>();

            for (int i = 0; i < eventsBeen.size(); i++) {
                Calendar startTime = getCalenderTime(eventsBeen.get(i).getStart_date());
                Calendar endTime = getCalenderTime(eventsBeen.get(i).getEnd_date());
                endTime.add(Calendar.MONTH, 0);


                BaseCalendarEvent event1 = new BaseCalendarEvent(eventsBeen.get(i).getText(),
                        eventsBeen.get(i).getDetails(),
                        "",
                        ContextCompat.getColor(getContext(), R.color.calender_colour),
                        startTime,
                        endTime,
                        true);
                eventList.add(event1);
            }

            agendaCalendarView.init(eventList, minDate, maxDate, Locale.getDefault(), CalendarFragment.this);

            noData.setVisibility(View.INVISIBLE);
        } else {
            noData.setVisibility(View.VISIBLE);
        }
        progress.setVisibility(View.INVISIBLE);
    }

    private Calendar getCalenderTime(String start_date) {
        Calendar cal = Calendar.getInstance();
        try {
            //2016-11-16 08:00:00
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
            cal.setTime(sdf.parse(start_date));// all done
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return cal;
    }

    private String getHumanReadableDate(Calendar cal) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return dateFormat.format(cal.getTime());
    }
}
