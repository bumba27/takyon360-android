package net.lasagu.takyon360.widgets;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import net.lasagu.takyon360.R;
import net.lasagu.takyon360.models.ItemsBean;
import net.lasagu.takyon360.ui.NotificationBoardDetailsActivity;

import java.util.ArrayList;

import butterknife.BindColor;
import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * Created by ajana on 05/07/2017.
 */

public class NoticeBoardListAdapter extends RecyclerView.Adapter<NoticeBoardListAdapter.MyViewHolder> {
    private Context context;
    private ArrayList<ItemsBean> noticeBoardItems;
    private ArrayList<ItemsBean> noticeBoardItemsCopy;


    public NoticeBoardListAdapter(Context context) {
        this.context = context;
        noticeBoardItems = new ArrayList<>();
        noticeBoardItemsCopy = new ArrayList<>();
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.title_and_sub_title_row, parent, false);
        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

        final ItemsBean itemsBean = noticeBoardItems.get(position);

        holder.title.setLines(2);
        holder.subTitle.setVisibility(View.GONE);
        if (itemsBean.getImage() != null && !itemsBean.getImage().equalsIgnoreCase(""))
            Glide.with(context).load(itemsBean.getImage()).into(holder.schoolImage);
        else
            holder.schoolImage.setImageDrawable(context.getDrawable(R.drawable.school_default));


        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            holder.title.setText(Html.fromHtml(itemsBean.getTitle(), Html.FROM_HTML_MODE_LEGACY));
            holder.subSubTitle.setText(Html.fromHtml(itemsBean.getDate(), Html.FROM_HTML_MODE_LEGACY));
        } else {
            holder.title.setText(Html.fromHtml(itemsBean.getTitle()));
            holder.subSubTitle.setText(Html.fromHtml(itemsBean.getDate()));
        }

        holder.mainLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, NotificationBoardDetailsActivity.class);
                intent.putExtra("CATEGORY_DETAILS", itemsBean.toString());
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return noticeBoardItems.size();
    }

    public void addAll(ArrayList<ItemsBean> responses) {
        clearAll();
        noticeBoardItems.addAll(responses);
        noticeBoardItemsCopy.addAll(responses);
        notifyDataSetChanged();
    }

    public void clearAll() {
        noticeBoardItems.clear();
    }

    public void filter(String text) {
        noticeBoardItems.clear();
        if(text.isEmpty()){
            noticeBoardItems.addAll(noticeBoardItemsCopy);
        } else{
            text = text.toLowerCase();
            for (int i = 0; i < noticeBoardItemsCopy.size(); i++) {
                if(noticeBoardItemsCopy.get(i).getTitle().toLowerCase().contains(text) ||
                        noticeBoardItemsCopy.get(i).getDate().toLowerCase().contains(text)) {
                    noticeBoardItems.add(noticeBoardItemsCopy.get(i));
                }
            }
        }
        notifyDataSetChanged();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.schoolImage)
        ImageView schoolImage;
        @BindView(R.id.title)
        TextView title;
        @BindView(R.id.subTitle)
        TextView subTitle;
        @BindView(R.id.subSubTitle)
        TextView subSubTitle;
        @BindView(R.id.mainLayout)
        LinearLayout mainLayout;
        @BindColor(R.color.colorAccent)
        int colorAccent;
        @BindColor(R.color.black)
        int black;

        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}