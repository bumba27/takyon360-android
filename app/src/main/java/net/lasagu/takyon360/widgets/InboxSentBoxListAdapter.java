package net.lasagu.takyon360.widgets;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import net.lasagu.takyon360.R;
import net.lasagu.takyon360.models.MessageList;
import net.lasagu.takyon360.ui.MessageDetailsActivity;
import net.lasagu.takyon360.utils.Constant;

import java.util.ArrayList;

import butterknife.BindColor;
import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * Created by ajana on 05/07/2017.
 */

public class InboxSentBoxListAdapter extends RecyclerView.Adapter<InboxSentBoxListAdapter.MyViewHolder> {
    private Context context;
    private int messageBox;
    private ArrayList<MessageList> messageLists;
    private ArrayList<MessageList> messageListsCopy;


    public InboxSentBoxListAdapter(Context context, int messageBox) {
        this.context = context;
        this.messageBox = messageBox;
        messageLists = new ArrayList<>();
        messageListsCopy = new ArrayList<>();
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.communicate_list_row, parent, false);
        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        final MessageList message = messageLists.get(position);
        if (message.getUserProfileImg() != null && !message.getUserProfileImg().equalsIgnoreCase(""))
            Glide.with(context).load(message.getUserProfileImg()).into(holder.profileImage);
        else
            holder.profileImage.setImageDrawable(context.getDrawable(R.drawable.empty_avatar));

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            holder.title.setText(Html.fromHtml(message.getUser(), Html.FROM_HTML_MODE_LEGACY));
            holder.subTitle.setText(Html.fromHtml(message.getSujbect(), Html.FROM_HTML_MODE_LEGACY));
            holder.subSubTitle.setText(Html.fromHtml(message.getDate(), Html.FROM_HTML_MODE_LEGACY));
        } else {
            holder.title.setText(Html.fromHtml(message.getUser()));
            holder.subTitle.setText(Html.fromHtml(message.getSujbect()));
            holder.subSubTitle.setText(Html.fromHtml(message.getDate()));
        }


        if (messageBox == Constant.INBOX) {
            if (message.getIsRead() == 1) {
                holder.title.setTextColor(holder.colorAccent);
                holder.subTitle.setTextColor(holder.colorAccent);
            } else {
                holder.title.setTextColor(holder.black);
                holder.subTitle.setTextColor(holder.black);
            }
        } else if (messageBox == Constant.OUTBOX) {
            holder.title.setTextColor(holder.colorAccent);
            holder.subTitle.setTextColor(holder.colorAccent);
        }

        if (message.getAttachIcon().equalsIgnoreCase("1")) {
            holder.attachmentIcon.setVisibility(View.VISIBLE);
        } else {
            holder.attachmentIcon.setVisibility(View.GONE);
        }

        holder.mainLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (messageBox == Constant.INBOX) {
                    holder.title.setTextColor(holder.colorAccent);
                    holder.subTitle.setTextColor(holder.colorAccent);
                }


                Intent intent = new Intent(context, MessageDetailsActivity.class);
                intent.putExtra("MESSAGE_ID", message.getId());
                intent.putExtra("MESSAGE_BOX", messageBox);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return messageLists.size();
    }

    public void addAll(ArrayList<MessageList> responses) {
        clearAll();
        messageLists.addAll(responses);
        messageListsCopy.addAll(responses);
        notifyDataSetChanged();
    }

    public void clearAll() {
        messageLists.clear();
        messageListsCopy.clear();
    }

    public void addMore(ArrayList<MessageList> messageArray) {
        messageLists.addAll(messageArray);
        messageListsCopy.addAll(messageArray);
        notifyDataSetChanged();
    }

    public void filter(String text) {
        messageLists.clear();
        if (text.isEmpty()) {
            messageLists.addAll(messageListsCopy);
        } else {
            text = text.toLowerCase();
            for (int i = 0; i < messageListsCopy.size(); i++) {
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N &&
                        (Html.fromHtml(messageListsCopy.get(i).getSujbect(), Html.FROM_HTML_MODE_LEGACY).toString().toLowerCase().contains(text) ||
                                Html.fromHtml(messageListsCopy.get(i).getUser(), Html.FROM_HTML_MODE_LEGACY).toString().toLowerCase().contains(text))) {
                    messageLists.add(messageListsCopy.get(i));
                } else if (Html.fromHtml(messageListsCopy.get(i).getSujbect()).toString().toLowerCase().contains(text) ||
                        Html.fromHtml(messageListsCopy.get(i).getUser()).toString().toLowerCase().contains(text)) {
                    messageLists.add(messageListsCopy.get(i));
                }
            }
        }
        notifyDataSetChanged();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.schoolImage)
        ImageView profileImage;
        @BindView(R.id.title)
        TextView title;
        @BindView(R.id.subTitle)
        TextView subTitle;
        @BindView(R.id.subSubTitle)
        TextView subSubTitle;
        @BindView(R.id.attachmentIcon)
        ImageView attachmentIcon;
        @BindView(R.id.mainLayout)
        LinearLayout mainLayout;
        @BindColor(R.color.colorAccent)
        int colorAccent;
        @BindColor(R.color.black)
        int black;

        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}