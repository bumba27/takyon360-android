package net.lasagu.takyon360.widgets;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import net.lasagu.takyon360.R;
import net.lasagu.takyon360.models.ArticleCategoriesBean;
import net.lasagu.takyon360.ui.AwarenessListActivity;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * Created by ajana on 05/07/2017.
 */

public class AwarenessAndPoliciesAdapter extends RecyclerView.Adapter<AwarenessAndPoliciesAdapter.MyViewHolder> {
    private Context context;
    private ArrayList<ArticleCategoriesBean> categoriesBeen;
    private ArrayList<ArticleCategoriesBean> categoriesBeenCopy;


    public AwarenessAndPoliciesAdapter(Context context) {
        this.context = context;
        categoriesBeen = new ArrayList<>();
        categoriesBeenCopy = new ArrayList<>();
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.category_list_row, parent, false);
        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

        final ArticleCategoriesBean categoriesBean = categoriesBeen.get(position);

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            holder.title.setText(Html.fromHtml(categoriesBean.getCategory(), Html.FROM_HTML_MODE_LEGACY));
        } else {
            holder.title.setText(Html.fromHtml(categoriesBean.getCategory()));
        }

        holder.mainLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, AwarenessListActivity.class);
                intent.putExtra("CATEGORY_ID", categoriesBean.getCategoryId());
                intent.putExtra("CATEGORY_NAME", categoriesBean.getCategory());
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return categoriesBeen.size();
    }

    public void addAll(ArrayList<ArticleCategoriesBean> responses) {
        clearAll();
        categoriesBeen.addAll(responses);
        categoriesBeenCopy.addAll(responses);
        notifyDataSetChanged();
    }

    public void clearAll() {
        categoriesBeen.clear();
    }

    public void filter(String text) {
        categoriesBeen.clear();
        if(text.isEmpty()){
            categoriesBeen.addAll(categoriesBeenCopy);
        } else{
            text = text.toLowerCase();
            for (int i = 0; i < categoriesBeenCopy.size(); i++) {
                if(categoriesBeenCopy.get(i).getCategory().toLowerCase().contains(text)) {
                    categoriesBeen.add(categoriesBeenCopy.get(i));
                }
            }
        }
        notifyDataSetChanged();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.title)
        TextView title;
        @BindView(R.id.mainLayout)
        LinearLayout mainLayout;

        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}