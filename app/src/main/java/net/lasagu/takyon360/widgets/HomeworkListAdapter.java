package net.lasagu.takyon360.widgets;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import net.lasagu.takyon360.R;
import net.lasagu.takyon360.models.HomeWorkBean;
import net.lasagu.takyon360.ui.WeeklyPlanDetailsActivity;
import net.lasagu.takyon360.ui.fragments.WeeklyPlanHomeworkFragment;

import java.util.ArrayList;

import butterknife.BindColor;
import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * Created by ajana on 05/07/2017.
 */

public class HomeworkListAdapter extends RecyclerView.Adapter<HomeworkListAdapter.MyViewHolder> {
    private Context context;
    private ArrayList<HomeWorkBean> homeWorkBeen;


    public HomeworkListAdapter(Context context) {
        this.context = context;
        homeWorkBeen = new ArrayList<>();
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.title_and_sub_title_row, parent, false);
        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        final HomeWorkBean singleHomeWorkBean = homeWorkBeen.get(position);

        holder.subSubTitle.setText(singleHomeWorkBean.getDate());
        holder.schoolImage.setScaleType(ImageView.ScaleType.CENTER_CROP);

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            holder.title.setText(Html.fromHtml(singleHomeWorkBean.getTopic(), Html.FROM_HTML_MODE_LEGACY));
            holder.subTitle.setText(Html.fromHtml(singleHomeWorkBean.getDescription(), Html.FROM_HTML_MODE_LEGACY));
        } else {
            holder.title.setText(Html.fromHtml(singleHomeWorkBean.getTopic()));
            holder.subTitle.setText(Html.fromHtml(singleHomeWorkBean.getDescription()));
        }

        if (singleHomeWorkBean.getAttachIcon() == 1) {
            holder.attachmentIcon.setVisibility(View.VISIBLE);
        } else {
            holder.attachmentIcon.setVisibility(View.INVISIBLE);
        }

        holder.mainLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, WeeklyPlanDetailsActivity.class);
                intent.putExtra("CATEGORY_DETAILS", singleHomeWorkBean.toString());
                intent.putExtra("CATEGORY_TYPE", WeeklyPlanHomeworkFragment.class.getSimpleName());
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return homeWorkBeen.size();
    }

    public void addAllHomeWork(ArrayList<HomeWorkBean> responses) {
        clearHomeworkAll();
        homeWorkBeen = responses;
        notifyDataSetChanged();
    }

    public void clearHomeworkAll() {
        homeWorkBeen.clear();
        notifyDataSetChanged();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.title)
        TextView title;
        @BindView(R.id.subTitle)
        TextView subTitle;
        @BindView(R.id.subSubTitle)
        TextView subSubTitle;
        @BindView(R.id.mainLayout)
        LinearLayout mainLayout;
        @BindView(R.id.attachmentIcon)
        ImageView attachmentIcon;
        @BindView(R.id.schoolImage)
        ImageView schoolImage;
        @BindColor(R.color.colorAccent)
        int colorAccent;
        @BindColor(R.color.black)
        int black;

        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}