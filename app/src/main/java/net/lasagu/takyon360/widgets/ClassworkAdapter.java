package net.lasagu.takyon360.widgets;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import net.lasagu.takyon360.R;
import net.lasagu.takyon360.models.ClassWorkBean;
import net.lasagu.takyon360.ui.WeeklyPlanDetailsActivity;
import net.lasagu.takyon360.ui.fragments.WeeklyPlanClassworkFragment;

import java.util.ArrayList;

import butterknife.BindColor;
import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * Created by ajana on 05/07/2017.
 */

public class ClassworkAdapter extends RecyclerView.Adapter<ClassworkAdapter.MyViewHolder> {
    private Context context;
    private ArrayList<ClassWorkBean> classWorkBean;


    public ClassworkAdapter(Context context) {
        this.context = context;
        classWorkBean = new ArrayList<>();
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.title_and_sub_title_row, parent, false);
        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        final ClassWorkBean singleClassWorkBean = classWorkBean.get(position);

        holder.schoolImage.setScaleType(ImageView.ScaleType.CENTER_CROP);

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            holder.title.setText(Html.fromHtml(singleClassWorkBean.getTopic(), Html.FROM_HTML_MODE_LEGACY));
            holder.subTitle.setText(Html.fromHtml(singleClassWorkBean.getDescription(), Html.FROM_HTML_MODE_LEGACY));
            holder.subSubTitle.setText(Html.fromHtml(singleClassWorkBean.getDate(), Html.FROM_HTML_MODE_LEGACY));
        } else {
            holder.title.setText(Html.fromHtml(singleClassWorkBean.getTopic()));
            holder.subTitle.setText(Html.fromHtml(singleClassWorkBean.getDescription()));
            holder.subTitle.setText(Html.fromHtml(singleClassWorkBean.getDate()));
        }


        holder.mainLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, WeeklyPlanDetailsActivity.class);
                intent.putExtra("CATEGORY_DETAILS", singleClassWorkBean.toString());
                intent.putExtra("CATEGORY_TYPE", WeeklyPlanClassworkFragment.class.getSimpleName());
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return classWorkBean.size();
    }

    public void addAllClassWork(ArrayList<ClassWorkBean> responses) {
        clearClassWorkAll();
        classWorkBean = responses;
        notifyDataSetChanged();
    }

    public void clearClassWorkAll() {
        classWorkBean.clear();
        notifyDataSetChanged();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.title)
        TextView title;
        @BindView(R.id.subTitle)
        TextView subTitle;
        @BindView(R.id.subSubTitle)
        TextView subSubTitle;
        @BindView(R.id.mainLayout)
        LinearLayout mainLayout;
        @BindView(R.id.attachmentIcon)
        ImageView attachmentIcon;
        @BindView(R.id.schoolImage)
        ImageView schoolImage;
        @BindColor(R.color.colorAccent)
        int colorAccent;
        @BindColor(R.color.black)
        int black;

        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}