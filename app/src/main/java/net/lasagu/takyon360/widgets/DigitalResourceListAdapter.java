package net.lasagu.takyon360.widgets;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import net.lasagu.takyon360.R;
import net.lasagu.takyon360.models.CategoryItemsBean;
import net.lasagu.takyon360.ui.DigitalResourceDetailsActivity;

import java.util.ArrayList;

import butterknife.BindColor;
import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * Created by ajana on 05/07/2017.
 */

public class DigitalResourceListAdapter extends RecyclerView.Adapter<DigitalResourceListAdapter.MyViewHolder> {
    private Context context;
    private ArrayList<CategoryItemsBean> categoryItemsBeen;


    public DigitalResourceListAdapter(Context context) {
        this.context = context;
        categoryItemsBeen = new ArrayList<>();
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.title_and_sub_title_row, parent, false);
        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

        final CategoryItemsBean categoryItemsBean = categoryItemsBeen.get(position);

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            holder.title.setText(Html.fromHtml(categoryItemsBean.getTitle(), Html.FROM_HTML_MODE_LEGACY));
            holder.subTitle.setText(Html.fromHtml(categoryItemsBean.getDate(), Html.FROM_HTML_MODE_LEGACY));
        } else {
            holder.title.setText(Html.fromHtml(categoryItemsBean.getTitle()));
            holder.subTitle.setText(Html.fromHtml(categoryItemsBean.getDate()));
        }

        holder.mainLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, DigitalResourceDetailsActivity.class);
                intent.putExtra("CATEGORY_DETAILS", categoryItemsBean.toString());
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return categoryItemsBeen.size();
    }

    public void addAll(ArrayList<CategoryItemsBean> responses) {
        clearAll();
        categoryItemsBeen = responses;
        notifyDataSetChanged();
    }

    public void clearAll() {
        categoryItemsBeen.clear();
        notifyDataSetChanged();
    }

    public void addMore(ArrayList<CategoryItemsBean> responses) {
        categoryItemsBeen.addAll(responses);
        notifyDataSetChanged();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.title)
        TextView title;
        @BindView(R.id.subTitle)
        TextView subTitle;
        @BindView(R.id.mainLayout)
        LinearLayout mainLayout;
        @BindColor(R.color.colorAccent)
        int colorAccent;
        @BindColor(R.color.black)
        int black;

        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}