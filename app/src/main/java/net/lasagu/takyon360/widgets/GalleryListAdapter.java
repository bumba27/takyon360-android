package net.lasagu.takyon360.widgets;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import net.lasagu.takyon360.R;
import net.lasagu.takyon360.models.CategoriesBeanGallery;
import net.lasagu.takyon360.models.GalleryItemsBean;
import net.lasagu.takyon360.ui.FullScreenImageGalleryActivity;
import net.lasagu.takyon360.ui.GalleryGridActivity;

import java.util.ArrayList;

import butterknife.BindColor;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by anirban on 8/19/17.
 */

public class GalleryListAdapter extends RecyclerView.Adapter<GalleryListAdapter.MyViewHolder> {
    private Context context;
    private ArrayList<GalleryItemsBean> galleryItemsBeen;
    private ArrayList<CategoriesBeanGallery> categories;


    public GalleryListAdapter(Context context) {
        this.context = context;
        galleryItemsBeen = new ArrayList<>();
        categories = new ArrayList<>();
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.image_title_row, parent, false);
        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

        if (galleryItemsBeen.size() > 0) {
            final GalleryItemsBean galleryItemsBean = galleryItemsBeen.get(position);

            holder.title.setText(Html.fromHtml(galleryItemsBean.getGalleryTitle()));
            if (galleryItemsBean.getThumbnail() != null && !galleryItemsBean.getThumbnail().equalsIgnoreCase(""))
                Glide.with(context).load(galleryItemsBean.getThumbnail()).apply(new RequestOptions().placeholder(R.drawable.image_placeholder)).into(holder.imageView);
            else
                Glide.with(context).load("").apply(new RequestOptions().placeholder(R.drawable.image_placeholder)).into(holder.imageView);

            holder.mainLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, FullScreenImageGalleryActivity.class);
                    intent.putExtra("IMAGE_URL", galleryItemsBean.getMedia());
                    intent.putExtra("IMAGE_TITLE", galleryItemsBean.getGalleryTitle());
                    intent.putExtra("GALLERY_ITEM_LIST", galleryItemsBeen.toString());
                    intent.putExtra("CURRENT_IMAGE_POSITION", position);
                    context.startActivity(intent);
                }
            });
        } else {
            final CategoriesBeanGallery categoriesBean = categories.get(position);

            holder.title.setText(Html.fromHtml(categoriesBean.getCategory()));
            if (categoriesBean.getThumbnail() != null && !categoriesBean.getThumbnail().equalsIgnoreCase(""))
                Glide.with(context).load(categoriesBean.getThumbnail()).apply(new RequestOptions().placeholder(R.drawable.image_placeholder)).into(holder.imageView);
            else
                holder.imageView.setImageDrawable(context.getDrawable(R.drawable.empty_avatar));

            holder.mainLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, GalleryGridActivity.class);
                    intent.putExtra("CATEGORY_ID", categoriesBean.getCategoryId());
                    intent.putExtra("CATEGORY_NAME", categoriesBean.getCategory());
                    context.startActivity(intent);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        if (galleryItemsBeen.size() > 0)
            return galleryItemsBeen.size();
        else
            return categories.size();
    }

    public void addAll(ArrayList<GalleryItemsBean> responses) {
        clearAll();
        galleryItemsBeen = responses;
        notifyDataSetChanged();
    }

    public void clearAll() {
        galleryItemsBeen.clear();
        notifyDataSetChanged();
    }

    public void addMore(ArrayList<GalleryItemsBean> responses) {
        galleryItemsBeen.addAll(responses);
        notifyDataSetChanged();
    }

    public void addCategory(ArrayList<CategoriesBeanGallery> categories) {
        clearAll();
        this.categories = categories;
        notifyDataSetChanged();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.title)
        TextView title;
        @BindView(R.id.imageView)
        ImageView imageView;
        @BindView(R.id.mainLayout)
        LinearLayout mainLayout;
        @BindColor(R.color.colorAccent)
        int colorAccent;
        @BindColor(R.color.black)
        int black;

        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}