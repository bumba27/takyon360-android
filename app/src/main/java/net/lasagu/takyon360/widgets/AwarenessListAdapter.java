package net.lasagu.takyon360.widgets;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import net.lasagu.takyon360.R;
import net.lasagu.takyon360.models.ArticlesItemsBean;
import net.lasagu.takyon360.ui.AwarenessDetailsActivity;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * Created by ajana on 05/07/2017.
 */

public class AwarenessListAdapter extends RecyclerView.Adapter<AwarenessListAdapter.MyViewHolder> {
    private Context context;
    private ArrayList<ArticlesItemsBean> categoryItemsBeen;


    public AwarenessListAdapter(Context context) {
        this.context = context;
        categoryItemsBeen = new ArrayList<>();
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.title_and_sub_title_row, parent, false);
        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

        final ArticlesItemsBean articlesItemsBean = categoryItemsBeen.get(position);

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            holder.title.setText(Html.fromHtml(articlesItemsBean.getArticleName(), Html.FROM_HTML_MODE_LEGACY));
            holder.subTitle.setText(Html.fromHtml(articlesItemsBean.getArticleDate(), Html.FROM_HTML_MODE_LEGACY));
        } else {
            holder.title.setText(Html.fromHtml(articlesItemsBean.getArticleName()));
            holder.subTitle.setText(Html.fromHtml(articlesItemsBean.getArticleDate()));
        }

        holder.mainLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, AwarenessDetailsActivity.class);
                intent.putExtra("CATEGORY_DETAILS", articlesItemsBean.toString());
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return categoryItemsBeen.size();
    }

    public void addAll(ArrayList<ArticlesItemsBean> responses) {
        clearAll();
        categoryItemsBeen = responses;
        notifyDataSetChanged();
    }

    public void clearAll() {
        categoryItemsBeen.clear();
        notifyDataSetChanged();
    }

    public void addMore(ArrayList<ArticlesItemsBean> responses) {
        categoryItemsBeen.addAll(responses);
        notifyDataSetChanged();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.title)
        TextView title;
        @BindView(R.id.subTitle)
        TextView subTitle;
        @BindView(R.id.mainLayout)
        LinearLayout mainLayout;

        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}