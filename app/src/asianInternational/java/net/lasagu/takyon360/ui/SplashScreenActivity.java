package net.lasagu.takyon360.ui;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.github.florent37.viewanimator.ViewAnimator;

import net.lasagu.takyon360.BaseActivity;
import net.lasagu.takyon360.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SplashScreenActivity extends BaseActivity {

    private final int SPLASH_DISPLAY_LENGTH = 2500;
    @BindView(R.id.imageLogo)
    ImageView imageLogo;
    @BindView(R.id.mainLogo)
    LinearLayout mainLogo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        ButterKnife.bind(this);

        ViewAnimator
                .animate(mainLogo)
                .translationY(-1000, 0)
                .alpha(0, 1)
                .duration(1000)
                .thenAnimate(imageLogo)
                .scale(1f, 0.5f, 1f)
                .accelerate()
                .duration(1000)
                .start();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent mainIntent = new Intent(SplashScreenActivity.this, LoginActivity.class);
                SplashScreenActivity.this.startActivity(mainIntent);
                SplashScreenActivity.this.finish();
            }
        }, SPLASH_DISPLAY_LENGTH);
    }
}
