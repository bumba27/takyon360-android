package net.lasagu.takyon360buzz.jobs;

import android.content.Context;
import android.util.Log;

import com.path.android.jobqueue.Job;
import com.path.android.jobqueue.Params;

import net.lasagu.takyon360buzz.MyApplication;
import net.lasagu.takyon360buzz.events.BusDetailsRefreshEvent;
import net.lasagu.takyon360buzz.events.BusDetailsResponseEvent;
import net.lasagu.takyon360buzz.models.BusDetailsResponse;
import net.lasagu.takyon360buzz.models.BusDetailsSubmission;
import net.lasagu.takyon360buzz.utils.BasicAuthInterceptor;
import net.lasagu.takyon360buzz.utils.PreferencesData;
import net.lasagu.takyon360buzz.utils.ReusableClass;
import net.lasagu.takyon360buzz.utils.URLConst;

import java.net.HttpURLConnection;
import java.util.concurrent.atomic.AtomicInteger;

import de.greenrobot.event.EventBus;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by anirban on 1/31/17.
 */

public class BusDetailsJob extends Job {
    private static final AtomicInteger jobCounter = new AtomicInteger(0);
    private final int id;
    private BusDetailsSubmission busDetailsSubmission;
    private boolean isRefresh;

    public BusDetailsJob(BusDetailsSubmission busDetailsSubmission, boolean isRefresh) {
        super(new Params(1000).requireNetwork());
        this.isRefresh = isRefresh;
        id = jobCounter.incrementAndGet();
        this.busDetailsSubmission = busDetailsSubmission;
    }


    @Override
    public void onAdded() {
    }

    @Override
    public void onRun() throws Throwable {

        if (id != jobCounter.get()) {
            // looks like other fetch jobs has been added after me. no reason to
            // keep fetching many times, cancel me, let the other one fetch
            return;
        }
        Context context = MyApplication.getInstance();

        String url = URLConst.getBusDetailsUrl(context);
        Log.d("TAG", "url = " + url);

        Log.d("TAG", "sending = " + busDetailsSubmission.toString());

        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(new BasicAuthInterceptor(PreferencesData.getUser(context).getUsername(), ReusableClass.getMd5(PreferencesData.getUser(context).getPassword())))
                .build();

        Request request = new Request.Builder()
                .url(url)
                .post(RequestBody.create(MediaType.parse("application/json; charset=utf-8"), busDetailsSubmission.toString()))
                .build();

        Response response = client.newCall(request).execute();
        String json = response.body().string();

        Log.d("TAG", "response = " + json);

        if (response.code() != HttpURLConnection.HTTP_ACCEPTED) {
            EventBus.getDefault().post(new BusDetailsResponseEvent.Fail("Some thing went wrong, did not get proper response from server."));
            return;
        } else {
            BusDetailsResponse busListResponse = BusDetailsResponse.fromJson(json);
            Log.d("TAG", "User Location = lat: " + busListResponse.getUserLatitude() + " lng: " + busListResponse.getUserLongitude());

            if (isRefresh)
                EventBus.getDefault().post(new BusDetailsRefreshEvent.Success(busListResponse));
            else
                EventBus.getDefault().post(new BusDetailsResponseEvent.Success(busListResponse));
        }
    }

    @Override
    protected void onCancel() {

    }

    @Override
    protected boolean shouldReRunOnThrowable(Throwable throwable) {
        EventBus.getDefault().post(new BusDetailsResponseEvent.Fail(throwable.getMessage()));
        return false;
    }
}
