package net.lasagu.takyon360buzz.widgets;


import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.sdsmdg.tastytoast.TastyToast;

import net.lasagu.takyon360buzz.MyApplication;
import net.lasagu.takyon360buzz.R;
import net.lasagu.takyon360buzz.utils.PreferencesData;
import net.lasagu.takyon360buzz.models.Notification;
import net.lasagu.takyon360buzz.utils.ReusableClass;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.greenrobot.event.EventBus;

/**
 * Created by ajana on 05/07/2017.
 */

public class NotificationListAdapter extends RecyclerView.Adapter<NotificationListAdapter.MyViewHolder> {
    private Context context;
    private ArrayList<Notification> notifications;


    public NotificationListAdapter(Context context) {
        this.context = context;
        notifications = new ArrayList<>();
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.notification_list_row_home_page, parent, false);
        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

        final Notification notification = notifications.get(position);

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            holder.title.setText(Html.fromHtml(notification.getTitle(), Html.FROM_HTML_MODE_LEGACY));
            if (!notification.getDetails().equalsIgnoreCase("NULL"))
                holder.subTitle.setText(Html.fromHtml(notification.getDetails(), Html.FROM_HTML_MODE_LEGACY));
        } else {
            holder.title.setText(Html.fromHtml(notification.getTitle()));
            if (!notification.getDetails().equalsIgnoreCase("NULL"))
                holder.subTitle.setText(Html.fromHtml(notification.getDetails()));
        }


        holder.subSubTitle.setText(notification.getDate().substring(0, notification.getDate().length() - 10));
        holder.imageViewAlertType.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.general_alert));


//        if (notification.getType().equalsIgnoreCase("GALL"))
//            holder.imageViewAlertType.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.small_gallery));
//        if (notification.getType().equalsIgnoreCase("HTML"))
//            holder.imageViewAlertType.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.small_digitalresourc));
//        if (notification.getType().equalsIgnoreCase("INTM"))
//            holder.imageViewAlertType.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.small_message));
//        if (notification.getType().equalsIgnoreCase("NWS"))
//            holder.imageViewAlertType.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.small_noticebrd));
//        if (notification.getType().equalsIgnoreCase("HMW"))
//            holder.imageViewAlertType.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.small_eventcalendar));
//        if (notification.getType().equalsIgnoreCase("BUS"))
//            holder.imageViewAlertType.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.small_busattendance));

        holder.mainLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                if (ReusableClass.isNetworkAvailable(context)) {
//                    ReadNotificationSubmission readNotificationSubmission = new ReadNotificationSubmission();
//                    readNotificationSubmission.setModuleCode(notification.getType());
//                    readNotificationSubmission.setUserId(PreferencesData.getUserId(context));
//                    readNotificationSubmission.setProcessId(Integer.parseInt(notification.getId()));
//
//                    delete(position);
//                    MyApplication.addJobInBackground(new ReadNotificationsJob(readNotificationSubmission, position));
//                } else
//                    TastyToast.makeText(context, "Sorry no internet connection.", Toast.LENGTH_LONG, TastyToast.ERROR).show();
//
//                if (notification.getType().equalsIgnoreCase("GALL"))
//                    EventBus.getDefault().post(new MenuOpenEvent.Success(R.id.nav_gallery));
//                if (notification.getType().equalsIgnoreCase("HTML"))
//                    EventBus.getDefault().post(new MenuOpenEvent.Success(R.id.nav_digital_resource));
//                if (notification.getType().equalsIgnoreCase("INTM"))
//                    EventBus.getDefault().post(new MenuOpenEvent.Success(R.id.nav_communicate));
//                if (notification.getType().equalsIgnoreCase("NWS"))
//                    EventBus.getDefault().post(new MenuOpenEvent.Success(R.id.nav_notice_board));
//                if (notification.getType().equalsIgnoreCase("HMW"))
//                    EventBus.getDefault().post(new MenuOpenEvent.Success(R.id.nav_weekly_plan));
            }
        });
    }

    @Override
    public int getItemCount() {
        return notifications.size();
    }

    public void addAll(ArrayList<Notification> responses) {
        clearAll();
        notifications = responses;
        notifyDataSetChanged();
    }

    public void clearAll() {
        notifications = new ArrayList<>();
        notifyDataSetChanged();
    }

    public void delete(int position) {
        notifications.remove(position);
        notifyDataSetChanged();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.title)
        TextView title;
        @BindView(R.id.subTitle)
        TextView subTitle;
        @BindView(R.id.subSubTitle)
        TextView subSubTitle;
        @BindView(R.id.mainLayout)
        LinearLayout mainLayout;
        @BindView(R.id.imageViewAlertType)
        ImageView imageViewAlertType;

        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}