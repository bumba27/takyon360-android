package net.lasagu.takyon360buzz.widgets;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import net.lasagu.takyon360buzz.R;
import net.lasagu.takyon360buzz.models.InboxMessages;
import net.lasagu.takyon360buzz.utils.ReusableClass;

import java.util.ArrayList;

import butterknife.BindColor;
import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * Created by ajana on 05/07/2017.
 */

public class InboxListAdapter extends RecyclerView.Adapter<InboxListAdapter.MyViewHolder> {
    private Context context;
    private ArrayList<InboxMessages> messageLists;


    public InboxListAdapter(Context context, ArrayList<InboxMessages> messageBox) {
        this.context = context;
        this.messageLists = messageBox;
        messageLists = new ArrayList<>();
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_list_row, parent, false);
        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        final InboxMessages message = messageLists.get(position);

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            holder.title.setText(Html.fromHtml((ReusableClass.getBase64(message.getInbox_mess()).replaceAll("%20", " ")), Html.FROM_HTML_MODE_LEGACY));
        } else {
            holder.title.setText(Html.fromHtml(ReusableClass.getBase64(message.getInbox_mess()).replaceAll("%20", " ")));
        }

        holder.mainLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent intent = new Intent(context, MessageDetailsActivity.class);
//                intent.putExtra("MESSAGE_ID", message.getId());
//                intent.putExtra("MESSAGE_BOX", messageBox);
//                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return messageLists.size();
    }

    public void addAll(ArrayList<InboxMessages> responses) {
        clearAll();
        messageLists = responses;
        notifyDataSetChanged();
    }

    public void clearAll() {
        messageLists.clear();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.title)
        TextView title;
        @BindView(R.id.mainLayout)
        LinearLayout mainLayout;
        @BindColor(R.color.colorAccent)
        int colorAccent;
        @BindColor(R.color.black)
        int black;

        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}