package net.lasagu.takyon360buzz.widgets;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import net.lasagu.takyon360buzz.models.BusDetailsResponse;
import net.lasagu.takyon360buzz.ui.fragments.MapFragment;
import net.lasagu.takyon360buzz.ui.fragments.SentboxFragment;

/**
 * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
 * one of the sections/tabs/pages.
 */
public class BusDetailsPagerAdapter extends FragmentPagerAdapter {

    private BusDetailsResponse busDetailsResponse;

    public BusDetailsPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return MapFragment.newInstance(busDetailsResponse);
            case 1:
                return SentboxFragment.newInstance(busDetailsResponse);
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        // Show 2 total pages.
        return 2;
    }

    public void setBusDetails(BusDetailsResponse busDetailsResponse) {
        this.busDetailsResponse = busDetailsResponse;
    }
}
