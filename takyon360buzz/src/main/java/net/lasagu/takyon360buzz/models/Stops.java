package net.lasagu.takyon360buzz.models;

import lombok.Data;

@Data
public class Stops {
    /**
     * latitude : 25.3564963
     * longitude : 55.3899178
     * stop_name : Ahaliya exchange, Rolla square
     * stop_order : 1
     */

    private String latitude;
    private String longitude;
    private String stop_name;
    private String stop_order;
}
