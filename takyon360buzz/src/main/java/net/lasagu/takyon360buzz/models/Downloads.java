package net.lasagu.takyon360buzz.models;

/**
 * Created by ajana on 05/07/2017.
 */

public class Downloads {
    private String Name;

    private String Item;

    private String Id;

    public String getName ()
    {
        return Name;
    }

    public void setName (String Name)
    {
        this.Name = Name;
    }

    public String getItem ()
    {
        return Item;
    }

    public void setItem (String Item)
    {
        this.Item = Item;
    }

    public String getId ()
    {
        return Id;
    }

    public void setId (String Id)
    {
        this.Id = Id;
    }
}
