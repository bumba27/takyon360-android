package net.lasagu.takyon360buzz.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.Gson;

import java.util.ArrayList;

import lombok.Data;

/**
 * Created by ajana on 10/04/2018.
 */

@Data
public class BusDetailsResponse{

    /**
     * StatusCode : 1
     * StatusMessage : Success
     * ETA : 06:58
     * Distance : "06:58"
     * InboxMessages : []
     * SentMessages : []
     * Map : [{"Brach":"5","BranchId":"2380","Latitude":"25.365125","Longitude":"55.4013185"}]
     * UserLatitude : 25.3542526
     * UserLongitude : 55.3908797
     * Stops : [{"latitude":"25.3564963","longitude":"55.3899178","stop_name":"Ahaliya exchange, Rolla square","stop_order":"1"},{"latitude":"25.3569268","longitude":"55.3924928","stop_name":"Rolla Tower Bldg Shj","stop_order":"2"},{"latitude":"25.3543648","longitude":"55.3907349","stop_name":"Damas, Burj 2000","stop_order":"3"},{"latitude":"25.3565287","longitude":"55.3883107","stop_name":"rolla","stop_order":"4"},{"latitude":"25.3569361","longitude":"55.3925104","stop_name":"AL GHUWAIR","stop_order":"6"},{"latitude":"25.3566045","longitude":"55.3899761","stop_name":"Ahalya exchange building rolla square sharjah","stop_order":"8"},{"latitude":"25.3569085","longitude":"55.3925014","stop_name":"Al Ghuwair market Shj","stop_order":"9"},{"latitude":"25.3572197","longitude":"55.3907474","stop_name":"Rolla Street","stop_order":"10"}]
     * Student_stops : [{"latitude":"25.3592155","longitude":"55.3946509","stop_name":"Al ghuwair market","stop_order":"13"},{"latitude":"25.3585771","longitude":"55.3936465","stop_name":"Al Guwair market","stop_order":"14"},{"latitude":"25.3589025","longitude":"55.3925757","stop_name":"Al ghuwair market","stop_order":"13"},{"latitude":"25.3581628","longitude":"55.3913265","stop_name":"Al ghuwair market","stop_order":"13"},{"latitude":"25.3581941","longitude":"55.3912478","stop_name":"ALAIN MARKET BLDNG-ROLLA","stop_order":"30"},{"latitude":"25.3581049","longitude":"55.3912872","stop_name":"Al Ghuwiar Market, Rolla","stop_order":"28"}]
     */

    private int StatusCode;
    private String StatusMessage;
    private String ETA;
    private String Distance;
    private String DistanceLabel;
    private String ETALabel;
    private String UserLatitude;
    private String UserLongitude;
    private ArrayList<InboxMessages> InboxMessages;
    private ArrayList<SentMessages> SentMessages;
    private ArrayList<MapBean> Map;
    private ArrayList<Stops> Stops;
    private ArrayList<StudentStops> Student_stops;



    public static BusDetailsResponse fromJson(String json) {
        return new Gson().fromJson(json, BusDetailsResponse.class);
    }
}
