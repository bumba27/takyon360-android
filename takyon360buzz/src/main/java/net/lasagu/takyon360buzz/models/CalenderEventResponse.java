package net.lasagu.takyon360buzz.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.Gson;

import lombok.Data;

@Data
public class CalenderEventResponse implements Parcelable {

    /**
     * alert_create_date : 12-Sep-2018 00:00:0000
     * alert_message : Your ward Tarun Hrishikesh Venkatesh has boarded the bus....
     * alert_url : ?id=6986
     * alert_type : BUS
     * alert_id : 6986
     * CreatedBy : Route 5
     * DeliveredTo : JAMBUNATHAN VENKATESH(Tarun Hrishikesh Venkatesh)
     * HashKey : T0009
     */

    private String alert_create_date;
    private String alert_message;
    private String alert_url;
    private String alert_type;
    private String alert_id;
    private String CreatedBy;
    private String DeliveredTo;
    private String HashKey;

    public static CalenderEventResponse fromJson(String json) {
        return new Gson().fromJson(json, CalenderEventResponse.class);
    }

    public static CalenderEventResponse[] toJsonList(String json) {
        return new Gson().fromJson(json, CalenderEventResponse[].class);
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.alert_create_date);
        dest.writeString(this.alert_message);
        dest.writeString(this.alert_url);
        dest.writeString(this.alert_type);
        dest.writeString(this.alert_id);
        dest.writeString(this.CreatedBy);
        dest.writeString(this.DeliveredTo);
        dest.writeString(this.HashKey);
    }

    protected CalenderEventResponse(Parcel in) {
        this.alert_create_date = in.readString();
        this.alert_message = in.readString();
        this.alert_url = in.readString();
        this.alert_type = in.readString();
        this.alert_id = in.readString();
        this.CreatedBy = in.readString();
        this.DeliveredTo = in.readString();
        this.HashKey = in.readString();
    }

    public static final Parcelable.Creator<CalenderEventResponse> CREATOR = new Parcelable.Creator<CalenderEventResponse>() {
        @Override
        public CalenderEventResponse createFromParcel(Parcel source) {
            return new CalenderEventResponse(source);
        }

        @Override
        public CalenderEventResponse[] newArray(int size) {
            return new CalenderEventResponse[size];
        }
    };
}
