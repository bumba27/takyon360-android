package net.lasagu.takyon360buzz.models;

import lombok.Data;

@Data
public class StudentStops {
    /**
     * latitude : 25.3592155
     * longitude : 55.3946509
     * stop_name : Al ghuwair market
     * stop_order : 13
     */

    private String latitude;
    private String longitude;
    private String stop_name;
    private String stop_order;
}
