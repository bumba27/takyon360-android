package net.lasagu.takyon360buzz.models;

import android.os.Parcel;
import android.os.Parcelable;

import lombok.Data;

@Data
public class InboxMessages implements Parcelable {
    /**
     * inbox_mess : WW91ciUyMGNoaWxkJTIwRXNoYWwlMjBBeWVzaGElMjBGYWhhZCUyMGRpZG4ndCUyMGJvYXJkZWQlMjB0aGUlMjBzY2hvb2wlMjBidXMlMjBvbiUyMDIwMTgtMDUtMzE=
     * Date : 2018-05-31 08:38:13
     */

    private String inbox_mess;
    private String Date;


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.inbox_mess);
        dest.writeString(this.Date);
    }

    protected InboxMessages(Parcel in) {
        this.inbox_mess = in.readString();
        this.Date = in.readString();
    }

    public static final Parcelable.Creator<InboxMessages> CREATOR = new Parcelable.Creator<InboxMessages>() {
        @Override
        public InboxMessages createFromParcel(Parcel source) {
            return new InboxMessages(source);
        }

        @Override
        public InboxMessages[] newArray(int size) {
            return new InboxMessages[size];
        }
    };
}
