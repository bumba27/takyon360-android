package net.lasagu.takyon360buzz.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;

@Data
public class BusList implements Parcelable {
    /**
     * BusName : 18
     * BusId : B645
     * BusContact : 055-8501575
     * Distance :
     * ETA :
     * InboxMessages : [{"inbox_mess":"WW91ciUyMGNoaWxkJTIwRXNoYWwlMjBBeWVzaGElMjBGYWhhZCUyMGRpZG4ndCUyMGJvYXJkZWQlMjB0aGUlMjBzY2hvb2wlMjBidXMlMjBvbiUyMDIwMTgtMDUtMzE=","Date":"2018-05-31 08:38:13"},{"inbox_mess":"WW91ciUyMGNoaWxkJTIwRXNoYWwlMjBBeWVzaGElMjBGYWhhZCUyMGRpZG4ndCUyMGJvYXJkZWQlMjB0aGUlMjBidXMlMjBmb3IlMjBkZXBhcnR1cmUlMjBvbiUyMDIwMTgtMDUtMjk=","Date":"2018-05-29 11:54:02"},{"inbox_mess":"WW91ciUyMGNoaWxkJTIwRXNoYWwlMjBBeWVzaGElMjBGYWhhZCUyMGRpZG4ndCUyMGJvYXJkZWQlMjB0aGUlMjBzY2hvb2wlMjBidXMlMjBvbiUyMDIwMTgtMDUtMjk=","Date":"2018-05-29 07:36:01"}]
     * SentMessages : []
     */

    private String BusName;
    private String BusId;
    private String BusContact;
    private String Distance;
    private String ETA;
    private ArrayList<InboxMessages> InboxMessages;
    private ArrayList<SentMessages> SentMessages;


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.BusName);
        dest.writeString(this.BusId);
        dest.writeString(this.BusContact);
        dest.writeString(this.Distance);
        dest.writeString(this.ETA);
        dest.writeList(this.InboxMessages);
        dest.writeList(this.SentMessages);
    }

    protected BusList(Parcel in) {
        this.BusName = in.readString();
        this.BusId = in.readString();
        this.BusContact = in.readString();
        this.Distance = in.readString();
        this.ETA = in.readString();
        this.InboxMessages = new ArrayList<net.lasagu.takyon360buzz.models.InboxMessages>();
        in.readList(this.InboxMessages, net.lasagu.takyon360buzz.models.InboxMessages.class.getClassLoader());
        this.SentMessages = new ArrayList<net.lasagu.takyon360buzz.models.SentMessages>();
        in.readList(this.SentMessages, net.lasagu.takyon360buzz.models.SentMessages.class.getClassLoader());
    }

    public static final Parcelable.Creator<BusList> CREATOR = new Parcelable.Creator<BusList>() {
        @Override
        public BusList createFromParcel(Parcel source) {
            return new BusList(source);
        }

        @Override
        public BusList[] newArray(int size) {
            return new BusList[size];
        }
    };
}
