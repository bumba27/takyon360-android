package net.lasagu.takyon360buzz.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

import lombok.Data;

@Data
public class BusDetails implements Parcelable {
    /**
     * Messages : [{"id":"7","name":"I will be dropping my child"},{"id":"8","name":"I will be picking my child"},{"id":"9","name":"My child will be absent today"}]
     * MessageToBusLabel : Message to Bus
     * MessageFromBusLabel : Message from Bus
     * MessageToDriverLabel : Message to Driver
     * DistanceLabel : Distance
     * ETALabel : ETA
     * BusList : [{"BusName":"18","BusId":"B645","BusContact":"055-8501575","Distance":"","ETA":"","InboxMessages":[{"inbox_mess":"WW91ciUyMGNoaWxkJTIwRXNoYWwlMjBBeWVzaGElMjBGYWhhZCUyMGRpZG4ndCUyMGJvYXJkZWQlMjB0aGUlMjBzY2hvb2wlMjBidXMlMjBvbiUyMDIwMTgtMDUtMzE=","Date":"2018-05-31 08:38:13"},{"inbox_mess":"WW91ciUyMGNoaWxkJTIwRXNoYWwlMjBBeWVzaGElMjBGYWhhZCUyMGRpZG4ndCUyMGJvYXJkZWQlMjB0aGUlMjBidXMlMjBmb3IlMjBkZXBhcnR1cmUlMjBvbiUyMDIwMTgtMDUtMjk=","Date":"2018-05-29 11:54:02"},{"inbox_mess":"WW91ciUyMGNoaWxkJTIwRXNoYWwlMjBBeWVzaGElMjBGYWhhZCUyMGRpZG4ndCUyMGJvYXJkZWQlMjB0aGUlMjBzY2hvb2wlMjBidXMlMjBvbiUyMDIwMTgtMDUtMjk=","Date":"2018-05-29 07:36:01"}],"SentMessages":[]}]
     * trip_id : 35982
     * staff_id : 152336
     */

    private String MessageToBusLabel;
    private String MessageFromBusLabel;
    private String MessageToDriverLabel;
    private String DistanceLabel;
    private String ETALabel;
    private String trip_id;
    private String staff_id;
    private ArrayList<Messages> Messages;
    private ArrayList<BusList> BusList;


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.MessageToBusLabel);
        dest.writeString(this.MessageFromBusLabel);
        dest.writeString(this.MessageToDriverLabel);
        dest.writeString(this.DistanceLabel);
        dest.writeString(this.ETALabel);
        dest.writeString(this.trip_id);
        dest.writeString(this.staff_id);
        dest.writeTypedList(this.Messages);
        dest.writeList(this.BusList);
    }

    protected BusDetails(Parcel in) {
        this.MessageToBusLabel = in.readString();
        this.MessageFromBusLabel = in.readString();
        this.MessageToDriverLabel = in.readString();
        this.DistanceLabel = in.readString();
        this.ETALabel = in.readString();
        this.trip_id = in.readString();
        this.staff_id = in.readString();
        this.Messages = in.createTypedArrayList(net.lasagu.takyon360buzz.models.Messages.CREATOR);
        this.BusList = new ArrayList<net.lasagu.takyon360buzz.models.BusList>();
        in.readList(this.BusList, net.lasagu.takyon360buzz.models.BusList.class.getClassLoader());
    }

    public static final Parcelable.Creator<BusDetails> CREATOR = new Parcelable.Creator<BusDetails>() {
        @Override
        public BusDetails createFromParcel(Parcel source) {
            return new BusDetails(source);
        }

        @Override
        public BusDetails[] newArray(int size) {
            return new BusDetails[size];
        }
    };
}
