package net.lasagu.takyon360buzz.models;

import lombok.Data;

/**
 * Created by ajana on 10/04/2018.
 */

@Data
public class MapBean {
    /**
     * Brach : bus 16
     * BranchId : 1360
     * Latitude : 25.3155566000
     * Longitude : 55.3834683000
     */

    private String Brach;
    private String BranchId;
    private String Latitude;
    private String Longitude;
}
