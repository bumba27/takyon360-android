package net.lasagu.takyon360buzz.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.Gson;

import java.util.ArrayList;

import lombok.Data;

/**
 * Created by ajana on 09/04/2018.
 */

@Data
public class BusListResponse implements Parcelable {
    /**
     * StatusCode : 1
     * StatusMessage : Success
     * NotificationLabel : Notifications
     * QuickLinkHeadLabel : Quick Links
     * TrackingLabel : Tracking
     * CalendarLabel : Calendar
     * ContactBusLabel : Call
     * ContactSchoolLabel : Call
     * Notification : [{"id":"111","Type":"BUS","Title":"Your child Eshal Ayesha Fahad has boarded the bus 18 for school at 7:00 AM on 06-01-2019","Details":"NULL","HashKey":"T0009","Date":"06-Jan-2019 00:00:0000","CreatedBy":"","DeliveredTo":"","url":"","cat_id":""}]
     * BusDetails : {"Messages":[{"id":"7","name":"I will be dropping my child"},{"id":"8","name":"I will be picking my child"},{"id":"9","name":"My child will be absent today"}],"MessageToBusLabel":"Message to Bus","MessageFromBusLabel":"Message from Bus","MessageToDriverLabel":"Message to Driver ","DistanceLabel":"Distance","ETALabel":"ETA","BusList":[{"BusName":"18","BusId":"B645","BusContact":"055-8501575","Distance":"","ETA":"","InboxMessages":[{"inbox_mess":"WW91ciUyMGNoaWxkJTIwRXNoYWwlMjBBeWVzaGElMjBGYWhhZCUyMGRpZG4ndCUyMGJvYXJkZWQlMjB0aGUlMjBzY2hvb2wlMjBidXMlMjBvbiUyMDIwMTgtMDUtMzE=","Date":"2018-05-31 08:38:13"},{"inbox_mess":"WW91ciUyMGNoaWxkJTIwRXNoYWwlMjBBeWVzaGElMjBGYWhhZCUyMGRpZG4ndCUyMGJvYXJkZWQlMjB0aGUlMjBidXMlMjBmb3IlMjBkZXBhcnR1cmUlMjBvbiUyMDIwMTgtMDUtMjk=","Date":"2018-05-29 11:54:02"},{"inbox_mess":"WW91ciUyMGNoaWxkJTIwRXNoYWwlMjBBeWVzaGElMjBGYWhhZCUyMGRpZG4ndCUyMGJvYXJkZWQlMjB0aGUlMjBzY2hvb2wlMjBidXMlMjBvbiUyMDIwMTgtMDUtMjk=","Date":"2018-05-29 07:36:01"}],"SentMessages":[]}],"trip_id":"35982","staff_id":"152336"}
     * SchoolContact : +9716 5676783
     */

    private int StatusCode;
    private String StatusMessage;
    private String HeadLabel;
    private String NotificationLabel;
    private String QuickLinkHeadLabel;
    private String TrackingLabel;
    private String CalendarLabel;
    private String ContactBusLabel;
    private String ContactSchoolLabel;
    private BusDetails BusDetails;
    private String SchoolContact;
    private ArrayList<Notification> Notification;



    public static BusListResponse fromJson(String json) {
        return new Gson().fromJson(json, BusListResponse.class);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.StatusCode);
        dest.writeString(this.StatusMessage);
        dest.writeString(this.HeadLabel);
        dest.writeString(this.NotificationLabel);
        dest.writeString(this.QuickLinkHeadLabel);
        dest.writeString(this.TrackingLabel);
        dest.writeString(this.CalendarLabel);
        dest.writeString(this.ContactBusLabel);
        dest.writeString(this.ContactSchoolLabel);
        dest.writeParcelable(this.BusDetails, flags);
        dest.writeString(this.SchoolContact);
        dest.writeList(this.Notification);
    }

    protected BusListResponse(Parcel in) {
        this.StatusCode = in.readInt();
        this.StatusMessage = in.readString();
        this.HeadLabel = in.readString();
        this.NotificationLabel = in.readString();
        this.QuickLinkHeadLabel = in.readString();
        this.TrackingLabel = in.readString();
        this.CalendarLabel = in.readString();
        this.ContactBusLabel = in.readString();
        this.ContactSchoolLabel = in.readString();
        this.BusDetails = in.readParcelable(net.lasagu.takyon360buzz.models.BusDetails.class.getClassLoader());
        this.SchoolContact = in.readString();
        this.Notification = new ArrayList<net.lasagu.takyon360buzz.models.Notification>();
        in.readList(this.Notification, net.lasagu.takyon360buzz.models.Notification.class.getClassLoader());
    }

    public static final Parcelable.Creator<BusListResponse> CREATOR = new Parcelable.Creator<BusListResponse>() {
        @Override
        public BusListResponse createFromParcel(Parcel source) {
            return new BusListResponse(source);
        }

        @Override
        public BusListResponse[] newArray(int size) {
            return new BusListResponse[size];
        }
    };
}
