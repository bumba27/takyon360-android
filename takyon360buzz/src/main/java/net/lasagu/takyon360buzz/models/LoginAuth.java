package net.lasagu.takyon360buzz.models;

import com.google.gson.Gson;

/**
 * Created by anirban on 2/9/17.
 */

public class LoginAuth {
    private String UserName;
    private String Password;
    private String Language;

    public String getUserName() {
        return UserName;
    }

    public void setUserName(String userName) {
        UserName = userName;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String password) {
        Password = password;
    }

    public String getLanguage() {
        return Language;
    }

    public void setLanguage(String language) {
        Language = language;
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }

}
