package net.lasagu.takyon360buzz.models;

import android.os.Parcel;
import android.os.Parcelable;

import lombok.Data;

/**
 * Created by ajana on 05/07/2017.
 */

@Data
public class Notification implements Parcelable {
    /**
     * id : 111
     * Type : BUS
     * Title : Your child Eshal Ayesha Fahad has boarded the bus 18 for school at 7:00 AM on 06-01-2019
     * Details : NULL
     * HashKey : T0009
     * Date : 06-Jan-2019 00:00:0000
     * CreatedBy :
     * DeliveredTo :
     * url :
     * cat_id :
     */

    private String id;
    private String Type;
    private String Title;
    private String Details;
    private String HashKey;
    private String Date;
    private String CreatedBy;
    private String DeliveredTo;
    private String url;
    private String cat_id;


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.Type);
        dest.writeString(this.Title);
        dest.writeString(this.Details);
        dest.writeString(this.HashKey);
        dest.writeString(this.Date);
        dest.writeString(this.CreatedBy);
        dest.writeString(this.DeliveredTo);
        dest.writeString(this.url);
        dest.writeString(this.cat_id);
    }

    protected Notification(Parcel in) {
        this.id = in.readString();
        this.Type = in.readString();
        this.Title = in.readString();
        this.Details = in.readString();
        this.HashKey = in.readString();
        this.Date = in.readString();
        this.CreatedBy = in.readString();
        this.DeliveredTo = in.readString();
        this.url = in.readString();
        this.cat_id = in.readString();
    }

    public static final Parcelable.Creator<Notification> CREATOR = new Parcelable.Creator<Notification>() {
        @Override
        public Notification createFromParcel(Parcel source) {
            return new Notification(source);
        }

        @Override
        public Notification[] newArray(int size) {
            return new Notification[size];
        }
    };
}
