package net.lasagu.takyon360buzz.models;

/**
 * Created by ajana on 05/07/2017.
 */

public class MenuList {
    private String id;
    private String ParentId;
    private String HashKey;
    private String MenuOrder;
    private String Label;
    private String Link;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getParentId() {
        return ParentId;
    }

    public void setParentId(String ParentId) {
        this.ParentId = ParentId;
    }

    public String getHashKey() {
        return HashKey;
    }

    public void setHashKey(String HashKey) {
        this.HashKey = HashKey;
    }

    public String getMenuOrder() {
        return MenuOrder;
    }

    public void setMenuOrder(String MenuOrder) {
        this.MenuOrder = MenuOrder;
    }

    public String getLabel() {
        return Label;
    }

    public void setLabel(String Label) {
        this.Label = Label;
    }

    public String getLink() {
        return Link;
    }

    public void setLink(String link) {
        Link = link;
    }
}
