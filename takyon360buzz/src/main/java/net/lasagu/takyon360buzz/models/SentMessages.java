package net.lasagu.takyon360buzz.models;

import android.os.Parcel;
import android.os.Parcelable;

import lombok.Data;

/**
 * Created by ajana on 10/04/2018.
 */

@Data
public class SentMessages implements Parcelable {
    /**
     * sent_mess : WW91ciUyMGNoaWxkJTIwT mFkYSUyME0uSHVzYW0lMjBBbHJhaWVzJTIwaXMlMjBmb3VuZCUyMHByZXNlbnQlMj AlRTIlODAlOEJpbiUyMHRoZSUyMHNjaG9vbCUyMGJ1cyUyMGJ1cyUyMDIwJTIwb24lMj AyMDE4LTAxLTI4JTIwMDUlM0EyOCUzQTM1
     * Date : 2018-05-31 08:38:13
     */

    private String sent_mess;
    private String Date;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.sent_mess);
        dest.writeString(this.Date);
    }

    protected SentMessages(Parcel in) {
        this.sent_mess = in.readString();
        this.Date = in.readString();
    }

    public static final Parcelable.Creator<SentMessages> CREATOR = new Parcelable.Creator<SentMessages>() {
        @Override
        public SentMessages createFromParcel(Parcel source) {
            return new SentMessages(source);
        }

        @Override
        public SentMessages[] newArray(int size) {
            return new SentMessages[size];
        }
    };
}
