package net.lasagu.takyon360buzz.models;

import com.google.gson.Gson;

import lombok.Data;

/**
 * Created by ajana on 10/04/2018.
 */

@Data
public class BusDetailsSubmission {
    /**
     * UserId : 241379
     * MapId : B1360
     * TripId :
     * StaffId : 241379
     */

    private String UserId;
    private String MapId;
    private String TripId;
    private int StaffId;

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}
