package net.lasagu.takyon360buzz.models;

import com.google.gson.Gson;

import lombok.Data;

/**
 * Created by anirban on 4/13/18.
 */

@Data
public class SendMessageSubmission {
    /**
     * UserId : 98189
     * TripId : 5043
     * MsgId : 7
     * StaffId : 241379
     */

    private String UserId;
    private String TripId;
    private int MsgId;
    private int StaffId;

    public static SendMessageSubmission fromJson(String str) {
        return new Gson().fromJson(str, SendMessageSubmission.class);
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}
