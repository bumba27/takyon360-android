package net.lasagu.takyon360buzz.models;

import com.google.gson.Gson;

/**
 * Created by anirbanjana on 01/02/2017.
 */

public class UserDetails {
    private String username;
    private String password;
    private String language;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public static UserDetails fromJson(String json) {
        return new Gson().fromJson(json, UserDetails.class);
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}
