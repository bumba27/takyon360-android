package net.lasagu.takyon360buzz.events;

import android.util.Log;

import net.lasagu.takyon360buzz.models.LoginResponse;


/**
 * Created by anirban on 1/31/17.
 */

public class LoginEvent {
    public static class Fail {
        String ex;

        public Fail(String ex) {
            this.ex = ex;
        }

        public String getEx() {
            Log.d("TAG", "Error");

            return ex;
        }
    }

    public static class Success {
        LoginResponse loginResponse;

        public Success(LoginResponse loginResponse) {
            this.loginResponse = loginResponse;
        }

        public LoginResponse getLoginResponse() {
            return loginResponse;
        }

    }
}
