package net.lasagu.takyon360buzz.events;

import android.util.Log;

import net.lasagu.takyon360buzz.models.BusDetailsResponse;


/**
 * Created by anirban on 1/31/17.
 */

public class BusDetailsResponseEvent {
    public static class Fail {
        String ex;

        public Fail(String ex) {
            this.ex = ex;
        }

        public String getEx() {
            Log.d("TAG", "Error");

            return ex;
        }
    }

    public static class Success {
        BusDetailsResponse busDetailsResponse;

        public Success(BusDetailsResponse busDetailsResponse) {
            this.busDetailsResponse = busDetailsResponse;
        }

        public BusDetailsResponse getBusDetailsResponse() {
            return busDetailsResponse;
        }

    }
}
