package net.lasagu.takyon360buzz.events;

import android.util.Log;


/**
 * Created by anirban on 1/31/17.
 */

public class MyCalenderEvent {
    public static class Fail {
        String ex;

        public Fail(String ex) {
            this.ex = ex;
        }

        public String getEx() {
            Log.d("TAG", "Error");

            return ex;
        }
    }

    public static class Success {
        String calenderEventResponse;

        public Success(String calenderEventResponse) {
            this.calenderEventResponse = calenderEventResponse;
        }

        public String getCalenderEventResponse() {
            return calenderEventResponse;
        }

    }
}
