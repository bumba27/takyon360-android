package net.lasagu.takyon360buzz.events;

import android.util.Log;

import net.lasagu.takyon360buzz.models.BusListResponse;


/**
 * Created by anirban on 1/31/17.
 */

public class BusListResponseEvent {
    public static class Fail {
        String ex;

        public Fail(String ex) {
            this.ex = ex;
        }

        public String getEx() {
            Log.d("TAG", "Error");

            return ex;
        }
    }

    public static class Success {
        BusListResponse busListResponse;

        public Success(BusListResponse busListResponse) {
            this.busListResponse = busListResponse;
        }

        public BusListResponse getBusListResponse() {
            return busListResponse;
        }

    }
}
