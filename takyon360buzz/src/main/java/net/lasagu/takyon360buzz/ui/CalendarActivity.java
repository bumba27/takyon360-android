package net.lasagu.takyon360buzz.ui;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.github.tibolte.agendacalendarview.AgendaCalendarView;
import com.github.tibolte.agendacalendarview.CalendarPickerController;
import com.github.tibolte.agendacalendarview.models.BaseCalendarEvent;
import com.github.tibolte.agendacalendarview.models.CalendarEvent;
import com.github.tibolte.agendacalendarview.models.DayItem;

import net.lasagu.takyon360buzz.R;
import net.lasagu.takyon360buzz.models.CalenderEventResponse;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CalendarActivity extends AppCompatActivity implements CalendarPickerController {

    public static final String CALENDER_DATA = "calenderData";
    public static final String CALENDER_TITLE = "title";
    AgendaCalendarView agendaCalendarView;
    @BindView(R.id.noData)
    LinearLayout noData;
    @BindView(R.id.progress)
    ProgressBar progress;
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calendar);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);

        agendaCalendarView = (AgendaCalendarView) findViewById(R.id.agenda_calendar_view);

        Intent intent = getIntent();
        String stringCalenderData = intent.getStringExtra(CALENDER_DATA);
        getSupportActionBar().setTitle(intent.getStringExtra(CALENDER_TITLE));

        if (stringCalenderData != null) {
            CalenderEventResponse[] calenderEventResponse = CalenderEventResponse.toJsonList(stringCalenderData);
            loadingEventData(calenderEventResponse);
        } else
            noData.setVisibility(View.VISIBLE);
    }

    @Override
    public void onDaySelected(DayItem dayItem) {

    }

    @Override
    public void onEventSelected(CalendarEvent event) {

    }

    @Override
    public void onScrollToDate(Calendar calendar) {

    }

    public void loadingEventData(CalenderEventResponse[] calenderEventResponse) {
        if (calenderEventResponse.length != 0) {

            // minimum and maximum date of our calendar
            // 2 month behind, one year ahead, example: March 2015 <-> May 2015 <-> May 2016
            Calendar minDate = Calendar.getInstance();
            Calendar maxDate = Calendar.getInstance();

            maxDate.set(Calendar.DAY_OF_MONTH, maxDate.getActualMaximum(Calendar.DAY_OF_MONTH));

            List<CalendarEvent> eventList = new ArrayList<>();

            for (int i = 0; i < calenderEventResponse.length; i++) {
                Calendar startTime = getCalenderTime(calenderEventResponse[i].getAlert_create_date());
                Calendar endTime = getCalenderTime(calenderEventResponse[i].getAlert_create_date());
                endTime.add(Calendar.MONTH, 0);


                BaseCalendarEvent event1 = new BaseCalendarEvent(calenderEventResponse[i].getAlert_message(),
                        "",
                        "",
                        ContextCompat.getColor(this, R.color.calender_colour),
                        startTime,
                        endTime,
                        true);
                eventList.add(event1);
            }

            agendaCalendarView.init(eventList, minDate, maxDate, Locale.getDefault(), this);

            noData.setVisibility(View.INVISIBLE);
        } else {
            noData.setVisibility(View.VISIBLE);
        }
        progress.setVisibility(View.INVISIBLE);
    }

    private Calendar getCalenderTime(String start_date) {
        Calendar cal = Calendar.getInstance();
        try {
            //2016-11-16 08:00:00
            //06-Jan-2019 00:00:0000
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss", Locale.ENGLISH);
            cal.setTime(sdf.parse(start_date));// all done
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return cal;
    }

    private String getHumanReadableDate(Calendar cal) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return dateFormat.format(cal.getTime());
    }
}
