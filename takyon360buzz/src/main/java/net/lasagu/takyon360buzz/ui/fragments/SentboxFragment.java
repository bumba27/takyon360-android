package net.lasagu.takyon360buzz.ui.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import net.lasagu.takyon360buzz.R;
import net.lasagu.takyon360buzz.models.BusDetailsResponse;
import net.lasagu.takyon360buzz.widgets.SentBoxListAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;

import static net.lasagu.takyon360buzz.ui.BusDetailsTabActivity.BUS_DETAILS;

/**
 * A simple {@link Fragment} subclass.
 */
public class SentboxFragment extends Fragment {

    private static BusDetailsResponse busDetailsResponse;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.swipeRefreshLayout)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.noData)
    LinearLayout noData;
    @BindView(R.id.progress)
    ProgressBar progress;

    private SentBoxListAdapter sentBoxListAdapter;
    LinearLayoutManager linearLayoutManager;

    public SentboxFragment() {
        // Required empty public constructor
    }

    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static SentboxFragment newInstance(BusDetailsResponse busDetailsResponse) {
        SentboxFragment.busDetailsResponse = busDetailsResponse;
        SentboxFragment fragment = new SentboxFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_message_inbox_sentbox, container, false);
        ButterKnife.bind(this, view);

        loadData(busDetailsResponse);
        return view;
    }

    private void loadData(BusDetailsResponse busDetailsResponse) {
        if(busDetailsResponse.getSentMessages()!= null && !busDetailsResponse.getSentMessages().isEmpty()) {
            sentBoxListAdapter = new SentBoxListAdapter(getContext(), busDetailsResponse.getSentMessages());
            linearLayoutManager = new LinearLayoutManager(getContext());
            recyclerView.setLayoutManager(linearLayoutManager);
            sentBoxListAdapter.addAll(busDetailsResponse.getSentMessages());
            recyclerView.setAdapter(sentBoxListAdapter);
        }else {
            noData.setVisibility(View.VISIBLE);
        }
    }

}
