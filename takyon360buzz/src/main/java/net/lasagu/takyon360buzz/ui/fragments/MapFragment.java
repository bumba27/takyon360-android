package net.lasagu.takyon360buzz.ui.fragments;


import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.location.Location;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.akexorcist.googledirection.DirectionCallback;
import com.akexorcist.googledirection.GoogleDirection;
import com.akexorcist.googledirection.constant.TransportMode;
import com.akexorcist.googledirection.model.Direction;
import com.akexorcist.googledirection.model.Leg;
import com.akexorcist.googledirection.model.Route;
import com.akexorcist.googledirection.model.Step;
import com.akexorcist.googledirection.util.DirectionConverter;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.sdsmdg.tastytoast.TastyToast;

import net.lasagu.takyon360buzz.R;
import net.lasagu.takyon360buzz.events.BusDetailsRefreshEvent;
import net.lasagu.takyon360buzz.models.BusDetailsResponse;
import net.lasagu.takyon360buzz.models.MapBean;
import net.lasagu.takyon360buzz.models.Stops;
import net.lasagu.takyon360buzz.models.StudentStops;
import net.lasagu.takyon360buzz.ui.BusDetailsTabActivity;
import net.lasagu.takyon360buzz.ui.MainActivity;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.greenrobot.event.EventBus;

import static net.lasagu.takyon360buzz.ui.BusDetailsTabActivity.BUS_DETAILS;

/**
 * A simple {@link Fragment} subclass.
 */
public class MapFragment extends Fragment implements OnMapReadyCallback {

    private static BusDetailsResponse busDetailsResponse;
    @BindView(R.id.distanceTextView)
    TextView distanceTextView;
    @BindView(R.id.etaTextView)
    TextView etaTextView;
    @BindView(R.id.etaLevel)
    TextView etaLevel;
    @BindView(R.id.distanceLevel)
    TextView distanceLevel;
    private SupportMapFragment mapFragment;
    private GoogleMap mMap;
    private Marker currentLocationMarker;
    private String TAG = MapFragment.class.getSimpleName();

    public MapFragment() {
        // Required empty public constructor
    }

    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static MapFragment newInstance(BusDetailsResponse busDetailsResponse) {
        MapFragment.busDetailsResponse = busDetailsResponse;
        MapFragment fragment = new MapFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_map, container, false);
        ButterKnife.bind(this, view);

        SupportMapFragment fragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        if (fragment!= null) {
            fragment.getMapAsync(this);
        }
        loadData(busDetailsResponse);

        return view;
    }

    private void loadData(final BusDetailsResponse busDetailsResponse) {
        etaLevel.setText(busDetailsResponse.getETALabel() + " ");
        distanceLevel.setText(busDetailsResponse.getDistanceLabel() + " ");

        ArrayList<MapBean> mapBeanArrayList = busDetailsResponse.getMap();
        if (!mapBeanArrayList.isEmpty()) {
            final String busLatitude = (mapBeanArrayList.get(0).getLatitude().isEmpty()) ? "0.0" : mapBeanArrayList.get(0).getLatitude();
            final String busLongitude = (mapBeanArrayList.get(0).getLongitude().isEmpty()) ? "0.0" : mapBeanArrayList.get(0).getLongitude();
            Log.d("TAG", "Bus Location = lat: " + busLatitude + " lng: " + busLongitude);


            ArrayList<Stops> stops = busDetailsResponse.getStops();
            final ArrayList<LatLng> latLngStop = new ArrayList<>();
            LatLng startPoint = null;
            LatLng endPoint = null;
            final boolean isStop;

            if (stops != null && stops.size() > 1) {
                isStop = true;
                String latitudeStart = stops.get(0).getLatitude();
                String longitudeStart = stops.get(0).getLongitude();
                String latitudeEnd = stops.get(stops.size() - 1).getLatitude();
                String longitudeEnd = stops.get(stops.size() - 1).getLongitude();
                if (latitudeStart != null || longitudeEnd != null)
                    startPoint = new LatLng(Double.parseDouble(latitudeStart), Double.parseDouble(longitudeStart));

                if (latitudeStart != null || longitudeEnd != null)
                    endPoint = new LatLng(Double.parseDouble(latitudeEnd), Double.parseDouble(longitudeEnd));

                if (stops.size() > 2) {
                    for (int i = 1; i < stops.size() - 1; i++) {
                        String latitude = stops.get(i).getLatitude();
                        String longitude = stops.get(i).getLongitude();
                        if (latitude != null || longitude != null) {
                            latLngStop.add(new LatLng(Double.parseDouble(latitude), Double.parseDouble(longitude)));
                        }
                    }
                }
            } else
                isStop = false;

            ArrayList<StudentStops> studentStops = busDetailsResponse.getStudent_stops();
            final ArrayList<LatLng> latLngStudentStops = new ArrayList<>();
            LatLng startStudentPoint = null;
            LatLng endStudentPoint = null;
            boolean isStudentStop;
            if (studentStops != null && studentStops.size() > 1) {
                isStudentStop = true;
                String latitudeStudentStart = studentStops.get(0).getLatitude();
                String longitudeStudentStart = studentStops.get(0).getLongitude();
                String latitudeStudentEnd = studentStops.get(studentStops.size() - 1).getLatitude();
                String longitudeStudentEnd = studentStops.get(studentStops.size() - 1).getLongitude();
                if (latitudeStudentStart != null || longitudeStudentEnd != null)
                    startStudentPoint = new LatLng(Double.parseDouble(latitudeStudentStart), Double.parseDouble(longitudeStudentEnd));

                if (longitudeStudentStart != null || longitudeStudentEnd != null)
                    endStudentPoint = new LatLng(Double.parseDouble(latitudeStudentEnd), Double.parseDouble(longitudeStudentEnd));

                if (studentStops.size() > 2) {
                    for (int i = 1; i < studentStops.size() - 1; i++) {
                        String latitude = studentStops.get(i).getLatitude();
                        String longitude = studentStops.get(i).getLongitude();
                        if (latitude != null || longitude != null) {
                            latLngStudentStops.add(new LatLng(Double.parseDouble(latitude), Double.parseDouble(longitude)));
                        }
                    }
                }
            } else
                isStudentStop = false;

            etaTextView.setText(busDetailsResponse.getETA());
            distanceTextView.setText(busDetailsResponse.getDistance());

            if (mMap != null) {
                if (isStop)
                    drawRouteOnMap(mMap, latLngStop, startPoint, endPoint, Color.BLUE, BitmapDescriptorFactory.fromResource(R.drawable.transparent_marker_icon), true);
                if (isStudentStop)
                    drawRouteOnMap(mMap, latLngStudentStops, startStudentPoint, endStudentPoint, Color.RED, BitmapDescriptorFactory.fromResource(R.drawable.transparent_marker_icon), false);

                loadingMarker(busDetailsResponse);
            }

            if (mapFragment == null) {
                mapFragment = SupportMapFragment.newInstance();
                final LatLng finalStartPoint = startPoint;
                final LatLng finalEndPoint = endPoint;
                final LatLng finalStartStudentPoint = startStudentPoint;
                final LatLng finalEndStudentPoint = endStudentPoint;
                final boolean finalIsStudentStop = isStudentStop;
                mapFragment.getMapAsync(new OnMapReadyCallback() {
                    @Override
                    public void onMapReady(GoogleMap googleMap) {
                        mMap = googleMap;
                        if (isStop)
                            drawRouteOnMap(mMap, latLngStop, finalStartPoint, finalEndPoint, Color.BLUE, BitmapDescriptorFactory.fromResource(R.drawable.transparent_marker_icon), true);
                        if (finalIsStudentStop)
                            drawRouteOnMap(mMap, latLngStudentStops, finalStartStudentPoint, finalEndStudentPoint, Color.RED, BitmapDescriptorFactory.fromResource(R.drawable.transparent_marker_icon), false);

                        loadingMarker(busDetailsResponse);
                    }
                });
            }
        }else
            Log.d(TAG, "mapBeanArrayList is empty");

    }

    private void loadingMarker(BusDetailsResponse busDetailsResponse) {
        if(busDetailsResponse.getMap() != null)
        {
            final String busLatitude = (busDetailsResponse.getMap().get(0).getLatitude().isEmpty()) ? "0.0" : busDetailsResponse.getMap().get(0).getLatitude();
            final String busLongitude = (busDetailsResponse.getMap().get(0).getLongitude().isEmpty()) ? "0.0" : busDetailsResponse.getMap().get(0).getLongitude();

            Location busLocation = new Location("");//provider name is unnecessary
            busLocation.setLatitude(Double.parseDouble(busLatitude));//your coords of course
            busLocation.setLongitude(Double.parseDouble(busLongitude));

            showMarker(busLocation, mMap, BitmapDescriptorFactory.fromResource(R.drawable.bus_marker));
        }
        final String userLatitude = (busDetailsResponse.getUserLatitude().isEmpty()) ? "0.0" : busDetailsResponse.getUserLatitude();
        final String userLongitude = (busDetailsResponse.getUserLongitude().isEmpty()) ? "0.0" : busDetailsResponse.getUserLongitude();

        Location userLocation = new Location("");//provider name is unnecessary
        userLocation.setLatitude(Double.parseDouble(userLatitude));//your coords of course
        userLocation.setLongitude(Double.parseDouble(userLongitude));

        showMarker(userLocation, mMap, BitmapDescriptorFactory.fromResource(R.drawable.home_marker));
    }

    private void showMarker(@NonNull Location currentLocation, GoogleMap googleMap, BitmapDescriptor markerIcon) {
        LatLng latLng = new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude());
        if (currentLocationMarker == null)
            currentLocationMarker = googleMap.addMarker(new MarkerOptions().icon(markerIcon).position(latLng));
        else{
            googleMap.addMarker(new MarkerOptions().icon(markerIcon).position(latLng));
        }
//            MarkerAnimation.animateMarkerToGB(currentLocationMarker, latLng, new LatLngInterpolator.Spherical());
    }

    private void drawRouteOnMap(final GoogleMap mMap, ArrayList<LatLng> wayPoints, LatLng start, LatLng end, final int color, final BitmapDescriptor markerIcon, final boolean boundMarker) {
        mMap.clear();
        GoogleDirection.withServerKey(getString(R.string.google_maps_key))
                .from(start)
                .and(wayPoints)
                .to(end)
                .transportMode(TransportMode.DRIVING)
                .execute(new DirectionCallback() {
                    @Override
                    public void onDirectionSuccess(Direction direction, String rawBody) {
                        if (direction.isOK()) {
                            Route route = direction.getRouteList().get(0);
                            int legCount = route.getLegList().size();
                            for (int index = 0; index < legCount; index++) {
                                Leg leg = route.getLegList().get(index);
                                mMap.addMarker(new MarkerOptions().icon(markerIcon).position(leg.getStartLocation().getCoordination()));
                                if (index == legCount - 1) {
                                    mMap.addMarker(new MarkerOptions().icon(markerIcon).position(leg.getEndLocation().getCoordination()));
                                }
                                List<Step> stepList = leg.getStepList();
                                ArrayList<PolylineOptions> polylineOptionList;
                                if (boundMarker)
                                    polylineOptionList = DirectionConverter.createTransitPolyline(getContext(), stepList, 2, color, 2, color);
                                else
                                    polylineOptionList = DirectionConverter.createTransitPolyline(getContext(), stepList, 1, color, 1, color);

                                for (PolylineOptions polylineOption : polylineOptionList) {
                                    mMap.addPolyline(polylineOption);
                                }
                            }
                            if (boundMarker)
                                setCameraWithCoordinationBounds(route, mMap);
                        } else {
                            Log.d(TAG, "Error : " + direction.getErrorMessage());
                        }
                    }

                    @Override
                    public void onDirectionFailure(Throwable t) {
                        TastyToast.makeText(getContext(), "Looks like no driving route found.", Toast.LENGTH_LONG, TastyToast.ERROR).show();
                    }
                });
    }

    private void setCameraWithCoordinationBounds(Route route, GoogleMap mMap) {
        LatLng southwest = route.getBound().getSouthwestCoordination().getCoordination();
        LatLng northeast = route.getBound().getNortheastCoordination().getCoordination();
        LatLngBounds bounds = new LatLngBounds(southwest, northeast);
        mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, 100));
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    public void onEventMainThread(final BusDetailsRefreshEvent.Success event) {
        final BusDetailsResponse busDetailsResponse = event.getBusDetailsResponse();
        loadData(busDetailsResponse);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                ((BusDetailsTabActivity) getActivity()).dismissProgress();
            }
        }, 800);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
    }
}
