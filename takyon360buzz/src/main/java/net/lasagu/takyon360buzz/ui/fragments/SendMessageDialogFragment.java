package net.lasagu.takyon360buzz.ui.fragments;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.PopupMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.sdsmdg.tastytoast.TastyToast;

import net.lasagu.takyon360buzz.MyApplication;
import net.lasagu.takyon360buzz.R;
import net.lasagu.takyon360buzz.events.MessageSendEvent;
import net.lasagu.takyon360buzz.jobs.SendMessageJob;
import net.lasagu.takyon360buzz.models.Messages;
import net.lasagu.takyon360buzz.models.SendMessageSubmission;
import net.lasagu.takyon360buzz.utils.Constant;
import net.lasagu.takyon360buzz.utils.PreferencesData;
import net.lasagu.takyon360buzz.utils.ReusableClass;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindColor;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.pedant.SweetAlert.SweetAlertDialog;
import de.greenrobot.event.EventBus;

/**
 * Created by ajana on 06/07/2017.
 */

public class SendMessageDialogFragment extends DialogFragment {

    @BindView(R.id.SelectMessageList)
    EditText EditTextSelectMessageList;
    @BindView(R.id.SendButton)
    Button SearchSendButton;
    @BindView(R.id.CloseButton)
    Button CloseButton;
    @BindColor(R.color.colorPrimary)
    int colorPrimary;
    private PopupMenu menu;
    private HashMap<String, String> messageHashMap;
    private SweetAlertDialog pDialog;
    private String tripId;
    private String staffId;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.send_message_dialog_fragment, container, false);
        ButterKnife.bind(this, view);

        Bundle bundle = getArguments();
        ArrayList<Messages> messagesBeanArrayList = bundle.getParcelableArrayList(Constant.MESSAGE_LIST);
        staffId = bundle.getString(Constant.STAFF_ID);
        tripId = bundle.getString(Constant.TRIP_ID);
        populateClassesPopUp(messagesBeanArrayList);

        return view;
    }

    private void populateClassesPopUp(ArrayList<Messages> messagesBeanArrayList) {
        menu = new PopupMenu(getContext(), EditTextSelectMessageList);
        messageHashMap = new HashMap<>();

        for (int i = 0; i < messagesBeanArrayList.size(); i++) {
            menu.getMenu().add(messagesBeanArrayList.get(i).getName());
            messageHashMap.put(messagesBeanArrayList.get(i).getName(), messagesBeanArrayList.get(i).getId());
        }

        menu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                String seletedTitle = item.getTitle().toString();
                EditTextSelectMessageList.setText(seletedTitle);
                EditTextSelectMessageList.setTag(messageHashMap.get(seletedTitle));
                return false;
            }
        });

        EditTextSelectMessageList.setText(messagesBeanArrayList.get(0).getName());
        EditTextSelectMessageList.setTag(messageHashMap.get(messagesBeanArrayList.get(0).getName()));
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @OnClick(R.id.SelectMessageList)
    public void selectMessage(){
        menu.show();
    }

    @OnClick(R.id.CloseButton)
    public void closingDialog(){
       dismiss();
    }

    @OnClick(R.id.SendButton)
    public void sendingMessage(){
        if (ReusableClass.isNetworkAvailable(getContext())) {
            pDialog = new SweetAlertDialog(getContext(), SweetAlertDialog.PROGRESS_TYPE);
            pDialog.getProgressHelper().setBarColor(colorPrimary);
            pDialog.setTitleText("Loading ...");
            pDialog.setCancelable(false);
            pDialog.show();

            SendMessageSubmission sendMessageSubmission = new SendMessageSubmission();
            sendMessageSubmission.setUserId(PreferencesData.getUserId(getContext()));
            sendMessageSubmission.setMsgId(Integer.parseInt(EditTextSelectMessageList.getTag().toString()));
            sendMessageSubmission.setStaffId(Integer.parseInt(staffId));
            sendMessageSubmission.setTripId(tripId);

            MyApplication.addJobInBackground(new SendMessageJob(sendMessageSubmission));
        } else
            TastyToast.makeText(getContext(), "Sorry no internet connection.", Toast.LENGTH_LONG, TastyToast.ERROR).show();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    public void onEventMainThread(final MessageSendEvent.Success event) {
        if (event.genericResponse().getStatusCode() == 1) {
            TastyToast.makeText(getContext(), "Successfully Send your message.", TastyToast.LENGTH_LONG, TastyToast.SUCCESS).show();
            pDialog.dismiss();
            dismiss();
        } else {
            TastyToast.makeText(getContext(), "Something went wrong while sending. Please try after sometime.", TastyToast.LENGTH_LONG, TastyToast.SUCCESS).show();
        }
    }

    public void onEventMainThread(MessageSendEvent.Fail event) {
        if (event.getEx() != null) {
            TastyToast.makeText(getContext(), "Unable to communicate with server, please try again after sometime.", TastyToast.LENGTH_LONG, TastyToast.ERROR).show();
        }
    }
}