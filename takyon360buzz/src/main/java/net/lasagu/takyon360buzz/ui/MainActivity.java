package net.lasagu.takyon360buzz.ui;

import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.sdsmdg.tastytoast.TastyToast;

import net.lasagu.takyon360.jobs.CalenderEventsJob;
import net.lasagu.takyon360buzz.BaseActivity;
import net.lasagu.takyon360buzz.MyApplication;
import net.lasagu.takyon360buzz.R;
import net.lasagu.takyon360buzz.events.BusListResponseEvent;
import net.lasagu.takyon360buzz.events.MessageSendEvent;
import net.lasagu.takyon360buzz.events.MyCalenderEvent;
import net.lasagu.takyon360buzz.jobs.BusListJob;
import net.lasagu.takyon360buzz.jobs.SendMessageJob;
import net.lasagu.takyon360buzz.models.BusList;
import net.lasagu.takyon360buzz.models.BusListResponse;
import net.lasagu.takyon360buzz.models.LoginResponse;
import net.lasagu.takyon360buzz.models.Messages;
import net.lasagu.takyon360buzz.models.Notification;
import net.lasagu.takyon360buzz.models.SendMessageSubmission;
import net.lasagu.takyon360buzz.models.UserDetails;
import net.lasagu.takyon360buzz.models.UserIdSubmission;
import net.lasagu.takyon360buzz.utils.Constant;
import net.lasagu.takyon360buzz.utils.PreferencesData;
import net.lasagu.takyon360buzz.utils.ReusableClass;
import net.lasagu.takyon360buzz.widgets.InboxListAdapter;
import net.lasagu.takyon360buzz.widgets.NotificationListAdapter;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindColor;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.pedant.SweetAlert.SweetAlertDialog;
import de.greenrobot.event.EventBus;
import de.hdodenhof.circleimageview.CircleImageView;

import static net.lasagu.takyon360buzz.utils.ReusableClass.setStatusBarGradiant;


public class MainActivity extends BaseActivity {


    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.noData)
    LinearLayout noData;
    @BindView(R.id.progress)
    ProgressBar progress;
    @BindView(R.id.studentName)
    TextView studentName;
    @BindView(R.id.className)
    TextView className;
    @BindView(R.id.divName)
    TextView divName;
    @BindView(R.id.noDataLinearLayout)
    TextView noDataLinearLayout;
    @BindView(R.id.imageViewStudentImage)
    CircleImageView imageViewStudentImage;
    @BindView(R.id.busListTextView)
    TextView busListTextView;
    @BindView(R.id.trackingLinearLayout)
    LinearLayout trackingLinearLayout;
    @BindView(R.id.etaTextView)
    TextView etaTextView;
    @BindView(R.id.distanceTextView)
    TextView distanceTextView;
    @BindView(R.id.NotificationLabel)
    TextView NotificationLabel;
    @BindView(R.id.QuickLinkHeadLabel)
    TextView QuickLinkHeadLabel;
    @BindView(R.id.TrackingLabel)
    TextView TrackingLabel;
    @BindView(R.id.CalendarLabel)
    TextView CalendarLabel;
    @BindView(R.id.ContactBusLabel)
    TextView ContactBusLabel;
    @BindView(R.id.ContactSchoolLabel)
    TextView ContactSchoolLabel;
    @BindView(R.id.etaLevel)
    TextView etaLevel;
    @BindView(R.id.distanceLevel)
    TextView distanceLevel;
    @BindView(R.id.calenderLinearLayout)
    LinearLayout calenderLinearLayout;
    @BindView(R.id.busCallLinearLayout)
    LinearLayout busCallLinearLayout;
    @BindView(R.id.schoolCallLinearLayout)
    LinearLayout schoolCallLinearLayout;
    @BindView(R.id.messageFromSchoolTextView)
    TextView messageFromSchoolTextView;
    @BindView(R.id.messageToSchoolTextView)
    TextView messageToSchoolTextView;
    @BindView(R.id.EditTextSelectMessageList)
    EditText EditTextSelectMessageList;
    @BindView(R.id.fabSend)
    FloatingActionButton fabSend;
    @BindView(R.id.recyclerViewInbox)
    RecyclerView recyclerViewInbox;
    @BindView(R.id.toolbar)
    Toolbar toolBar;
    private PopupMenu menuBusList;
    private PopupMenu menuMessageList;

    private NotificationListAdapter notificationListAdapter;
    LinearLayoutManager linearLayoutManager;
    LinearLayoutManager linearLayoutManagerInbox;
    private ArrayList<Messages> messagesBeanArrayList;
    private ArrayList<BusList> busBeanArrayList;
    private String tripId;
    private String staffId;
    private String busName = "";
    private HashMap<String, BusList> busListHashMap;
    private BusListResponse busListResponse;
    private SweetAlertDialog pDialog;
    @BindColor(R.color.colorPrimary)
    int colorPrimary;
    private BusList currentBusDetails;
    private HashMap<String, String> messageHashMap;
    private InboxListAdapter inboxListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        setSupportActionBar(toolBar);
        setStatusBarGradiant(this, R.drawable.main_screen_top_bg);

        notificationListAdapter = new NotificationListAdapter(this);
        linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(notificationListAdapter);

        linearLayoutManagerInbox = new LinearLayoutManager(this);
        recyclerViewInbox.setLayoutManager(linearLayoutManagerInbox);

        menuBusList = new PopupMenu(this, busListTextView);

        menuBusList.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                String seletedTitle = item.getTitle().toString();
                selectingMenuItem(seletedTitle);
                return false;
            }
        });

        receiveUserData();
        loadingBusData();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_logout) {
            loggingOut();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void receiveUserData() {
        if (getIntent().getType() != null) {
            if ("text/plain".equals(getIntent().getType())) {
                String loginData = getIntent().getStringExtra("LOGIN_DATA");
                String userData = getIntent().getStringExtra("USER_DATA");

                UserDetails userDetails = UserDetails.fromJson(loginData);
                PreferencesData.setUser(this, userDetails);

                LoginResponse loginResponse = LoginResponse.fromJson(userData);
                PreferencesData.setLoginResponse(this, loginResponse);
            }
        } else {

        }
    }

    public void loggingOut() {
        new SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE)
                .setTitleText("Confirm please")
                .setContentText("Are you sure, you want to logout?")
                .setConfirmText("Yes, Please!")
                .setCancelText("Want to stay")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        sweetAlertDialog.dismissWithAnimation();
                        PreferencesData.resetUser(MainActivity.this);
                        Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                        finish();
                        startActivity(intent);
                    }
                })
                .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        sweetAlertDialog.dismissWithAnimation();
                    }
                })
                .show();
    }

    private void selectingMenuItem(String selectedTitle) {
        busName = selectedTitle;
        busListTextView.setText(busName);
        busListTextView.setTag(busListHashMap.get(selectedTitle).getBusId());

        loadingBusDetailsData(busListHashMap.get(selectedTitle));
        currentBusDetails = busListHashMap.get(selectedTitle);
        loadingInBox();
    }

    private void loadingInBox() {
        if (currentBusDetails != null) {
            if (currentBusDetails.getInboxMessages() != null && !currentBusDetails.getInboxMessages().isEmpty()) {
                inboxListAdapter = new InboxListAdapter(this, currentBusDetails.getInboxMessages());
                inboxListAdapter.addAll(currentBusDetails.getInboxMessages());
                recyclerViewInbox.setAdapter(inboxListAdapter);
            }
        } else
            TastyToast.makeText(this, "Please select a bus first.", TastyToast.LENGTH_LONG, TastyToast.ERROR).show();
    }

    private void populateMessagePopUp(ArrayList<Messages> messagesBeanArrayList) {
        menuMessageList = new PopupMenu(this, EditTextSelectMessageList);
        messageHashMap = new HashMap<>();

        for (int i = 0; i < messagesBeanArrayList.size(); i++) {
            menuMessageList.getMenu().add(messagesBeanArrayList.get(i).getName());
            messageHashMap.put(messagesBeanArrayList.get(i).getName(), messagesBeanArrayList.get(i).getId());
        }

        menuMessageList.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                String seletedTitle = item.getTitle().toString();
                EditTextSelectMessageList.setText(seletedTitle);
                EditTextSelectMessageList.setTag(messageHashMap.get(seletedTitle));
                return false;
            }
        });

        if (messagesBeanArrayList.size() > 0) {
            EditTextSelectMessageList.setText(messagesBeanArrayList.get(0).getName());
            EditTextSelectMessageList.setTag(messageHashMap.get(messagesBeanArrayList.get(0).getName()));
        }
    }

    private void loadingBusDetailsData(BusList busList) {
        etaLevel.setText(busListResponse.getBusDetails().getETALabel() + " ");
        distanceLevel.setText(busListResponse.getBusDetails().getDistanceLabel() + " ");

        etaTextView.setText(Html.fromHtml((TextUtils.isEmpty(busList.getETA())) ? "" : busList.getETA()));
        distanceTextView.setText(Html.fromHtml((TextUtils.isEmpty(busList.getDistance())) ? "" : busList.getDistance()));

    }

    private void loadingData(ArrayList<Notification> notificationList) {
        progress.setVisibility(View.VISIBLE);

        if (getIntent().getType() != null) {
            if ("text/plain".equals(getIntent().getType())) {
                String loginData = getIntent().getStringExtra("LOGIN_DATA");
                String userData = getIntent().getStringExtra("USER_DATA");

                UserDetails userDetails = UserDetails.fromJson(loginData);
                PreferencesData.setUser(this, userDetails);

                LoginResponse loginResponse = LoginResponse.fromJson(userData);
                PreferencesData.setLoginResponse(this, loginResponse);

                studentName.setText(loginResponse.getStudentName());
                className.setText(loginResponse.getClassName());

                if (loginResponse.getProfileImage() != null && !loginResponse.getProfileImage().equalsIgnoreCase(""))
                    Glide.with(this).load(loginResponse.getProfileImage()).into(imageViewStudentImage);
                else
                    imageViewStudentImage.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.empty_avatar));


                notificationListAdapter.clearAll();
                if (notificationList.size() != 0) {
                    notificationListAdapter.addAll(notificationList);
                    noDataLinearLayout.setVisibility(View.INVISIBLE);
                } else {
                    noDataLinearLayout.setVisibility(View.VISIBLE);
                }
                progress.setVisibility(View.INVISIBLE);
            }
        } else {
            LoginResponse loginResponse = PreferencesData.getLoginResponse(this);
            studentName.setText(loginResponse.getStudentName());
            className.setText(loginResponse.getClassName());

            notificationListAdapter.clearAll();
            if (notificationList.size() != 0) {
                notificationListAdapter.addAll(notificationList);
                noDataLinearLayout.setVisibility(View.INVISIBLE);
            } else {
                noDataLinearLayout.setVisibility(View.VISIBLE);
            }
            progress.setVisibility(View.INVISIBLE);
        }
    }

    private void loadingBusData() {
        if (ReusableClass.isNetworkAvailable(this)) {
            progress.setVisibility(View.VISIBLE);

            UserIdSubmission userIdSubmission = new UserIdSubmission();
            userIdSubmission.setUserId(PreferencesData.getUserId(this));

            MyApplication.addJobInBackground(new BusListJob(userIdSubmission));
        } else {
            TastyToast.makeText(this, "Sorry no internet connection.", Toast.LENGTH_LONG, TastyToast.ERROR).show();
            progress.setVisibility(View.INVISIBLE);
        }
    }

    @OnClick(R.id.trackingLinearLayout)
    public void tracking() {
        if (busListTextView.getTag() != null) {
            if (tripId != null && staffId != null && !tripId.isEmpty() && !staffId.isEmpty()) {
                Intent intent = new Intent(this, BusDetailsTabActivity.class);
                intent.putExtra(Constant.MAP_ID, busListTextView.getTag().toString());
                intent.putExtra(Constant.TRIP_ID, tripId);
                intent.putExtra(Constant.STAFF_ID, staffId);
                intent.putExtra(Constant.BUS_NAME, busName);
                intent.putExtra(Constant.BUS_LIST_RESPONSE, busListResponse);
                intent.putParcelableArrayListExtra(Constant.MESSAGE_LIST, messagesBeanArrayList);
                startActivity(intent);
            } else {
                Intent intent = new Intent(this, BusDetailsTabActivity.class);
                intent.putExtra(Constant.MAP_ID, busListTextView.getTag().toString());
                intent.putExtra(Constant.TRIP_ID, "");
                intent.putExtra(Constant.STAFF_ID, "0");
                intent.putExtra(Constant.BUS_NAME, busName);
                intent.putExtra(Constant.BUS_LIST_RESPONSE, busListResponse);
                intent.putParcelableArrayListExtra(Constant.MESSAGE_LIST, messagesBeanArrayList);
                startActivity(intent);
            }
        } else
            TastyToast.makeText(this, "Please select a bus first.", TastyToast.LENGTH_LONG, TastyToast.ERROR).show();
    }

    @OnClick(R.id.calenderLinearLayout)
    public void openingCalender() {
        if (ReusableClass.isNetworkAvailable(this)) {
            pDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
            pDialog.getProgressHelper().setBarColor(colorPrimary);
            pDialog.setTitleText("Loading ...");
            pDialog.setCancelable(false);
            pDialog.show();

            UserIdSubmission userIdSubmission = new UserIdSubmission();
            userIdSubmission.setUserId(PreferencesData.getUserId(this));

            MyApplication.addJobInBackground(new CalenderEventsJob(userIdSubmission));
        } else
            TastyToast.makeText(this, "Sorry no internet connection.", Toast.LENGTH_LONG, TastyToast.ERROR).show();
    }

    @OnClick(R.id.busCallLinearLayout)
    public void callingBusNumber() {
        if (currentBusDetails != null) {
            Intent intent = new Intent(Intent.ACTION_DIAL);
            intent.setData(Uri.parse("tel:" + currentBusDetails.getBusContact()));
            startActivity(intent);
        } else
            TastyToast.makeText(this, "Please select a bus first.", TastyToast.LENGTH_LONG, TastyToast.ERROR).show();
    }

    @OnClick(R.id.schoolCallLinearLayout)
    public void callingSchoolNumber() {
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:" + busListResponse.getSchoolContact()));
        startActivity(intent);
    }

    @OnClick(R.id.busListTextView)
    public void selectBusLists() {
        menuBusList.show();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    public void onEventMainThread(final BusListResponseEvent.Success event) {
        busListResponse = event.getBusListResponse();

        NotificationLabel.setText(Html.fromHtml((TextUtils.isEmpty(busListResponse.getNotificationLabel())) ? "" : busListResponse.getNotificationLabel()));
        QuickLinkHeadLabel.setText(Html.fromHtml((TextUtils.isEmpty(busListResponse.getQuickLinkHeadLabel())) ? "" : busListResponse.getQuickLinkHeadLabel()));
        TrackingLabel.setText(Html.fromHtml((TextUtils.isEmpty(busListResponse.getTrackingLabel())) ? "" : busListResponse.getTrackingLabel()));
        CalendarLabel.setText(Html.fromHtml((TextUtils.isEmpty(busListResponse.getCalendarLabel())) ? "" : busListResponse.getCalendarLabel()));
        ContactBusLabel.setText(Html.fromHtml((TextUtils.isEmpty(busListResponse.getContactBusLabel())) ? "" : busListResponse.getContactBusLabel()));
        ContactSchoolLabel.setText(Html.fromHtml((TextUtils.isEmpty(busListResponse.getContactSchoolLabel())) ? "" : busListResponse.getContactSchoolLabel()));
        messageFromSchoolTextView.setText(Html.fromHtml((TextUtils.isEmpty(busListResponse.getBusDetails().getMessageFromBusLabel())) ? "" : busListResponse.getBusDetails().getMessageFromBusLabel()));
        messageToSchoolTextView.setText(Html.fromHtml((TextUtils.isEmpty(busListResponse.getBusDetails().getMessageToBusLabel())) ? "" : busListResponse.getBusDetails().getMessageToBusLabel()));
        getSupportActionBar().setTitle(Html.fromHtml((TextUtils.isEmpty(busListResponse.getHeadLabel())) ? "" : busListResponse.getHeadLabel()));

        loadingData(busListResponse.getNotification());

        messagesBeanArrayList = busListResponse.getBusDetails().getMessages();
        populateMessagePopUp(messagesBeanArrayList);

        busBeanArrayList = busListResponse.getBusDetails().getBusList();
        tripId = busListResponse.getBusDetails().getTrip_id();
        staffId = busListResponse.getBusDetails().getStaff_id();

        if (busBeanArrayList.size() != 0) {
            busListHashMap = new HashMap<String, BusList>();
            for (int i = 0; i < busBeanArrayList.size(); i++) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    menuBusList.getMenu().add(Html.fromHtml(busBeanArrayList.get(i).getBusName(), Html.FROM_HTML_MODE_LEGACY));
                } else {
                    menuBusList.getMenu().add(Html.fromHtml(busBeanArrayList.get(i).getBusName()));
                }
                busListHashMap.put(busBeanArrayList.get(i).getBusName(), busBeanArrayList.get(i));
            }
            selectingMenuItem(busBeanArrayList.get(0).getBusName());

            noData.setVisibility(View.INVISIBLE);
            progress.setVisibility(View.INVISIBLE);
        } else {
            noData.setVisibility(View.VISIBLE);
            menuBusList.getMenu().clear();
            progress.setVisibility(View.INVISIBLE);
        }


    }

    /**
     * START
     * Send message to school bus
     **/

    @OnClick(R.id.EditTextSelectMessageList)
    public void selectMessage() {
        menuMessageList.show();
    }

    public void sendingMessage(View view) {
        if (ReusableClass.isNetworkAvailable(this)) {
            pDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
            pDialog.getProgressHelper().setBarColor(colorPrimary);
            pDialog.setTitleText("Loading ...");
            pDialog.setCancelable(false);
            pDialog.show();

            SendMessageSubmission sendMessageSubmission = new SendMessageSubmission();
            sendMessageSubmission.setUserId(PreferencesData.getUserId(this));
            sendMessageSubmission.setMsgId(Integer.parseInt(EditTextSelectMessageList.getTag().toString()));
            sendMessageSubmission.setStaffId(Integer.parseInt(staffId));
            sendMessageSubmission.setTripId(tripId);

            MyApplication.addJobInBackground(new SendMessageJob(sendMessageSubmission));
        } else
            TastyToast.makeText(this, "Sorry no internet connection.", Toast.LENGTH_LONG, TastyToast.ERROR).show();
    }

    public void onEventMainThread(final MessageSendEvent.Success event) {
        if (event.genericResponse().getStatusCode() == 1) {
            pDialog.changeAlertType(SweetAlertDialog.SUCCESS_TYPE);
            pDialog.setTitleText("Successfully send!");
            pDialog.setContentText("Successfully Send your message to the school bus.!");
            pDialog.getProgressHelper().setBarColor(colorPrimary);
            pDialog.setCancelable(false);
            pDialog.show();
        } else {
            pDialog.changeAlertType(SweetAlertDialog.ERROR_TYPE);
            pDialog.getProgressHelper().setBarColor(colorPrimary);
            pDialog.setTitleText("Opps failed to send");
            pDialog.setContentText("Something went wrong while sending. Please try after sometime.");
            pDialog.setCancelable(false);
            pDialog.show();
        }
    }

    public void onEventMainThread(MessageSendEvent.Fail event) {
        if (event.getEx() != null) {
            TastyToast.makeText(this, "Unable to communicate with server, please try again after sometime.", TastyToast.LENGTH_LONG, TastyToast.ERROR).show();
        }
    }

    /**
     * END
     * Send message to school bus
     **/


    /**
     * START
     * Calender events
     **/

    public void onEventMainThread(final MyCalenderEvent.Success event) {
        pDialog.cancel();
        Intent intent = new Intent(this, CalendarActivity.class);
        intent.putExtra(CalendarActivity.CALENDER_DATA, event.getCalenderEventResponse());
        intent.putExtra(CalendarActivity.CALENDER_TITLE, Html.fromHtml((TextUtils.isEmpty(busListResponse.getCalendarLabel())) ? "" : busListResponse.getCalendarLabel()).toString());
        startActivity(intent);
    }

    public void onEventMainThread(MyCalenderEvent.Fail event) {
        pDialog.cancel();

        if (event.getEx() != null) {
            TastyToast.makeText(this, "Seems server is busy try after sometime.", TastyToast.LENGTH_LONG, TastyToast.ERROR).show();
        }
    }
}
