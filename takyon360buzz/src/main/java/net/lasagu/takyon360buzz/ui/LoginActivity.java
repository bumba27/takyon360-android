package net.lasagu.takyon360buzz.ui;


import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import net.lasagu.takyon360buzz.BaseActivity;
import net.lasagu.takyon360buzz.BuildConfig;
import net.lasagu.takyon360buzz.MyApplication;
import net.lasagu.takyon360buzz.R;
import net.lasagu.takyon360buzz.events.LoginEvent;
import net.lasagu.takyon360buzz.jobs.LoginJob;
import net.lasagu.takyon360buzz.models.LoginSubmission;
import net.lasagu.takyon360buzz.models.UserDetails;
import net.lasagu.takyon360buzz.utils.Constant;
import net.lasagu.takyon360buzz.utils.PreferencesData;
import net.lasagu.takyon360buzz.utils.ReusableClass;
import net.lasagu.takyon360buzz.utils.SharedPreferenceStore;

import butterknife.BindColor;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.pedant.SweetAlert.SweetAlertDialog;
import de.greenrobot.event.EventBus;


public class LoginActivity extends BaseActivity {

    SweetAlertDialog pDialog;
    @BindColor(R.color.colorPrimary)
    int colorPrimary;
    @BindView(R.id.toolBar)
    Toolbar toolBar;
    @BindView(R.id.input_username)
    EditText inputUsername;
    @BindView(R.id.input_password)
    EditText inputPassword;
    @BindView(R.id.input_language)
    EditText inputLanguage;
    @BindView(R.id.rememberMe)
    CheckBox rememberMe;
    @BindView(R.id.loginButton)
    Button loginButton;
    @BindView(R.id.versionNo)
    TextView versionNo;
    @BindView(R.id.activity_main)
    LinearLayout activityMain;
    private String language = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        setSupportActionBar(toolBar);
        inputLanguage.setText("English");
        language = "English";

        versionNo.setText("Version no: " + BuildConfig.VERSION_NAME);
        if (PreferencesData.isRemembered(this)) {
            UserDetails userDetails = PreferencesData.getUser(this);
            inputUsername.setText(userDetails.getUsername());
            inputPassword.setText(userDetails.getPassword());
            language = userDetails.getLanguage();
            inputLanguage.setText(language);
            rememberMe.setChecked(true);
            loginButton.performClick();
        }

        if (BuildConfig.DEBUG) {
//            inputUsername.setText("hbtp0519");
//            inputPassword.setText("123456");
//            inputUsername.setText("ASDP0010955");
//            inputPassword.setText("123456");
//            inputUsername.setText("amrp8109");
//            inputPassword.setText("jj123arav");
//            inputUsername.setText("AAPSP3018");
//            inputPassword.setText("123456");
//            inputUsername.setText("AAPSP2044");
//            inputPassword.setText("123456");
//            inputUsername.setText("1@amanaschool.com");
//            inputPassword.setText("12345678");
//            inputUsername.setText("AAPSP0808");
//            inputPassword.setText("123456");
//            inputUsername.setText("HBTGP1614");
//            inputPassword.setText("123456");
//            inputUsername.setText("ICS1P001521");
//            inputPassword.setText("123456");
//            inputUsername.setText("7@leadersprivateschool.com");
//            inputPassword.setText("12345678");
//            inputUsername.setText("TAPSP00893");
//            inputPassword.setText("123456");
//            inputUsername.setText("AAPSP3135");
//            inputPassword.setText("123456");
//            inputUsername.setText("LPSP17280");
//            inputPassword.setText("12345678");
//            inputUsername.setText("TAK1P004775");
//            inputPassword.setText("123456");
            inputUsername.setText("hbtp0518");
            inputPassword.setText("123456");
        }
    }

    @OnClick(R.id.input_language)
    public void selectingLaguage(View view) {
        PopupMenu popup = new PopupMenu(LoginActivity.this, view);
        popup.getMenuInflater().inflate(R.menu.language_menu, popup.getMenu());

        //registering popup with OnMenuItemClickListener
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                language = item.getTitle().toString();
                inputLanguage.setText(language);
                return true;
            }
        });

        popup.show(); //showing popup menu
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    public void loggedIn(View view) {

        String email = inputUsername.getText().toString();
        String password = inputPassword.getText().toString();
        if (!TextUtils.isEmpty(email) && !TextUtils.isEmpty(password) && !TextUtils.isEmpty(language)) {
            if (ReusableClass.isNetworkAvailable(this)) {
                pDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
                pDialog.getProgressHelper().setBarColor(colorPrimary);
                pDialog.setTitleText("Loading ...");
                pDialog.setCancelable(false);
                pDialog.show();

                LoginSubmission loginSubmission = new LoginSubmission();
                loginSubmission.setUsername(email);
                loginSubmission.setPassword(password);
                loginSubmission.setLanguage(language);

                MyApplication.addJobInBackground(new LoginJob(loginSubmission));
            } else {
                Snackbar.make(view, "Sorry no internet connection available.", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        } else {
            Snackbar.make(view, "All field are mandatory.", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
        }
    }

    public void onEventMainThread(final LoginEvent.Success event) {
        if (event.getLoginResponse().getStatusCode() == 1) {

            if (rememberMe.isChecked())
                PreferencesData.setRememberMe(this);
            else
                PreferencesData.setDontRemember(this);

            pDialog.setTitleText("Success!!!");
            pDialog.changeAlertType(SweetAlertDialog.SUCCESS_TYPE);
            UserDetails userDetails = new UserDetails();
            userDetails.setUsername(inputUsername.getText().toString());
            userDetails.setPassword(inputPassword.getText().toString());
            userDetails.setLanguage(language);

            PreferencesData.setUser(this, userDetails);

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    PreferencesData.setLoginResponse(LoginActivity.this, event.getLoginResponse());
                    SharedPreferenceStore.storeValue(LoginActivity.this, Constant.BASE_URL, event.getLoginResponse().getBaseURL());
                    Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                    pDialog.dismiss();
                    finish();
                    startActivity(intent);
                }
            }, 800);

        } else {
            pDialog.setTitleText("Please check your details.");
            pDialog.changeAlertType(SweetAlertDialog.ERROR_TYPE);
            pDialog.setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                @Override
                public void onClick(SweetAlertDialog sweetAlertDialog) {
                    pDialog.dismiss();
                }
            });
        }
    }

    public void onEventMainThread(LoginEvent.Fail event) {

        if (event.getEx() != null) {
            pDialog.setTitleText("Oops Server Error");
            pDialog.setContentText(event.getEx());
            pDialog.changeAlertType(SweetAlertDialog.ERROR_TYPE);
        }
    }

    public void resetPassword(View view) {

    }
}