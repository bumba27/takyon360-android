package net.lasagu.takyon360buzz.ui;

import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.sdsmdg.tastytoast.TastyToast;

import net.lasagu.takyon360buzz.MyApplication;
import net.lasagu.takyon360buzz.R;
import net.lasagu.takyon360buzz.events.BusDetailsResponseEvent;
import net.lasagu.takyon360buzz.jobs.BusDetailsJob;
import net.lasagu.takyon360buzz.models.BusDetailsResponse;
import net.lasagu.takyon360buzz.models.BusDetailsSubmission;
import net.lasagu.takyon360buzz.models.BusListResponse;
import net.lasagu.takyon360buzz.models.Messages;
import net.lasagu.takyon360buzz.ui.fragments.SendMessageDialogFragment;
import net.lasagu.takyon360buzz.utils.Constant;
import net.lasagu.takyon360buzz.utils.PreferencesData;
import net.lasagu.takyon360buzz.utils.ReusableClass;
import net.lasagu.takyon360buzz.widgets.BusDetailsPagerAdapter;

import java.util.ArrayList;

import butterknife.BindColor;
import butterknife.BindView;
import butterknife.ButterKnife;
import cn.pedant.SweetAlert.SweetAlertDialog;
import de.greenrobot.event.EventBus;


public class BusDetailsTabActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.tabs)
    TabLayout tabs;
    @BindView(R.id.container)
    ViewPager container;
    @BindView(R.id.main_content)
    CoordinatorLayout mainContent;
    @BindColor(R.color.colorPrimary)
    int colorPrimary;
    @BindView(R.id.progressToolbar)
    ProgressBar progressToolbar;
    private BusDetailsPagerAdapter mSectionsPagerAdapter;
    private ViewPager mViewPager;
    public static final String BUS_DETAILS = "section_number";
    private SweetAlertDialog pDialog;
    private String mapId;
    private String tripId;
    private String staffId;
    private ArrayList<Messages> messageList;
    private Handler handler;
    private String TAG = BusDetailsTabActivity.class.getSimpleName();
    private String busName = "";
    private BusListResponse busListResponse;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.bus_details_tab_activity);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);

        mSectionsPagerAdapter = new BusDetailsPagerAdapter(getSupportFragmentManager());
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setOffscreenPageLimit(2);

        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabs));
        tabs.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(mViewPager));

        mapId = getIntent().getStringExtra(Constant.MAP_ID);
        staffId = getIntent().getStringExtra(Constant.STAFF_ID);
        tripId = getIntent().getStringExtra(Constant.TRIP_ID);
        busName = getIntent().getStringExtra(Constant.BUS_NAME);
        messageList = getIntent().getParcelableArrayListExtra(Constant.MESSAGE_LIST);
        busListResponse = getIntent().getParcelableExtra(Constant.BUS_LIST_RESPONSE);

        getSupportActionBar().setTitle(busName);
        loadingData();
        setUpTabName(busListResponse);
    }

    private void loadingData() {
        if (ReusableClass.isNetworkAvailable(this)) {
            pDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
            pDialog.getProgressHelper().setBarColor(colorPrimary);
            pDialog.setTitleText("Loading ...");
            pDialog.setCancelable(false);
            pDialog.show();

            BusDetailsSubmission busDetailsSubmission = new BusDetailsSubmission();
            busDetailsSubmission.setUserId(PreferencesData.getUserId(this));
            busDetailsSubmission.setMapId(mapId);
            busDetailsSubmission.setStaffId(Integer.parseInt(staffId));
            busDetailsSubmission.setTripId(tripId);

            MyApplication.addJobInBackground(new BusDetailsJob(busDetailsSubmission, false));
        } else
            TastyToast.makeText(this, "Sorry no internet connection.", Toast.LENGTH_LONG, TastyToast.ERROR).show();
    }

    private void refreshData() {
        if (ReusableClass.isNetworkAvailable(this)) {
            progressToolbar.setVisibility(View.VISIBLE);

            BusDetailsSubmission busDetailsSubmission = new BusDetailsSubmission();
            busDetailsSubmission.setUserId(PreferencesData.getUserId(this));
            busDetailsSubmission.setMapId(mapId);
            busDetailsSubmission.setStaffId(Integer.parseInt(staffId));
            busDetailsSubmission.setTripId(tripId);

            MyApplication.addJobInBackground(new BusDetailsJob(busDetailsSubmission, true));
        } else
            TastyToast.makeText(this, "Sorry no internet connection.", Toast.LENGTH_LONG, TastyToast.ERROR).show();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Override
    protected void onResume() {
        super.onResume();

        handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                refreshData();
                handler.postDelayed(this, Constant.RELOAD_DATA_TIME_INTERVAL); // reschedule the handler

                Log.d(TAG, "Loading data");
            }
        }, Constant.RELOAD_DATA_TIME_INTERVAL);
    }

    @Override
    protected void onPause() {
        super.onPause();
        handler.removeCallbacksAndMessages(null);
    }

    public void onEventMainThread(final BusDetailsResponseEvent.Success event) {
        final BusDetailsResponse busDetailsResponse = event.getBusDetailsResponse();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                pDialog.dismiss();
            }
        }, 800);
        mSectionsPagerAdapter.setBusDetails(busDetailsResponse);
        mViewPager.setAdapter(mSectionsPagerAdapter);
    }

    public void onEventMainThread(BusDetailsResponseEvent.Fail event) {
        if (event.getEx() != null) {
            pDialog.dismissWithAnimation();
            TastyToast.makeText(this, "Seems server is busy try after sometime.", TastyToast.LENGTH_LONG, TastyToast.ERROR).show();
        }
    }

    public void clickingSend(View view) {
        if (messageList.size() > 0) {
            SendMessageDialogFragment sendMessageDialogFragment = new SendMessageDialogFragment();
            Bundle bundle = new Bundle();
            bundle.putParcelableArrayList(Constant.MESSAGE_LIST, messageList);
            bundle.putString(Constant.TRIP_ID, tripId);
            bundle.putString(Constant.STAFF_ID, staffId);
            sendMessageDialogFragment.setArguments(bundle);
            sendMessageDialogFragment.show(getSupportFragmentManager(), "sendMessageDialogFragment");
        } else
            TastyToast.makeText(this, "Sorry no message to send.", TastyToast.LENGTH_LONG, TastyToast.ERROR).show();
    }

    public void setUpTabName(BusListResponse busListResponse) {
        tabs.getTabAt(0).setText("Location".toUpperCase());
        tabs.getTabAt(1).setText(busListResponse.getBusDetails().getMessageToBusLabel());
    }

    public void dismissProgress() {
        progressToolbar.setVisibility(View.INVISIBLE);
    }
}
