package net.lasagu.takyon360buzz.ui;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.github.florent37.viewanimator.ViewAnimator;

import net.lasagu.takyon360buzz.BaseActivity;
import net.lasagu.takyon360buzz.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SplashScreenActivity extends BaseActivity {

    private final int SPLASH_DISPLAY_LENGTH = 2000;
    @BindView(R.id.imageLogo)
    ImageView imageLogo;
    @BindView(R.id.mainLogo)
    LinearLayout mainLogo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        ButterKnife.bind(this);

        ViewAnimator
                .animate(mainLogo)
                .scale(0, 1)
                .duration(1500)
                .start();

//        ViewAnimator
//                .animate(mainLogo)
//                .alpha(0, 1)
//                .duration(1000)
//                .thenAnimate(imageLogo)
//                .scale(0.1f, 1f)
//                .accelerate()
//                .duration(1000)
//                .start();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mainLogo.setVisibility(View.VISIBLE);
            }
        }, 100);


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent mainIntent = new Intent(SplashScreenActivity.this, LoginActivity.class);
                SplashScreenActivity.this.startActivity(mainIntent);
                SplashScreenActivity.this.finish();
            }
        }, SPLASH_DISPLAY_LENGTH);
    }
}
