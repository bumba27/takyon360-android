package net.lasagu.takyon360buzz.utils;

/**
 * Created by ajana on 26/04/2017.
 */

public class Constant {
    public static final String BASE_URL = "baseUrl";
    public static final String LOGIN_RESPONSE = "loginResponse";
    public static final String TRIP_ID = "tripId";
    public static final String STAFF_ID = "staffId";
    public static final String MAP_ID = "mapId";
    public static final String INBOX = "inbox";
    public static final String SENTBOX = "sentbox";
    public static final String MESSAGE_LIST = "messageList";
    public static final long RELOAD_DATA_TIME_INTERVAL = 1000 * 30; // 30 Sec
    public static final String BUS_NAME = "busName";
    public static final String BUS_LIST_RESPONSE = "busListResponse";
    public static final String PARENT = "Parent";
    public static final String ADMIN = "admin";
    public static final String STUDENT = "Student";
}
