package net.lasagu.takyon360buzz.utils;

import android.content.Context;
import android.net.Uri;

import net.lasagu.takyon360buzz.R;


/**
 * Created by anirban on 1/31/17.
 */

public class URLConst {

    public static String getLoginUrl(Context context) {
        return Uri.parse(context.getResources().getString(R.string.base_url) + "/LOGIN").buildUpon()
                .toString();
    }

    public static String getBusListUrl(Context context) {
        return Uri.parse(context.getResources().getString(R.string.base_url) + "/T0010B").buildUpon()
                .toString();
    }

    public static String getBusDetailsUrl(Context context) {
        return Uri.parse(context.getResources().getString(R.string.base_url) + "/T0034").buildUpon()
                .toString();
    }

    public static String getSendMessageUrl(Context context) {
        return Uri.parse(context.getResources().getString(R.string.base_url) + "/T0045").buildUpon()
                .toString();
    }

    public static String getCalenderEventUrl(Context context) {
        return Uri.parse(context.getResources().getString(R.string.base_url) + "/T0068").buildUpon()
                .toString();
    }
}
